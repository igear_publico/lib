﻿


var textoFondoMudo = " ";

var textoFondoMapaNiveles = 4;
var textoFondoMapaNivel = new Array();
var textoFondoMapa = new Array();
textoFondoMapaNivel[0] = 15010;
textoFondoMapa[0] = "Fondo: MTN 25 IGN";
textoFondoMapaNivel[1] = 100010;
textoFondoMapa[1] = "Fondo: MTN 50 IGN";
textoFondoMapaNivel[2] = 400010;
textoFondoMapa[2] = "Fondo: MTN 200 IGN";
textoFondoMapaNivel[3] = 10000010;
textoFondoMapa[3] = "Fondo: ME 500 IGN";

var textoFondoMapaPorDefecto = "MTN";

/*var textoFondoFotoNiveles = 3;
var textoFondoFotoNivel = new Array();
var textoFondoFoto = new Array();
textoFondoFotoNivel[0] = 16047;
textoFondoFoto[0] = "Fondo: PNOA 50 cm. 2012";
textoFondoFotoNivel[1] = 530010;
textoFondoFoto[1] = "Fondo: SPOT 2005";
textoFondoFotoNivel[2] = 10000010;
textoFondoFoto[2] = "Fondo: MODIS 2009";*/
var textoFondoFotoNiveles = 0;
var textoFondoFoto = new Array();

var textoFondoArray = new Array();
textoFondoArray[0] = "Fondo: PNOA 50 cm. 2012";
var textoFondoFotoDefault = new Array();
textoFondoFoto[0] = "Fondo: PNOA Año 2018 color";
textoFondoFoto[1] = "Fondo: Ortofoto 1956-1957";
textoFondoFoto[2] = "Fondo: SIG Oleícola Año 1998";
textoFondoFoto[3] = "Fondo: Plan Cartografico de Aragón Año 2000";
textoFondoFoto[4] = "Fondo: FEGA Año 2003";
textoFondoFoto[5] = "Fondo: PNOA Año 2006 color";
textoFondoFoto[6] = "Fondo: PNOA Año 2009 color";
textoFondoFoto[7] = "Fondo: PNOA Año 2012 color";
textoFondoFoto[8] = "Fondo: PNOA Año 2015 color";
textoFondoFoto[9] = "Fondo: PNOA Año 2018 color";

var textoFondoSombreado = "Fondo: Sombreado";
var textoFondoMDT = "Fondo: MDT Año 2015 5m";
var textoFondoFotoPorDefecto = "Fondo: PNOA 50 cm. 2012";

var textoFondoSateliteNiveles = 4;
var textoFondoSateliteNivel = new Array();
var textoFondoSatelite = new Array();

textoFondoSateliteNivel[0] = 16350;
textoFondoSatelite[0] = "Fondo: PNOA Año 2018 color";
textoFondoSateliteNivel[1] = 100010;
textoFondoSatelite[1] = "Fondo: SPOT 2011";
textoFondoSateliteNivel[2] = 530010;
textoFondoSatelite[2] = "Fondo: Landsat 2017";
textoFondoSateliteNivel[3] = 10000010;
textoFondoSatelite[3] = "Fondo: MODIS 2018";

var textoFondoSatelitePorDefecto = "Fondo: MODIS 2018";

var idxOrtoAuxiliar = 1;
