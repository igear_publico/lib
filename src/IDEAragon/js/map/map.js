/**
 * 
 */
var DOTS_PER_M=2834.3472;
var ol,tl,bl,stl,sl;
var markers;
var lastPathUnits;
var lastAreaUnits;
var attributionControl,navigationControl,navHistory,areaMeasure,pathMeasure;
var mapHeight, mapWidth; // altura y anchura del mapa (para comparar si ha
//cambiado al cambiar el tamaño de la ventana)

var loading_layer=false;  // indica si se está cargando una capa (usado para ocultar el mensaje de Loading)
var scale5=100;
var scale_units="Km";

var needScaleUpdate=false;

var idxWMSFondoFoto = 0;

var fVueloQuery = new Array();
var buffer;

var     base_attribution='<div id="hueco">' +
'<div id=scaleBox ><label class="shadow" id=scale1>0Km</label> <label class="shadow" id=scale2>10Km</label>'+
'<label class="shadow" id=scale3>40Km</label> <label class="shadow" id=scale4>70Km</label> <label class="shadow" id=scale5>100Km</label>'+
'<div><img border="0" id="scaleBar" src="/lib/IDEAragon/themes/default/images/scaleBar.png"  			alt="escala"></div> </div> </div>     		';

var grpCapaUsuario;
function decTranspSlider(slider){
	var value =Math.max($("#"+slider).slider("option", "value") - 10,  $("#"+slider).slider( "option","min"));
	$("#"+slider).slider( "option","value", value);
	
}

function incTranspSlider(slider){
	var value = Math.min($("#"+slider).slider("option", "value") + 10,  $("#"+slider).slider( "option","max"));
	$("#"+slider).slider( "option","value", value);
	
	
}

function showTranspWarning(value){
	 if (value>0){
			showGenerico('transpMsg');
		 }
		 else if (($( "#transpTocSlider" ).slider("option","value")<=0)&&( $( "#transpFondoSlider" ).slider("option","value")<=0)){
			 hideGenerico('transpMsg');
		 }
}

function changeFondoFoto(idxWMS) {
		// marcar la opción Foto de los fondos
	document.getElementById("bgFotoShowId").checked = true;
	$("#bgFotoShowId").button("refresh");
	if (document.getElementById('bgFotoShowId').checked) {
		if (wmsList[idxWMS]["visib"] == true) {
			wmsList[0]["visib"] = false;
			idxWMSFondoFoto = idxWMS;
		} else {
			wmsList[0]["visib"] = true;
			idxWMSFondoFoto = 0;
		}
	} else {
		if (wmsList[idxWMS]["visib"] == true) {
			idxWMSFondoFoto = idxWMS;
		} else {
			idxWMSFondoFoto = 0;
		}
	}
	updateFVueloQuery(idxWMSFondoFoto);

	updateBaseLayer();
}

function updateFVueloQuery(idxWMSFondoFoto) {
	if (typeof whereQuery=='undefined'){
		return;
	}
	if (idxWMSFondoFoto != -1) {
		if (fVueloQuery[idxWMSFondoFoto]) {
			whereQuery["FechaVueloRec"] = fVueloQuery[idxWMSFondoFoto];
		} else {
			whereQuery["FechaVueloRec"] = defaultFVueloQuery;
		}
	} else {
		whereQuery["FechaVueloRec"] = imposibleFVueloQuery;
	}
}

function getTextoFondoSatelite(idxWMSFondoSatelite) {
	if (idxWMSFondoSatelite != -1) {
		if (textoFondoArray[idxWMSFondoSatelite]) {
			return textoFondoArray[idxWMSFondoSatelite];
		}
	}

	var escalaActual = getMapScale(false);
	changeScaleFondoSatelite(escalaActual);
	for (var i=0; i < textoFondoSateliteNiveles; i++) {
		if (escalaActual <= textoFondoSateliteNivel[i]) {
			return textoFondoSatelite[i];
		}
	}
	return textoFondoSatelitePorDefecto;
}

function getTextoFondoFoto(idxWMSFondoFoto) {
	if (idxWMSFondoFoto != -1) {
		if (textoFondoArray[idxWMSFondoFoto]) {
			return textoFondoArray[idxWMSFondoFoto];
		}
	}

	var escalaActual = getMapScale(false);
	for (var i=0; i < textoFondoFotoNiveles; i++) {
		if (escalaActual <= textoFondoFotoNivel[i]) {
			return textoFondoFoto[i];
		}
	}
	return textoFondoFotoPorDefecto;
}

function getTextoFondoMapa() {
	var escalaActual = getMapScale(false);
	for (var i=0; i < textoFondoMapaNiveles; i++) {
		if (escalaActual <= textoFondoMapaNivel[i]) {
			return textoFondoMapa[i];
		}
	}
	return textoFondoMapaPorDefecto;
}


function showFondo(){
	updateFVueloQuery(idxWMSFondoFoto);

	clearWMS(0);
	$("#fondos input").each(function(index){
		if($(this)[0].checked){
			var idx = 0;
			if ($("#"+$(this)[0].id+"Selector")&&($("#"+$(this)[0].id+"Selector").length>0)){
				idx=document.getElementById($(this)[0].id+"Selector").selectedIndex;
			if ((idx<0)||(idx >fondosWMS[$(this)[0].id].length)){
				idx=0;
			}
			}
			wmsList[0]["url"] = fondosWMS[$(this)[0].id][idx];
	
			
			
	updateWMS(0);
		wmsList[idxOrtoAuxiliar]["visib"] = fondosConOrto[$(this)[0].id];
	if (fondosConOrto[$(this)[0].id]){
		var mostrarOrto=false;
		if (fondosScaleOrto[$(this)[0].id]){
			var escala = fondosScaleOrto[$(this)[0].id];
			var escalaActual = getMapScale(false);
			if (escala >=escalaActual){
				mostrarOrto=true;
			}
		}
		else{
			mostrarOrto = true;
		}
		if (mostrarOrto){
		$("#"+wmsList[idxOrtoAuxiliar]["id"]).removeClass (function (index, className) {
			return (className.match (/transp[0-9]+/) || []).join(' ');
		});
		if (fondosTranspOrto[$(this)[0].id]){
			$("#"+wmsList[idxOrtoAuxiliar]["id"]).addClass("transp"+fondosTranspOrto[$(this)[0].id]);
		}
		updateWMS(1);
		}
	}else{
		clearWMS(1);
	}
		
	//textoFondoArray[0] = textoFondoFotoDefault[idx];
		}
	});

	

	//updateAcetateMarca();

	setColorMarcas("#ffff00");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").removeClass("textoAzul");
	}
	
	function showFondoLevel(fondoId,escalaActual){
		var mostrarOrto=false;
		if (fondosScaleOrto[fondoId]){
			var escala = fondosScaleOrto[fondoId];
		
			if (escala >=escalaActual){
				mostrarOrto=true;
			}
		
		wmsList[idxOrtoAuxiliar]["visib"] = mostrarOrto;
		if (mostrarOrto){
			$("#"+wmsList[idxOrtoAuxiliar]["id"]).removeClass (function (index, className) {
				return (className.match (/transp[0-9]+/) || []).join(' ');
			});
			if (fondosTranspOrto[fondoId]){
				$("#"+wmsList[idxOrtoAuxiliar]["id"]).addClass("transp"+fondosTranspOrto[fondoId]);
			}
			updateWMS(1);	
		}
		else{
			
		clearWMS(1);
	}
	}
	}
	function getTextoFondo() {
		if ($("#fondos input").length>0){
			var texto;
			$("#fondos input").each(function(index){
				if($(this)[0].checked){
					var textoFondoNiveles = fondosNiveles[$(this)[0].id];
					if (textoFondoNiveles != null){
						var escalaActual = getMapScale(false);
						showFondoLevel($(this)[0].id,escalaActual);
						var textos = fondosTextos[$(this)[0].id];
						if ($("#"+$(this)[0].id+'Selector').length>0){ // tiene opciones
							if ((textoFondoNiveles.length > document.getElementById($(this)[0].id+'Selector').selectedIndex)&&
							(textoFondoNiveles[document.getElementById($(this)[0].id+'Selector').selectedIndex]!=null)){// hay niveles definidos para la opción seleccionada
								textoFondoNiveles = textoFondoNiveles[document.getElementById($(this)[0].id+'Selector').selectedIndex];
								textos = textos[document.getElementById($(this)[0].id+'Selector').selectedIndex];
							}
							else{
								texto= fondosTextos[$(this)[0].id][document.getElementById($(this)[0].id+'Selector').selectedIndex];
								textoFondoNiveles=[];
							}
						}
						if (textoFondoNiveles){
							for (var i=0; i < textoFondoNiveles.length; i++) {
								if (escalaActual <= textoFondoNiveles[i]) {
									texto= textos[i];
									break;
								}
							}
						}

					}
					else if ($("#"+$(this)[0].id+'Selector').length>0){  // tiene opciones
						texto= fondosTextos[$(this)[0].id][document.getElementById($(this)[0].id+'Selector').selectedIndex];	
					}
					else{
						texto= fondosTextos[$(this)[0].id];
					}
				}
			});
			return texto;
		}
		else{
			console.log("Código de getTextoFondo en map.js a eliminar")
			if (document.getElementById('bgFotoShowId')) {
				if (document.getElementById('bgFotoShowId').checked) {
					//return textoFondoFoto;//(idxWMSFondoFoto);
					return getTextoFondoFoto(idxWMSFondoFoto);
				} else if (document.getElementById('bgMapaShowId').checked) {
					return getTextoFondoMapa();
				} else if (document.getElementById('satelite').checked) {
					return getTextoFondoSatelite();
				} 
				else if (document.getElementById('sombreado').checked) {
					return textoFondoSombreado;
				} else if (document.getElementById('mdt').checked) {
					return textoFondoMDT;
				} 
				else {
					return textoFondoMudo;
				}
			} else {
				return textoFondoFotoDefault;
			}
		}
	}
function getFondoLabel(txt){

	//alert(txt);
	return (txt==""? '<div id="fondo"></div>':'<div id="fondo">'+txt+'</div>');
}
function showFondoMapa() {
	console.log("Función a eliminar: showFondoMapa ");

	updateFVueloQuery(-1);

	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = false;
		clearWMS(idxWMSFondoFoto);
	}
	wmsList[0]["url"] = fondoMapa[0];
	wmsList[0]["visib"] = true;
	clearWMS(0);
	updateWMS(0);
	wmsList[idxOrtoAuxiliar]["visib"] = false;
	clearWMS(1);
	
	
	//updateAcetateMarca();

	setColorMarcas("#0000ff");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").addClass("textoAzul");
}


function changeScaleFondoSatelite(escalaActual){
	console.log("Función a eliminar: changeScaleFondoSatelite ");
	var mostrarOrto=false;
	mostrarOrto =(escalaActual <= textoFondoSateliteNivel[0]);
	wmsList[0]["visib"] = !mostrarOrto;
	clearWMS(0);
	if (!mostrarOrto){
		
	updateWMS(0);
	}
	wmsList[idxOrtoAuxiliar]["visib"] = mostrarOrto;
	$("#"+wmsList[idxOrtoAuxiliar]["id"]).removeClass('transp30');
	clearWMS(1);
	if (mostrarOrto){
		
	updateWMS(1);
	}
}
function showFondoSatelite() {
	
console.log("Función a eliminar: showFondoSatelite ");
	updateFVueloQuery(-1);

	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = false;
		clearWMS(idxWMSFondoFoto);
	}
	
	
	
		
			
	wmsList[0]["url"] = fondoSatelite[0];
	var escalaActual = getMapScale(false);
	changeScaleFondoSatelite(escalaActual);
	
	
	//updateAcetateMarca();

	setColorMarcas("#0000ff");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").removeClass("textoAzul");
}

function showFondoFoto() {
	console.log("Función a eliminar: showFondoFoto ");

	updateFVueloQuery(idxWMSFondoFoto);

	clearWMS(0);
	var idx = document.getElementById("ortoSelector").selectedIndex;
	if ((idx<0)||(idx >fondoFoto.length)){
		idx=0;
	}
	wmsList[0]["url"] = fondoFoto[idx];
	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = true;
		wmsList[0]["visib"] = false;
	}
	clearWMS(idxWMSFondoFoto);
	updateWMS(idxWMSFondoFoto);
	wmsList[idxOrtoAuxiliar]["visib"] = false;
	clearWMS(1);
	
	textoFondoArray[0] = textoFondoFotoDefault[idx];

	//updateAcetateMarca();

	setColorMarcas("#ffff00");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").removeClass("textoAzul");
}



function showFondoMudo() {
	console.log("Función a eliminar: showFondoMudo ");

	updateFVueloQuery(-1);

	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = false;
		clearWMS(idxWMSFondoFoto);
	}
	wmsList[0]["url"] = fondoMudo[0];
	wmsList[0]["visib"] = true;
	clearWMS(0);
	updateWMS(0);
	wmsList[idxOrtoAuxiliar]["visib"] = false;
	clearWMS(1);
	
	
	//updateAcetateMarca();

	setColorMarcas("#0000ff");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").addClass("textoAzul");
}

function showFondoSombreado() {
	console.log("Función a eliminar: showFondoSombreado ");

	updateFVueloQuery(-1);

	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = false;
		clearWMS(idxWMSFondoFoto);
	}
	wmsList[0]["url"] = fondoSombreado[0];
	wmsList[0]["visib"] = true;
	clearWMS(0);
	updateWMS(0);
	wmsList[idxOrtoAuxiliar]["visib"] = false;
	clearWMS(1);
	
	//updateAcetateMarca();

	setColorMarcas("#0000ff");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").removeClass("textoAzul");
}

function showFondoMDT() {
	
console.log("Función a eliminar: showFondoMDT ");
	updateFVueloQuery(-1);

	if (idxWMSFondoFoto != 0) {
		wmsList[idxWMSFondoFoto]["visib"] = false;
		clearWMS(idxWMSFondoFoto);
	}
	wmsList[0]["url"] = fondoMDT[0];
	wmsList[0]["visib"] = true;
	clearWMS(0);
	updateWMS(0);
	wmsList[idxOrtoAuxiliar]["visib"] = true;
	$("#"+wmsList[idxOrtoAuxiliar]["id"]).addClass('transp30');
	updateWMS(1);
	
	
	//updateAcetateMarca();

	setColorMarcas("#0000ff");
	updateMapClicks();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	$("#textosInferior").removeClass("textoAzul");
}
/**
 * Oculta el mensaje de cargando cuando no está cargando ninguna capa (espera hasta que ocurra)
 */
function hideLoadingAfterLoading(){
	if (!loading_layer){ // no se está cargando la capa
		showLoading(false);
	}
	else{  // todavía se está cargando la capa
		// intentarlo de nuevo dentro de 1 seg.
		setTimeout(hideLoadingAfterLoading,1000);
	}
}

function prevZoom(){
	navHistory.previousTrigger();
}

function changeImgSrc(img,src){
	img.src=src;
}

/**
 * inicializa el mapa, con las capas de fondo
 */
function initMap() {
	initMapButtons();
initMapDialogs();

}
var creadoMapOL=false;
function createMapOL(){
	 var map_projection = new ol.proj.Projection({
	        code:WMS_SRS,
	        units: 'm',
	        extent:[WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX]// necesario para que se vea el overview
	      });
	var options = {
			projection : map_projection,
//			maxExtent : new OpenLayers.Bounds(WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX),
		//	center : [675533, 4589000],
			/*resolutions : resoluciones*//*,
			           controls : [ new OpenLayers.Control.ArgParser()
			                       ]*/

	};
	//attribution = getFondoLabel()+base_attribution;
	mapOL = new ol.Map({
		controls: [],
     target: $('#mapOL')[0],
     view: new ol.View(options)
   });
	creadoMapOL=true;
	 attributionControl=new ol.control.Attribution({
   	  collapsible: false,
   	 
   	  collapsed: false,
   	 
   	});
   attributionControl = new ol.control.Attribution({collapsible:false, collapsed:false});
	mapOL.addControl(attributionControl);  
	  var source = new ol.source.Vector({features: []});
      markers= new ol.layer.Vector({  source: source});
  	// markers = new OpenLayers.Layer.Markers( "Markers" );
      mapOL.addLayer(markers);
      markers.getSource().setAttributions(getTextoFondo());
      ScaleBarControl = function(opt_options) {

          var options = opt_options || {};

          var scaleBar = document.createElement('scaleBarDiv');
          scaleBar.innerHTML =base_attribution;

         

          ol.control.Control.call(this, {
            element: scaleBar,
            target: options.target
          });

        };
        ol.inherits(ScaleBarControl, ol.control.Control);
        mapOL.addControl(new ScaleBarControl());

}
function changeBaseLayerOption(buttonId){
	document.getElementById(buttonId).checked=true;
	$("#"+buttonId).button("refresh");
	updateBaseLayer();
}
function showBaseLayerSelector(img,selectorId){
	if (!document.getElementById(selectorId)){
		selectorId=selectorId.replace("_chosen","");
	}
	if ((document.getElementById(selectorId).style.display=="")||(document.getElementById(selectorId).style.display=="none")){
	document.getElementById(selectorId).style.display="block";
	img.src="/lib/IDEAragon/themes/default/images/up-arrow-mini.png";
	}
	else{
		document.getElementById(selectorId).style.display="none";
		img.src="/lib/IDEAragon/themes/default/images/down-arrow-mini.png";
	}
}
function info(){
	if (document.getElementById("infoTool").checked){
		setActiveTool('identifyAll');
		
		showSelTool(true,"info-icon");
	}
	else{
	
		showSelTool(false,"info-icon");
	}


}

function download(){
	if (document.getElementById("download").checked){
		setActiveTool('descarga');
		
		showSelTool(true,"download-icon");
	}
	else{
	
		showSelTool(false,"download-icon");
	}
	

}

function profile(){
	if (document.getElementById("profileTool").checked){
		
		/*clearMeasureArea(true);*/
		
		setActiveTool('profile');
		
		showSelTool(true,"profile-icon");
	}
	else{
	
		showSelTool(false,"profile-icon");
	}

	
}
function measurePath(){
	if (document.getElementById("distance").checked){
		
		/*clearMeasureArea(true);*/
		
		setActiveTool('distanceMeasure');
		
		showSelTool(true,"distance-icon");
	}
	else{
	
		showSelTool(false,"distance-icon");
	}

	
}

function measureArea(){
	if (document.getElementById("area").checked){
		/*clearMeasurePath(true);*/
		
		setActiveTool('areaMeasure');
		
		showSelTool(true,"area-icon");
	}
	else{
	
		showSelTool(false,"area-icon");
	}

	
}


/*function clearMeasureArea(close){

	document.getElementById("area").checked=false;
	$("#area").button("refresh");
	if (close){
		$( "#measureArea" ).dialog( "close" );
	}

}*/

/*function clearMeasurePath(close){

	document.getElementById("distance").checked=false;
	$("#distance").button("refresh");
	if (close){
		$( "#measurePath" ).dialog( "close" );
	}
	
}*/

function updateBaseLayer(){
	if ($("#fondos input").length>0){
		showFondo();
	}
	else{
		console.log("codigo en updateBaseLayer a eliminar");
	if (document.getElementById("bgFotoShowId").checked){
		 showFondoFoto();
	}
	else if (document.getElementById("bgMudoShowId").checked){
		 showFondoMudo();
	}
	else if (document.getElementById("satelite").checked){
		showFondoSatelite();
	}
	else if (document.getElementById("bgMapaShowId").checked){
		 showFondoMapa();
	}
	else if (document.getElementById("sombreado").checked){
		showFondoSombreado();
	}
	else if (document.getElementById("mdt").checked){
		showFondoMDT();
	}
	}
	
	markers.getSource().setAttributions(getFondoLabel(getTextoFondo()));
	attributionControl.changed();
}


function changeBaseLayer(buttonId){
	document.getElementById(buttonId).checked=true;
	$("#"+buttonId).button("refresh");

	updateBaseLayer();
}


function showResultOL(selectionWhere, idSelectionLayer, queryUrl,customBbox,async){
	var source = markers.getSource();
	var searchResultQuery=queryUrl+'?service=WFS&' +
	'version=1.0.0&request=GetFeature&typename='+idSelectionLayer+'&' +
	'outputFormat=application/json&srsname=EPSG:25830&' +
	'CQL_FILTER='+selectionWhere;
	$.ajax({
		url: searchResultQuery,
		type: 'GET',
		success:  function(data){
			
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
		
		
			
			if (features.length>0){
				 buffer=defaultBuffer;
			
				
				for (var i=0; i<features.length; i++){
					var geom = features[i].getGeometry();
					if (geom.getType()=='Point') {
						buffer=bufferPoints;
						features[i].setStyle(new ol.style.Style({image:new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})}));
					}

					if (geom.getType()!='Point') {
						features[i].set("layer",idSelectionLayer);
						features[i].set("cql_filter",selectionWhere);
					}
					source.addFeature(features[i]);
					source.refresh();	
					if (async && !customBbox){
						var bbox = ol.extent.buffer( source.getExtent(),buffer);
						getMap(bbox[0],bbox[1],bbox[2],bbox[3],false, true);
					}						
				}
				
			}
			else{
				alert("No se ha encontrado el objeto con la condicion "+selectionWhere+" en la capa "+idSelectionLayer);
			}
		},
		async: async,
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			alert("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
		}


	});	


}

function showSearchResultOL(parameters){
	selectionWhere=getInitialParam(parameters, "QUERY=");
	var partes = selectionWhere.split("=");
	var igual = selectionWhere.indexOf("=");
	selectionWhere = partes[0].toLowerCase()+selectionWhere.substr(igual);
	idSelectionLayer = getInitialParam(parameters, "ACTIVELAYER="); // la que sea
	var queryservice = getInitialParam(parameters, "QUERYSERVICE=");
	if (queryservice !=""){
	queryUrl = server + "/geoserver/"+queryservice + "/ows";
	}
	else{
		queryUrl=url_ows;
	}
		if (selectionWhere){
			var source = markers.getSource();
						source.clear(true);
			
					$("#tabs").tabs("option", "active", 0);
					var format = new ol.format.GeoJSON();
					var features = format.readFeatures(data);
					var bboxString = getInitialParam(parameters, "BOX=");
					var customBbox = false;
					if (bboxString != "") {
						var xyBox = bboxString.split(":");
						if (xyBox.length==4) {
							customMinx = parseFloat(xyBox[0]);
							customMiny = parseFloat(xyBox[1]);
							customMaxx = parseFloat(xyBox[2]);
							customMaxy = parseFloat(xyBox[3]);
							getMap(customMinx,customMiny,customMaxx,customMaxy,false, true);
							customBbox = true;
						}
					}
					var async = (getInitialParam(parameters, "QUERY_2=")=="");
					showResultOL(selectionWhere, idSelectionLayer, queryUrl,customBbox,async);
					var i=2;
					var hayMas= true;
					while (hayMas){
						selectionWhere=getInitialParam(parameters, "QUERY_"+i+"=");
						partes = selectionWhere.split("=");
						igual = selectionWhere.indexOf("=");
						selectionWhere = partes[0].toLowerCase()+selectionWhere.substr(igual);
						idSelectionLayer = getInitialParam(parameters, "ACTIVELAYER_"+i+"="); // la que sea
						var queryservice = getInitialParam(parameters, "QUERYSERVICE_"+i+"=");
						if (selectionWhere){
							showResultOL(selectionWhere, idSelectionLayer, queryUrl,customBbox,false);
						}
						else{
							hayMas=false;
						}
						i++;
					}
				if (!async && !customBbox){
						var bbox = ol.extent.buffer( source.getExtent(),buffer);
						getMap(bbox[0],bbox[1],bbox[2],bbox[3],false, true);
					}		


		}
		else{
			
		
	
	bbox = getInitialParam(parameters,"BOX=").split(":");
	$("#tabs").tabs("option", "active", 0);
	//mapOL.getView().fit(bbox, mapOL.getSize()); 
	//getMap(Number(bbox[0]),Number(bbox[1]),Number(bbox[2]),Number(bbox[3]),false, true);
	markerx = getInitialParam(parameters, "MARKERX=");
	markery = getInitialParam(parameters, "MARKERY=");
	showMarker(markerx,markery,bbox);
	/*if ((markerx!="")&&(markery!="")){

		var source = markers.getSource();
		source.clear(true);
		var point = new ol.Feature({
			  geometry:  new ol.geom.Point([markerx, markery])
			});
		point.setStyle(new ol.style.Style({image:new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})}));
		source.addFeature(point);
		source.refresh();		

	}*/
		}
}

function showMarker(markerx,markery, bboxParam){
	
	if ((markerx!="")&&(markery!="")){
		
		var source = markers.getSource();
		source.clear(true);
		var point = new ol.Feature({
			  geometry:  new ol.geom.Point([markerx, markery])
			});
		point.setStyle(new ol.style.Style({image:new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})}));
		source.addFeature(point);
		source.refresh();		
		var bbox = ol.extent.buffer( point.getGeometry().getExtent(),bufferPoints);
		if (bboxParam){
			bbox=bboxParam;
		}
		getMap(Number(bbox[0]),Number(bbox[1]),Number(bbox[2]),Number(bbox[3]),false, true);
	}
}
function showSearchResult(parameters){
console.log("Función a eliminar. Map.js showSearchResult(parameters)");
	
	selectionType="poly"; //line o poin (point sin t)
	
	selectionWhere=getInitialParam(parameters, "QUERY=");
	idSelectionLayer = getInitialParam(parameters, "ACTIVELAYER="); // la que sea
	var queryservice = getInitialParam(parameters, "QUERYSERVICE=");
	if (queryservice !=""){
	queryUrl = server + "/servlet/com.esri.esrimap.Esrimap?ServiceName=" +queryservice + "&Form=False&ClientVersion=4.0&CustomService=Query";
	}
	else{
		queryUrl=url;
	}
		if ((idSelectionLayer == "BusCallUrb")
				||(idSelectionLayer=="VIAS_COMUNICACION")||(idSelectionLayer=="VIAS_COMUNICACION_TEMP")){
			selectionType="line";
		}
		else if ((idSelectionLayer=="PUNTOS_AGUA")||(idSelectionLayer=="PUNTOS_AGUA_TEMP")
				||(idSelectionLayer=="PUESTOS_VIG")||(idSelectionLayer=="PUESTOS_VIG_TEMP")
				||(idSelectionLayer=="MEDIOS")||(idSelectionLayer=="MEDIOS_TEMP")
				||(idSelectionLayer=="PUNTOS_IGNICION")||(idSelectionLayer=="PUNTOS_IGNICION_TEMP")){
			selectionType="poin";
		}
		
	
		

	var	coords = getInitialParam(parameters, "BOX=");
	//params = coords.split("&");
	bbox = coords.split(":");
	
	getMap(parseInt(bbox[0]), parseInt(bbox[1]), parseInt(bbox[2]),
			parseInt(bbox[3]),false);
	markerx = getInitialParam(parameters, "MARKERX=");
	markery = getInitialParam(parameters, "MARKERY=");
	if ((markerx!="")&&(markery!="")){
		updateAcetateMarca("","",parseInt(markerx),parseInt(markery));
	
	}

	$("#tabs").tabs("option", "active", 0);
	
	if ((typeof TABLET_WIDTH !== 'undefined')&&(screen.width<=TABLET_WIDTH)){
		collapseTools();
	}

}
function showPortalInMap(data,bbox_calle){
	count =getObjectField(data,"count=\"");
	var bbox = bbox_calle;
	if (count[0]!="0"){
		
			
		minx= getObjectField(data,"minx=\"");
		miny= getObjectField(data,"miny=\"");
		maxx=getObjectField(data,"maxx=\"");
		maxy=getObjectField(data,"maxy=\"");
		bbox = (parseInt(minx[0])-100)+":"+  (parseInt(miny[0])-100)+":"+  (parseInt(maxx[0])+100)+":"+ ( parseInt(maxy[0])+100)+
		"&MARKERX="+minx[0]+"&MARKERY="+miny[0];;
		
	}
	else{
		alert("No se localiza el portal. Se muestra la calle completa");
	}
	showSearchResult("BOX="+bbox);
}
function removeMarkers(){
	markers.getSource().clear(true);
	
}

function fitMap(bbox){
	getMap(bbox[0],bbox[1],bbox[2],bbox[3],false, true);
}
function showPortalResult(calle,portal,bbox_calle){
	showPortalResultOL(calle,portal,bbox_calle);
}
function showPortalResultOL(calle,portal,bbox_calle){

	
	$.ajax({
		url: url_ows+'?service=WFS&' +
		'version=1.0.0&request=GetFeature&typename=TroidesV&' +
		'outputFormat=application/json&srsname=EPSG:25830&' +
		"CQL_FILTER=c_mun_via='"+calle+"' AND numero="+portal,
		type: 'GET',
		success:  function(data){
			$("#tabs").tabs("option", "active", 0);
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
			if (features.length>0){
				
				var geom = features[0].getGeometry();
				var buffer=defaultBuffer;
				if (geom.getType()=='Point') {
					buffer=bufferPoints;
				}
				//mapOL.getView().fit(ol.extent.buffer( features[0].getGeometry().getExtent(),buffer), mapOL.getSize()); 
				var bbox = ol.extent.buffer( features[0].getGeometry().getExtent(),buffer);
				//getMap(bbox[0],bbox[1],bbox[2],bbox[3],false, true);
				fitMap(bbox);
				var source = markers.getSource();
				features[0].setStyle(new ol.style.Style({image:new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})}));
				source.clear(true);
				source.addFeature(features[0]);
				source.refresh();

			}
			else{
				alert("No se ha encontrado el portal indicado, se mostrará la calle completa");
				showSearchResultOL( "ACTIVELAYER=BusCallUrb&QUERY=c_mun_via="+calle);
			}
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			alert("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
		}


	});	
}

/**
 * inicializa los botones de JQuery
 */
function initMapButtons() {
	$("#toolsButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});

	$("#baseLayerButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});
	$("#scaleButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});
	$("#notificarButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});
	$("#panTool").button({
		text:false,
		icons : {
			primary : "pan-icon"
		}
	});
	$("#zoomRectTool").button({
		text:false,
		icons : {
			primary : "zoom-rect-icon"
		}
	});
	$("#zoomOutTool").button({
		text:false,
		icons : {
			primary : "zoom-out-icon"
		}
	});
	$("#prevZoom").button({
		text:false,
		icons : {
			primary : "zoom-prev-icon"
		}
	});
	$("#fullExtentTool").button({
		text:false,
		icons : {
			primary : "full-extent-icon"
		}
	});
	$("#infoTool").button({
		text:false,
		icons : {
			primary : "info-icon"
		}
	});
	$("#download").button({
		text:false,
		icons : {
			primary : "download-icon"
		}
	});
	$("#impShp").button({
		text:false,
		icons : {
			primary : "shp-icon"
		}
	});
	$("#impGPX").button({
		text:false,
		icons : {
			primary : "gpx-icon"
		}
	});
	$("#georref").button({
		text:false,
		icons : {
			primary : "edit-icon"
		}
	});
	$("#circulo").button({
		text:false,
		icons : {
			primary : "circle-icon"
		}
	});
	$("#catastro").button({
		text:false,
		icons : {
			primary : "ovc-icon"
		}
	});
	$("#histCoords").button({
		text:false,
		icons : {
			primary : "coords-icon"
		}
	});
	$("#distance").button({
		text:false,
		icons : {
			primary : "distance-icon"
		}
	});
	$("#area").button({
		text:false,
		icons : {
			primary : "area-icon"
		}
	});
	$("#profileTool").button({
		text:false,
		icons : {
			primary : "profile-icon"
		}
	});
	$("#delete").button({
		text:false,
		icons : {
			primary : "delete-icon"
		}
	});
	$("#rjt").button({
		text:false,
		icons : {
			primary : "rjt-icon"
		}
	});
	$("#tableToMap").button({
		text:false,
		icons : {
			primary : "tableToMap-icon"
		}
	});
	$("#3D").button({
		text:false,
		icons : {
			primary : "v3d-icon"
		}
	});
	$("#btnCleanMeasurePath").button({
		
		icons : {
			primary : "clean-icon"
		}
	});
	
	$("#btnCleanMeasureArea").button({
		
		icons : {
			primary : "clean-icon"
		}
	});
	
//	$("#bgMudoShowId").button({icons : {
//		primary : "basico-icon"}
//	});
//	$("#bgMapaShowId").button({icons : {
//		primary : "topo-icon"}
//	});
//	$("#sombreado").button({icons : {
//		primary : "sombreado-icon"}
//	});
//	$("#mdt").button({icons : {
//		primary : "mdt-icon"}
//	});
//	$("#bgFotoShowId").button({icons : {
//		primary : "orto-icon"}
//	});
//	$("#satelite").button({icons : {
//		primary : "satelite-icon"}
//	});
//	document.getElementById("satelite").checked=true;
//	$("#satelite").button("refresh");
	
	 $( "#transpFondoSlider" ).slider({
		 min: 0,
		 max: 100,
		 step: 1,
		 slide:function( event, ui ) {
			 $("#theImageWMS0").css("opacity",(100-ui.value)/100);
		 },
		 change: function( event, ui ) {
			 $("#theImageWMS0").css("opacity",(100-ui.value)/100);
			 showTranspWarning(ui.value) ;
		 }
		 });
	document.getElementById("zoomRectTool").checked=true;
	$("#zoomRectTool").button("refresh");
	showSelTool(true,"zoom-rect-icon");

	 // $( "#baseToolbar" ).buttonset();
	  
		
		 $( "#zoomSlider" ).slider({
			 value:0,
			 min: 0,
			 max: 8,
			 step: 1,
			 orientation:"vertical",
			 change: function( event, ui ) {
				 if (needScaleUpdate){
				doZoom(zoomBarScales[ui.value]);
				 }
				 needScaleUpdate=true;
			 }
			 });
}

function zoomTo(level){
	$( "#zoomSlider" ).slider( "value", level );
}
function catastro(){
	if (document.getElementById("catastro").checked){
	setActiveTool('identifyOVC');
	showSelTool(true,"ovc-icon");
	}
	else{
	
		showSelTool(false,"ovc-icon");
	}

}

function histCoords(){
	if (document.getElementById("histCoords").checked){
	setActiveTool('historico');
	/*clearMeasurePath(true);
	clearMeasureArea(true);*/
	showSelTool(true,"coords-icon");
	}
	else{
	
		showSelTool(false,"coords-icon");
	}

}

function circulo(){
	if (document.getElementById("circulo").checked){
		setActiveTool('bufferCirculo');
		/*clearMeasurePath(true);
		clearMeasureArea(true);*/
		showSelTool(true,"circle-icon");
	}
	else{
	
		showSelTool(false,"circle-icon");
	}

}

function georref(){
	if (document.getElementById("georref").checked){
		setActiveTool('editar');
		/*clearMeasurePath(true);
		clearMeasureArea(true);*/
		showSelTool(true,"edit-icon");
	}
	else{
	
		showSelTool(false,"edit-icon");
	}

}

function showSelTool(show,icon){
	$("#selTool").removeClass();
	if (show){
		
		$("#selTool").addClass(icon);
		$("#toolLabel").addClass("selToolLabel");
	}
	else{
		

		$("#toolLabel").removeClass("selToolLabel");
		}
}

function activateZoomRect(){
	document.getElementById("zoomRectTool").checked=true;
	$("#zoomRectTool").button("refresh");
zoomRect();
}
function zoomRect(){
	if (document.getElementById("zoomRectTool").checked){
		setActiveTool('zoomIn');
		
		showSelTool(true,"zoom-rect-icon");
	}
	else{
	
		showSelTool(false,"zoom-rect-icon");
	}
}

function zoomOutTool(){
	if (document.getElementById("zoomOutTool").checked){
	setActiveTool('zoomOut');
	showSelTool(true,"zoom-out-icon");
	}
	else{
	
		showSelTool(false,"zoom-out-icon");
	}

}

function panTool(){
	if (document.getElementById("panTool").checked){
	setActiveTool('pan');
	showSelTool(true,"pan-icon");
	}
	else{
	
		showSelTool(false,"pan-icon");
	}

}


function doPanSouth(){
	pan(0,-40);
}

function doPanWest(){
	pan(-40,0);
}

function doPanEast(){
	pan(40,0);
}

function doPanNorth(){
	pan(0,40);
}
function doZoom(scale){
	
	doZoomScale(scale);
	setNewScale(scale);
	
}

function updateMapOL(){
	mapOL.getView().fit([minx,miny,maxx,maxy], [mwidth,mheight],{constrainResolution: false}); 
	setScaleBarOL();
	markers.getSource().setAttributions(getFondoLabel(getTextoFondo()));
	attributionControl.changed();
	//var axl = getMapRequest(_minx, _miny, _maxx, _maxy);
	var olBounds = mapOL.getView().calculateExtent( [mwidth,mheight]);//mapOL.getExtent();
	minx=olBounds[0];
	miny=olBounds[1];
	maxx=olBounds[2];
	maxy=olBounds[3];
}
function updateZoomBar(scale){
	needScaleUpdate = false;
	var anterior=zoomBarScales[0];
	
	for (var i=0; i<zoomBarScales.length; i++){
		if (scale>=zoomBarScales[i]){
			if ((anterior-scale)>=(scale-zoomBarScales[i])){
				$( "#zoomSlider" ).slider("option","value",i);
			}
			else{
				$( "#zoomSlider" ).slider("option","value",i-1);
			}
			return;
		}
		anterior=zoomBarScales[i];
	}
	$( "#zoomSlider" ).slider("option","value",zoomBarScales.length-1);
	
}
function doZoomIn(){
	needScaleUpdate = true;
	valor = $( "#zoomSlider" ).slider("value")+1;
	if  (valor <=$( "#zoomSlider" ).slider("option","max")){
		$( "#zoomSlider" ).slider("value",valor);
	}
}

function doZoomOut(){
	needScaleUpdate = true;
	valor = $( "#zoomSlider" ).slider("value")-1;
	if  (valor >=$( "#zoomSlider" ).slider("option","min")){
		$( "#zoomSlider" ).slider("value",valor);
	}
}
function initMapDialogs(){
	 $( "#profile" ).dialog({
		 autoOpen: false,
		 height: 'auto',
		 width: 'auto',
		 modal: false,
		 resizable:false,
		 close:closeProfileWin,
		 open:openProfileWin,
		 appendTo:"#map",
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	 
		
	 $("#profile").dialog("widget").draggable("option", "containment", "#mapContainer" );
	 $( "#measureArea" ).dialog({
		 autoOpen: false,
		 height: 150,
		 width: 230,
		 modal: false,
		 resizable:false,
		 close:closeMeasureAreaWin,
		 open:openMeasureAreaWin,
		 appendTo:"#map",
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	 
		
	 $("#measureArea").dialog("widget").draggable("option", "containment", "#mapContainer" );
	 
	 $( "#measurePath" ).dialog({
		 autoOpen: false,
		 height: 170,
		 width: 230,
		 modal: false,
		 resizable:false,
		 open:openMeasurePathWin,
		 close:closeMeasurePathWin,
		 appendTo:"#map",
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	 $("#measurePath").dialog("widget").draggable("option", "containment", "#mapContainer" );
	 var printWin_width =450;
	 var printWin_height =170;

	 if(parseInt($("#toolsClose").css("width"))>29){ // es móvil){
		 printWin_width=590;
		 printWin_height =190;
		 
	 }
	 else 	if((typeof TABLET_WIDTH !== 'undefined')&&(screen.width<=TABLET_WIDTH)){
		printWin_width=360;
	}
	 $( "#printWin" ).dialog({
		 autoOpen: false,
		 height: printWin_height,
		 width: printWin_width,
		 modal: true,
		 resizable:false,
		
		
		 position: { my: "middle middle", at: "middle middle", of: $("#tabs") }});
	 $("#printWin").dialog("widget").draggable("option", "containment", "#tabs" );
	 
	 $( "#v3dWin" ).dialog({
		 autoOpen: false,
		 height: 270,
		 width: 600,
		 resizable:false,
		
		
		 position: { my: "middle middle", at: "middle middle", of: $("#tabs") }});
	 $("#v3dWin").dialog("widget").draggable("option", "containment", "#tabs" );
	 
	 $( "#historicoWin" ).dialog({
		 autoOpen: false,
		 height: 160,
		 width: 260,
		 modal: false,
		 resizable:false,
		 open:openHistoricoWin,
		 close:closeHistoricoWin,
		 appendTo:"#map",
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	 $("#historicoWin").dialog("widget").draggable("option", "containment", "#map" );
	 
	 $( "#georrefWin" ).dialog({
		 autoOpen: false,
		 height: 160,
		 width: 245,
		 modal: false,
		 resizable:false,
		 appendTo:"#map",
		 open:openGeorrefWin,
		 close:closeGeorrefWin,
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	
	 $("#georrefWin").dialog("widget").draggable("option", "containment", "#map" );
	 
	 $( "#circleWin" ).dialog({
		 autoOpen: false,
		 height: 160,
		 width: 260,
		 modal: false,
		 resizable:false,
		 open: openCircleWin,
		 close: closeCircleWin,
		 appendTo:"#map",
		 position: { my: "right top", at: "right bottom", of: $("#mapToolbar") }});
	 $("#circleWin").dialog("widget").draggable("option", "containment", "#map" );
	 
	 
	 
}
function openProfileWin(){
	document.getElementById("profileTool").checked=true;
	$("#profileTool").button("refresh");
}
function openMeasureAreaWin(){
	document.getElementById("area").checked=true;
	$("#area").button("refresh");
}

function openMeasurePathWin(){
	document.getElementById("distance").checked=true;
	$("#distance").button("refresh");
}


function openGeorrefWin(){
	document.getElementById("georref").checked=true;
	$("#georref").button("refresh");
	
}

function openHistoricoWin(){
	document.getElementById("histCoords").checked=true;
	$("#histCoords").button("refresh");
	
}

function openCircleWin(){
	document.getElementById("circulo").checked=true;
	$("#circulo").button("refresh");
	
}
function closeProfileWin(){
	limpiarProfile();
	//clearMeasureArea();
	activateZoomRect();
}
function closeMeasureAreaWin(){
	limpiarMedicionArea();
	//clearMeasureArea();
	activateZoomRect();
}
function closeMeasurePathWin(){
	limpiarMedicionDistancia();
//	clearMeasurePath();
	activateZoomRect();
}
function closeGeorrefWin(){
	limpiarEdicion();
	activateZoomRect();
}


function closeHistoricoWin(){
	limpiarHistorico();
	activateZoomRect();
}

function closeCircleWin(){
	borrarBufferCirculo();
	activateZoomRect();
	
}

function showBaseLayers(show){
	if (show) {
		$("#baseToolbar").show( "Slide" );
	} else{
		$("#baseToolbar").hide("Slide");
	}
}
function showTools(show){
	if (show) {
		$("#mapToolbar").show( "Slide" );
	} else {
		$("#mapToolbar").hide("Slide");
	}
}
function showScaleAndCoords(show){
	if (show) {
		$("#scaleToolbar").show( "Slide" );
	} else {
		$("#scaleToolbar").hide("Slide");
	}
}
function showNotificar(show){
	if (show) {
		$("#notificarToolbar").show( "Slide" );
	} else {
		$("#notificarToolbar").hide("Slide");
	}
}

function ocultaLayerLoading() {
	hideLayer('loading');
}

function muestraLayerLoading() {
	showLayer('loading');
	setTimeout("ocultaLayerLoading()", 30000);
}



function setNewCoords(x, y) {
	comboSRS = document.getElementById('cboSRSClient');
	theSRS = comboSRS.options[comboSRS.selectedIndex].value;

	theX = x;
	theY = y;
	latLon = coordTransformProj4s("EPSG:25830", "EPSG:4258", theX, theY);
		// convertir del 25830 al srs elegido
	if (theSRS != "EPSG:25830") {
		destino = coordTransformProj4s("EPSG:25830", theSRS, x, y);
		theX = destino.ord[0];
		theY = destino.ord[1];
	}
	theX = RoundDecimal(theX, numDecimals);
	theY = RoundDecimal(theY, numDecimals);

	document.getElementById("Lx").value = theX;
	document.getElementById("Ly").value = theY;

	theLon = RoundDecimal(latLon.ord[0], 8);
	theLat = RoundDecimal(latLon.ord[1], 8);

	document.getElementById("Llat").value = theLat;
	document.getElementById("Llon").value = theLon;
}

function cboEscala_onkeypress(evt) {
	if (isEnterKey(evt)) {
		updateScale();
	}
}

function setNewScale(newScale) {
	document.getElementById('scaleTextBox').value = parseInt(newScale);
	try{
	updateLegends(newScale);
	}
	catch(err){
		console.log(err);
	}
}

function updScale() {
	updateScale();
}

function updateScale() {
	newValue = document.getElementById('scaleTextBox').value;
	if (! isNaN(newValue)) {
		if (newValue >= 500) {
			doZoomScale(newValue);
			updateZoomBar(newValue);
			return false;
		} else if ((newValue > 0) && (newValue < 500)) {
			window.alert("La escala debe ser mayor que 500");
		} else if (newValue <= 0) {
			window.alert("La escala debe ser un número positivo mayor que 500");
		}
	} else {
		window.alert("La escala debe ser un número positivo mayor que 500");
	}
}

/**
 * Establece en la barra de escala los valores correspondientes a la escala de
 * visualización del mapa en este momento
 */
function setScaleBarOL() {
	
	// Cambiar la escala para que no ponga 1:1M (como lo pone OpenLayers) sino 1:1.000.000
	if ( document.getElementById("scaleBox") ==null){
		return;
	}

	ancho = 100000/mapOL.getView().getResolution();
	var coef =Math.max(1,Math.floor(ancho/100));
	while ((ancho/coef < 140)&&(coef>1)){
		coef--;
	}
	
	anchura = ancho/coef;
	
	// ajustar el tamaño de la escala en función de la resolución del mapa
	$("#scaleBar").css('width',anchura);
	$("#scaleBox").css('width',anchura+60);

	// ajustar la posición de las etiquetas de la escala
	$("#scale2").css('left',anchura*0.1+25);
	$("#scale3").css('left',anchura*0.4+25);
	$("#scale4").css('left',anchura*0.7+25);
	$("#scale5").css('left',anchura+25);

	 scale5 = 100/coef;
	 scale_units="Km";
	if (scale5 <10){
		scale_units="m";
		scale5 = scale5*1000;
	}
	
	
	document.getElementById("scale5").innerHTML=Math.round(scale5)+scale_units;
	document.getElementById("scale4").innerHTML=Math.round(scale5*0.7)+scale_units;
	document.getElementById("scale3").innerHTML=Math.round(scale5*0.4)+scale_units;
	document.getElementById("scale2").innerHTML=Math.round(scale5*0.1)+scale_units;
	document.getElementById("scale1").innerHTML=0+scale_units;

	
}

function getPrintScale(printWidth, printHeight,minx,maxx){
	console.log("Deprecated. Usar la definida en print.js");
	var resolucion = (maxx-minx)/printWidth;
	ancho = 100000/resolucion;
	var coef =Math.max(1,Math.floor(ancho/100));
	while ((ancho/coef < 140)&&(coef>1)){
		coef--;
	}
	
	anchura = ancho/coef;
	
	
	 scale5 = 100/coef;
	 scale_units="Km";
	if (scale5 <10){
		scale_units="m";
		scale5 = scale5*1000;
	}
	
	return urlAbsoluta(printScaleURL)+'?width='+printWidth+'&height='+printHeight+'&scaleWidth='+Math.round(anchura)+'&length='+Math.round(scale5)+'&units='+scale_units;

}
function setColorMarcas(newColor) {
	//document.getElementById("textosInferior").style.color = newColor;
jg.setColor(newColor);

jgPoint.setColor(newColor);
jgPointLabel.setColor(newColor);

jgLine.setColor(newColor);
jgLineLabel.setColor(newColor);

jgPol.setColor(newColor);
jgPolLabel.setColor(newColor);

jgNotificar.setColor(newColor);

jgHistorico.setColor(newColor);
jgHistoricoLabel.setColor(newColor);

jgCircle.setColor(newColor);

jgCircleGranjas.setColor(newColor);
}

function hideLoadingWMS(idx){
	loadingWMSId = "wmsLoading" + idx;
	// compruebo si esta visible ese elemento (puede estar dentro de una carpeta plegada)
if (document.getElementById(loadingWMSId)) {
	document.getElementById(loadingWMSId).style.visibility = "hidden";
}
}
function loadedWMS(idx) {
	//alert("loaded" + idx);
	document.getElementById(wmsList[idx]["id"]).style.display = "block";
	hideLoadingWMS(idx);
	
}

function pintaSliderMap(){
var width=30;
var height=15;
if(parseInt($("#zoomSlider .ui-slider-handle").css("width"))>40){
	height = 26;
	width=47;
}
 $("#sliderMap").append('<area href="javascript:zoomTo(\'8\')" coords="0,0,'+width+','+height+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'7\')" coords="0,'+height+','+width+','+height*2+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'6\')" coords="0,'+height*2+','+width+','+height*3+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'5\')" coords="0,'+height*3+','+width+','+height*4+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'4\')" coords="0,'+height*4+','+width+','+height*5+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'3\')" coords="0,'+height*5+','+width+','+height*6+'" shape="rect">'+
 '<area href="javascript:zoomTo(\'2\')" coords="0,'+height*6+','+width+','+height*7+'" shape="rect">'+
  '<area href="javascript:zoomTo(\'1\')" coords="0,'+height*7+','+width+','+height*8+'" shape="rect">'+
   '<area href="javascript:zoomTo(\'0\')" coords="0,'+height*8+','+width+','+height*9+'" shape="rect">');
}


function showGPXDialog(formGeom,type) {

	var dialog= $('<div></div>')
	.html('<div class="ui-widget"><form method="post" id="formGPX" enctype="multipart/form-data" action="/BD_GIS/fileForwarder.jsp"><label>Archivo:</label><input id="fileToUpload" type="file" name="file" /></form><p>En funci&oacute;n del tama&ntilde;o del fichero esta operaci&oacute;n puede ser muy lenta. En ese caso, pruebe con un fichero de menor tama&ntilde;o</div>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: {
			Aceptar: function() {


				var filename = document.getElementById('fileToUpload').value;
				if ( filename && filename.length >0){
					muestraLayerLoading();
					$('#formGPX').ajaxForm({
						beforeSubmit: function(a,f,o) {
						},
						success: function(data) { 

							loadGPX(data,filename,formGeom, type);
						},
						error: function(data) { 
							ocultaLayerLoading();
							alert("Error cargando el fichero");
							if (debugOn) {
								alert(data.responseText); 
							}
						}
					});
					$('#formGPX' ).submit();

					$( this ).dialog( "close" );
					//$( this ).dialog( "destroy" );
				}
				else{
					alert("Debe especificar el archivo");
				}
			}
		},
		width: 600,
		title: 'Carga GPX' ,
		modal: true
	});
}


function addUserLayer2TOC(filePath, layerOL){
	if (!grpCapaUsuario){
	
		grpCapaUsuario = toc.addGroup( new GROUPNV('<b>Capas de usuario</b>',false, null,null, 'metadatos/wms.html') );
	}	
	var layerName = filePath;
	var idx = filePath.lastIndexOf("/");
	if (idx<0){
		idx = filePath.lastIndexOf("\\");
	}
	if (idx>=0){
		layerName = filePath.substr(idx+1);
	}
		grpCapaUsuario.addLayer( new LAYEROL(layerName, layerName, '',null,null,'', layerOL) );
		toc.refresh();
}
function loadGPX(data, layerName) {

	
	if ((typeof mapOL =='undefined')||(mapOL==null)){
		var bounds = new OpenLayers.Bounds(limitLeft-500000, limitBottom-500000, limitRight+500000, limitTop+500000);
		var genericMapOptions = {projection: "EPSG:25830", units: 'm', maxExtent: bounds};
		var mapControls = [ 
		       			new OpenLayers.Control.Navigation()
		       		];

		       		var mapOptions = OpenLayers.Util.extend({
		       				controls: mapControls
		       		}, genericMapOptions);

	mapOL = new OpenLayers.Map('mapOL', mapOptions);
	mapOL.fractionalZoom = true;
	}

	if (typeof OpenLayers !='undefined') {
			// OpenLayers 2
		if ((typeof gpxLayer == 'undefined')||(gpxLayer==null)){
			gpxLayer = new OpenLayers.Layer.Vector(layerName, {
			style: {strokeColor: "#FFbb00", strokeWidth: 5, strokeOpacity: 0.5}
			});
			gpxLayer.isBaseLayer=true;
			mapOL.addLayer(gpxLayer);
		
		}
		gpxLayer.destroyFeatures();
		var format = new OpenLayers.Format.GPX({
			'internalProjection': new OpenLayers.Projection("EPSG:25830"),
			'externalProjection': new OpenLayers.Projection("EPSG:4326")
		});
		var features = format.read(data);
		gpxLayer.addFeatures(features);

		// forzar refresco para que se pinte la capa
		mapOL.zoomToExtent(new OpenLayers.Bounds(minx, miny, maxx, maxy), false);
	} else {
			// OpenLayers 3
		
	// Permito que haya varios GPX cargados y no reuso la capa
		// gpxLayer.getSource().clear() ??
		//gpxLayer.destroyFeatures();

		var format = new ol.format.GPX();
		
		var features = format.readFeatures(data.trim(), {
			featureProjection: new ol.proj.Projection({code:"EPSG:25830"}),
			dataProjection: new ol.proj.Projection({code:"EPSG:4326"})
		});
		var source = new ol.source.Vector();

		var gpxLayer = new ol.layer.Vector({
			source: source
		});
		
	//	gpxLayer.isBaseLayer=true;
		mapOL.addLayer(gpxLayer);
		gpxLayer.getSource().addFeatures(features);
		addUserLayer2TOC(layerName, gpxLayer);
		

	}

	ocultaLayerLoading();
}

function showGMLDialog() {
	
	var dialog= $('<div></div>')
	.html('<div class="ui-widget"><form method="post" id="formGML" enctype="multipart/form-data" action="/BD_GIS/fileForwarder.jsp"><label>Archivo:</label><input id="fileToUpload" type="file" name="file" /></form><p>En funci&oacute;n del tama&ntilde;o del fichero esta operaci&oacute;n puede ser muy lenta. En ese caso, pruebe con un fichero de menor tama&ntilde;o</div>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: {
			Aceptar: function() {
				
			
				var filename = document.getElementById('fileToUpload').value;
				if ( filename && filename.length >0){
					muestraLayerLoading();
				$('#formGML').ajaxForm({
					beforeSubmit: function(a,f,o) {
					},
					success: function(data) { 
						
						loadGML(data,filename);
					},
					error: function(data) { 
						ocultaLayerLoading();
						alert("Error cargando el fichero");
						if (debugOn) {
							alert(data.responseText); 
						}
					}
				});
				$('#formGML' ).submit();
			
				$( this ).dialog( "close" );
				//$( this ).dialog( "destroy" );
				}
				else{
				alert("Debe especificar el archivo");
				}
			}
		},
		width: 600,
		title: 'Carga GML' ,
		modal: true
	});
}
function loadGML(data, layerName) {



		var format = new ol.format.WFS();
		
		var features = format.readFeatures(data.trim());
		var source = new ol.source.Vector();

		var gmlLayer = new ol.layer.Vector({
			source: source
		});
		
	//	gpxLayer.isBaseLayer=true;
		mapOL.addLayer(gmlLayer);
		gmlLayer.getSource().addFeatures(features);


	ocultaLayerLoading();
	addUserLayer2TOC(layerName, gmlLayer);
}			


function showKMLDialog() {
	
	var dialog= $('<div></div>')
	.html('<div class="ui-widget"><form method="post" id="formKML" enctype="multipart/form-data" action="/BD_GIS/fileForwarder.jsp"><label>Archivo:</label><input id="fileToUpload" type="file" name="file" /></form><p>En funci&oacute;n del tama&ntilde;o del fichero esta operaci&oacute;n puede ser muy lenta. En ese caso, pruebe con un fichero de menor tama&ntilde;o</div>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: {
			Aceptar: function() {
				
			
				var filename = document.getElementById('fileToUpload').value;
				if ( filename && filename.length >0){
					muestraLayerLoading();
				$('#formKML').ajaxForm({
					beforeSubmit: function(a,f,o) {
					},
					success: function(data) { 
						
						loadKML(data,filename);
					},
					error: function(data) { 
						ocultaLayerLoading();
						alert("Error cargando el fichero");
						if (debugOn) {
							alert(data.responseText); 
						}
					}
				});
				$('#formKML' ).submit();
			
				$( this ).dialog( "close" );
				//$( this ).dialog( "destroy" );
				}
				else{
				alert("Debe especificar el archivo");
				}
			}
		},
		width: 600,
		title: 'Carga KML' ,
		modal: true
	});
}
function loadKML(data, layerName) {



		var format = new ol.format.KML({
                            extractStyles: false
                          });
		
		var features = format.readFeatures(data.trim(), {
			featureProjection: new ol.proj.Projection({code:"EPSG:25830"}),
			dataProjection: new ol.proj.Projection({code:"EPSG:4326"})
		});
		var source = new ol.source.Vector();

		var kmlLayer = new ol.layer.Vector({
			source: source/*,
			style: [new ol.style.Style({stroke:new ol.style.Stroke({width:5, color:'#FFbb00', opacity: 0.5})})]*/
		});
		
	//	gpxLayer.isBaseLayer=true;
		mapOL.addLayer(kmlLayer);
		kmlLayer.getSource().addFeatures(features);


	ocultaLayerLoading();
		addUserLayer2TOC(layerName, kmlLayer);
}			


function showGeoJSONDialog() {
	
	var dialog= $('<div></div>')
	.html('<div class="ui-widget"><form method="post" id="formGeoJSON" enctype="multipart/form-data" action="/BD_GIS/fileForwarder.jsp"><label>Archivo:</label><input id="fileToUpload" type="file" name="file" /></form><p>En funci&oacute;n del tama&ntilde;o del fichero esta operaci&oacute;n puede ser muy lenta. En ese caso, pruebe con un fichero de menor tama&ntilde;o</div>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: {
			Aceptar: function() {
				
			
				var filename = document.getElementById('fileToUpload').value;
				if ( filename && filename.length >0){
					muestraLayerLoading();
				$('#formGeoJSON').ajaxForm({
					beforeSubmit: function(a,f,o) {
					},
					success: function(data) { 
						
						loadGeoJSON(data,filename);
					},
					error: function(data) { 
						ocultaLayerLoading();
						alert("Error cargando el fichero");
						if (debugOn) {
							alert(data.responseText); 
						}
					}
				});
				$('#formGeoJSON' ).submit();
			
				$( this ).dialog( "close" );
				//$( this ).dialog( "destroy" );
				}
				else{
				alert("Debe especificar el archivo");
				}
			}
		},
		width: 600,
		title: 'Carga GeoJSON' ,
		modal: true
	});
}
function loadGeoJSON(data, layerName) {



		var format = new ol.format.GeoJSON();
		
		var features = format.readFeatures(data.trim());
		var source = new ol.source.Vector();

		var jsonLayer = new ol.layer.Vector({
			source: source
		});
		
	//	gpxLayer.isBaseLayer=true;
		mapOL.addLayer(jsonLayer);
		jsonLayer.getSource().addFeatures(features);


	ocultaLayerLoading();
		addUserLayer2TOC(layerName, jsonLayer);
}			
function resizeMapOL(newW,newH){
	if ((typeof mapOL !='undefined') && (mapOL!=null)){
		$("#mapOL").css('height', heightt);
		$("#mapOL").css('width', widthh);
		mapOL.updateSize();
	}
}
function markersOnTop(){
	var capas = mapOL.getLayers().getArray();
	var idx = mapOL.getLayers().getArray().indexOf(markers);
	if (idx<capas.length-1){
		mapOL.getLayers().removeAt(idx);
		mapOL.getLayers().push(markers);	
	}
	
}
function createWMSLayer(idxWMS){
	//console.log(idxWMS);
	var layerURL =  wmsList[idxWMS]["url"];
	if (layerURL.indexOf("@")>0){
		return createWMSLayerGroup(idxWMS);
		
	}
	var paramsOL = new Array();
	try{
		var wmsURL = layerURL.split('?');
		var params = wmsURL[1].split("&");

		for (var i=0; i<params.length; i++){
			var values = params[i].split("=");
			paramsOL[values[0].toUpperCase()]=values[1];
			// añadir si hubiera más caracteres separados por = (caso de cql_filter)
			for (var j=2; j<values.length; j++){
				paramsOL[values[0].toUpperCase()]=paramsOL[values[0].toUpperCase()]+"="+values[j];
			}
		}
	}catch(err){}

	var	layer =new ol.layer.Image({
		source: new ol.source.ImageWMS({

			url: wmsURL[0],
			params: paramsOL
		})});
	layer.set("idxWMS",idxWMS);
	try{
	if (wmsList[idxWMS]["clase"].startsWith("transp")){
		layer.setOpacity(wmsList[idxWMS]["clase"].trim().replace("transp","")/100);
	}
	}
	catch(err){
		console.log("No se ha podido establecer la transparencia para idxWMS="+idxWMS+". "+err);
	}
	layer.setVisible(wmsList[idxWMS]["visib"]);
	return layer;

}

function createWMSLayerGroup(idxWMS){
	var layerGroup = new ol.layer.Group({ });
	var layerURL =  wmsList[idxWMS]["url"].split("@");

	for (var k=0; k<layerURL.length;k++){
		var paramsOL = new Array();
		try{
			var wmsURL = layerURL[k].split('?');
			var params = wmsURL[1].split("&");

			for (var i=0; i<params.length; i++){
				var values = params[i].split("=");
				paramsOL[values[0].toUpperCase()]=values[1];
				// añadir si hubiera más caracteres separados por = (caso de cql_filter)
				for (var j=2; j<values.length; j++){
					paramsOL[values[0].toUpperCase()]=paramsOL[values[0].toUpperCase()]+"="+values[j];
				}
			}
		}catch(err){}

		var	layer =new ol.layer.Image({
			source: new ol.source.ImageWMS({

				url: wmsURL[0],
				params: paramsOL
			})});
		var layers = layerGroup.getLayers();
		layers.push(layer);
		layerGroup.setLayers(layers);

	}
	layerGroup.set("idxWMS",idxWMS);
	try{
		if (wmsList[idxWMS]["clase"].startsWith("transp")){
			layerGroup.setOpacity(wmsList[idxWMS]["clase"].trim().replace("transp","")/100);
		}
	}
	catch(err){
		console.log("No se ha podido establecer la transparencia para idxWMS="+idxWMS+". "+err);
	}
	layerGroup.setVisible(wmsList[idxWMS]["visib"]);
	return layerGroup;

}
function addOLLayer(layer,zIndex){
	if (!creadoMapOL){
		createMapOL();
	}
	var layers = mapOL.getLayers();
	if (layers.getLength()<=0){
		mapOL.addLayer(layer);
	}
	// añadir en la posición correspondiente a su zIdx
	var added=false;
	for (var i=0; i< layers.getLength();i++){
		try{
			var zIdx = wmsList[layers.item(i).get("idxWMS")]["zIdx"];
			if (zIdx>=zIndex){
				layers.insertAt(i, layer);
				added=true;
				break;
			}
		}
		catch(err){
			//console.log(err);
		}
	}
	if (!added){
		mapOL.addLayer(layer);
	}
}