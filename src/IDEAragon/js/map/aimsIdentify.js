var idMinX, idMinY, idMaxX, idMaxY;
var idCount = 0;
var selectedId = 1;

var currentQueriedGFI = 0;

// donde ha hecho click el usuario
xUsr = -1;
yUsr = -1;
xCoordUsr = "";
yCoordUsr = "";

urlOVC = "";

altitudCalculada = false;
laCota = '';

var resultOVC = -1;
var boaCalculado = false;
var boas='';
var capasBOA;
var codigosBOA;
//var countResultQueryableLayer = new Array();

var askedI = -1;
var askedJ = -1;
var askedK = -1;

function checkScale(actLayer) {
	currentScale = getMapScale();
	if ((minScale[actLayer] < currentScale) && (currentScale > maxScale[actLayer])) {
		return false;
	} else {
		return true;
	}
}

var isDownloading = false;
function descarga(x1, y1) {
	resetResultadosAnterioresGFI();

	xUsr = x1;
	yUsr = y1;

	var coordUsr = convertPixelToMap(xUsr, yUsr);
	xCoordUsr = coordUsr[0];
	yCoordUsr = coordUsr[1];
	//muestraLayerLoading();
	registerQuery(xCoordUsr, yCoordUsr);
	filtrosBusqueda="desca.descargas!='' and";

	/*
	askedI = 0;
	askedJ = 0;
	askedK = 1;
	isDownloading = true;

	if (capasEstanListas(askedI, askedJ, askedK)) {
		updateAcetateMarca(findXCoordUsr, findYCoordUsr, xCoordUsr, yCoordUsr);

		var objDcha = document.getElementById('detailedResultsTable');
		if (! objDcha) {
			updateResults();
		}
		$("#tabs").tabs( "enable", 2);
		$("#tabs").tabs("option", "active", 2);
		idContent = getDetailedResultHTML(i, j, k);
		document.getElementById('detailedResultsTable').innerHTML = idContent;
		highlightOpcion(i, j, k);

		ocultaLayerLoading();
	}*/
	identifyProductos(xCoordUsr, yCoordUsr, "muestraResultadosDescarga(idContent,'infoTab');");
}

function muestraResultadosDescarga(idContent,resultadosIdZone){
	document.getElementById(resultadosIdZone).innerHTML = idContent;
	$("#tabs").tabs( "enable", 2);
	$("#tabs").tabs("option", "active", 2);
}

function identifyOVC(x1, y1) {
	urlOVC = 'http:/' + '/ovc.catastro.meh.es/Cartografia/WMS/ServidorWMS.aspx?SRS=EPSG:25830&Query_Layers=Catastro&service=WMS&request=GetFeatureInfo&version=1.1.1&WIDTH=' + mwidth + '&HEIGHT=' + mheight;
	urlOVC += '&INFO_FORMAT=text/html&EXCEPTIONS=application/vnd.ogc.se_xml&FEATURE_COUNT=5';
	urlOVC += '&BBOX=' + minx + ',' + miny + ',' + maxx + ',' + maxy;
	urlOVC += '&X=' + x1 + '&Y=' + y1;
	window.open(urlOVC);
}

function resetConsultaIdentify() {
	theHiddenURL = new Array();
	theHiddenURLGeneral = "";

	selectedId = 1;

	currentQueriedGFI = 0;
	resetResultadosAnteriores();
	resetResultadosAnterioresGFI();

	altitudCalculada = false;
	resultOVC = -1;
	boaCalculado = false;
	if (detailedResultsWin) {
		detailedResultsWin.hide();
	}
	if (resultsWin) {
		resultsWin.hide();
	}


	xUsr = -1;
	yUsr = -1;
	xCoordUsr = "";
	yCoordUsr = "";
}

function identify(x1, y1) {
	resetConsultaIdentify();

	xUsr = x1;
	yUsr = y1;

	var coordUsr = convertPixelToMap(xUsr, yUsr);
	xCoordUsr = coordUsr[0];
	yCoordUsr = coordUsr[1];

	registerQuery(xCoordUsr, yCoordUsr);
	if (toleranceInPixels) {
		// tolerancia por pixeles
		var idMins = convertPixelToMap(x1-(pixelTolerance/2), y1+(pixelTolerance/2));
		var idMaxs = convertPixelToMap(x1+(pixelTolerance/2), y1-(pixelTolerance/2));
		idMinX = idMins[0];
		idMinY = idMins[1];
		idMaxX = idMaxs[0];
		idMaxY = idMaxs[1];
	} else {
		// tolerancia por metros
		idMinX = xCoordUsr - toleranceMeters;
		idMinY = yCoordUsr - toleranceMeters;
		idMaxX = xCoordUsr + (toleranceMeters*1);
		idMaxY = yCoordUsr + (toleranceMeters*1);
	}

	xUsr = Math.floor(xUsr);
	yUsr = Math.floor(yUsr);

	muestraLayerLoading();

	calculateInitialQueries();
}

function registerQuery(x,y){
	//alert("type="+type+"&app="+APP_NAME+"&objectid="+objectid+"&literal="+literal);
	$.ajax({
		url: REGISTRA_QUERY_JSP+"?app="+APP_NAME+"&x="+x+"&y="+y,
		type: 'GET'//,
			//async:false,
			//	success: function(data){alert("registrado "+REGISTRA_ELECCION_JSP+"?type="+type+"&app="+APP_NAME+"&objectid="+objectid+"&literal="+literal);},
			//	error: function(jqXHR, textStatus, errorThrown) { alert("Error alregistrar "+textStatus +"\n "+errorThrown);}
	});
}

function calculateInitialQueries() {
	if ((resultOVC == -1) && (needCalculateOVC)) {
		calculaRefCatOVC();
		return;
	}

	if ((! altitudCalculada) && (needCalculateAltitud)) {
		if (debugOn) {
			alert("calcular altitud");
		}
		calculaAltitudXY(xCoordUsr, yCoordUsr, true);
		return;
	}
	if ((! boaCalculado) && (typeof needCalculateBOA!='undefined')&&(needCalculateBOA)) {
		if (debugOn) {
			alert("calcular BOA");
		}
		
			if (showBoaLegalText()){
				
		var fecha = $("#boaDateLabel").text();
		calculaBOA(xCoordUsr, yCoordUsr, getGeoBOADate(fecha.substr(0,2),fecha.substr(3,2),fecha.substr(6,4)));
		
		return;	
		}

	}
	/*
	if (currentQueriedLayer == "9999") {
		if (queryableLayer[9999]["camposRespuesta"]["count"]>0) {
			refCatUEstaCalculada();
			return;
		} else {
			currentQueriedLayer++;
			ActiveLayer = "CatR";
			performVectorialQuery();
			return;
		}
	}
	if (currentQueriedLayer == "10000") {
		refCatREstaCalculada();
		return;
	}*/
	getDetailedResult(0,0,0);
	
	
}

function getBOAFeatures(capa, campo,capas,codigos){
	var data = '<wfs:GetFeature '+
	' xmlns:wfs="http://www.opengis.net/wfs"'+
	' service="WFS"'+
	' version="1.0.0"'+
	' outputFormat="json"'+
	' xsi:schemaLocation="http://www.opengis.net/wfs'+
	' http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"'+
	' xmlns:gml="http://www.opengis.net/gml"'+
	' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+
	'<wfs:Query typeName="'+capa+'" srsName="EPSG:25830">'+
	'<wfs:PropertyName>'+campo+'</wfs:PropertyName>'+
	' <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">'+
	'<ogc:Intersects>'+
	'<ogc:PropertyName>shape</ogc:PropertyName>'+
	'<gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="EPSG:25830">'+
	'  <gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>'+idMinX+','+ idMinY+' '+idMinX+','+ idMaxY+' '+idMaxX+','+ idMaxY+' '+idMaxX+','+ idMinY+' '+idMinX+','+ idMinY+
	'</gml:coordinates>'+
	'      </gml:LinearRing>'+
	'     </gml:outerBoundaryIs>'+
	'    </gml:Polygon>'+
	'   </ogc:Intersects>'+

	'  </ogc:Filter>'+
	' </wfs:Query>'+
	'</wfs:GetFeature>';
	$.ajax({
		url: urlWFS_Visor2D,
		type: 'POST',
		contentType: "text/xml",
		dataType:"text",
		async:false,
		data: data,
		timeout:15000,
		success:	function(data) {


			if (debugOn) {
				alert('Respuesta WFS: ' + data);
			}
			var response = $.parseJSON(data);

			for (var i = 0; i < response.features.length; i++) {
				capas.push(capa);
				codigos.push(response.features[i].properties[campo]);
			}
		},
		error: 		function(data, textStatus, errorThrown) {
			//sinRespuestaWFS();
			
				console.log('Error obteniendo features. State: ' + data.readyState + ", Status:" + data.status);
			
		}
	});

}
function showGeoBOADoc(docn){
	
	$.ajax({
		url: "/BD_GIS/getGeoBOA.jsp?docn="+docn,
		type: 'GET',
		success:  function(datos){

			var json = $.xmlToJSON(datos);

		

			if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){
				return;
			}
			
			for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
				
				var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
				var resultados ="";
				if ((busquedaLine.Count[0].Text!="0")&&(busquedaLine.Count[0].Text!="-1")){
					hayResultados=true;
					if(typeof busquedaLine.List != 'undefined'){
						resultados = busquedaLine.List[0].Text.split("\n");
						for (var i=0; i<resultados.length; i++){
							var resultado = resultados[i].split("#");
							showSearchResultOL("ACTIVELAYER="+resultado[2]+"&QUERY="+getBOAFilter(resultado[2], resultado[1]));
						}

					}
				}
			}
		},

		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			alert("No se ha podido cargar el BOA con docn "+docn+" en el mapa "+textStatus +"\n "+errorThrown);
		}


	});	
	

}
function updFechaBOAI(){
	
	var fecha = $( "#boaDateI" ).datepicker( "getDate" );;
	$("#boaList").empty();
	showSearching(true);
	boas="";
	recalculaBOA(fecha);
	$("#boaList").append(boas);
	showSearching(false);
}
function changeBoaDateI(evt){
	if (isEnterKey(evt)){
		updFechaBOAI();
		
	}
}
function onInputDateI(evt){
	evt.stopPropagation();
	$("#boaDateTextI").css("display","none");
	var fecha = document.getElementById("boaDateLabelI").innerHTML.split("/");
	var fechaDate = getGeoBOADate(fecha[0],fecha[1],fecha[2]);
	$( "#boaDateI" ).datepicker( "setDate", fechaDate );
	
	$("#boaDateEditorI").css("display","inline-block");
	document.getElementById("boaDateI").focus();
}
function recalculaBOA(fecha){
	
	if (capasBOA.length>0){
		$.ajax({
			url: "/BD_GIS/getGeoBOA.jsp?filtro=false",
			type: 'POST',
			 data:jQuery.param( { fecha: formatGeoBOADate(fecha), capas : capasBOA.toString(), codigos : codigosBOA.toString()}) ,
			 
			    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			async:false,
			success:  function(datos){

				var json = $.xmlToJSON(datos);

				if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){
					return;
				}
				
				for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
					
					var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
					var resultados ="";
					if ((busquedaLine.Count[0].Text!="0")&&(busquedaLine.Count[0].Text!="-1")){
						hayResultados=true;
						if(typeof busquedaLine.List != 'undefined'){
							resultados = busquedaLine.List[0].Text.split("\n");
							for (var i=0; i<resultados.length; i++){
								
								var resultado = resultados[i].split("#");
								var url=urlBOA.replace("<<docn>>",resultado[2]).replace("<<tipo>>",resultado[3]);
								boas+='<li><a target="_blank" href="'+url+'">'+resultado[4]+' ('+resultado[0]+') </a><a href=javascript:showGeoBOADoc("'+resultado[2]+'")><img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="Ver en mapa"></a></li>';
							}

						}
					}
				}
			
			},

			error:  function(  jqXHR,  textStatus,  errorThrown) { 

				console.log("No se ha podido cargar el BOA del día "+fecha+" en el mapa "+textStatus +"\n "+errorThrown);
				
			}


		});	
	}
}
function calculaBOA(theX, theY,fecha) {
	 capasBOA=new Array();
	codigosBOA=new Array();
	getBOAFeatures("BusMun", "c_muni_ine",capasBOA,codigosBOA);
	getBOAFeatures("Comarca", "c_comarca",capasBOA,codigosBOA);
	getBOAFeatures("BusSIGPAC", "refpar",capasBOA,codigosBOA);
	 boas='<fieldset><legend><b>Referencias del BOA del</b> <span id="boaDateTextI" onmousedown="onInputDateI(event)" style="cursor: pointer;"><label id="boaDateLabelI">'+$("#boaDateLabel").text()+'</label><img id="boaDateEditI" class="imgToc" src="/lib/IDEAragon/themes/default/images/editar.png" width="16" height="16" onmousedown="onInputDateI(event);" alt="Pulse para cambiar la fecha" title="Pulse para cambiar la fecha" border="0"></span><span id="boaDateEditorI" style="display:none;"><input id="boaDateI" name="boaDateI" onmousedown="stopPropagation(event);" onkeypress="changeBoaDateI(event)" type="text"><a class="textoInferior" style="color:#0079B1!important;margin-left: 5px;" onmousedown="stopPropagation(event);" href="javascript:updFechaBOAI()" title="Actualiza la fecha"><b>&gt;</b></a></span></legend>';
	boas+='<ul id="boaList">';
	recalculaBOA(fecha);
	boas+='</ul></fieldset>';
	boaCalculado = true;
	calculateInitialQueries();
	//}
}
function calculaAltitudXY() {

		$.ajax({
			url: urlWCTS+"?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS=3,4,5,6,7,8,9,10,11&STYLES=&INFO_FORMAT=application/geojson&CRS=EPSG:25830&BBOX="+ minx + ',' + miny + ',' + maxx + ',' + maxy+'&WIDTH=' + mwidth +'&HEIGHT=' + mheight+ "&QUERY_LAYERS=3,4,5,6,7,8,9,10,11&X=" + xUsr + '&Y=' + yUsr,
			
			type: 'GET',
			
			success:	function(data) {
				var features = data.features;
				var altitud5,altitud25,orientacion5,orientacion25, pteGrados5, pteGrados25,ptePorcentaje5,ptePorcentaje25,ptebaja5;
				for (var i=0; i<features.length;i++){
					if (features[i].layerName=="10"){
						altitud25=features[i].properties.PixelValue;
					}
					else if (features[i].layerName=="11"){
						altitud5=features[i].properties.PixelValue;
					}
					else if (features[i].layerName=="3"){
						ptePorcentaje25=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="4"){
						ptePorcentaje5=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="5"){
						ptebaja5=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="6"){
						pteGrados25=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="7"){
						pteGrados5=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="8"){
						orientacion25=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
					else if (features[i].layerName=="9"){
						orientacion5=parseFloat(features[i].properties.PixelValue).toFixed(2);
					}
				}
				laCota="<ul id='relieve'>";
				laCota+="<li>Altitud (m): "+altitud25+" / "+altitud5+"</li>";
				laCota+="<li>Orientaci&oacute;n (Grados): "+orientacion25+" / "+orientacion5+"</li>";
				laCota+="<li>Pendientes (Grados): "+pteGrados25+" / "+pteGrados5+"</li>";
				laCota+="<li>Pendientes (%): "+ptePorcentaje25+" / "+ptePorcentaje5+"</li>";
				laCota+="<li>Pendientes <15% (Agricultura): "+ptebaja5+"</li>";
				laCota+="</ul>";
				
				altitudCalculada = true;
				calculateInitialQueries();
			},
			error: 		function(data, textStatus, errorThrown) {
				//sinRespuestaWFS();
				laCota="No disponible";
				altitudCalculada = true;
					console.log('Error obteniendo la altitud. State: ' + data.readyState + ", Status:" + data.status);
					calculateInitialQueries();
			}
		});
		
	

	//}
}

function sinRespuestaGFI_OVC() {
	resultOVC = "El servicio no est&aacute; operativo en estos momentos.";
	//doNextQuery();
	calculateInitialQueries();
	//drawDetailedResult(askedI,askedJ,askedK);
}

function calculaRefCatOVC() {
	queryURL_OVC = urlGFI_OVC;
	queryURL_OVC += '&BBOX=' + minx + ',' + miny + ',' + maxx + ',' + maxy;
	queryURL_OVC += '&WIDTH=' + mwidth +'&HEIGHT=' + mheight + '&X=' + xUsr + '&Y=' + yUsr;

	if (debugOn) {
		window.open(queryURL_OVC);
	}

	http = getHTTPObject();
	if ((http != null)) {
		http.open("GET", queryURL_OVC);
		http.onreadystatechange = parseResultOVC;
		isWorking = true;

		t_timeout = setTimeout("sinRespuestaGFI_OVC()", 8000);

		http.send(null);
	}
}

function parseResultOVC() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			processResultOVC(result);

			//doNextQuery();
			calculateInitialQueries();
			//drawDetailedResult(askedI,askedJ,askedK);
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI_OVC();
		}
	}
}

function processResultOVC(theReply) {
	gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
	gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = 0;

	searchStr = '<a';
	startpos = theReply.indexOf(searchStr, 1);

	endpos = theReply.indexOf("</a>", startpos);
	if ((startpos != -1) && (endpos != -1)) {
		resultOVCAux = theReply.substring(startpos+2, endpos+4);
		resultOVC = "<a class='itemEnlace normal' target='_blank' " + resultOVCAux;
	} else {
		resultOVC = "No disponible";
	}
}

function processResultOVC_customGeorref(theReply) {
	searchStr = '<a';
	startpos = theReply.indexOf(searchStr, 1);
	aux = theReply.substring(startpos+2);
	startpos2 = aux.indexOf(">", 1);

	endpos = aux.indexOf("</a>", startpos2);
	if ((startpos != -1) && (startpos2 != -1) && (endpos != -1)) {
		resultOVCAux = aux.substring(startpos2+1, endpos);
		resultOVC = resultOVCAux;
	} else {
		resultOVC = "No disponible";
	}
}

function calculaRefCatOVC_customGeorref(x1, y1) {
	xUsr = x1;
	yUsr = y1;

	var coordUsr = convertPixelToMap(xUsr, yUsr);
	xCoordUsr = coordUsr[0];
	yCoordUsr = coordUsr[1];

	queryURL_OVC = urlGFI_OVC;
	queryURL_OVC += '&BBOX=' + minx + ',' + miny + ',' + maxx + ',' + maxy;
	queryURL_OVC += '&WIDTH=' + mwidth +'&HEIGHT=' + mheight + '&X=' + xUsr + '&Y=' + yUsr;

	if (debugOn) {
		window.open(queryURL_OVC);
	}

	http = getHTTPObject();
	if ((http != null)) {
		http.open("GET", queryURL_OVC);
		http.onreadystatechange = parseResultOVC_customGeorref;
		isWorking = true;

		t_timeout = setTimeout("sinRespuestaGFI_OVC_customGeorref()", 8000);

		http.send(null);
	}
}

function parseResultOVC_customGeorref() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			//alert("RESULTADO:" + result);
			processResultOVC_customGeorref(result);

			writeRefCat_customGeorref();
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI_OVC_customGeorref();
		}
	}
}

function writeRefCat_customGeorref() {
	if (resultOVC != "No disponible") {
		writePersistentCookie("refcat", resultOVC, "months", 1);
		writePersistentCookie("parcela", "", "months", 1);
		writePersistentCookie("poligono", "", "months", 1);
		writePersistentCookie("coorX", xCoordUsr, "months", 1);
		writePersistentCookie("coorY", yCoordUsr, "months", 1);

		top.close();
	} else {
		alert("Error obteniendo la referencia catastral del servicio web de la Sede Electrónica de Catastro. Por favor, inténtelo más tarde.");
	}
}

function sinRespuestaGFI_OVC_customGeorref() {
	resultOVC = "No disponible";
	writeRefCat_customGeorref();
}

function getIdRequest(countOnly, beginRecord) {
	var axl = '<?xml version="1.0"?>';
	var beginRecordNum = 0;
	if (beginRecord) {
		beginRecordNum = beginRecord;
	}

	axl += '<ARCXML version="1.1">\n<REQUEST>\n<GET_FEATURES geometry="false" outputmode="xml" checkesc ="true" ';
	if(countOnly) axl += 'envelope="false" skipfeatures="true">\n';
	else axl += 'envelope="true" skipfeatures="false" beginrecord="' + beginRecordNum + '" featurelimit="99">\n';
	axl += '<LAYER id="' + ActiveLayer + '" />';
	axl += '<SPATIALQUERY subfields="' + getFields(ActiveLayer)+ '" where="' + getWhereQuery(ActiveLayer) + '">';

	axl += '<SPATIALFILTER relation="area_intersection" >';
	axl += '<ENVELOPE maxy="' + idMaxY + '" maxx="' + idMaxX + '" miny="' + idMinY + '" minx="' + idMinX + '" />';
	axl += '</SPATIALFILTER>';
	axl += '</SPATIALQUERY>';
	axl += '</GET_FEATURES>';
	axl += '</REQUEST>';
	axl += '</ARCXML>';
	return axl;
}

function writeRasterInfoXML(theLayer,theX, theY, theCoordSys) {
	var theString = '<?xml version="1.0"?>';
	theString += '<ARCXML version="1.1">\n<REQUEST>\n';
	theString += '<GET_RASTER_INFO x="' + theX + '" y="' + theY + '" layerid="' + theLayer + '" >';
	theString += '<COORDSYS id="' + theCoordSys + '" />';
	theString += '</GET_RASTER_INFO>';
	theString += '</REQUEST>';
	theString += '</ARCXML>';
	return theString;
}

function genericParseResult(callFunc) {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			if (debugOn) {
				alert(result);
			}
			eval(callFunc);
		} else {
			alert("Error de ejecución");
		}
	}
}

/*
function parseResultForCount() {
	genericParseResult("idCount = getCount(result);countResultQueryableLayer[currentQueriedLayer] = idCount;doNextQuery();");
}*/

function parseResultMuestraDatos() {
	genericParseResult("processResult(result);drawDetailedResult(askedI,askedJ,askedK);");
}

function parseResult() {
	genericParseResult("processResult(result);doNextQuery();");
}

function parseResultAltitud_mustContinue() {
	genericParseResult("processResultAltitud(result);doNextQuery();");
}

function parseResultAltitud_noContinue() {
	genericParseResult("processResultAltitud(result);altitudEstaCalculada();");
}

function parseResultAltitud() {
	genericParseResult("processResultAltitud(result);altitudEstaCalculada();");
}

function processResultAltitud(theReply) {
	searchStr = '<BAND number="0" value="';
	pos = theReply.indexOf(searchStr, 1);

	startpos = pos + searchStr.length;
	// truncar los decimales, que pueden venir con coma o punto de separador
	endpos =theReply.indexOf(".", startpos);
	if (endpos == -1) {
		endpos =theReply.indexOf(",", startpos);
	}
	if (endpos == -1) {
		endpos =theReply.indexOf("\"", startpos);
	}
	if ((pos != -1) && (endpos != -1)) {
		laCota = theReply.substring(startpos, endpos);
	}
	altitudCalculada = true;
}

/*
function getCount(theReply) {
	var theCount;
	var startpos = 0;
	var endpos = 0;
	var pos = theReply.indexOf("FEATURECOUNT");
	var pos1 = theReply.indexOf("count", pos + 12);
	if (pos1 != -1) {
		pos1 += 7;
		var pos2 = theReply.indexOf("\"", pos1);
		theCount = parseFloat(theReply.substring(pos1, pos2));
	} else {
		theCount=0;
	}
	return theCount;
}
 */

function processResult(theReply) {
	/*
	//TODO : Use XML DOM parser instead to parse the response

	var selectedData = "";
	var endpos = 1;
	reply = theReply;

	queryableLayer[currentQueriedLayer]["camposRespuesta"]["count"] = 0;
	queryableLayer[currentQueriedLayer]["valoresRespuesta"]["count"] = 0;

	resultNum = 0;
	var pos = reply.indexOf("<FIELDS ",endpos);
	while (pos!=-1) {
		startpos = pos + 8;
		endpos = reply.indexOf('" />', startpos);
		selectedData = reply.substring(startpos, endpos);

		if(selectedData != "") {
			var fValue1 = getFieldValues(selectedData);
			var fName1 = getFieldNames(selectedData);

			queryableLayer[currentQueriedLayer]["camposRespuesta"][resultNum] = fName1;
			queryableLayer[currentQueriedLayer]["valoresRespuesta"][resultNum] = fValue1;

			queryableLayer[currentQueriedLayer]["camposRespuesta"]["count"]++;
			queryableLayer[currentQueriedLayer]["valoresRespuesta"]["count"]++;

			resultNum++;
		}

		reply = reply.substring(endpos);
		pos = reply.indexOf("<FIELDS ", 1);
	}*/
}

function activarCapaConcreta(idx, objId) {
	layerID = queryableLayer[idx]["ID"];

	//if (toc.findItemByCaption(layerName)) {
	var lay = toc.findItemByAxlID(layerID);

	if (! lay) {
		var layerName = queryableLayer[idx]["NAME"];
		lay = toc.findItemByCaption(layerName);
	}

	if (lay) {
		if (! lay.getVisible()) {
			// como ya era visible no hago nada
			lay.setVisible(true);
			toc.refresh();
		}
	} else {
		if (debugOn) {
			alert("la capa " + layerID + " no se encuentra definida");
		}
	}
	idSelectionLayer = layerID;
	selectionWhere = nombreCampoId +'=' + objId;
	selectionType = LayerTypeById[idSelectionLayer.toUpperCase()];
	$("#tabs").tabs("option", "active", 0);
	getMapWithCurrentExtent();
}

function calculateAltitud(x1, y1) {
	xUsr = x1;
	yUsr = y1;

	var coordUsr = convertPixelToMap(xUsr, yUsr);
	xCoordUsr = coordUsr[0];
	yCoordUsr = coordUsr[1];

	calculaAltitudXY(xCoordUsr, yCoordUsr, false);
	writePersistentCookie("coordenadas", RoundDecimal(xCoordUsr, 2) + "," + RoundDecimal(yCoordUsr, 2) + "," + "ee", "months", 1);
}


function altitudEstaCalculada() {
	if (customToolEditarType == "GEOREF1") {
		writePersistentCookie("coordenadas", RoundDecimal(xCoordUsr, 2) + "," + RoundDecimal(yCoordUsr, 2) + "," + laCota, "months", 1);
		top.close();
	}

	calculateInitialQueries();
	//document.cookie = "coordenadas=" + theX + "_" + theY + "; domain=aragon.es; expires=Thu, 11-May-06 00:00:01 GMT";
//	document.cookie = "coordenadas=" + theX + "_" + theY + "; domain=aragon.es; path=/;";
}

function refCatUEstaCalculada() {
	var idx = 9999;

	if (queryableLayer[idx]["camposRespuesta"]["count"]>0) {
		// si hace click en varias parcelas, cojo la primera
		var kk = 0;

		fName1 = queryableLayer[idx]["camposRespuesta"][kk];
		fValue1 = queryableLayer[idx]["valoresRespuesta"][kk];

		var indexRefCat = -1;
		var indexParcela = -1;
		var indexCoorX = -1;
		var indexCoorY = -1;

		for (var ii=0;ii<fName1.length;ii++) {
			tempString = fName1[ii];
			if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
				nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
			} else {
				nombreCampo = tempString;
			}
			if (nombreCampo == "REFPAR") {
				indexRefCat = ii;
			}
			if (nombreCampo == "PARCELA") {
				indexParcela = ii;
			}
			if (nombreCampo == "COORX") {
				indexCoorX = ii;
			}
			if (nombreCampo == "COORY") {
				indexCoorY = ii;
			}
		}
		writePersistentCookie("refcat", fValue1[indexRefCat], "months", 1);
		writePersistentCookie("parcela", fValue1[indexParcela], "months", 1);
		writePersistentCookie("poligono", "0000", "months", 1);
		writePersistentCookie("coorX", fValue1[indexCoorX], "months", 1);
		writePersistentCookie("coorY", fValue1[indexCoorY], "months", 1);

		top.close();
	}
}

function refCatREstaCalculada() {
	var idx = 10000;

	if (queryableLayer[idx]["camposRespuesta"]["count"]>0) {
		// si hace click en varias parcelas, cojo la primera
		var kk = 0;

		fName1 = queryableLayer[idx]["camposRespuesta"][kk];
		fValue1 = queryableLayer[idx]["valoresRespuesta"][kk];

		var indexRefCat = -1;
		var indexParcela = -1;
		var indexPoligono = -1;
		var indexCoorX = -1;
		var indexCoorY = -1;

		for (var ii=0;ii<fName1.length;ii++) {
			tempString = fName1[ii];
			if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
				nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
			} else {
				nombreCampo = tempString;
			}
			if (nombreCampo == "REFCAT") {
				indexRefCat = ii;
			}
			if (nombreCampo == "PARCELA") {
				indexParcela = ii;
			}
			if (nombreCampo == "SUBPARCE") {
				indexPoligono = ii;
			}
			if (nombreCampo == "COORX") {
				indexCoorX = ii;
			}
			if (nombreCampo == "COORY") {
				indexCoorY = ii;
			}
		}
		writePersistentCookie("refcat", fValue1[indexRefCat], "months", 1);
		writePersistentCookie("parcela", fValue1[indexParcela], "months", 1);
		writePersistentCookie("poligono", fValue1[indexPoligono], "months", 1);
		writePersistentCookie("coorX", fValue1[indexCoorX], "months", 1);
		writePersistentCookie("coorY", fValue1[indexCoorY], "months", 1);

		top.close();
	} else {
		// ni rustica ni urbana
		writePersistentCookie("refcat", "", "months", 1);
		top.close();
	}
}

//get a list of field names from the returned record
function getFieldNames(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fName1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		fName1[f] = v[0];
	}
	return fName1;

}

//get a list field values from the returned record
function getFieldValues(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fValue1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		if ((v[1]=="") || (v[1]==null)) v[1] = "&nbsp;";
		fValue1[f] = v[1];
	}
	return fValue1;
}

function getWhereQuery(idCapa) {
	if (whereQuery[idCapa]) {
		return whereQuery[idCapa];
	} else {
		return '';
	}
}

function getFields(idCapa) {
	selectFields = '';
	if (selFieldList[idCapa]) {
		selectFields=selFieldList[idCapa];
	} else {
		selectFields = "#ALL#";
	}
	return selectFields;
}

function resetResultadosAnteriores() {
	for (var jj = 0; jj < queryableLayerCount; jj++) {
		queryableLayer[jj]["camposRespuesta"] = new Array();
		queryableLayer[jj]["camposRespuesta"]["count"] = -1;
		queryableLayer[jj]["valoresRespuesta"] = new Array();
		queryableLayer[jj]["valoresRespuesta"]["count"] = -1;

		//countResultQueryableLayer[jj] = -1;
	}
}

function convertPixelToMap(px, py) {
	var dx = (maxx - minx)/mwidth;
	var mx = minx + (dx * px);
	var my = miny + (dx * (mheight - py));
	var newpoint = new Array();
	newpoint[0] = mx;
	newpoint[1] = my;
	return newpoint;
}

function convertCoordToPixel(px, py) {
	var dx = (maxx - minx)/mwidth;
	var mx = (px - minx)/(dx);
	var my = (maxy - py)/(dx);
	var newpoint = new Array();
	newpoint[0] = mx;
	newpoint[1] = my;
	return newpoint;
}
/*
function updateResultsGFI_SITAR() {
	var idContent = getResultsGFIHTML_sitar();

	document.getElementById('resultadosIdZone').style.top = toppResults + "px";
	document.getElementById('resultadosIdZone').style.left = lefttResults + "px";
	//document.getElementById('resultadosIdZone').style.border = "#000000 solid 2px";
	document.getElementById('resultadosIdZone').innerHTML = idContent;

	//hideFotograma();

	//hideTextoAyuda();

	showResults();
}
 */

