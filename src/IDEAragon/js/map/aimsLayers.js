var LayerName = new Array();
var LayerID = new Array();
var LayerVisible = new Array();
var EditingLayer;
var LayerType = new Array();
var LayerTypeById = new Array();
var ActiveLayerIndex = 0;
var ActiveLayer;
var layerCount;

function getLayers(/*theReply*/) {
	//TODO : use XML DOM parser instead to parse the response

	//alert("LayerInfo:\n" + theReply.length);
	//var theReplyUC = theReply.toUpperCase();
	var startpos = 0;
	var endpos = 0;
	var pos = -1;
	var lpos = 1;
	var epos = 1;
	var zpos=1;
	var zpos2 = 1;
	var tempString="";
	var visString = "";
	var typeString="";
	var fieldString = "";
	var testString = "";
	var testString2 = "";
	var minString = "";
	var maxString = "";
	var dQuote='"';

	getInitialVisibilityLayers();
	layerCount = 0;
	LayerName.length=1;
	LayerVisible.length=1;
	LayerID.length=1;
	LayerType.length=1;
	//alert("Processing LayerInfo");
	/*lpos = theReplyUC.indexOf("<LAYERINFO",zpos);

	while (lpos > -1) {
		//alert("<LAYERINFO - pos " + lpos );

		if (lpos != -1) {

			zpos = theReplyUC.indexOf("</LAYERINFO",lpos);
			if (zpos == -1) {
					// esto pasa en las acetate, porque no tiene tag de cierre sino que es el mismo de apertura
				zpos = theReplyUC.indexOf("/>",lpos);
			}	
			//alert("</LAYERINFO - pos " + zpos);
			if (zpos!=-1) {
				pos = theReplyUC.indexOf("NAME=",lpos);
				if (pos != -1) {
					startpos = pos + 6;
					endpos = theReply.indexOf(dQuote, startpos);
					tempString = theReply.substring(startpos,endpos);
					tempString = tempString.replace(/&apos;/g, "'");
					LayerName[layerCount] = tempString;
					startpos = theReplyUC.indexOf("VISIBLE=",lpos);
					if (startpos != -1) {
						startpos = startpos + 9;
						endpos = startpos + 4;
						visString = theReply.substring(startpos,endpos);
					}
					startpos = theReplyUC.indexOf("ID=",lpos);
					if ((startpos != -1) && (startpos<zpos)) {
						startpos = startpos + 4;
						endpos = theReply.indexOf(dQuote, startpos);
						tempString = theReply.substring(startpos,endpos);
						tempString = tempString.replace(/&apos;/g, "'");
						LayerID[layerCount] = tempString;
					} else {
						LayerID[layerCount] = LayerName[layerCount];
					}
					//alert(LayerID[layerCount]);

					if (visString=="true") {
						LayerVisible[layerCount] = 1;

					} else {
						LayerVisible[layerCount] = 0;
					}

					if (isIncluded(initialVisibleLayers, LayerID[layerCount], initialVisibleLayers.length)) {									
						LayerVisible[layerCount] = 1;
					}

					if (isIncluded(initialHiddenLayers, LayerID[layerCount], initialHiddenLayers.length)) {									
						LayerVisible[layerCount] = 0;
					}
					
					if(hasToc){
					var item =toc.findItemByAxlID(LayerName[layerCount]);
					if (item!=null){
						if (item.parent!=null){
							if(item.parent.iconOpened!=null){
								item.setVisible(LayerVisible[layerCount]);
							}
							else{
								item.parent.setVisible(LayerVisible[layerCount]);
							}
						}
					}
					}
					acetatepos = theReplyUC.indexOf("TYPE=\"acetate\"",lpos);
					if (acetatepos == -1) {	
							// por defecto supongo que son polygon
						LayerType[layerCount] = "poly";
						LayerTypeById[LayerID[layerCount]] = "poly";
						startpos = theReplyUC.indexOf("<FCLASS TYPE=",lpos);
						if (startpos != -1) {
							startpos = startpos + 14;
							endpos = startpos + 4;
							theLayerType = theReply.substring(startpos,endpos);
							LayerType[layerCount] = theLayerType;
							LayerTypeById[LayerID[layerCount].toString().toUpperCase()] = theLayerType;
							//alert("Layer " + layerCount + ": " + LayerID[layerCount].toString().toUpperCase());
						}
					} else {
						LayerType[layerCount] = "acetate";
						LayerTypeById[LayerID[layerCount]] = "acetate";
					}

					//alert("Layer " + layerCount + ": " + LayerName[layerCount]);
					layerCount += 1;
					endpos = zpos;

					lpos = theReplyUC.indexOf("<LAYERINFO",zpos);
				} else {
					lpos = -1;
				}

			} else {
					// no deberia ocurrir, pero por si acaso la ignoro y sigo con la siguiente => para que no se cuelgue
					lpos = theReplyUC.indexOf("<LAYERINFO",lpos+10);
			}
		}
		//alert("LayerInfo processed");
	}*/
	LayerName.reverse();
	LayerVisible.reverse();
	LayerID.reverse();
	LayerType.reverse();
	ActiveLayer = LayerID[ActiveLayerIndex];

	if (customMapService) {
		
		
		createNewCustomToc();
	}

		// comprobar a ver si es alguna de las capas inicialmente visibles/ocultas es un WMS
	for (var i = 0; i < initialVisibleLayers.length; i++) {
		var idxx = wmsIdxList[initialVisibleLayers[i]];
		if (idxx) {
			wmsList[idxx]["visib"] = true;
			if(hasToc){
			var item =toc.findItemByAxlID(initialVisibleLayers[i]);
			if (item!=null){
				item.setVisible(1);
			}
			}
		}
	}

	for (var i = 0; i < initialHiddenLayers.length; i++) {
		var idxx = wmsIdxList[initialHiddenLayers[i]];
		if (idxx) {
			wmsList[idxx]["visib"] = false;
			if(hasToc){
			var item =toc.findItemByAxlID(initialHiddenLayers[i]);
			if (item!=null){
				item.setVisible(0);
			}
			}
		}
	}

	//alert(layerCount + " layers");
	if (hasToc) {
		displayToc();
	}
	return false;
}

function createNewCustomToc() {
	toc = new TOCNR('','',false,'');
	wmsList.splice(2, wmsList.length-2); 
	wmsList["count"] = 2;
	 var parser = new ol.format.WMSCapabilities();
	 $.ajax({
			url: url+'?service=WMS&version=1.1.1&request=GetCapabilities',
			type: 'GET',
			async: false,
			success:  
	   function(doc){

	       

	        var c = parser.read(doc);
	        if (!c || !c.Capability) {
	            alert("No se pudieron obtener las capas del servicio "+url);
	            return;
	        }       
	        var capas = c.Capability.Layer.Layer;
	        // Here is result, do whatever you want with it
	        for (var i=0; i<capas.length; i++){
	        	addWMS(url + "?SRS=EPSG:25830&version=1.1.1&service=WMS&FORMAT=image/png&TRANSPARENT=TRUE&request=GetMap&Layers="+capas[i].Name, 80, false, "", false);
	        	toc.addLayer(  new LAYERWMS(capas[i].Name,capas[i].Title,'',null,null,'metadatos/edificaciones.html',i+2));
	        }
	    },
	    failure : function(r) {
	    	 alert("No se pudieron obtener las capas del servicio "+url);
	    }
	});
	
}

function getIdxFromIdLayer(theID) {
	for (var i=0; i<layerCount; i++) {
		if (LayerID[i] == theID) {
			return i;
		}
	}
	return -1;
}
