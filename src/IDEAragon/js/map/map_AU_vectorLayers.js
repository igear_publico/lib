//Stuff related to geojson layers for Municipalities, Comarcas, Provinces and so on


function getSiglaFromUnidadTerritorial(val) {
	if (val == "Municipio") {
		return "M";
	} else if (val == "Comarca") {
		return "C";
	} else {
		return "P";
	}
}

function getUnidadTerritorialFromSigla(val) {
	if (val == "M") {
		return "Municipio";
	} else if (val == "C") {
		return "Comarca";
	} else {
		return "Provincia";
	}
}

function getNamesList(type) {
	if (type == "Municipio") {
		return dMuniList;
	} else if (type == "Comarca") {
		return dComList;
	} else {
		return dProvList;
	}
}


function loadProvinciaLayer(map, urlData, srs) {
	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData,
			projection: srs,
			format: new ol.format.GeoJSON()
		}),
		style: function(feature) {
			// filtrar o hacer lo que se necesite
			//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#CCC',
					width: 1
				}),
				fill: new ol.style.Fill({
					color: genericGetColorSelected(feature.get('CPROV'), 'P'),
				})/*,
				text: new ol.style.Text({
					font: '12px Verdana',
					text: dProvList[feature.get('CPROV')],
					fill: new ol.style.Fill({color: 'black'}),
					stroke: new ol.style.Stroke({color: 'white', width: 2})
				})*/
			});
			return style;
		}
	});
	layer.setZIndex(2);
	layer.setOpacity(0.75);
	layer.setVisible(false);
	map.addLayer(layer);
	return layer;
}


function loadComarcaLayer(map, urlData, srs) {
	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData,
			projection: srs,
			format: new ol.format.GeoJSON()
		}),
		style: function(feature) {
			// filtrar o hacer lo que se necesite
			//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#CCC',
					width: 1
				}),
				fill: new ol.style.Fill({
					color: genericGetColorSelected(feature.get('C_COMARCA'), 'C'),
				})/*,
				text: new ol.style.Text({
					font: map.getView().getZoom() >= 2 ? '12px Verdana' : (map.getView().getZoom() == 1 ? '10px Verdana' : '0px Verdana'),
					text: dComList[feature.get('C_COMARCA')],
					fill: new ol.style.Fill({color: 'black'}),
					stroke: new ol.style.Stroke({color: 'white', width: 2})
				})*/
			});
			return style;
		}
	});
	layer.setZIndex(3);
	layer.setOpacity(0.75);
	layer.setVisible(false);
	map.addLayer(layer);
	return layer;
}

function loadMunicipioLayer(map, urlData, srs) {
	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData, 
			projection: srs,
			format: new ol.format.GeoJSON()
		}),
		style: function(feature) {
			// filtrar o hacer lo que se necesite
			//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#CCC',
					width: 1
				}),
				fill: new ol.style.Fill({
					color: genericGetColorSelected(feature.get('CMUNINE'), 'M'),
				})/*,
				text: new ol.style.Text({
					font: map.getView().getZoom() >= 4 ? '12px Verdana' : (map.getView().getZoom() == 3 ? '10px Verdana' : '0px Verdana'),
					text: dMuniList[feature.get('CMUNINE')],
					fill: new ol.style.Fill({color: 'black'}),
					stroke: new ol.style.Stroke({color: 'white', width: 2})
				})*/
			});
			return style;
		}
	});
	layer.setZIndex(4);
	layer.setOpacity(0.75);
	layer.setVisible(false);
	map.addLayer(layer);
	return layer;
}


function loadNUTS2Layer(map, urlData, srs) {
	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData, 
			projection: srs,
			format: new ol.format.GeoJSON()
		}),
		style: function(feature) {
			// filtrar o hacer lo que se necesite
			//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#CCC',
					width: 1
				}),
				fill: new ol.style.Fill({
					color: genericGetColorSelected(feature.get('nuts_id'), 'A'),
				})/*,
				text: new ol.style.Text({
					font: map.getView().getZoom() >= 4 ? '12px Verdana' : (map.getView().getZoom() == 3 ? '10px Verdana' : '0px Verdana'),
					text: dMuniList[feature.get('CMUNINE')],
					fill: new ol.style.Fill({color: 'black'}),
					stroke: new ol.style.Stroke({color: 'white', width: 2})
				})*/
			});
			return style;
		}
	});
	layer.setZIndex(4);
	layer.setOpacity(0.75);
	layer.setVisible(false);
	map.addLayer(layer);
	return layer;
}

var getCapitalesText = function(feature) {
	var text = feature.get('D_NUCLEO_I');
	return text;
};

var createCapitalesTextStyle = function(feature) {
	return new ol.style.Text({
		textAlign: 'center',
		textBaseline: 'bottom',
		font: '9px Verdana',
		text: getCapitalesText(feature),
		fill: new ol.style.Fill({color: 'black'}),
		stroke: new ol.style.Stroke({color: 'white', width: 1}),
		offsetY: "-4"
	});
};

//Polygons
var createCapitalesStyleFunction = function() {
	return function(feature) {
		return [new ol.style.Style({
			image: new ol.style.RegularShape({stroke:new ol.style.Stroke({width:1, color:'black'}),
				fill: new ol.style.Fill({
					color: "#FF69B4"
				}),
				points: 4,
				radius:3,
				angle:Math.PI / 4}),
				text:createCapitalesTextStyle(feature)})];
	};
};
function loadCapitalesComarcaLayer(map, urlData, srs) {
	var format = new ol.format.GeoJSON();


	var source = new ol.source.Vector();

	var capitales_layer = new ol.layer.Vector({
		source: source,
		style: createCapitalesStyleFunction() 
	});
	capitales_layer.setZIndex(5);
	capitales_layer.setVisible(true);
	map.addLayer(capitales_layer);

	$.ajax({
		url: urlData,
		type: 'GET',
		success:  function(data){
			var features = format.readFeatures(data);
			capitales_layer.getSource().addFeatures(features);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			console.log("No se han podido cargar las capitales de comarca "+textStatus +"\n "+errorThrown);
		}


	});	


	return capitales_layer;
}

function getVectorLayer4Print(capa,fieldCode,newBboxPrint,printMapWidth, printMapHeight,callback){
	var getMap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
	"<ogc:GetMap xmlns:ogc=\"http://www.opengis.net/ows\"\n" +
	"            xmlns:gml=\"http://www.opengis.net/gml\"\n" +
	"   version=\"1.1.1\" service=\"WMS\">\n";
	var sld='<sld:StyledLayerDescriptor version="1.0.0" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc">'+
	'<sld:NamedLayer>	'+
	'		<sld:Name>'+capa.get('wmsLayer')+'</sld:Name>'+
	'		<sld:UserStyle>'+
	'		<sld:Name>Nombre</sld:Name>'+
	'		<sld:Title>Titulo</sld:Title>'+
	'		<sld:FeatureTypeStyle>';


	var features = capa.getSource().getFeatures();
	for (var i = 0; i < features.length; i++) {
var feature = features[i];
		sld += "\t\t\t<sld:Rule>\n" +
		"\t\t\t\t<ogc:Filter>\n";
		sld += "\t\t\t\t\t\t<ogc:PropertyIsEqualTo>\n" +
		"\t\t\t         \t<ogc:PropertyName>"+fieldCode.toLowerCase()+"</ogc:PropertyName>\n" +
		"\t\t\t      \t<ogc:Literal>" + feature.get(fieldCode).replace("P","").replace("C","").replace("M","").replace("A","") + "</ogc:Literal>\n" +
		"\t\t\t\t\t\t</ogc:PropertyIsEqualTo>\n";

		//}
		sld +=
			"\t\t\t\t</ogc:Filter>\n";

		sld += "\t\t\t\t<sld:PolygonSymbolizer>\n" +
		"\t\t\t\t\t<sld:Fill>\n" +
		"\t\t\t\t\t\t<sld:CssParameter name=\"fill\">" + genericGetColorSelected(feature.get(fieldCode)) + "</sld:CssParameter>\n" +
		"\t\t\t\t\t\t<sld:CssParameter name=\"fill-opacity\">1</sld:CssParameter>\n" +
		"\t\t\t\t\t</sld:Fill>\n" +
		"\t\t\t\t\t<sld:Stroke>\n" +
		"\t\t\t\t\t\t<sld:CssParameter name=\"stroke\">#969696</sld:CssParameter>\n" +
		"\t\t\t\t\t\t<sld:CssParameter name=\"stroke-width\">1</sld:CssParameter>\n" +
		"\t\t\t\t\t</sld:Stroke>\n" +
		"\t\t\t\t</sld:PolygonSymbolizer>\n";
		sld += "\t\t\t</sld:Rule>\n";
		
	}
	sld +="</sld:FeatureTypeStyle>\n" +
	"</sld:UserStyle>\n" +
"</sld:NamedLayer>\n" +
"</sld:StyledLayerDescriptor>\n" ;
	getMap +=sld;
	getMap += "<BoundingBox srsName=\"http://www.opengis.net/gml/srs/epsg.xml#25830\">\n";
	
	getMap += "<gml:coord><gml:X>" + newBboxPrint.minx + "</gml:X><gml:Y>" + newBboxPrint.miny + "</gml:Y></gml:coord>\n";
	getMap += "<gml:coord><gml:X>" + newBboxPrint.maxx + "</gml:X><gml:Y>" + newBboxPrint.maxy + "</gml:Y></gml:coord>\n";
	//getMap +="<gml:coord><gml:X>591377.2</gml:X><gml:Y>4581970.0</gml:Y></gml:coord>	<gml:coord><gml:X>868338.8</gml:X><gml:Y>4755071.0</gml:Y></gml:coord>";

	getMap += "</BoundingBox>\n";
	getMap += "<Output>\n" +
	"   <Format>image/png</Format>\n" +
	"   <Size><Width>" + printMapWidth + "</Width><Height>" + printMapHeight + "</Height></Size>\n" +
	
	"</Output>" +
	"</ogc:GetMap>\n";
	 var xhr = new XMLHttpRequest();
     xhr.onload = function (e) {
       if (this.status === 200) {
         var uInt8Array = new Uint8Array(this.response);
         var i = uInt8Array.length;
         var binaryString = new Array(i);
         while (i--) {
           binaryString[i] = String.fromCharCode(uInt8Array[i]);
         }
         var data = binaryString.join('');
         var type = xhr.getResponseHeader('content-type');
         if (type.indexOf('image') === 0) {
           var imagen64 = 'data:' + type + ';base64,' + window.btoa(data);
           callback(newBboxPrint,imagen64);
         }
       }
     };
     xhr.open('POST', '/Visor2D?SERVICE=WMS', true);
     xhr.setRequestHeader("Content-type", "text/xml; charset=\"utf-8\"");
     xhr.responseType = 'arraybuffer';
     xhr.send(getMap);
     
    
}




