var theX, theY;
var x1, y1, x2, y2;
var zleft,zright,ztop,zbottom;
var mtop,mleft,mwidth,mheight,mborder;
var mapDivId;
var activeTool = 'zoomIn';
var dragging = false;
var panning = false;
var waitingForResponse = false;

var polygonUserList = new Array();
var polygonUserCount = 0;
var lineUserList= new Array();
var lineUserCount = 0;
var pointUserList= new Array();
var pointUserCount = 0;
var mapContainerCreated= false;

function setMapDivProperties(_top, _left, _width, _height, _border, _mapId) {
	mtop = _top+_border;
	mleft = _left+_border;
	mwidth = Math.floor(_width-(2*_border));
	mheight = Math.floor(_height-(2*_border));
	mborder = _border;
	mapDivId = _mapId;
}

function updateMapDivProperties(width, height) {
	mwidth = Math.floor(width - (2*borderr));
	mheight = Math.floor(height - (2*borderr));
}

function createZoomBoxDivs() {
	createLayer("zoomboxTop",mleft,mtop,mwidth,mheight,false,"",11001);
	createLayer("zoomboxBottom",mleft,mtop,mwidth,mheight,false,"",11001);
	createLayer("zoomboxLeft",mleft,mtop,mwidth,mheight,false,"",11001);
	createLayer("zoomboxRight",mleft,mtop,mwidth,mheight,false,"",11001);
	setLayerBackgroundColor("zoomboxTop","blue");
	setLayerBackgroundColor("zoomboxBottom","blue");
	setLayerBackgroundColor("zoomboxLeft","blue");
	setLayerBackgroundColor("zoomboxRight","blue");
}
function createIdResultDiv() {
	$("#map").append('<div id="IdResult" style="position:absolute; overflow:auto; left:0px; top:0px; width:500px; height:460px; background-color:White; border-style:ridge;visibility:hidden;"></div>');
}

function changeActiveTool(newActiveTool) {

	switch (newActiveTool) {
		case 'zoomIn':
		case 'zoomOut':
		case 'pan':
		case 'panOffset':
		case 'identifyAll':
		case 'descarga':
		case 'identifyOVC':
		case 'profile':
		case 'distanceMeasure':
		case 'areaMeasure':
		case 'editar':
		case 'historico':
		case 'bufferCirculo':
												//limpiaActiveTool(activeTool);
												activeTool = newActiveTool;
												//marcaActiveTool(activeTool);
												break;
		default:
												break;
	}
}


function setActiveToolFotograma(newActiveTool,layer){
	
}
function setActiveTool(newActiveTool){
	changeActiveTool(newActiveTool);

	var layer = document.getElementById("eventosMapa");
	//var layer = document.getElementById(mapDivId);
	if (newActiveTool == "zoomIn" || newActiveTool == "zoomOut") {
		layer.onmousedown= startDragging;
		layer.onclick = null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	}	else if (newActiveTool == "pan") {
		layer.onmousedown= startMapDragging;
		layer.onclick = null;
		layer.ondblclick=null;
		layer.style.cursor = "move";
	} else if(newActiveTool == "identifyAll") {
		layer.onmousedown= pointClick;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	} else if(newActiveTool == "editFeature") {
		layer.onmousedown= pointClickEdition;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	} else if(newActiveTool == "descarga") {
		layer.onmousedown= pointClickDescarga;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	} else if(newActiveTool == "identifyOVC") {
		layer.onmousedown= pointClickOVC;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	} else if(newActiveTool == "toc") {
		showToc();
	} else if(newActiveTool == "find") {
		showFind();
	} else if(newActiveTool == "ayuda") {
		window.open("http://idearagon.aragon.es/datosdescarga/descarga.php?file=documentacion/web/manual-uso-visor2d.pdf");
	} else if(newActiveTool == "tableToMap") {
		window.open("http://idearagon.aragon.es/tab2map");
	} else if(newActiveTool == "RJT") {
		openRJT();
	} else if(newActiveTool == "fullExtent") {
		getFullMap();
	} else if(newActiveTool == "overview") {
		showOverview();
	} else if(newActiveTool == "bufferCirculo") {
		isDistanceMeasuring = false;
		isAreaMeasuring = false;
		isDistanceMeasuring = false;
		isGeoreferencing = false;
		isHistorico = false;
		isProfileMaking = false;
		
		$( "#measurePath" ).dialog( "close" );
		$( "#profile" ).dialog( "close" );
		$( "#measureArea" ).dialog( "close" );
		$( "#georrefWin" ).dialog( "close" );
		$( "#historicoWin" ).dialog( "close" );

		$( "#circleWin" ).dialog( "open" );
		layer.onmousedown= pointClickBufferCirculo;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
	} else if(newActiveTool == "imprimir") {
		$( "#printWin" ).dialog( "open" );
	} else if(newActiveTool == "borrar") {
		limpiarMapa();
		limpiarMarcaMapa();
		borrarBufferCirculo();
		borrarBufferCirculoGranjas();
		if ((typeof mapOL !='undefined') && (mapOL!=null)){
			removeMarkers();
		}
		resetNotificar();

		getMapWithCurrentExtent();
	} else if(newActiveTool == "zoomLast") {
		previousExtent();
	} else if(newActiveTool == "informeGeneral") {
		informeGeneral();
	}else if(newActiveTool == "profile") {
		if (clickCount>0) {
			if (isAreaMeasuring||isDistanceMeasuring) {
				resetClick();
			}
		}
		if (/*isGeoreferencing ||*/ isAreaMeasuring /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarMedicionArea();
			$( "#measureArea" ).dialog( "close" );
			/*$( "#georrefWin" ).dialog( "close" );
			$( "#historicoWin" ).dialog( "close" );*/
		}
		if (/*isGeoreferencing ||*/ isDistanceMeasuring /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarMedicionDistancia();
			$( "#measurePath" ).dialog( "close" );
			//$( "#georrefWin" ).dialog( "close" );
			//$( "#historicoWin" ).dialog( "close" );
		}

		layer.onmousedown= clickAddPoint_profile;
		layer.ondblclick= dblClickAddPoint_profile;
		layer.onclick= null;
		layer.style.cursor = "crosshair";

		$( "#circleWin" ).dialog( "close" );
		$( "#profile" ).dialog( "open" );
		if (!isProfileMaking){
		limpiarMedicionDistancia();
		//distanceMeasureWin.show();
		clearDistanceMeasureBox();
		}
		isProfileMaking = true;
		isDistanceMeasuring = false;
		isAreaMeasuring = false;
		/*isGeoreferencing = false;
		isHistorico = false;*/
	} else if(newActiveTool == "distanceMeasure") {
		if (clickCount>0) {
			if (isAreaMeasuring) {
				resetClick();
			}
		}
		if (/*isGeoreferencing ||*/ isAreaMeasuring /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarMedicionArea();
			$( "#measureArea" ).dialog( "close" );
			/*$( "#georrefWin" ).dialog( "close" );
			$( "#historicoWin" ).dialog( "close" );*/
		}
		if (/*isGeoreferencing ||*/ isProfileMaking /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarProfile();
			$( "#profile" ).dialog( "close" );
			/*$( "#georrefWin" ).dialog( "close" );
			$( "#historicoWin" ).dialog( "close" );*/
		}

		layer.onmousedown= clickAddPoint_distanceMeasure;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";

		$( "#circleWin" ).dialog( "close" );
		$( "#measurePath" ).dialog( "open" );
		if (!isDistanceMeasuring){
		limpiarMedicionDistancia();
		//distanceMeasureWin.show();
		clearDistanceMeasureBox();
		}
		isDistanceMeasuring = true;
		isAreaMeasuring = false;
		isProfileMaking = false;
		/*isGeoreferencing = false;
		isHistorico = false;*/
	} else if(newActiveTool == "areaMeasure") {
		if (clickCount > 0) {
			if (isDistanceMeasuring) {
				resetClick();
			}
		}
		if (/*isGeoreferencing ||*/ isDistanceMeasuring /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarMedicionDistancia();
			$( "#measurePath" ).dialog( "close" );
			//$( "#georrefWin" ).dialog( "close" );
			//$( "#historicoWin" ).dialog( "close" );
		}
		
		if (/*isGeoreferencing ||*/ isProfileMaking /*|| isHistorico*/) {
			//limpiarMapa();
			limpiarProfile();
			$( "#profile" ).dialog( "close" );
			/*$( "#georrefWin" ).dialog( "close" );
			$( "#historicoWin" ).dialog( "close" );*/
		}
		layer.onmousedown= clickAddPoint_areaMeasure;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";

		$( "#circleWin" ).dialog( "close" );
		$( "#measureArea" ).dialog( "open" );
		//areaMeasureWin.show();
		if(!isAreaMeasuring){
			limpiarMedicionArea();
			clearAreaMeasureBox();
		}
		isDistanceMeasuring = false;
		isAreaMeasuring = true;
		isProfileMaking = false;
	/*	isGeoreferencing = false;
		isHistorico = false;*/
	} else if(newActiveTool == "editar") {
		if (clickCount>0) {
			resetClick();
		}
		if (/*isDistanceMeasuring || isAreaMeasuring ||*/ isHistorico) {
			//limpiarMapa();
			//$( "#measurePath" ).dialog( "close" );
			//$( "#measureArea" ).dialog( "close" );
			limpiarHistorico();
			$( "#historicoWin" ).dialog( "close" );
		}

		isDistanceMeasuring = false;
		isAreaMeasuring = false;
		isProfileMaking = false;
		isGeoreferencing = true;
		isHistorico = false;

		$( "#circleWin" ).dialog( "close" );
		if (customToolEditar) {
			if (customToolEditarType == "GEOREF1") {
				layer.onmousedown = georef1PointClick;
				layer.onclick= null;
				layer.ondblclick=null;
				layer.style.cursor = "crosshair";
			} else {
				layer.onmousedown = georef2PointClick;
				layer.onclick= null;
				layer.ondblclick=null;
				layer.style.cursor = "crosshair";
			}
		} else {
			if (originador == null) {
				changeOriginador();
			}

			updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);

			layer.onmousedown = clickAddPoint_editar;
			layer.onclick= null;
			layer.ondblclick=null;
			layer.style.cursor = "crosshair";
			$( "#georrefWin" ).dialog( "open" );
		}
	} 
	 else if(newActiveTool == "createFeature") {
			newFeature();
	 }
	else if(newActiveTool == "historico") {
		if (clickCount>0) {
			resetClick();
		}
		if (/*isDistanceMeasuring || isAreaMeasuring ||*/ isGeoreferencing) {
			limpiarEdicion();
			/*limpiarMapa();
			$( "#measurePath" ).dialog( "close" );
			$( "#measureArea" ).dialog( "close" );*/
			$( "#georrefWin" ).dialog( "close" );
		}

		isDistanceMeasuring = false;
		isAreaMeasuring = false;
		isProfileMaking = false;
		isGeoreferencing = false;
		isHistorico = true;

		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);

		layer.onmousedown = clickAddPoint_historico;
		layer.onclick= null;
		layer.ondblclick=null;
		layer.style.cursor = "crosshair";
		$( "#historicoWin" ).dialog( "open" );
	} else if(newActiveTool=="3d"){
		
		/*layer.onmousedown = clickPoint_3D;
		layer.onclick= null;
		layer.style.cursor = "crosshair";*/
		
		var coords = ol.extent.getCenter(mapOL.getView().calculateExtent([mwidth,mheight]));
		
		latLon = coordTransformProj4s("EPSG:25830", "EPSG:4258", coords[0], coords[1]);
		theLon = RoundDecimal(latLon.ord[0], 8);
		theLat = RoundDecimal(latLon.ord[1], 8);
		window.open("http://vuelos3d.aragon.es/?latitud="+theLat+"&longitud="+theLon);
		
	}
	setActiveToolFotograma(newActiveTool,layer);
}

function resetNotificar() {
	notificarCurrentMode = "";

	pointUserList = new Array();
	pointUserCount = 0;
	lineUserList = new Array();
	lineUserCount = 0;
	polygonUserList = new Array();
	polygonUserCount = 0;

	if (previousOnMouseDown != null) {
		var layer = document.getElementById("eventosMapa");
		layer.onmousedown = previousOnMouseDown;
		layer.cursor = previousCursor;
	}
}

function limpiarMarcaMapa() {
	xCoordUsr = "";
	yCoordUsr = "";

	findXCoordUsr = "";
	findYCoordUsr = "";
	document.getElementById('theAcetateMarca').style.display = 'none';
}

function limpiarMapa() {
	try{
	limpiarMedicionDistancia();
	}
	catch(err){
		
	}
	try{
	limpiarMedicionArea();
	}
	catch(err){
		
	}
	try{
	limpiarEdicion();
	}
	catch(err){
		
	}
	try{
	limpiarHistorico();
	}
	catch(err){
		
	}
	

	if (idSelectionLayer) {
		idSelectionLayer = "";
		//getMapWithCurrentExtent();
	}
}

function startDragging(e) {
	panning = false;
	if (! dragging) {
		dragging = true;
		getXY(e);
		x1 = theX;
		y1 = theY;
		x2 = x1+1;
		y2 = y1+1;

		showZoomBox(x1,y1,x2,y2);

		document.onmousemove = updateDragging;
		document.onmouseup = stopDragging;

	}
	return false;
}

function startMapDragging(e) {
	dragging = false;
	if (! panning) {
		panning = true;
		getXY(e);
		x1 = theX;
		y1 = theY;
		x2 = x1+1;
		y2 = y1+1;

		jg.clear();
		limpiarEdicion();
		limpiarHistorico();
		limpiarBufferCirculo();

		document.onmousemove = updateMapDragging;
		document.onmouseup = stopMapDragging;
	}
	return false;
}

function georef1PointClick(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	calculateAltitud(x1, y1);
	return false;
}

function georef2PointClick(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	calculaRefCatOVC_customGeorref(x1, y1);
	return false;
}

var notificarCurrentMode = "";
var previousOnMouseDown = null;
var previousCursor = null;
function pointClick_notificar_point(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	clickAddPoint(e, false);

	pointUserList[pointUserCount] = new Array();
	pointUserList[pointUserCount]['0x'] = clickPointX[clickCount-1];
	pointUserList[pointUserCount]['0y'] = clickPointY[clickCount-1];
	pointUserList[pointUserCount]['count'] = 1;

	pointUserCount++;

	//updateMapClicks();
	updateNotifications();
	resetClick();
}
function pointClick_notificar_linea(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	clickAddPoint(e, false);

	if (lineUserCount == 0) {
		maxxPoligono = clickPointX[0];
		minxPoligono = clickPointX[0];
		maxyPoligono = clickPointY[0];
		minyPoligono = clickPointY[0];
	}
	if (! lineUserList[lineUserCount]) {
		lineUserList[lineUserCount] = new Array();
		lineUserList[lineUserCount]['count'] = 0;
	}

	var aux = lineUserList[lineUserCount]['count'];
	lineUserList[lineUserCount][aux + 'x'] = clickPointX[clickCount-1];
	lineUserList[lineUserCount][aux + 'y'] = clickPointY[clickCount-1];
	lineUserList[lineUserCount]['count'] = aux+1;

	coordsPoligonoTmp = "<PATH><COORDS>";
	for (var ii = 0; ii < clickCount; ii++) {
		coordsPoligonoTmp += clickPointX[ii] + ' ' + clickPointY[ii] + ';';

		if (clickPointX[ii] > maxxPoligono) {
			maxxPoligono = clickPointX[ii];
		}
		if (clickPointY[ii] > maxyPoligono) {
			maxyPoligono = clickPointY[ii];
		}
		if (clickPointX[ii] < minxPoligono) {
			minxPoligono = clickPointX[ii];
		}
		if (clickPointY[ii] < minyPoligono) {
			minyPoligono = clickPointY[ii];
		}
	}

	coordsPoligonoTmp += "</COORDS></PATH>";

	//jgLine.clear();

	updateMapClicks();

	jgLine.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
	if (clickCount > 1) {
		jgLine.drawLine(screenClickPointX[clickCount-1], screenClickPointY[clickCount-1], screenClickPointX[clickCount-2], screenClickPointY[clickCount-2]);
	}
	jgLine.paint();
}
function pointClick_notificar_polygon(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	clickAddPoint(e, false);

	if (polygonUserCount == 0) {
		maxxPoligono = clickPointX[0];
		minxPoligono = clickPointX[0];
		maxyPoligono = clickPointY[0];
		minyPoligono = clickPointY[0];
	}
	if (! polygonUserList[polygonUserCount]) {
		polygonUserList[polygonUserCount] = new Array();
		polygonUserList[polygonUserCount]['count'] = 0;
	}

	var aux = polygonUserList[polygonUserCount]['count'];
	polygonUserList[polygonUserCount][aux + 'x'] = clickPointX[clickCount-1];
	polygonUserList[polygonUserCount][aux + 'y'] = clickPointY[clickCount-1];
	polygonUserList[polygonUserCount]['count'] = aux+1;

	coordsPoligonoTmp = "<RING>";
	for (var ii = 0; ii < clickCount; ii++) {
		coordsPoligonoTmp += '<POINT x="' + clickPointX[ii] + '" y="' + clickPointY[ii] + '" />';
		if (clickPointX[ii] > maxxPoligono) {
			maxxPoligono = clickPointX[ii];
		}
		if (clickPointY[ii] > maxyPoligono) {
			maxyPoligono = clickPointY[ii];
		}
		if (clickPointX[ii] < minxPoligono) {
			minxPoligono = clickPointX[ii];
		}
		if (clickPointY[ii] < minyPoligono) {
			minyPoligono = clickPointY[ii];
		}
	}
	coordsPoligonoTmp += "</RING>";

	jgPol.clear();
	updateMapClicks();
	jgPol.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
	if (clickCount > 1) {
		jgPol.drawPolygon(screenClickPointX, screenClickPointY);
	}
	jgPol.paint();
}

function addGeomNotificar() {
	if (notificarCurrentMode == "polygon") {
		if (clickCount >= 3) {
			endNotificar();
		}
	}

	if (notificarCurrentMode == "line") {
		if (clickCount >= 2) {
			endNotificar();
		}
	}
}

function newPointNotificar() {
		// Si había alguna a medias, y era válida, la añado automáticamente
	addGeomNotificar();

	resetClick();
	updateMapClicks();

	var layer = document.getElementById("eventosMapa");
	if (notificarCurrentMode == "") {
		previousOnMouseDown = layer.onmousedown;
		previousCursor = layer.style.cursor;
	}
	notificarCurrentMode = "point";
	layer.onmousedown= pointClick_notificar_point;
	layer.style.cursor = "crosshair";

	pointUserList[pointUserCount] = new Array();
	pointUserList[pointUserCount]['count'] = 0;
}

function newLineNotificar() {
		// Si había alguna a medias, y era válida, la añado automáticamente
	addGeomNotificar();

	resetClick();
	updateMapClicks();
	jgLine.clear();

	var layer = document.getElementById("eventosMapa");
	if (notificarCurrentMode == "") {
		previousOnMouseDown = layer.onmousedown;
		previousCursor = layer.style.cursor;
	}
	notificarCurrentMode = "line";
	layer.onmousedown= pointClick_notificar_linea;
	layer.style.cursor = "crosshair";

	lineUserList[lineUserCount] = new Array();
	lineUserList[lineUserCount]['count'] = 0;
}

function newPolygonNotificar() {
		// Si había alguna a medias, y era válida, la añado automáticamente
	addGeomNotificar();

	resetClick();
	updateMapClicks();
	jgPol.clear();

	var layer = document.getElementById("eventosMapa");
	if (notificarCurrentMode == "") {
		previousOnMouseDown = layer.onmousedown;
		previousCursor = layer.style.cursor;
	}
	notificarCurrentMode = "polygon";
	layer.onmousedown= pointClick_notificar_polygon;
	layer.style.cursor = "crosshair";

	polygonUserList[polygonUserCount] = new Array();
	polygonUserList[polygonUserCount]['count'] = 0;
}

function endNotificar() {
	if (notificarCurrentMode == "point") {
	} else if (notificarCurrentMode == "line") {
		if (clickCount >= 2) {
			lineUserList[lineUserCount]['txt'] = coordsPoligonoTmp;
			lineUserCount++;
			updateMapClicks();
			updateNotifications();
	
			resetClick();
		} else {
			alert("Debe definir al menos 2 puntos para definir una línea");
		}
	} else if (notificarCurrentMode == "polygon") {
		if (clickCount >= 3) {
			polygonUserList[polygonUserCount]['txt'] = coordsPoligonoTmp;
			polygonUserCount++;

			coordsPoligono = "<POLYGON>";

			for (var jj = 0; jj < polygonUserCount; jj++) {
				coordsPoligono += polygonUserList[jj]['txt'];
			}
		
			//		coordsPoligono += coordsPoligonoTmp;
			coordsPoligono += "</POLYGON>";

			updateMapClicks();
			updateNotifications();

			resetClick();
		} else {
			alert("Debe definir al menos 3 puntos");
		}
	}
}
function sendNotificar() {
		// Si había alguna a medias, y era válida, la añado automáticamente
	addGeomNotificar();
	var txt = window.prompt("Introduzca un breve comentario explicativo de su notificación", "");
	var contacto = window.prompt("Introduzca una forma de contacto", "");
	if (txt != "") {
		if (contacto != "") {
			sendNotifications( txt, contacto);
			limpiarEdicion();
			resetNotificar();
			updateNotifications();
		} else {
	 		alert("Debe informar de una forma de contacto");
		}
	} else {
	 alert("Debe informar de un comentario");
	}
}
function sendNotifications(txt, contacto) {
	var id = contacto + new Date().getTime();
	var coordsGML = '<?xml version="1.0" encoding="UTF-8"?><ogr:FeatureCollection xmlns:ogr="http://ogr.maptools.org/">';
  var coordsGMLidx = 0;

	for (var idx = 0; idx < pointUserCount; idx++) {
		var coords = "";
		for (var qq = 0; qq < pointUserList[idx]['count']; qq++) {
			var theX = pointUserList[idx][(qq) + 'x'];
			var theY = pointUserList[idx][(qq) + 'y'];
			coords += pointUserList[idx][(qq) + 'x'] + ',' + pointUserList[idx][(qq) + 'y'] + ";";

			coordsGML += '<gml:featureMember xmlns:gml="http://www.opengis.net/gml"><feature xmlns="http://www.opengis.net/gml" fid="' + coordsGMLidx + '">';
			coordsGML += '<geom><gml:Point srsName="EPSG:25830"><gml:pos>' + pointUserList[idx][(qq) + 'x'] + ' ' + pointUserList[idx][(qq) + 'y'] + '</gml:pos></gml:Point>';
			coordsGML += '</geom></feature></gml:featureMember>';
			coordsGMLidx++;
		}
		$.ajax({
				type: "POST",
				url: "/BD_GIS/registraNotificacion.jsp",
				data:  { 
					'id_notif': id,
					'contacto': contacto,
					'coords': coords, 
					'arcxml': '',
					'tipo': 'point'
				},
				success: function(data) {
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
					alert(thrownError);
				}
		});
	}

	for (var idx = 0; idx < lineUserCount; idx++) {
		var coords = "";

		coordsGML += '<gml:featureMember xmlns:gml="http://www.opengis.net/gml"><feature xmlns="http://www.opengis.net/gml" fid="' + coordsGMLidx + '">';
		coordsGML += '<geom><gml:LineString srsName="EPSG:25830"><gml:posList>';
		for (var qq = 0; qq < lineUserList[idx]['count']; qq++) {
			coords += lineUserList[idx][(qq) + 'x'] + ',' + lineUserList[idx][(qq) + 'y'] + ";";
			coordsGML += lineUserList[idx][(qq) + 'x'] + ' ' + lineUserList[idx][(qq) + 'y'] + ' ';
		}
		coordsGML += '</gml:posList></gml:LineString></geom>';
		coordsGML += '</feature></gml:featureMember>';
		coordsGMLidx++;
		$.ajax({
				type: "POST",
				url: "/BD_GIS/registraNotificacion.jsp",
				data:  { 
					'id_notif': id,
					'contacto': contacto,
					'coords': coords, 
					'arcxml': lineUserList[idx]['txt'],
					'tipo': 'line'
				},
				success: function(data) {
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
					alert(thrownError);
				}
		});
	}

	for (var idx = 0; idx < polygonUserCount; idx++) {
		var coords = "";
		coordsGML += '<gml:featureMember xmlns:gml="http://www.opengis.net/gml"><feature xmlns="http://www.opengis.net/gml" fid="' + coordsGMLidx + '">';
		coordsGML += '<geom><gml:Polygon srsName="EPSG:25830"><gml:exterior><gml:LinearRing><gml:posList>';
		for (var qq = 0; qq < polygonUserList[idx]['count']; qq++) {
			coords += polygonUserList[idx][(qq) + 'x'] + ',' + polygonUserList[idx][(qq) + 'y'] + ";";
			coordsGML += polygonUserList[idx][(qq) + 'x'] + ' ' + polygonUserList[idx][(qq) + 'y'] + ' ';
		}
			// cerrar el poligono
		coords += polygonUserList[idx]['0x'] + ',' + polygonUserList[idx]['0y'] + ";";
		coordsGML += polygonUserList[idx]['0x'] + ' ' + polygonUserList[idx]['0y'] + ' ';
		coordsGML += '</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon>';
		coordsGML += '</geom></feature></gml:featureMember>';
		coordsGMLidx++;

		$.ajax({
				type: "POST",
				url: "/BD_GIS/registraNotificacion.jsp",
				data:  { 
					'id_notif': id,
					'contacto': contacto,
					'coords': coords, 
					'arcxml': polygonUserList[idx]['txt'],
					'tipo': 'polygon'
				},
				success: function(data) {
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
					alert(thrownError);
				}
		});
	}
	if (window.confirm("Petición notificada correctamente. ¿Desea una copia de su información en formato GML?")) {
	  coordsGML += '</ogr:FeatureCollection>';
		/*
		$.ajax({
				type: "POST",
				url: "/FileSave/fileSave",
				data:  {
					'txt': coordsGML
				},
				success: function(data) {
			  	if (data) {
    	      var dlif = $('<iframe/>',{'src':data}).hide();
        	  this.append(dlif);
	  	    } else {
      	  	alert('No es posible crear el fichero GML en este momento');
	  	    }
				},
				error: function (xhr, ajaxOptions, thrownError) {
      	  alert('No es posible crear el fichero GML en este momento');
					//alert(xhr.status);
					//alert(thrownError);
				}
		});
		*/

	  var $form = $("#notifyForm");
    if ($form.length == 0) {
        $form = $("<form>").attr({ "target": "hiddenFrame", "id": "notifyForm", "method": "POST", "action": "/BD_GIS/fileSave" }).hide();
        $("body").append($form);
    }
    $form.find("input").remove();
    $form.append($("<input>").attr({"value":coordsGML, "name":"txt"}));
    $form.append($("<input>").attr({"value":"idearagon.gml", "name":"filename"}));
    $form.submit();
	}
}

function pointClick(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	identify(x1, y1);
	if (typeof document.getElementById("filtrosZoneAccordion") !='undefined'){
		$("#filtrosZoneAccordion").accordion( "option", "active", false );
	}
	
	return false;
}

function pointClickEdition(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	editFeature(x1, y1);
	return false;
}
function pointClickDescarga(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	descarga(x1, y1);
	return false;
}

function pointClickBufferCirculo(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	doBufferCirculo(x1, y1);
	return false;
}

var valueRadius = -1;
var xRadius = -1;
var yRadius = -1;

var xRadiusGranjas = -1;
var yRadiusGranjas = -1;
function doBufferCirculo(x1, y1) {
	var coordClick = getMapXY(x1, y1);
	xRadius = coordClick[0];
	yRadius = coordClick[1];

	valueRadius = document.getElementById('radiusTextId').value;
	valueRadius = replaceChar(valueRadius, ',', '.');
	if (! isNaN(valueRadius)) {
		var tamPixel = Math.abs(maxx - minx) / mwidth;
		var numPixels = valueRadius / tamPixel;
		if ((numPixels > 0) && (numPixels*2 <= Math.max(mwidth,mheight))) {
			jgCircle.clear();
			jgCircle.drawEllipse(x1-numPixels, y1-numPixels, numPixels*2, numPixels*2);

			if (numPixels*2>10) {
				jgCircle.drawLine(x1-4, y1-4, x1+4, y1+4);
				jgCircle.drawLine(x1+4, y1-4, x1-4, y1+4);
			}

			jgCircle.paint();
		} else {
			alert("El valor del radio debe ser inferior al tamaño del mapa actual");
		}
	} else {
		alert("El valor del radio debe ser un número");
	}
}

function pointClickOVC(e) {
	getXY(e);
	var x1 = theX - mleft;
	var y1 = theY - mtop;

	identifyOVC(x1, y1);
	return false;
}


function updateDragging(e) {
	if (dragging) {
		getXY(e);
		x2 = theX;
		y2 = theY;
		//var inside = true;
		if (x2 < mleft) {
			x2 = mleft;
			//inside = false;
		}
		if (x2 > mleft + mwidth ) {
			x2 = mleft + mwidth;
			//inside = false;
		}
		if (y2 < mtop ) {
			y2 = mtop;
			//inside = false;
		}
		if (y2 > mtop + mheight) {
			y2 = mtop + mheight;
			//inside = false;
		}
		setClip();
	}
	return false;
}

function updateMapDragging(e) {
	if (panning) {
		getXY(e);
		x2 = theX;
		y2 = theY;
		if (x2 < mleft) x2 = mleft;
		if (x2 > mleft + mwidth) x2 = mleft + mwidth;
		if (y2 < mtop) y2 = mtop;
		if (y2 > mtop + mheight) y2 = mtop + mheight;
		var dx = x2-x1;
		var dy = y2-y1;

		var cLeft = - dx;
		var cTop = - dy;
		var cRight = mwidth;
		var cBottom = mheight;
		if (dx>0) {
			cLeft = 0;
			cRight = mwidth - dx;
		}
		if (dy>0) {
			cTop = 0;
			cBottom = mheight - dy;
		}

		moveLayer(mapDivId,dx ,dy);
		panClipLayer(mapDivId,cLeft,cTop,cRight,cBottom);

		if ((typeof comparing != 'undefined')&& comparing){
			moveLayer("fotogramaId1",dx/*+lefttFotograma*/ ,dy/*+toppFotograma*/);
			panClipLayer("fotogramaId1",cLeft,cTop,cRight,cBottom);

			$(".fotograma1Extra").each(function( index ) {
				moveLayer(this.id,dx/*+lefttFotograma*/ ,dy/*+toppFotograma*/);
				panClipLayer(this.id,cLeft,cTop,cRight,cBottom);

			});
			moveLayer("fotogramaIdBase1",dx/*+lefttFotograma*/ ,dy/*+toppFotograma*/);
			panClipLayer("fotogramaIdBase1",cLeft,cTop,cRight,cBottom);
		}
	}
	return false;
}

function stopDragging(e) {
	if (dragging) {
		dragging = false;

		getXY(e);
		document.onmousemove = null;
		document.onmouseup = null;
		hideZoomBox();
		setClip();
		// adjust for offsets
		zleft -= mleft;
		zright -= mleft;
		zbottom -= mtop;
		ztop -= mtop;

			// si solo ha hecho click => zoom x2
		if (((zright - zleft)==1) && ((zbottom - ztop)==1)) {
			if (activeTool == "zoomIn") {
				fixedZoomIn(zright,zbottom,1);
			} else {
				fixedZoomOut(zright,zbottom,1);
			}
		} else {
			zoom(zleft,zbottom,zright,ztop);
		}
		return false;
	}
}

function stopMapDragging(e) {
	if (panning) {
		panning = false;

		getXY(e);
		document.onmousemove = null;
		document.onmouseup = null;

		var ixOffset = x1-x2;
		var iyOffset = y2-y1;
		//document.getElementById('theImage').onload = resetAfterPan;
		resetAfterPan();
		pan(ixOffset, iyOffset);
		return false;
	}
}

function resetAfterPan() {
	moveLayer(mapDivId,0,0);
	clipLayer2(mapDivId,0,0,mwidth,mheight);
	document.getElementById('theImage').onload = null;
	if ((typeof comparing != 'undefined')&& comparing){
		moveLayer("fotogramaId1",0,0);
		clipLayer2("fotogramaId1",0,0,mwidth,mheight);

		$(".fotograma1Extra").each(function( index ) {
			moveLayer(this.id,0,0);
			clipLayer2(this.id,0,0,mwidth,mheight);


		});
		moveLayer("fotogramaIdBase1",0,0);
		clipLayer2("fotogramaIdBase1",0,0,mwidth,mheight);

	}
}

function createLayer(name, inleft, intop, width, height, visible, content, zidx) {
	var layer;
	_zidx = zidx || 1;
	$("#map").append('<div id="' + name + '" style="position:absolute; overflow:hidden; left:' + inleft + 'px; top:' + intop + 'px; width:100%; height:100%;' + '; z-index:' + _zidx + '; visibility:' + (visible ? 'visible;' : 'hidden;') + '">'+
	content+'</div>');
}

function createMapContainer(name, content, itop, ileft, iwidth, iheight, border, mapclass) {
	if(!isIE) {
		iwidth = iwidth - (2*border);
		iheight = iheight - (2*border);
	}
	$("#map").append('<div id="' + name + '" class="' + mapclass + '" style="position:absolute; overflow:hidden; left:' + ileft + 'px; top:' + itop + 'px; visibility:visible;border-width:' +border +'">'+
	content+'</div>');
	mapContainerCreated= true;
	if (typeof document.getElementById("mapContainer") != 'undefined') {
		widthh=$("#map").width();

		heightt = $("#map").height();
		$("#mapContainer").css('height', heightt);
		$("#mapContainer").css('width', widthh);
	}
}

function createMapContainer_deprecated(name, content, itop, ileft, iwidth, iheight, border, mapclass){
	if(!isIE){
		iwidth = iwidth - (2*border);
		iheight = iheight - (2*border);
	}
	document.writeln('<div id="' + name + '" class="' + mapclass + '" style="position:absolute; overflow:hidden; left:' + ileft + 'px; top:' + itop + 'px; visibility:visible;border-width:' +border +'">');
	document.writeln(content);
	document.writeln('</div>');
	mapContainerCreated= true;
}

/*function updateMapContainer(name, iwidth, iheight) {
	if(!isIE) {
		iwidth = iwidth - (2*borderr);
		iheight = iheight - (2*borderr);
	}
	document.getElementById(name).style.width = iwidth + "px";
	document.getElementById(name).style.height = iheight + "px";
}*/

function getLayer(name) {
	var theObj = document.getElementById(name);
	if (theObj!=null) {
		return theObj.style;
	} else {
		return(null);
	}
}


function isVisible(name) {
	var layer = getLayer(name);
	if (isNav && layer.visibility == "show") {
		return(true);
	}
	if (isIE && layer.visibility == "visible") {
		return(true);
	}
	return(false);
}

function getXY(e) {
	if (isNav) {
		theX=Math.floor(e.pageX-$("#mapContainer").offset().left);
		theY=Math.floor(e.pageY-$("#mapContainer").offset().top);
	} else {
		var restar = (typeof document.getElementById("mapContainer")!='undefined');
		theX=Math.floor(event.clientX + document.body.scrollLeft-(restar ? $("#mapContainer").offset().left:0));
		theY=Math.floor(event.clientY + document.body.scrollTop-(restar ?$("#mapContainer").offset().top:0));
	}

	return false;
}
function getXY2(e) {
	if (isNav) {
		theX=Math.floor(e.pageX-$("#mapContainer").offset().left);
		theY=Math.floor(e.pageY-$("#mapContainer").offset().top);
			//dportoles: chapucilla para que este ok
		theX = theX - mleft;
		theY = theY - mtop;
	} else {
		var restar = (typeof document.getElementById("mapContainer")!='undefined');
		theX=Math.floor(event.clientX + document.body.scrollLeft-(restar ? $("#mapContainer").offset().left:0));
		theY=Math.floor(event.clientY + document.body.scrollTop-(restar ?$("#mapContainer").offset().top:0));
			//dportoles: chapucilla para que este ok
		theX = theX - mleft - borderr;
		theY = theY - mtop - borderr;
	}

	return false;
}
function showZoomBox(left, top, right, bottom) {
	clipLayer("zoomboxTop",left,top,right,bottom);
	clipLayer("zoomboxLeft",left,top,right,bottom);
	clipLayer("zoomboxRight",left,top,right,bottom);
	clipLayer("zoomboxBottom",left,top,right,bottom);
	showLayer("zoomboxTop");
	showLayer("zoomboxLeft");
	showLayer("zoomboxRight");
	showLayer("zoomboxBottom");
}
function hideZoomBox()
{
	window.scrollTo(0,0);
	hideLayer("zoomboxTop");
	hideLayer("zoomboxLeft");
	hideLayer("zoomboxRight");
	hideLayer("zoomboxBottom");
}


function moveLayer(name, x, y) {
	var layer = getLayer(name);
	layer.left = x + "px";
	layer.top = y + "px";
}


function setLayerBackgroundColor(name, color) {
	var layer = getLayer(name);
	layer.backgroundColor = color;
}


function hideLayer(name) {
	var layer = getLayer(name);
	layer.visibility = "hidden";
}


function showLayer(name) {
	var layer = getLayer(name);
	layer.visibility = "visible";
}


function clipLayer2(name, clipleft, cliptop, clipright, clipbottom) {
	var layer = getLayer(name);
	layer.clip = 'rect(' + cliptop + 'px ' + clipright + 'px ' + clipbottom + 'px ' + clipleft +'px)';
}

function clipLayer(name, clipleft, cliptop, clipright, clipbottom) {
	var layer = getLayer(name);
	var newWidth = clipright - clipleft;
	var newHeight = clipbottom - cliptop;
	layer.height = newHeight + "px";
	layer.width = newWidth + "px";
	layer.top = cliptop + "px";
	layer.left = clipleft + "px";
}
function panClipLayer(name, clipleft, cliptop, clipright, clipbottom) {
	var layer = getLayer(name);
	if (layer!=null) {
		layer.clip = 'rect(' + cliptop + 'px ' + clipright + 'px ' + clipbottom + 'px ' + clipleft +'px)';
	}
	return false;
}
function setClip() {
	var inSize = 3;
	var lSize = parseInt(inSize) - 1;
	if (lSize < 1) lSize = 1;
	var tempX=x1;
	var tempY=y1;
	if (x1>x2) {
		zright=x1;
		zleft=x2;
	} else {
		zleft=x1;
		zright=x2;
	}
	if (y1>y2) {
		zbottom=y1;
		ztop=y2;
	} else {
		ztop=y1;
		zbottom=y2;
	}
	if ((x1 != x2) && (y1 != y2)) {
		clipLayer("zoomboxTop",zleft,ztop,zright,ztop+lSize);
		clipLayer("zoomboxLeft",zleft,ztop,zleft+lSize,zbottom);
		clipLayer("zoomboxRight",zright-lSize,ztop,zright,zbottom);
		clipLayer("zoomboxBottom",zleft,zbottom-lSize,zright,zbottom);
	}
	return false;
}

var isMoving = false;
var idx, idy;
var idLayer;
/*
function startMove(e, divId) {
	idLayer = document.getElementById(divId);
	isMoving = true;
	getXY(e);
	idx = theX - idLayer.offsetLeft;
	idy = theY - idLayer.offsetTop;
	document.onmousemove = updateMove;
	document.onmouseup = stopMove;
}

function updateMove(e) {
	if(!isMoving || (idLayer == null)) return;
	getXY(e);
	idLayer.style.left = theX - idx;
	idLayer.style.top = theY - idy;
	if (idLayer.offsetLeft < 0)
		idLayer.style.left = 0;
	if (idLayer.offsetTop < 0)
		idLayer.style.top = 0;
}

function stopMove() {
	if(!isMoving || (idLayer == null)) return;
	idLeft = idLayer.offsetLeft;
	idTop = idLayer.offsetTop;
	isMoving = false;
	idLayer = null;

}
*/

// incluyo esta funcion
// Returns map scale assuming 96 dpi
// If formated == true then it is returned as a string in the format "1:x,xxx"
// Otherwise an integer is returned
function getMapScale(formated) {
	return (getMapScale2(formated, minx, maxx, mwidth));
}

function getMapScale2(formated, _minx, _maxx, _mwidth) {
	var tempScale = (_maxx - _minx) / _mwidth;
	var tempFactor = 96;
	var inchPerMeter = 39.4;

	if (formated) {
		var scaleValue = parseInt((tempScale * tempFactor * inchPerMeter) + 0.5);
		var scale = scaleValue.toString(10);

		var formatedScale = "";
		var scaleLength = scale.length;

		if (scaleLength > 3) {
			var rightPos = scaleLength;
				// The first three numbers on the right
			formatedScale = scale.substring(rightPos - 3, rightPos);
				// Now all the bits in the middle
			for (rightPos -= 3; rightPos > 3; rightPos -= 3) {
				formatedScale = scale.substring(rightPos - 3, rightPos) + "." + formatedScale;
			}
				// Finally the header
			formatedScale = scale.substring(0, rightPos) + "." + formatedScale;
		} else {
			formatedScale = scale;
		}
		return "1:" + formatedScale;
	} else {
		return parseInt((tempScale * tempFactor * inchPerMeter) + 0.5);
	}
}
