var obl,orl,tl,bl,stl,sl,ml;
var overviewMapControl;
var selectControl, drawControl,dragBoxControl, modifyControl,dragZoomControl, contextMenuCorte, contextMenuUnion, snapControl;
var drawingLayer,wfs_layer;
var areaMeasureListener, pathMeasureListener;
var mapOL;
var resoluciones =[ 1750000/DOTS_PER_M, 750000/DOTS_PER_M, 200000/DOTS_PER_M,100000/DOTS_PER_M,25000/DOTS_PER_M,10000/DOTS_PER_M,5000/DOTS_PER_M,1000/DOTS_PER_M,100/DOTS_PER_M ]; 
var bbox_aragon = [571580, 4412223, 812351,
                   4756639];
proj4.defs('EPSG:4326', '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees');	
Proj4js.defs['EPSG:4326']='+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees';
Proj4js.defs["EPSG:25830"] = "+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs";
proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs");
Proj4js.defs["EPSG:23030"]="+title=23030 +proj=utm +zone=30 +ellps=intl +units=m +no_defs ";
Proj4js.defs['urn:ogc:def:crs:EPSG::25830']= Proj4js.defs['EPSG:25830'];
proj4.defs('EPSG:23030', "+title=23030 +proj=utm +zone=30 +ellps=intl +units=m +no_defs ");

var singleclickEvent;

function muestraLayerLoading() {
	showLoading(true);
	setTimeout("ocultaLayerLoading()", 30000);
}
function ocultaLayerLoading() {
	showLoading(false);

}
function prevZoom(){
	navHistory.previousTrigger();
}



/**
 * inicializa el mapa, con las capas de fondo
 */
function initMap() {
	//OpenLayers.IMAGE_RELOAD_ATTEMPTS = 5;
	initMapButtons();
	showLoading(true);
	var map_projection = new ol.proj.Projection({
		code:WMS_SRS,
		units: 'm',
		extent:[WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX]// necesario para que se vea el overview
	});
	var options = {
			projection : map_projection,
//			maxExtent : new OpenLayers.Bounds(WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX),
			center : [675533, 4589000],
			resolutions : resoluciones/*,
			           controls : [ new OpenLayers.Control.ArgParser()
			                       ]*/

	};
	//attribution = getFondoLabel()+base_attribution;
	mapOL = new ol.Map({
		controls: [],
		target: $('#map')[0],
		view: new ol.View(options)
	});
	mapOL.addInteraction(new ol.interaction.MouseWheelZoom());
	if ((typeof SAT_SOURCES !='undefined') && SAT_SOURCES.length >0){
		stl =new ol.layer.Tile({
			source: SAT_SOURCES[0]
		});
		stl.set("wms_url",SATELITE_WMTS_URL_WMS);
		stl.set("wms_layer",SATELITE_WMTS_LAYER);
		
	}
	else{
	try{
		$.ajax({
			url: SATELITE_WMTS_URL+"?REQUEST=GetCapabilities",
			type: "GET",
			async: false,
			success:  function(capabilities){
				var parser = new ol.format.WMTSCapabilities();
				var result = parser.read(capabilities);
				var options = ol.source.WMTS.optionsFromCapabilities(result,
						{layer: SATELITE_WMTS_LAYER, matrixSet: SATELITE_WMTS_GRIDSET, format: 'image/jpeg'});

				stl =new ol.layer.Tile({
					source: new ol.source.WMTS(options)
				});
				stl.set("wms_url",SATELITE_WMTS_URL_WMS);
			},
			timeout:5000,
			error:  function(  jqXHR,  textStatus,  errorThrown) { 
				console.log("Error al obtener el capabilities de "+wmts_url+" "+textStatus +"\n "+errorThrown);
				console.log("No se ha podido cargar el fondo WMTS, se carga el WMS")
				stl =new ol.layer.Image({
					source: new ol.source.ImageWMS({
						params: {'LAYERS': SATELITE_WMS_LAYERS[0],'VERSION':'1.1.1','FORMAT':'image/jpeg'},
						url: SATELITE_WMS_URL[0],
						projection: map_projection
					})
				});
				
			},
			contentType: "text/xml",
			dataType: "text"

		});	
	}
	catch(err){
		console.log("No se ha podido cargar el fondo WMTS, se carga el WMS:"+err);
		stl =new ol.layer.Image({
			source: new ol.source.ImageWMS({
				params: {'LAYERS': SATELITE_WMS_LAYERS[0],'VERSION':'1.1.1','FORMAT':'image/jpeg'},
				url: SATELITE_WMS_URL[0],
				projection: map_projection
			})
		});
	}
	}
	orl = new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': ORTO_WMS_LAYERS[0],'VERSION':'1.1.1','FORMAT':'image/jpeg'},
			url: ORTO_WMS_URL[0],
			projection: map_projection
		})
	});
	orl.setVisible(false);
	obl =new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': ORTO_WMS_LAYERS[0],'VERSION':'1.1.1','FORMAT':'image/jpeg'},
			url: ORTO_WMS_URL[0],
			projection: map_projection
		})
	});
	obl.setVisible(false);
	obl.setOpacity(0.3);
	bl = new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': BASICO_WMS_LAYERS[0],'VERSION':'1.1.1',
				'TRANSPARENT':true,
				'FORMAT':"image/png"},
				url: BASICO_WMS_URL[0],
				projection: map_projection
		})
	});
	bl.setOpacity(0.5);
	//  bl.setVisible(false);
	var topo_layer = TOPO_WMS_LAYERS;
	try{
		topo_layer = TOPO_WMS_LAYERS[0];
	}
	catch(err){
		console.log("DEPRECATED: definir TOPO_WMS_LAYERS como vector");
	}
	tl = new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': topo_layer,'VERSION':'1.1.1','FORMAT':'image/jpeg'},
			url: TOPO_WMS_URL[0],
			projection: map_projection
		})
	});
	tl.setVisible(false);

	//stl.setVisible(false);
	var proj23030 = ol.proj.get('EPSG:23030');
	proj23030.setExtent([WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX]);
	sl = new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': SOMBRA_WMS_LAYERS[0],'VERSION':'1.1.1',
				'STYLES':SOMBRA_WMS_STYLES[0]},
				url:  SOMBRA_WMS_URL[0],
				projection:map_projection
		})
	});

	sl.setVisible(false);
	ml = new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': MDT_WMS_LAYERS[0],'VERSION':'1.1.1',
				'FORMAT':"image/gif"},
				url:  MDT_WMS_URL[0],
				projection:map_projection
		})
	});

	ml.setVisible(false);
	mapOL.addLayer(stl);
	mapOL.addLayer(sl);
	mapOL.addLayer(orl);
	mapOL.addLayer(tl);
	mapOL.addLayer(ml);
	mapOL.addLayer(obl);
	mapOL.addLayer(bl);

	mapOL.getView().on('change:resolution',function(e){


		updateAttribution();
		setScaleBar();
		setNewScale(mapOL.getView().getResolution()*DOTS_PER_M);

		checkMinScale(toc.getEditingLayer());
		return true;
	},mapOL);

	var controlOptions = {
			projection : map_projection,
			center : [675533, 4589000],
			maxResolution: (OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130,
			minResolution:(OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130/*,
	    			maxZoom:0,
	    			minZoom:0*/

	};
	var overviewLayer = new ol.layer.Image(
			{
				source: new ol.source.ImageStatic({url:'/lib/IDEAragon/themes/default/images/mapaOverview.gif',
					imageExtent:[OVERVIEW_BOUND_X_MIN, OVERVIEW_BOUND_Y_MIN, OVERVIEW_BOUND_X_MAX,
					             OVERVIEW_BOUND_Y_MAX],
					             imageSize:[130,140]})
			}
	);

	overviewMapControl = new ol.control.OverviewMap({
		target:$('#overview_map')[0],
		view:new ol.View(controlOptions),
		collapsed:true,
		collapsible:true,
		// className: 'ol-overviewmap ol-custom-overviewmap',
		layers: [overviewLayer]
	});

	/* overview1.isSuitableOverview = function() {
	            return true;
	        };*/
	mapOL.addControl(overviewMapControl);
	attributionControl=new ol.control.Attribution({
		collapsible: false,

		collapsed: false,

	});
	attributionControl = new ol.control.Attribution({collapsible:false, collapsed:false});
	mapOL.addControl(attributionControl);  
	mapOL.addControl(new ol.control.MousePosition({coordinateFormat:  function(pos){
		comboSRS = document.getElementById('cboSRSClient');
		if (comboSRS.selectedIndex<0){
			comboSRS.setSelectedIndex(0);
		}
		theSRS = comboSRS.options[comboSRS.selectedIndex].value;

		theX = pos[0];
		theY = pos[1];
		var latLon = coordTransformProj4s("EPSG:25830", "EPSG:4258", theX, theY);
		// convertir del 25830 al srs elegido
		if (theSRS != "EPSG:25830") {
			destino = coordTransformProj4s("EPSG:25830", theSRS, theX, theY);
			theX = destino[0];
			theY = destino[1];
		}


		document.getElementById("Lx").value = theX.toFixed(2);
		document.getElementById("Ly").value = theY.toFixed(2);
		theLon = latLon[0].toFixed(8);
		theLat = latLon[1].toFixed(8);

		document.getElementById("Llat").value = theLat;
		document.getElementById("Llon").value = theLon;
		return "";
	}
	}));



	//map.addControl(new ol.control.ScaleLine());
	ScaleBarControl = function(opt_options) {

		var options = opt_options || {};

		var scaleBar = document.createElement('scaleBarDiv');
		scaleBar.innerHTML =base_attribution;



		ol.control.Control.call(this, {
			element: scaleBar,
			target: options.target
		});

	};
	ol.inherits(ScaleBarControl, ol.control.Control);
	mapOL.addControl(new ScaleBarControl());
	/*navHistory = new OpenLayers.Control.NavigationHistory();
	map.addControl(navHistory);
	 */
	var source = new ol.source.Vector({features: []});
	markers= new ol.layer.Vector({  source: source});
	// markers = new OpenLayers.Layer.Markers( "Markers" );
	mapOL.addLayer(markers);

	areaMeasure = new ol.interaction.Draw({
		source: source,
		type: 'Polygon'

	});
	areaMeasure.setActive(false);
	areaMeasure.on('drawstart',
			function(evt) {
		// set sketch
		removeMarkers();
		sketch = evt.feature;


		areaMeasureListener = sketch.getGeometry().on('change', function(evt) {
			updateAreaMeasure(evt.target);

		});
	}, this);
	areaMeasure.on('drawend',
			function(evt) {
		ol.Observable.unByKey(areaMeasureListener);
	}, this);
	mapOL.addInteraction(areaMeasure);
	pathMeasure = new ol.interaction.Draw({
		source: source,
		type: 'LineString'

	});
	pathMeasure.setActive(false);
	pathMeasure.on('drawstart',
			function(evt) {
		// set sketch
		removeMarkers();
		sketch = evt.feature;


		pathMeasureListener = mapOL.on('singleclick',function(evt) {
			updatePathMeasure(sketch.getGeometry());

		});
	}, this);
	pathMeasure.on('drawend',
			function(evt) {
		ol.Observable.unByKey(pathMeasureListener);
	}, this);
	mapOL.addInteraction(pathMeasure);



	mapOL.on('pointermove', function(e) {

		var pixel = mapOL.getEventPixel(e.originalEvent);
		var hit = mapOL.hasFeatureAtPixel(pixel);
		mapOL.getTarget().style.cursor = hit ? 'pointer' : '';
	});

	dragZoomControl= new ol.interaction.DragZoom ({
		condition:  function(mapBrowserEvent) {
			return document.getElementById("zoomRectTool").checked;
		}      
	});
	mapOL.addInteraction(dragZoomControl);


	initMapDialogs();


	updateZoom();
	initZoombar();

	showLoading(false);
}

function initZoombar(){
	pintaSliderMap();
}

function updateZoom(){
	if (resized){
		resizeMap();
		showAragon();
	}
	else{
		setTimeout(updateZoom,200);
	}
}

function updateAttribution(){
	if (document.getElementById('bgFotoShowId')) {
		if (document.getElementById('bgFotoShowId').checked) {
			//return textoFondoFoto;//(idxWMSFondoFoto);
			var idx = document.getElementById("ortoSelector").selectedIndex;
			if ((idx<0)||(idx >textoFondoFoto.length)){
				idx=0;
			}

			orl.getSource().setAttributions( getFondoLabel(textoFondoFoto[idx]));
		} else if (document.getElementById('bgMapaShowId').checked) {
			tl.getSource().setAttributions( getFondoLabel(getTextoFondoMapa()));
		} else if (document.getElementById('satelite').checked) {
			var fondo = getFondoLabel(getTextoFondoSatelite());
			stl.getSource().setAttributions( fondo);
			obl.getSource().setAttributions( fondo);
		} 
		else if (document.getElementById('sombreado').checked) {
			sl.getSource().setAttributions(getFondoLabel(textoFondoSombreado));
		} else if (document.getElementById('mdt').checked) {
			var fondo =getFondoLabel( textoFondoMDT);
			ml.getSource().setAttributions(fondo);
			obl.getSource().setAttributions("");
		} 
		else {
			bl.getSource().setAttributions(getFondoLabel(textoFondoMudo));
		}
	} else {
		orl.getSource().setAttributions( getFondoLabel( textoFondoFotoDefault));
	}

	attributionControl.changed();
	// $(".customCBox").chosen({ disable_search: true});



}

function changeScaleFondoSatelite(escalaActual){
	var mostrarOrto=false;
	mostrarOrto =(escalaActual <= textoFondoSateliteNivel[0]);

	stl.setVisible(!mostrarOrto);
	obl.setVisible(mostrarOrto);
	if (mostrarOrto){

		obl.setOpacity(1);

	}
}
function updateBaseLayer(){


	if (document.getElementById("bgFotoShowId").checked){
		obl.setVisible(false);
		orl.setOpacity(1);
		orl.setVisible(true);
		tl.setVisible(false);
		sl.setVisible(false);
		stl.setVisible(false);

		bl.setOpacity(0.5);
		bl.setVisible(true);
		ml.setVisible(false);
	}
	else if (document.getElementById("bgMudoShowId").checked){
		obl.setVisible(false);
		orl.setVisible(false);
		tl.setVisible(false);
		sl.setVisible(false);
		stl.setVisible(false);
		bl.setOpacity(1);
		ml.setVisible(false);
	}
	else if (document.getElementById("satelite").checked){
		var escalaActual = getMapScale(false);

		obl.setVisible(false);
		orl.setVisible(false);
		tl.setVisible(false);
		sl.setVisible(false);
		changeScaleFondoSatelite(escalaActual);
		bl.setOpacity(0.5);
		bl.setVisible(true);
		ml.setVisible(false);
	}
	else if (document.getElementById("bgMapaShowId").checked){
		obl.setVisible(false);
		orl.setVisible(false);
		tl.setVisible(true);
		sl.setVisible(false);
		stl.setVisible(false);
		bl.setOpacity(0.5);
		bl.setVisible(true);
		ml.setVisible(false);
	}
	else if (document.getElementById("sombreado").checked){
		obl.setVisible(false);
		orl.setVisible(false);
		tl.setVisible(false);
		sl.setVisible(true);
		stl.setVisible(false);
		bl.isBaseLayer = false;
		bl.setOpacity(0.5);
		bl.setVisible(true);
		ml.setVisible(false);


	}
	else if (document.getElementById("mdt").checked){
		orl.setVisible(false);
		obl.setVisible(false);
		tl.setVisible(false);
		sl.setVisible(false);
		stl.setVisible(false);
		ml.setVisible(true);
		bl.setOpacity(0.5);
		bl.setVisible(true);
		obl.setOpacity(0.3);
		obl.setVisible(true);
	}
	updateAttribution();
}
function updatePath(){
	units=document.getElementById("unidadDist").value;
	if ((lastPathUnits =="m")&&(units=="Km")){
		document.getElementById("segmento").value=(document.getElementById("segmento").value/1000).toFixed(2);
		document.getElementById("totalPath").value=(document.getElementById("totalPath").value/1000).toFixed(2);
	}
	else if((lastPathUnits =="Km")&&(units=="m")){
		document.getElementById("segmento").value=(document.getElementById("segmento").value*1000).toFixed(2);
		document.getElementById("totalPath").value=(document.getElementById("totalPath").value*1000).toFixed(2);
	}
	lastPathUnits=units;
}

function updateArea(){
	units=document.getElementById("unidadArea").value;
	if ((lastAreaUnits =="m2")&&(units=="Km2")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value/1000000).toFixed(2);
	}
	else if((lastAreaUnits =="Km2")&&(units=="m2")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value*1000000).toFixed(2);
	}
	else if((lastAreaUnits =="Ha")&&(units=="m2")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value*10000).toFixed(2);
	}
	else if((lastAreaUnits =="m2")&&(units=="Ha")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value/10000).toFixed(2);
	}
	else if((lastAreaUnits =="Ha")&&(units=="Km2")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value/100).toFixed(2);
	}
	else if((lastAreaUnits =="Km2")&&(units=="Ha")){
		document.getElementById("totalArea").value=(document.getElementById("totalArea").value*100).toFixed(2);
	}

	lastAreaUnits=units;
}
function updateAreaMeasure(poly){
	lastAreaUnits=document.getElementById("cboAreaScale").value;
	if ((document.getElementById("cboAreaScale").value=="Km2")){
		document.getElementById("theAreaMedTotal").value=(poly.getArea()/1000000).toFixed(2);
	}
	else if  ((document.getElementById("cboAreaScale").value=="Ha")){
		document.getElementById("theAreaMedTotal").value=(poly.getArea()/10000).toFixed(2);
	}

	else{
		document.getElementById("theAreaMedTotal").value=poly.getArea().toFixed(2);
	}
}
function updatePathMeasure(line){
	lastPathUnits=document.getElementById("cboLineScale").value;
	anterior = document.getElementById("medTotal").value;
//	alert("anterior "+anterior);
	if ((document.getElementById("cboLineScale").value=="Km"))
		medida = (line.getLength()/1000).toFixed(2);

	else
		medida = line.getLength().toFixed(2);
//	alert("segmento "+(medida-anterior))
	if ((anterior>0)&&(medida-anterior>0)){
		document.getElementById("medSegmento").value=(medida-anterior).toFixed(2);
	}
	else{
		document.getElementById("medSegmento").value=medida;
	}
	document.getElementById("medTotal").value=medida;

}
function measurePath(){

	if (document.getElementById("distance").checked){
		showSelTool(true,"distance-icon");
		if (selectControl)
			selectControl.setActive(false);
		if (singleclickEvent){
			mapOL.unByKey(singleclickEvent);
		}
		pathMeasure.setActive(true);

		document.getElementById("area").checked=false;
		$("#area").button("refresh");
		areaMeasure.setActive(false);
		$( "#measureArea" ).dialog( "close" );
		$( "#measurePath" ).dialog( "open" );

		//	updateAttribution(attribution_bak);
		//	setScaleBar();
	}
	else{
		$( "#measurePath" ).dialog( "close" );
		pathMeasure.setActive(false);
	}


}

function measureArea(){

	if (document.getElementById("area").checked){
		showSelTool(true,"area-icon");
		if (selectControl)
			selectControl.setActive(false);
		if (singleclickEvent){
			mapOL.unByKey(singleclickEvent);
		}
		areaMeasure.setActive(true);
		//
		document.getElementById("distance").checked=false;
		$("#distance").button("refresh");
		pathMeasure.setActive(false);
		$( "#measurePath" ).dialog( "close" );
		$( "#measureArea" ).dialog( "open" );

		//updateAttribution(attribution_bak);
		setScaleBar();
	}
	else{
		$( "#measureArea" ).dialog( "close" );
		areaMeasure.setActive(false);
	}

}

function limpiarMedicionDistancia() {
	try{
		pathMeasure.finishDrawing();
	}catch(err){}
	removeMarkers();
	document.getElementById("medTotal").value = "";
	document.getElementById("medSegmento").value = "";

}

function limpiarMedicionArea() {
	try{
		areaMeasure.finishDrawing();
	}catch(err){}
	removeMarkers();
	document.getElementById("theAreaMedTotal").value = "";

}
function closeMeasureArea(){

	closeMeasureAreaWin();
	$("#measureArea").dialog('close'); 
}
function closeMeasurePath(){

	closeMeasurePathWin();
	$("#measurePath").dialog('close'); 
}
function closeMeasureAreaWin(){
	limpiarMedicionArea();
	areaMeasure.setActive(false);

}
function closeMeasurePathWin(){
	limpiarMedicionDistancia();
	pathMeasure.setActive(false);

}
function updateScale(){


	newValue = document.getElementById('scaleTextBox').value;
	if (! isNaN(newValue)) {
		if (newValue >= 0) {
			mapOL.getView().setResolution(newValue/DOTS_PER_M);
			return true;
		}
	} 
	setScaleBar();
	return true;

}

function showAragon(){
	fullExtent();
	try{
		overviewMapControl.setCollapsed(false);
		overviewMapControl.setCollapsible(false);
	}
	catch(err){
		console.log(err);
	}
	if (mapOL.getView().getZoom()==0){
		var bounds = mapOL.getView().calculateExtent(mapOL.getSize());
		if (!ol.extent.containsExtent(bounds,bbox_aragon)){

			resoluciones[0]=resoluciones[0]+(250000/DOTS_PER_M);

			mapOL.getView().resolutions=resoluciones;
			mapOL.getView().changed();
			//initMap();

			showAragon();
		}
	}
}
/**
 * Ajusta el zoom del mapa para ir a la vista completa de Arag�n
 */
function fullExtent() {
	fitMap(bbox_aragon); 

}
function fitMap(bbox){
	mapOL.getView().fit(bbox, mapOL.getSize());
}






function showSearchResult(parameters){
	selectionWhere=getInitialParam(parameters, "QUERY=");
	idSelectionLayer = getInitialParam(parameters, "ACTIVELAYER="); // la que sea
	var wfsservice = getInitialParam(parameters, "QUERYSERVICE=");
	try{
		if((wfsservice =="")&&queryService[idSelectionLayer]){
			wfsservice=queryService[idSelectionLayer];
			idSelectionLayer=idSelectionLayer.toUpperCase();
		}
	}
	catch(err){
		console.log(err);
	}
	if (wfsservice !=""){
		queryUrl = server + "/geoserver/"+wfsservice + "/ows";
	}else{
		queryUrl=url;
	}
	if (selectionWhere){
		$.ajax({
			url: queryUrl+'?service=WFS&' +
			'version=1.0.0&request=GetFeature&typename='+idSelectionLayer+'&' +
			'outputFormat=application/json&srsname=EPSG:25830&' +
			'CQL_FILTER='+selectionWhere,
			type: 'GET',
			success:  function(data){
				$("#tabs").tabs("option", "active", 0);
				var format = new ol.format.GeoJSON();
				var features = format.readFeatures(data);
				if (features.length>0){
					if (wfsservice=="DAGMA_INCENDIOS"){
						showEditionForm(idSelectionLayer,idSelectionLayer,features[0],true);
					}
					else{
						var geom = features[0].getGeometry();
						var buffer=defaultBuffer;
						if (geom.getType()=='Point') {
							buffer=bufferPoints;
						}

						mapOL.getView().fit(ol.extent.buffer( features[0].getGeometry().getExtent(),buffer), mapOL.getSize()); 

						var source = markers.getSource();
						source.clear(true);
						source.addFeature(features[0]);
						source.refresh();		
					}
				}
				else{
					alert("No se ha encontrado el objeto con la condicion "+selectionWhere+" en la capa "+idSelectionLayer);
				}
			},
			error:  function(  jqXHR,  textStatus,  errorThrown) { 

				alert("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
			}


		});	
	}
	else{



		bbox = getInitialParam(parameters,"BOX=").split(":");
		$("#tabs").tabs("option", "active", 0);
		mapOL.getView().fit(bbox, mapOL.getSize()); 

		markerx = getInitialParam(parameters, "MARKERX=");
		markery = getInitialParam(parameters, "MARKERY=");
		if ((markerx!="")&&(markery!="")){

			var source = markers.getSource();
			source.clear(true);
			var point = new ol.Feature({
				geometry:  new ol.geom.Point([markerx, markery])
			});
			point.setStyle(new ol.style.Style({image:new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})}));
			source.addFeature(point);
			source.refresh();		

		}
	}
}

function removeMarkers(){
	markers.getSource().clear(true);
	try{
		drawingLayer.getSource().clear(true);
	}
	catch(err){
		// no estar� creada la capa de dibujo
	}
}


function changeBaseLayerOption(buttonId){
	document.getElementById(buttonId).checked=true;
	$("#"+buttonId).button("refresh");
	if (document.getElementById("bgFotoShowId").checked){
		var idx = document.getElementById("ortoSelector").selectedIndex;
		if ((idx<0)||(idx >ORTO_WMS_URL.length)){
			idx=0;
		}
		if ((orl.getSource().url != ORTO_WMS_URL[idx])||(orl.getSource().get('LAYERS') != ORTO_WMS_LAYERS[idx])){
			orl.getSource().setUrl(ORTO_WMS_URL[idx]);
			var wmsparams= orl.getSource().getParams();
			wmsparams['LAYERS']=ORTO_WMS_LAYERS[idx];
			orl.getSource().updateParams(wmsparams);
			orl.getSource().refresh();

		}
	}
	else if (document.getElementById("bgMapaShowId").checked){
		var idx = document.getElementById("topoSelector").selectedIndex;
		if ((idx<0)||(idx >TOPO_WMS_URL.length)){
			idx=0;
		}
		if ((tl.getSource().url != TOPO_WMS_URL[idx])||(tl.getSource().get('LAYERS') != TOPO_WMS_LAYERS[idx])){
			tl.getSource().setUrl(TOPO_WMS_URL[idx]);
			var wmsparams= tl.getSource().getParams();
			wmsparams['LAYERS']=TOPO_WMS_LAYERS[idx];
			tl.getSource().updateParams(wmsparams);
			tl.getSource().refresh();

		}
	}
	else if ($("#satSelector") && document.getElementById("satelite").checked){
		var idx = document.getElementById("satSelector").selectedIndex;
		if ((idx<0)||(idx >SAT_SOURCES.length)){
			idx=0;
		}
		stl.setSource(SAT_SOURCES[idx])
	}
	updateBaseLayer();
}

function getMapScale(){
	res= mapOL.getView().getResolution() * DOTS_PER_M; 
	return res;
}

function updateZoomBar(){
	needScaleUpdate = false;
	var zoomLevel=mapOL.getView().getZoom();
	$( "#zoomSlider" ).slider("option","value",zoomLevel);

}
/**
 * Establece en la barra de escala los valores correspondientes a la escala de
 * visualizaci�n del mapa en este momento
 */
function setScaleBar() {
	setScaleBarOL();
	setTimeout(updateZoomBar(),100);
}

/**
 * ajusta la vista del mapa en funci�n
 * del tama�o de la pesta�a
 */
function resizeMap(){
	if ((mapHeight != $("#map").height()) || (mapWidth != $("#map").width())) { // ha
		// cambiado
		// el
		// tama�o
		// del
		// mapa
		mapHeight = $("#map").height();
		mapWidth = $("#map").width();
		$("#map").css('max-height', mapHeight);
		$("#map").css('height', mapHeight);
		//	map.setSize([mapWidth,mapHeight]);
		mapOL.updateSize();
		/*map
		.zoomToExtent(new OpenLayers.Bounds(571580, 4412223, 812351,
				4756639));*/
	}
}

/**
 * invocado al activarse la pesta�a del mapa, despues de un cambio en el tama�o
 * de la ventana Ajusta el mapa.
 */
function onActivateMap() {
	if ($("#tabs").tabs("option", "active") == 0) {
		resizeMap();
	}
	// quito la captura del evento para q solo se ajuste la primera
	// vez
	$("#tabs").tabs("option", "activate", null);
}


function coordTransformProj4s(srsOrigen, srsDestino, coordX, coordY) {
	destino = new Array();
	if (srsOrigen == srsDestino) {
		destino[0] = coordX;
		destino[1] = coordY;
		return destino;
	}
	else {
		var source = new Proj4js.Proj(srsOrigen);    //source coordinates
		var dest = new Proj4js.Proj(srsDestino);     //destination coordinates 

		// transforming point coordinates
		var p = new Proj4js.Point(coordX,coordY);   //any object will do as long as it has 'x' and 'y' properties
		Proj4js.transform(source, dest, p);      //do the transformation.  x and y are modified in place

		destino[0] = p.x;
		destino[1] = p.y;
		return destino;
	}
}


function checkDeformation( mwidth, mheight) {
	//var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);
	var bounds = mapOL.getView().calculateExtent(mapOL.getSize());

	var _minx = bounds[0];
	var _miny = bounds[1];
	var _maxx = bounds[2];
	var _maxy = bounds[3];
	var ratioTamano = mwidth / mheight;
	var diffX = _maxx - _minx;
	var diffY = _maxy - _miny;
	var ratioCoord = diffX / diffY;
//	alert("ratioT" + ratioTamano + "," + ratioCoord)
	if (ratioCoord > ratioTamano) {
		// estirar la Y
		//	alert("estirar la Y")

		var newDiffY = diffX / ratioTamano;

		var gapY = newDiffY - diffY;

		if (gapY > 0) {
			_miny = _miny - (gapY/2);
			_maxy = _maxy + (gapY/2);
		} else {
			_maxy = _miny - (gapY/2);
			_miny = _maxy + (gapY/2);
		}
	} else if (ratioCoord < ratioTamano){
		// estirar la X
		//alert("estirar la X")

		var newDiffX = ratioTamano * diffY;

		var gapX = newDiffX - diffX;
		if (gapX > 0) {
			_minx = _minx - (gapX/2);
			_maxx = _maxx + (gapX/2);
		} else {
			_maxx = _minx - (gapX/2);
			_minx = _maxx + (gapX/2);
		}
	}
	return _minx+","+ _miny+","+ _maxx+","+_maxy;
}

function zoomOutTool(){
	if (document.getElementById("zoomOutTool").checked){
		showSelTool(true,"zoom-out-icon");
		if (singleclickEvent){
			mapOL.unByKey(singleclickEvent);
		}
		singleclickEvent= mapOL.on('singleclick', function(evt) {
			mapOL.getView().setCenter(evt.coordinate);
			map.getView().setZoom(mapOL.getView().getZoom() - 1);
		});
	}
	else{

		showSelTool(false,"zoom-out-icon");
	}

}

function pan(ix, iy) {
	//	backupExtent();
	var extent = mapOL.getView().calculateExtent(mapOL.getSize());
	var center = mapOL.getView().getCenter();
	var dx = (extent[2] - extent[0])/mapOL.getSize()[0];
	var mx = dx*ix;
	var my = dx*iy;
	mapOL.getView().setCenter([center[0]+mx,center[1]+my]);

}
function initMapButtons() {
	$("#infoTool").button({
		text:false,
		icons : {
			primary : "info-icon"
		}
	});
	$("#zoomRectTool").button({
		text:false,
		icons : {
			primary : "zoom-rect-icon"
		}
	});
	$("#zoomOutTool").button({
		text:false,
		icons : {
			primary : "zoom-out-icon"
		}
	});
	$("#panTool").button({
		text:false,
		icons : {
			primary : "pan-icon"
		}
	});
	$("#toolsButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});

	$("#toolWinButton").button({
		icons : {
			text:false,
			primary : "right-arrow-icon"
		}
	});
	$("#baseLayerButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});
	$("#scaleButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});
	$("#notificarButton").button({
		icons : {
			primary : "arrow-icon"
		}
	});

	$("#fullExtentTool").button({
		text:false,
		icons : {
			primary : "full-extent-icon"
		}
	});

	$("#download").button({
		text:false,
		icons : {
			primary : "download-icon"
		}
	});
	$("#impShp").button({
		text:false,
		icons : {
			primary : "shp-icon"
		}
	});
	$("#impGPX").button({
		text:false,
		icons : {
			primary : "gpx-icon"
		}
	});
	$("#georref").button({
		text:false,
		icons : {
			primary : "edit-icon"
		}
	});

	$("#distance").button({
		text:false,
		icons : {
			primary : "distance-icon"
		}
	});
	$("#area").button({
		text:false,
		icons : {
			primary : "area-icon"
		}
	});
	$("#delete").button({
		text:false,
		icons : {
			primary : "delete-icon"
		}
	});

	$("#btnCleanMeasurePath").button({

		icons : {
			primary : "clean-icon"
		}
	});

	$("#btnCleanMeasureArea").button({

		icons : {
			primary : "clean-icon"
		}
	});

	$("#bgMudoShowId").button({icons : {
		primary : "basico-icon"}
	});
	$("#bgMapaShowId").button({icons : {
		primary : "topo-icon"}
	});
	$("#sombreado").button({icons : {
		primary : "sombreado-icon"}
	});
	$("#mdt").button({icons : {
		primary : "mdt-icon"}
	});
	$("#bgFotoShowId").button({icons : {
		primary : "orto-icon"}
	});
	$("#satelite").button({icons : {
		primary : "satelite-icon"}
	});
	document.getElementById("satelite").checked=true;
	$("#satelite").button("refresh");

	$( "#transpFondoSlider" ).slider({
		min: 0,
		max: 100,
		step: 1,
		slide:function( event, ui ) {
			setBaseLayersOpacity((100-ui.value)/100);
		},
		change: function( event, ui ) {
			setBaseLayersOpacity((100-ui.value)/100);
			showTranspWarning(ui.value) ;
		}
	});

	$( "#zoomSlider" ).slider({
		value:0,
		min: 0,
		max: 8,
		step: 1,
		orientation:"vertical",
		change: function( event, ui ) {
			if (needScaleUpdate){
				mapOL.getView().setZoom(ui.value);
				setNewScale(resoluciones[ui.value]*DOTS_PER_M);
			}
			needScaleUpdate=true;
		}
	});
	document.getElementById("infoTool").checked=true;
	$("#infoTool").button("refresh");
	showSelTool(true,"info-icon");
}

function setBaseLayersOpacity(opacity){
	obl.setOpacity(opacity);
	orl.setOpacity(opacity);
	tl.setOpacity(opacity);
	bl.setOpacity(opacity);
	stl.setOpacity(opacity);
	sl.setOpacity(opacity);
	ml.setOpacity(opacity);
}

