function getRoturas(numclases, valormin, valormax){
	var limites = new Array();
	var interval = Math.round((valormax-valormin)/numclases);
	var valor = valormin;
	for (var i=1;i<numclases;i++){
		valor=valor+interval;
		limites.push(valor);
	}
	return limites;
}
/**
 * devuelve los límites de las categorías de la leyenda, en función de los valores pasados como parámetro y el metodo seleccionado
 * por el usuario
 * @param values valores que se van a representar
 * @returns vector que contiene en la primera posición el límite inferior de la primera clase y en el resto los límites superiores
 * para todas las categorías
 */
function getLegendCategories(values){
	var num_classes = $("#numclases").val();
	return getLegendClasses(values,num_classes);
}

function getLegendClasses(values,num_classes){
	
	switch (document.getElementById("categories").selectedIndex){
	case 0: return getJenksBreaks(num_classes,values);
	break;
	case 1: return getPercentiles(getRoturas(num_classes,0,100), values);
	break;
	case 2: return getStdDevRanges(num_classes, values);
	break;
	case 3: return getUserRanges(num_classes, values);
	break;
	}
}
/**
 * devuelve un vector con los percentiles definidos en el vector percentiles
 * para el conjunto de valores del vector values. En la primera posición del
 * resultado devuelve el valor mínimo (menos 1 por posibles redondeos) del
 * vector values y en la última el máximo (más 1 por posibles redondeos)
 * 
 * @param percentiles
 *            vector con los percentiles que hay que calcular
 * @param values
 *            vector con los valores sobre los que hay que calcular los
 *            percentiles
 * @returns {Array} vector con los valores de percentil indicados. Además en la
 *          primera posición se incluye el valor mínimo -1 y en la última el
 *          máximo +1
 */
function getPercentiles(percentiles, values){
	var percValues = new Array();
	// ordenar los valores de menor a mayor
	values = values.sort(function(a,b){return a-b;});
	// poner en la primera posición el valor mínimo -1
	percValues.push(values[0]-1);
	// calcular los percentiles
	for (var i=0; i<percentiles.length; i++){
		// calcular la posición del vector correspondiente al percentil
		var index = (values.length*percentiles[i])/100;

		if (index == Math.floor(index)){ // si el valor es un entero, ese es
			// el índice a buscar
			index = Math.floor(index);
		}
		else{ // si el valor no es entero cojo el primer entero mayor
			index = Math.floor(index)+1;
		}
		percValues[i+1] = values[index];
	}
	// poner en la ultima posición el valor máximo + 1
	percValues.push(values[values.length-1]+1);

	return percValues;
}

function getUserRanges(num_classes, values){

	// calcular los límites
	var userValues = new Array();
	// poner en la primera posición el valor mínimo -1
	userValues.push(values[0]-1);
	// userValues los límites correspondientes a las desviaciones establecidas en
	// el vector
	for (var i=0; i<num_classes-1; i++){
		userValues.push($("#class"+i).val());
	}
	// poner en la ultima posición el valor máximo + 1
	userValues.push(values[values.length-1]+1);

	return userValues;


}
/**
 * devuelve un vector con la desviación estandar esablecida en el vector de
 * desviaciones para el conjunto de valores del vector values. En la primera
 * posición del resultado devuelve el valor mínimo (menos 1 por posibles
 * redondeos) del vector values y en la última el máximo (más 1 por posibles
 * redondeos)
 * 
 * @param range
 *            vector con las desviaciones que hay que calcular
 * @param values
 *            vector con los valores sobre los que hay que calcular las
 *            desviacones
 * @returns {Array} vector con los valores de correspondientes a las
 *          desviaciones indicadas. Además en la primera posición se incluye el
 *          valor mínimo -1 y en la última el máximo +1
 */
function getStdDevRanges(num_classes, values){
	// calcular la media de la serie
	avg = getAvg(values);
	// calcular la desviación típica de la serie
	stdDev = getStdDev(values, avg);

	// ordenar los valores de menor a mayor
	values = values.sort(function(a,b){return a-b;});

	var min = 100*(values[0]-avg)/stdDev;
	var max = 100*(values[values.length-1]-avg)/stdDev;
	var range = getRoturas(num_classes, min,max);
	// calcular los límites
	var stdDevValues = new Array();
	// poner en la primera posición el valor mínimo -1
	stdDevValues.push(values[0]-1);
	// calcular los límites correspondientes a las desviaciones establecidas en
	// el vector
	for (var i=0; i<range.length; i++){
		stdDevValues[i+1] = Math.round(avg + ((stdDev * range[i] )/100)),stdDevValues[i];
	}
	// poner en la ultima posición el valor máximo + 1
	stdDevValues.push(values[values.length-1]+1);

	return stdDevValues;


}

/**
 * obtiene la media de la serie
 * @param values serie
 * @returns {Number} media
 */
function getAvg(values){
	var avg = 0;
	for (var i = 0; i<values.length; i++){
		avg = avg + values[i];
	}
	return avg / values.length;
}

/**
 * obtiene la desviación tipica de la serie
 * @param values serie
 * @param avg media de la serie. Si es nula, se calcula.
 * @returns desviación típica
 */
function getStdDev(values, avg){
	if (avg == null){
		avg = getAvg(values);
	}

	// calcular la desviación típica de la serie
	var stdDev = 0;
	for (var i = 0; i<values.length; i++){
		stdDev = stdDev + Math.pow(values[i]-avg,2);
	}

	return Math.sqrt(stdDev /( values.length-1));
}

/**
 * obtiene los límtes de clases para la leyenda aplicacndo el algorítmo de rotura natural de Jenks
 * @param values serie
 * @returns {Array} vector con los límites de clases (en la primera posición se incluye el límite inferior de la primera clase, el resto
 * serán los límites superiores de cada clase).
 */
function getJenksBreaks(num_classes, values)
{
	var numdata = values.length;

	// ordenar la serie ascendentemente
	values.sort(function(a,b){return a-b;});

	// inicialización
	var pResult = new Array();
	for (var i = 0; i < num_classes; i++){
		pResult.push(0);
	}

	var mat1 = new Array(); // servirá para guardar los índices en la serie de los límites de clases ?
	var mat2 = new Array(); // servirá para guardar la varianza de cada clase ?
	for (var i = 1; i <= num_classes; i++)
	{
		if (mat1[1]== null){
			mat1[1] = new Array();
		}
		mat1[1][i] = 1;

		if (mat2[1]== null){
			mat2[1] = new Array();
		}
		mat2[1][i] = 0;

		for (var j = 2; j <= numdata; j++)
		{
			if (mat1[j]== null)
				mat1[j] = new Array();
			if (mat2[j]== null)
				mat2[j] = new Array();
			mat2[j][i] = 999999999;
		}
	}
	// mat 1 queda con la fila 1 con todo 1 y mat2 con la fila 1 con todo 0
	// y el resto infinito


	var ssd = 0;
	for (var rangeEnd = 2; rangeEnd <= numdata; rangeEnd++)
	{
		var sumX = 0;
		var sumX2 = 0;
		var w = 0;
		var dataId;
		for (var m = 1; m <= rangeEnd; m++)
		{
			dataId = rangeEnd - m + 1;
			var val = values[dataId - 1];
			sumX2 += val * val;
			sumX += val;
			w++;
			ssd = sumX2 - (sumX * sumX) / w;
			for (var j = 2; j <= num_classes; j++)
			{
				// alert(dataId +" "+mat2[dataId - 1]);
				if ((dataId >1)&&(!(mat2[rangeEnd][j] < (ssd + mat2[dataId - 1][j - 1]))))
				{
					mat1[rangeEnd][j] = dataId;
					mat2[rangeEnd][j] = ssd + mat2[dataId - 1][j - 1];
				}
			}
		}
		mat1[rangeEnd][1] = 1;
		mat2[rangeEnd][1] = ssd;
	}
	// mat1 contendrá en la última fila los índices del vector de valores
	// que deben representar los límites superiores de cada clase

	// en la primera posición ponemos el límite inferior de la primera clase
	// (menor valor -1 por posibles redondeos)
	pResult[0] = values[0]-1;
	// en la última posición ponemos el límite superior de la ultima clase
	// (mayor valor +1 por posibles redondeos)
	pResult[num_classes] = values[numdata - 1]+1;

	// establecer los límites superiores de cada clase (salvo la última que
	// ya está metido)
	var k = numdata;
	for (var j = num_classes; j >= 2; j--)
	{
		/*if (typeof mat1[k] =="undefined"){
			alert(k);
		}*/
		var id = Math.round((mat1[k][j]) - 2);
		pResult[j - 1] = values[id]+1; // sumamos 1 pq nosotros
		// consideramos el límite superior
		// no incluido en la clase
		k = Math.round(mat1[k][ j] - 1);
		if (k==0){
			k=1;
		}
	}
	return pResult;
}