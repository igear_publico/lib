
/*function getServiceInfo() {
	var axl = '<?xml version="1.0" encoding="UTF-8"?><ARCXML version="1.1">\n<REQUEST>\n<GET_SERVICE_INFO renderer="false" extensions="false" fields="false" />\n</REQUEST>\n</ARCXML>\n';
	http = getHTTPObject();
	//if ((http != null) && (!isWorking)) {
	if ((http != null)) {
		http.open("POST", url, true);
		http.onreadystatechange = parseLayers;
		isWorking = true;
		muestraLayerLoading();
		http.send(axl);
	}
}*/

function showInitialMap() {
	getLayers();
initBbox();
			if (customFotograma) {
				if (((! minxFotograma) || (! minyFotograma)) || ((! maxxFotograma) || (! maxyFotograma))) {
					minxFotograma = fullLeft;
					minyFotograma = fullBottom;
					maxxFotograma = fullRight;
					maxyFotograma = fullTop;
				}
				minx = minxFotograma;
				miny = minyFotograma;
				maxx = maxxFotograma;
				maxy = maxyFotograma;

				isGeorrefBoolean = (isGeorrefFotograma.toUpperCase() == "TRUE");
				
			
					if (escalaFotograma != "") {				
						selectedImage(locatFotograma, servicioFotograma, capaFotograma, null, null, null, null, coleccFotograma, true, isGeorrefBoolean, 0, 0, -1, '', fechaFotograma);
						minx = minxFotograma;
						miny = minyFotograma;
						maxx = maxxFotograma;
						maxy = maxyFotograma;
						getMap(minxFotograma, minyFotograma, maxxFotograma, maxyFotograma);
						doZoomScale(escalaFotograma);
						setNewScale(escalaFotograma);
						updateZoomBar(escalaFotograma);
					} else {
						selectedImage(locatFotograma, servicioFotograma, capaFotograma, minxFotograma, minyFotograma, maxxFotograma, maxyFotograma, coleccFotograma, true, isGeorrefBoolean, 0, 0, -1, '', fechaFotograma);
						getMap(minxFotograma, minyFotograma, maxxFotograma, maxyFotograma);
					}
					
			} else {
				if (customQuery) {
						// resolver la query inicial para obtener bbox inicial
					eval(initialQueryString);
				} else {
					if (customInitialMap) {
						getMap(customMinx, customMiny, customMaxx, customMaxy);
					} else {
						getFullMap();
					}
				}
			}
		
}

function initBbox(){
/*	var bbox_aragon = [571580, 4412223, 812351,
	                   4756639];*/
	fullLeft = 571580;
	fullBottom = 4412223;
	fullRight = 812351;
	fullTop = 4756639;
}
function getBbox(reply) {
		// lo hago mas grande el bbox xq sino no va bien
//	alert(reply);
//	reply = '<ENVELOPE minx="702000" miny="4685287" maxx="742000" maxy="4735000" name="Initial_Extent" />';
	//reply = '<ENVELOPE minx="627124,7964094538" miny="4685287,634576324" maxx="806419,6519864325" maxy="4747666,747920636" name="Initial_Extent" />';

	posEnv = reply.indexOf("<ENVELOPE ",0);
	startposEnv = posEnv + 10;
	endposEnv = reply.indexOf('/>',startposEnv);
	envelopeData = reply.substring(startposEnv,endposEnv);
	bbox = new Array();
	if (envelopeData != "") {
		var fValueEnv = getFieldValuesQuery(envelopeData);
		var fNameEnv = getFieldNamesQuery(envelopeData);
		parseEnvelope(fNameEnv, fValueEnv, bbox);
	}
	fullLeft = parseFloat(bbox["minx"]);
	fullBottom = parseFloat(bbox["miny"]);
	fullRight = parseFloat(bbox["maxx"]);
	fullTop = parseFloat(bbox["maxy"]);

	//fullWidth = limitRight - limitLeft;
	//fullHeight = limitBottom - limitTop;
}
/*
function parseEnvelopePoligono(theString, startpos) {
	theString = theString.toUpperCase();
	var tempString = "";
	var dQuote="\"";
	var pos = theString.indexOf("ENVELOPE",startpos);
	if (pos!=-1) {
		pos = pos + 8;
		startpos = theString.indexOf("MINX=",pos);
		startpos += 6;
		var endpos = theString.indexOf(dQuote,startpos);
		tempString = theString.substring(startpos,endpos);
		minxPoligono = parseFloat(tempString);
		startpos = theString.indexOf("MINY=",pos);
		startpos += 6;
		endpos = theString.indexOf(dQuote,startpos);
		tempString = theString.substring(startpos,endpos);
		minyPoligono = parseFloat(tempString);
		startpos = theString.indexOf("MAXX=",pos);
		startpos += 6;
		endpos = theString.indexOf(dQuote,startpos);
		tempString = theString.substring(startpos,endpos);
		maxxPoligono = parseFloat(tempString);
		startpos = theString.indexOf("MAXY=",pos);
		startpos += 6;
		endpos = theString.indexOf(dQuote,startpos);
		tempString = theString.substring(startpos,endpos);
		maxyPoligono = parseFloat(tempString);
	}
}
*/
