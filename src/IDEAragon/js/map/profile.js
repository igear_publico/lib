/** Basado en código de visor 3D ***/
var altitudMin=9000;
var altitudMax=0;
var altitudTotal=0;
var pteGradosMin=360;
var pteGradosMax=0;
var pteGradosTotal=0;
var ptePorcentajeMin=100;
var ptePorcentajeMax=0;
var ptePorcentajeTotal=0;
var pickPositionIndexes;
var altitudX = 0;
var desnivelPos=0
var desnivelNeg=0
var profilePlot;
var profileData;

//REDONDEOS DECIMALES

	/**
	 * Ajuste decimal de un número.
	 *
	 * @param	{String}	type	El tipo de ajuste.
	 * @param	{Number}	value	El número.
	 * @param	{Integer}	exp		El exponente(el logaritmo en base 10 del ajuste).
	 * @returns	{Number}			El valor ajustado.
	 */
	function decimalAdjust(type, value, exp) {
		// Si el exp es indefinido o cero...
		if (typeof exp === 'undefined' || +exp === 0) {
			return Math[type](value);
		}
		value = +value;
		exp = +exp;
		// Si el valor no es un número o el exp no es un entero...
		if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
			return NaN;
		}
		// Cambio
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		// Volver a cambiar
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	}

	
	// Redondeo hacia abajo
	
	  function floor10(value, exp) {
			return decimalAdjust('floor', value, exp);
		}
	
	function ceil10(value, exp) {
			return decimalAdjust('ceil', value, exp);
		}

/**
 * Devuelve los puntos del perfil entre las posiciones indicadas con su altitud calculada
 */
function completeProfileData(lastPos, position,includeLast){ 
	
	var scale = document.getElementById('scaleTextBox').value;
			
        	var positions = samplePositions(lastPos, position, scale/100,lastPos.distance);
        	if (!includeLast){
        	 positions.splice(0, 1);
        	}
        	
        	
        setHeights(positions);

return positions;	
}

function setMDEData(point){

	$.ajax({
		url: urlWCTS+"?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS=11,4,7&STYLES=&INFO_FORMAT=application/geojson&CRS=EPSG:25830&BBOX="+ point.x + ',' + point.y + ',' + (point.x+1) + ',' + (point.y+1)+'&WIDTH=1&HEIGHT=1&QUERY_LAYERS=11,4,7&X=1&Y=1',
		async: false,
		type: 'GET',
		
		success:	function(data) {
			var features = data.features;
			var altitud5,ptePorcentaje5,pteGrados5;
			for (var i=0; i<features.length;i++){
				 if (features[i].layerName=="11"){
					altitud5=parseInt(features[i].properties.PixelValue);
				}
				
				else if (features[i].layerName=="4"){
					ptePorcentaje5=parseFloat(features[i].properties.PixelValue);
				}
				
				else if (features[i].layerName=="7"){
					pteGrados5=parseFloat(features[i].properties.PixelValue);
				}
				
			}
					
					if (altitud5>altitudMax){
						altitudMax=altitud5;
					}
					if (altitud5<altitudMin){
						altitudMin=altitud5;
					}
					altitudTotal+=altitud5;
					point.altitud=altitud5;
					point.y=altitud5.toString();
					
					if (ptePorcentaje5>ptePorcentajeMax){
						ptePorcentajeMax=ptePorcentaje5;
					}
					if (ptePorcentaje5<ptePorcentajeMin){
						ptePorcentajeMin=ptePorcentaje5;
					}
					ptePorcentajeTotal+=ptePorcentaje5;
					point.ptePorcentaje=ptePorcentaje5.toFixed(2);
					
					if (pteGrados5>pteGradosMax){
						pteGradosMax=pteGrados5;
					}
					if (pteGrados5<pteGradosMin){
						pteGradosMin=pteGrados5;
					}
					pteGradosTotal+=pteGrados5;
					point.pteGrados=pteGrados5.toFixed(2);

					
					
		},
		error: 		function(data, textStatus, errorThrown) {
				console.log('Error obteniendo la altitud. State: ' + data.readyState + ", Status:" + data.status);
		}
	});
	
}
 function setHeights(positions) {

	 for (var i = 0 ; i < positions.length ; i++)
    {
		 positions[i].xcoord = positions[i].x;
		 positions[i].ycoord = positions[i].y;
		 setMDEData(positions[i]);
		 if ($("#unitsMeasureSelectGraph").val()=='km'){
			 positions[i].x=(positions[i].distance/1000).toString();
		 }
		 else{
		 positions[i].x=positions[i].distance.toString();
		 
		 }
if (i>0){
	if (positions[i].altitud>positions[i-1].altitud){
		desnivelPos += positions[i].altitud-positions[i-1].altitud;
	}
	else{
		desnivelNeg += positions[i-1].altitud-positions[i].altitud;
	}
}		 

    }
}
function samplePositions(start, end, sampleLength, distanciaX){
    var lineToSample = new Cesium.Cartesian3(0, 0, 0), tmp = new Cesium.Cartesian3(0, 0, 0);
    Cesium.Cartesian3.subtract(end, start, lineToSample);

    if (sampleLength === undefined)
        sampleLength = 3;
    var distancia = Cesium.Cartesian3.magnitude(lineToSample);
  
    var numGaps = Math.ceil(distancia / sampleLength);
    
    var positions = new Array(numGaps + 1);
    Cesium.Cartesian3.normalize(lineToSample, lineToSample);

    for (var i = 0 ; i < numGaps ; i++)
    {
        positions[i] = new Cesium.Cartesian3(0, 0, 0);
        Cesium.Cartesian3.multiplyByScalar(lineToSample, sampleLength * i, positions[i]);
        Cesium.Cartesian3.add(start, positions[i], positions[i]);
        positions[i].distance=distanciaX+sampleLength * i;
    }
    positions[numGaps] = end;
    positions[numGaps].distance=distanciaX+sampleLength * numGaps;
    return positions;
};
function calculateDataPlotAltitude(){
	altitudMin=9000;
	altitudMax=0;
	altitudTotal=0;
	desnivelPos=0
	desnivelNeg=0
	pteGradosMin=360;
	pteGradosMax=0;
	pteGradosTotal=0;
	ptePorcentajeMin=100;
	ptePorcentajeMax=0;
	ptePorcentajeTotal=0;
	var lastpos =  new Cesium.Cartesian3(clickPointX[0], clickPointY[0], 0);
	lastpos.distance=0;
	 pickPositionIndexes = new Array();
	 pickPositionIndexes.push(0);
	var data = new Array();
	for (var i=1; i<clickPointX.length;i++){
		if ((clickPointX[i]!=clickPointX[i-1])||(clickPointY[i]!=clickPointY[i-1])){
		var pos = new Cesium.Cartesian3(clickPointX[i], clickPointY[i], 0)
		var positions = completeProfileData(lastpos,pos,i==1);
		lastpos = new Cesium.Cartesian3(clickPointX[i], clickPointY[i], 0);
		lastpos.distance=pos.distance;	
		if(i>1){
			
				if (positions[0].altitud>data[data.length-1].altitud){
					desnivelPos +=positions[0].altitud-data[data.length-1].altitud;
				}
				else{
					desnivelNeg +=  data[data.length-1].altitud-positions[0].altitud;
				}
			
		}
			data=data.concat(positions);
		
		pickPositionIndexes.push(data.length-1);
	}
	}
	return data;
}
function calculatePlotAltitude() {

	$("#altitudeGraph").empty();

	$( "#unitsGraph" ).text('Distancia (' + $("#unitsMeasureSelectGraph option:selected").text() + ')');
	$( "#unitsGraph" ).show();
	$( "#measureGraph" ).text('Altitud (metros)');
	$( "#measureGraph" ).show();

	profileData = calculateDataPlotAltitude();


	profilePlot = Morris.Area({
		element: 'altitudeGraph',
		parseTime: false,
		data: profileData,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Altitud'],
		pointSize : 0,
		lineWidth: 3,
		ymin: floor10(altitudMin,1),
		ymax:  ceil10(altitudMax,1),
		units: ' m',
		redraw: true,
		smooth: true,
		xPreUnits: 'Distancia: ',
		xPostUnits: ' '/*+$("#unitsMeasureSelectGraph").val()*/,
		xDecimalMaxUnits: null,
		continuousLine: true,
		xLabelMargin: 40,
		events: pickPositionIndexes,
		eventLineColors: ["#006BB5"],
		eventStrokeWidth: 2,

		hoverCallback: function(index, options, content) {
			//console.log("hoverCallback: " + content);
			var data = options.data[index];
			//$(".morris-hover").html('<div class="morris-hover-point" style="color:#a15a20;">Custom label: ' + data.label + '</div>');
			//$(".morris-hover").html('<div class="morris-hover-point" style="color:#a15a20;">Custom label: ' + data.label + '</div>');
			var units = $("#unitsMeasureSelectGraph").val();
			var distancia = data.distance;
			if (units=='km'){
				distancia = (distancia/1000).toFixed(2);
			}
			else{//m.
				distancia = Math.round(distancia);
			}
			return  '<div class="morris-hover-point" style="color:#a15a20;">Distancia: ' + distancia + ' ' + units + '</div>'
			+ '<div class="morris-hover-point" style="color:#a15a20;">Altitud: ' + data.y + ' m</div>'
			+ '<div class="morris-hover-point" style="color:#a15a20;">Pendiente: ' + data.pteGrados + 'º ( ' + data.ptePorcentaje + '% )</div>';
		},
		//lineColors: ['#543005'], //['#ca6728']
		lineColors: ['#a15a22'],
		pointFillColors: ['#ff0000']

		//behaveLikeLine: true
	});
	$("#calculatingProfileOverlay").css("display","none");
	paintDatatableResults(profileData);
	
}


 function  changeProfileUnits(){
	 if(profilePlot){ // si no hay un gráfico creado no hay que hacer nada
	$( "#unitsGraph" ).text('Distancia (' + $("#unitsMeasureSelectGraph option:selected").text() + ')');
	var units =$("#unitsMeasureSelectGraph").val();
	
	for (var i=0; i<profileData.length;i++){
		if (units=="m"){
			profileData[i].x=profileData[i].distance.toString();
		}
		else{//km
			profileData[i].x=(profileData[i].distance/1000).toString();
		}
	}
	profilePlot.setData(profileData);
	var distancia = profileData[profileData.length-1].distance;
	setProfileDistanceTotal(units,distancia);
	
 }
 }
 
 function setProfileDistanceTotal(units,distancia){
	 
		if (units=='km'){
			distancia = (distancia/1000).toFixed(2);
		}
		else{//m.
			distancia = Math.round(distancia);
		}
		$("#profileDistance").text('Distancia total: ' + distancia + ' ' + units);
 }
function paintDatatableResults(data){ 
	var $message = $('#divTablePointsPerfil'); //$('.bootstrap-dialog-message');
	$("#divTablePointsPerfil").empty();
	var pickPositionListTable = [];
	var units =$("#unitsMeasureSelectGraph").val();

		var $tabla = $('<table id="tablePointsPerfil" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>');
		$message.append('<div id="numberPointsPerfil" class="row">DATOS APROXIMADOS</div>');
		$message.append('<div id="profileDistance" class="row text-center">Distancia total: ' + data[data.length-1].x + ' ' + units + '</div>');
		$message.append($tabla);

		var distancia = data[data.length-1].distance;
		setProfileDistanceTotal(units,distancia);

		altitudMax = Math.floor(altitudMax) + '';
		altitudMin = Math.floor(altitudMin) + '';
		altitudMed = Math.floor(altitudTotal/data.length) + '';
		var desnivel =  (Math.floor(data[data.length-1].y)-Math.abs(Math.floor(data[0].y) ))+ '';
		desnivelPos = Math.floor(desnivelPos) + '';
		desnivelNeg = Math.floor(desnivelNeg) + '';
		pteGradosMax = Math.floor(pteGradosMax) + '';
		ptePorcentajeMax = Math.floor(ptePorcentajeMax) + '';		
		pteGradosMin = Math.floor(pteGradosMin) + '';
		ptePorcentajeMin = Math.floor(ptePorcentajeMin) + '';
		ptePorcentajeMed = Math.floor(ptePorcentajeTotal/data.length) + '';	
		pteGradosMed = Math.floor(pteGradosTotal/data.length) + '';

		pickPositionListTable[0] = [];
		pickPositionListTable[1] = [];
		pickPositionListTable[2] = [];
		pickPositionListTable[0][0] = 'Altitud máxima: ' + altitudMax.replace(".", ",") + ' m.';
		pickPositionListTable[1][0] = 'Altitud mínima: ' + altitudMin.replace(".", ",") + ' m.';
		pickPositionListTable[2][0] = 'Altitud media: ' + altitudMed.replace(".", ",") + ' m.';	
		pickPositionListTable[0][1] = 'Desnivel positivo: ' + desnivelPos.replace(".", ",") + ' m.';
		pickPositionListTable[1][1] = 'Desnivel negativo: ' + desnivelNeg.replace(".", ",") + ' m.';
		pickPositionListTable[2][1] = 'Desnivel origen-destino: ' + desnivel.toString().replace(".", ",") + ' m.';
		pickPositionListTable[0][2] = 'Pendiente máxima: ' + pteGradosMax.replace(".", ",") + 'º ( ' + ptePorcentajeMax.replace(".", ",") + '% )';
		pickPositionListTable[1][2] = 'Pendiente mínima: '  + pteGradosMin.replace(".", ",") + 'º ( ' + ptePorcentajeMin.replace(".", ",") + '% )';
		pickPositionListTable[2][2] = 'Pendiente media: '  + pteGradosMed.replace(".", ",") + 'º ( ' + ptePorcentajeMed.replace(".", ",") + '% )'; 		


	//var titleLongTable = 'Distancia (' + $("#unitsMeasureSelectGraph option:selected").text() + ')';
	$('#tablePointsPerfil').DataTable({
		scrollY : 120,
		scrollCollapse : true,
		data: pickPositionListTable,
		columns: [
		          { title: "Altitud", orderable: false },
		          { title: "Desnivel", orderable: false },
		          { title: "Pendiente", orderable: false },
		          ],
		          columnDefs: [
		                       { type: 'spanish-string', targets: 0 }
		                       ],

		                       paging: false,
		                       searching: false,
		                       //scrollY: true,
		                       ordering: false,
		                       info:     false,        
		                       responsive: true,

		                       destroy: true
	});	
}


function getXAxisPoints(maxValue, maxIndex) {
	//console.log(maxValue);
	var decimales = maxValue.toString().indexOf('.') > -1 ? ",00" : "";
	if ($("#unitsMeasureSelectGraph").val()=="m"){
		decimales="";
	}
	var points = [];
	if (maxValue <= 1.00) {
		for (i = 0.1; i <= maxValue; i+=0.1) {
			points.push([i.toFixed(2).replace(".", ","), i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 5.00) {
		for (i = 0.5; i <= maxValue; i+=0.5) {
			points.push([i.toFixed(2).replace(".", ","), i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 10.00) {
		for (i = 1; i <= maxValue; i++) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 50.00) {
		for (i = 5; i <= maxValue; i+=5) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 100.00) {
		for (i = 10; i <= maxValue; i+=10) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 500.00) {
		for (i = 50; i < maxValue; i+=50) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 1000.00) {
		for (i = 100; i <= maxValue; i+=100) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 5000.00) {
		for (i = 500; i <= maxValue; i+=500) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 10000.00) {
		for (i = 1000; i < maxValue; i+=1000) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 50000.00) {
		for (i = 5000; i <= maxValue; i+=5000) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 100000.00) {
		for (i = 10000; i <= maxValue; i+=10000) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 500000.00) {
		for (i = 50000; i <= maxValue; i+=50000) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	} else if (maxValue <= 1000000.00) {
		for (i = 100000; i <= maxValue; i+=100000) {
			points.push([i+decimales, i*maxIndex/maxValue]);
		}
	}
	return points;
}

function cerrarProfile(){
	$( "#profile" ).dialog( "close" );
}