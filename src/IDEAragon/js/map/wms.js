wmsList = new Array();
wmsList["count"] = 0;

function addWMS(url, zIdx, visib, clase, supportHQ) {
	idxWMS = wmsList["count"];
	wmsList[idxWMS] = new Array();
	wmsList[idxWMS]["url"] = url;
	wmsList[idxWMS]["id"] = "theImageWMS" + idxWMS;
	wmsList[idxWMS]["zIdx"] = zIdx;
	wmsList[idxWMS]["visib"] = visib || false;
	wmsList[idxWMS]["clase"] = clase || '';
	wmsList[idxWMS]["supportHQ"] = supportHQ || false;
	wmsList["count"]++;
	return idxWMS;
}