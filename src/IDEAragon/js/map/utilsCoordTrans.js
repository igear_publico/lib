try{
proj4.defs('EPSG:4326', '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees');	
	proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs");
}
catch(err){
	console.log("No está cargada la versión de proj4js compatible con OL3");
}
Proj4js.defs["EPSG:4230"]="+title=Geograficas ED50 +proj=longlat +ellps=intl +no_defs ";
Proj4js.defs["EPSG:4258"]="+title=Geograficas ETRS89 +proj=longlat +ellps=GRS80 +no_defs ";
Proj4js.defs["EPSG:4326"]="+title=Geograficas WGS84 +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ";
Proj4js.defs["EPSG:25830"]="+title=25830 +proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs ";
Proj4js.defs["EPSG:25831"]="+title=25831 +proj=utm +zone=31 +ellps=GRS80 +units=m +no_defs ";
Proj4js.defs["EPSG:23030"]="+title=23030 +proj=utm +zone=30 +ellps=intl +units=m +no_defs ";
Proj4js.defs["EPSG:23031"]="+title=23031 +proj=utm +zone=31 +ellps=intl +units=m +no_defs ";

function coordTransformProj4s(srsOrigen, srsDestino, coordX, coordY) {
	var destino = new CoordinatePoint(2);
	if (srsOrigen == srsDestino) {
		destino.ord[0] = coordX;
		destino.ord[1] = coordY;
		return destino;
	}
	else {
		var source = new Proj4js.Proj(srsOrigen);    //source coordinates
		var dest = new Proj4js.Proj(srsDestino);     //destination coordinates 

			// transforming point coordinates
		var p = new Proj4js.Point(coordX,coordY);   //any object will do as long as it has 'x' and 'y' properties
		Proj4js.transform(source, dest, p);      //do the transformation.  x and y are modified in place

		destino.ord[0] = p.x;
		destino.ord[1] = p.y;
		return destino;
	}
}


function getURN(srs) {
	switch (srs) {
		case 'EPSG:4230': return 'urn:epsg:crs:4230';
		case 'EPSG:4258': return 'urn:epsg:crs:4258';
		case 'EPSG:23030': return 'urn:epsg:crs:23030';
		case 'EPSG:23031': return 'urn:epsg:crs:23031';
		case 'EPSG:25830': return 'urn:epsg:crs:25830';
		case 'EPSG:25831': return 'urn:epsg:crs:25831';
		default: return '';
	}
}

function getQueryWCTS_XML(srsOrigenURN, srsDestinoURN, srsOrigen, coordX, coordY) {
	var content = '';
	content += '<?xml version="1.0" encoding="ISO-8859-1" ?>\n';
	content += '<Transform xmlns="http://www.opengeospatial.net/wcts" xmlns:gml="http://www.opengis.net/gml" service="WCTS" version="1.0.0">\n';
	content += '<SourceCRS>' + srsOrigenURN + '</SourceCRS>\n';
	content += '<TargetCRS>' + srsDestinoURN + '</TargetCRS>\n';
	content += '<InputData>\n';
	content += '<gml:FeatureCollection>\n';
	content += '<gml:featureMember>\n';
	content += '<Point>\n';
	content += '<gml:pointProperty>\n';
	content += '<gml:Point srsName="' + srsOrigen + '">\n';
	content += '<gml:pos srsDimension="2">' + coordX + ' ' + coordY + '</gml:pos>\n';
	content += '</gml:Point>\n';
	content += '</gml:pointProperty>\n';
	content += '</Point>\n';
	content += '</gml:featureMember>\n';
	content += '</gml:FeatureCollection>\n';
	content += '</InputData>\n';
	content += '<OutputFormat>text/xml; gmlVersion=3.1.0</OutputFormat>\n';
	content += '</Transform>\n';
	return content;
}

var t_timeoutWCTS;
function coordTransformIGN(srsOrigen, srsDestino, coordX, coordY) {
	http = getHTTPObject();
	if ((http != null)) {
		queryXML = getQueryWCTS_XML(getURN(srsOrigen), getURN(srsDestino), srsOrigen, coordX, coordY);
		http.open("POST", urlWCTS);

		http.onreadystatechange = parseResultWCTS;

		isWorking = true;
		muestraLayerLoading();

		t_timeoutWCTS = setTimeout("sinRespuestaWCTS()", 20000);

		muestraLayerLoading();

		http.send(queryXML);
	}
}

function sinRespuestaWCTS() {
	ocultaLayerLoading();
	alert("No hay respuesta del servicio WCTS");
}

var pendingFuncWCTS = "";
var xWCTS = "";
var yWCTS= "";

function parseResultWCTS() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeoutWCTS);
									
			var result = http.responseText;
			processResult_WCTS(result);
			eval(pendingFuncWCTS);
			ocultaLayerLoading();
		} else {
			clearTimeout(t_timeoutWCTS);
			sinRespuestaWCTS();
		}
	}
}

function processResult_WCTS(theReply) {
	var selectedData = "";
	var reply = theReply;

	var pos = reply.indexOf("<gml:Point ", 1);
	if (pos != -1) {
		startpos = pos + 11;
		endpos = reply.indexOf('</gml:Point>',startpos);
		selectedData = reply.substring(startpos,endpos);
		var pos2 = selectedData.indexOf('ts=" ">', 1);
		if (pos2 != -1) {
			startpos2 = pos2 + 7;
			endpos = selectedData.indexOf('</gml:coordinates>', startpos2);
			var selectedData2 = selectedData.substring(startpos2,endpos);

			commapos = selectedData2.indexOf(',',0);

			if (selectedData2 != "") {
				xWCTS = selectedData2.substring(0, commapos);
				yWCTS = selectedData2.substring(commapos+1);
			}
		}
	}
}
function getLatLonDeegrees( decimalValue, isLat) {
	var value=decimalValue;
	var direccion = (isLat ? 'N':'E');
	if (decimalValue <0) {
		direccion = (isLat ? 'S':'W');
		value = 0-value;
	}
	var deegrees = Math.floor(value);
	value = (value -deegrees)*60;
	var mins = Math.floor(value);
	var segs = ((value -mins)*60).toFixed(2);
	
	return deegrees+"º "+mins+"' "+segs+"'' "+direccion;
}
