// aimsClick.js
/*
*	JavaScript template file for ArcIMS HTML Viewer
*		dependent on aimsXML.js, ArcIMSparam.js, aimsCommon.js, aimsMap.js,
*		aimsLayers.js, aimsDHTML.js
*		aimsNavigation.js
*/


var onOVArea = false;

var measurePointsX=[];
var measurePointsY=[];
var totalMeasure=0;
var currentMeasure=0;
var lastTotMeasure=0;

	//dportoles
tamPixel = 0;

areaMeasure = 0;

perimeter = 0;
errorAreaMeasure = 0;
errorDistMeasure = 0;

RAIZ_DE_DOS = Math.sqrt(2);

// variables for interactive clicks
var clickCount = 0;
var clickPointX = new Array();
var clickPointY = new Array();
var clickMeasure = new Array();
	// type - 1=Measure; 2=SelectLine ; 3=SelectPolygon
var clickType = 1;

var leftButton =1;
var rightButton = 2;
if (isNav) {
	leftButton = 1;
	rightButton = 3;
}

var screenClickPointX = new Array();
var screenClickPointY = new Array();

isHistorico = false;
isGeoreferencing = false;
isAreaMeasuring = false;
isDistanceMeasuring = false;
isProfileMaking = false;
/******************************************************
*	Point click functions
*	used by Measure and Select by Line/Polygon
*	*****************************************************
*/

function movement_distanceMeasure(coordActualX, coordActualY) {
	if (clickCount >= 1) {
		calcDistance(coordActualX, coordActualY);

		var segmentoText = "";
		var unitText = "";
		if (getScaleUnit() == "Km") {
			segmentoText = RoundDecimal(currentMeasure / 1000, numDecimals);
			unitText = "Km";
		} else {
			segmentoText = RoundDecimal(currentMeasure, numDecimals);
			unitText = "m";
		}

		document.getElementById("medSegmento").value = segmentoText + " " + unitText;
	}
}

// put a point at click and add to clickCount
//

function clickAddPoint_distanceMeasure(e) {
	clickAddPoint(e, true);

	updateDistanceMeasureBox();

	jg.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);

	if (clickCount > 1){
		jg.drawLine(screenClickPointX[clickCount-1], screenClickPointY[clickCount-1], screenClickPointX[clickCount-2], screenClickPointY[clickCount-2]);
	}
	jg.paint();
}
function clickAddPoint_profile(e) {
	clickAddPoint(e, true);

	//updateDistanceMeasureBox();

	jg.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);

	if (clickCount > 1){
		jg.drawLine(screenClickPointX[clickCount-1], screenClickPointY[clickCount-1], screenClickPointX[clickCount-2], screenClickPointY[clickCount-2]);
	}
	jg.paint();
}

function dblClickAddPoint_profile(e) {
	e.preventDefault();
	clickAddPoint(e, true);

	//updateDistanceMeasureBox();

	jg.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);

	if (clickCount > 1){
		jg.drawLine(screenClickPointX[clickCount-1], screenClickPointY[clickCount-1], screenClickPointX[clickCount-2], screenClickPointY[clickCount-2]);
	}
	jg.paint();
	$("#calculatingProfileOverlay").css("display","block");
	$("#profileMessage").css("display","none");
	setTimeout(calculatePlotAltitude,100);
}
function clickAddPoint_areaMeasure(e) {
	clickAddPoint(e, true);
	areaM = 0;

	for (var i = 0; i < clickCount; i++) {
		j = (i+1) % clickCount;
		areaM += clickPointX[i] * clickPointY[j];
		areaM -= clickPointY[i] * clickPointX[j];
	}
	areaM /= 2;
	if (areaM < 0) {
		areaMeasure = (areaM * -1);
	} else {
		areaMeasure = areaM;
	}

	updateAreaMeasureBox();

	updateMapClicks();
	if (clickCount > 1){
		jg.drawPolygon(screenClickPointX, screenClickPointY);
	}

	jg.paint();
}

function clickAddPoint_editar(e) {
	var theButton= 0;
	// get the button pushed
	if (isNav) {
		theButton = e.which;
	} else {
		theButton = window.event.button;
	}
	if (theButton == leftButton) {
		clickAddPoint(e, true);

		switch (getTipoGeorref()) {
			case 'point':
									jgPoint.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
									jgPoint.paint();
									finPointGeoref();
									break;
			case 'line':
									jgLine.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
									if (clickCount > 1){
										jgLine.drawLine(screenClickPointX[clickCount-1], screenClickPointY[clickCount-1], screenClickPointX[clickCount-2], screenClickPointY[clickCount-2]);
									}
									jgLine.paint();
									break;
			case 'polygon':
									updateMapClicks();
									jgPol.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
									if (clickCount > 1){
										jgPol.drawPolygon(screenClickPointX, screenClickPointY);
									}
									jgPol.paint();
									break;
			default:			//no-op
		}
	} else if (theButton == rightButton) {
		finGeom();
		return false;
	}
}

function clickAddPoint_historico(e) {
	clickAddPoint(e, true);
	jgHistorico.fillRect(screenClickPointX[clickCount-1] - 1, screenClickPointY[clickCount-1] - 1, 3, 3);
	jgHistorico.paint();
	finHistorico();
}
/*
function clickPoint_3D(e) {
	getXY2(e);
	var coordClick = getMapXY(theX, theY);
	latLon = coordTransformProj4s("EPSG:25830", "EPSG:4258", coordClick[0], coordClick[1]);
	theLon = RoundDecimal(latLon.ord[0], 8);
	theLat = RoundDecimal(latLon.ord[1], 8);
	window.open("http://vuelos3d.aragon.es/?latitud="+theLat+"&longitud="+theLon);
}*/

function clickAddPoint(e, isMeasuring) {
	getXY2(e);
	screenClickPointX[clickCount] = theX;
	screenClickPointY[clickCount] = theY;

	var coordClick = getMapXY(theX, theY);
	var coordClickX = coordClick[0];
	var coordClickY = coordClick[1];

	clickPointX[clickCount]=coordClickX;
	clickPointY[clickCount]=coordClickY;

	

	if (isMeasuring) {
		calcDistance(coordClickX, coordClickY);
		clickCount += 1;
		selectCount = 0;
		totalMeasure = totalMeasure + currentMeasure;
			//var u = Math.pow(10, numDecimals);
			//if (totalMeasure!=0) totalMeasure = parseInt(totalMeasure*u+0.5)/u;

		clickMeasure[clickCount] = totalMeasure;
	}
	else{
		clickCount += 1;
	}
}


// zero out all clicks in clickCount
function resetClick() {
	if (isAreaMeasuring ||isDistanceMeasuring){
		if ((clickCount>0)&&(measurePointsX.length==0)) {
			measurePointsX= clickPointX.slice();
			measurePointsY=clickPointY.slice();
		}
	}
	var c1 = clickCount;
	clickCount=0;
	clickPointX.length=1;
	clickPointY.length=1;
	screenClickPointX.length=1;
	screenClickPointY.length=1;
	currentMeasure=0;
	totalMeasure=0;
	lastTotMeasure=0;
	clickMeasure.length=1;
	selectCount=0;

	areaMeasure=0;
	errorAreaMeasure = 0;
	errorDistMeasure = 0;
	perimeter = 0;

/*
	legendTemp=legendVisible;
	legendVisible=false;
	var theString = writeXML();
	var theNum = 99;
		//showRetrieveMap();
	sendToServer(imsURL, theString, theNum);
	if (toolMode==20) updateMeasureBox();
	*/

}

// remove last click from clickCount
function deleteClick() {
	var c1 = clickCount;
	clickCount=clickCount-1;
	selectCount=0;
	if (clickCount<0) clickCount=0;
	if (clickCount>0) {
		totalMeasure = clickMeasure[clickCount];
		clickPointX.length=clickCount;
		clickPointY.length=clickCount;
		screenClickPointX.length=clickCount;
		screenClickPointY.length=clickCount;
		clickMeasure.length=clickCount;

	} else {
		totalMeasure=0;
		clickMeasure[0]=0;
	}
	currentMeasure=0;
/*
	if (c1>0) {
		legendTemp=legendVisible;
		legendVisible=false;
		var theString = writeXML();
		var theNum = 99;
		sendToServer(imsURL, theString, theNum);
	}
*/

	updateMapClicks();
}

function clearProfile(){
	profilePlot=null;
	profileData=null;
	$('#altitudeGraph').empty();
	
	$( "#unitsGraph" ).hide();
	$( "#measureGraph" ).hide();
	$("#divTablePointsPerfil").empty();
	profilePlot=null;
	profileData=null;
}
function clearDistanceMeasureBox() {
	document.getElementById("medTotal").value = "";
	document.getElementById("medSegmento").value = "";
	document.getElementById("medErrorDist").value = "";

}

function clearAreaMeasureBox() {
	document.getElementById("theAreaMedTotal").value = "";
	document.getElementById("medErrorArea").value = "";
	
}

function updateDistanceMeasureBox() {
/*
	var j = 1;
	for (var i=0;i<sUnitList.length;i++) {
		if (ScaleBarUnits==sUnitList[i]) j=i;
	}

		var u = Math.pow(10, numDecimals);
		var tMeas = 0;
		if (totalMeasure!=0) tMeas = parseInt(totalMeasure*u+0.5)/u;
	theForm.theMeasTotal.value = tMeas + " " + unitList[j];
	theForm.theMeasSegment.value = currentMeasure + " " + unitList[j];
*/

	tamPixel = Math.abs(maxx - minx) / mwidth;

	var currentMeasureText;
	var totalMeasureText;
	var errorMeasureText;

	var unitText;

	errorUnPixel = Math.sqrt( (Math.pow(tamPixel/2, 2)) * 2 );
	errorDistMeasure = clickCount * errorUnPixel;

	if (getScaleUnit() == "Km") {
		currentMeasureText = RoundDecimal(currentMeasure / 1000, numDecimals);
		totalMeasureText = RoundDecimal(totalMeasure / 1000, numDecimals);
		errorMeasureText = RoundDecimal(errorDistMeasure / 1000, numDecimals);
		unitText = "Km";
	} else {
		currentMeasureText = RoundDecimal(currentMeasure, numDecimals);
		totalMeasureText = RoundDecimal(totalMeasure, numDecimals);
		errorMeasureText = RoundDecimal(errorDistMeasure, numDecimals);
		unitText = "m";
	}

	document.getElementById("medTotal").value = totalMeasureText + " " + unitText;
	document.getElementById("medSegmento").value = currentMeasureText + " " + unitText;
	document.getElementById("medErrorDist").value = "± " + errorMeasureText + " " + unitText;
}

function updateAreaMeasureBox() {
/*
	var j = 1;
	for (var i=0;i<sUnitList.length;i++) {
		if (ScaleBarUnits==sUnitList[i]) j=i;
	}

		var u = Math.pow(10, numDecimals);
		var tMeas = 0;
		if (totalMeasure!=0) tMeas = parseInt(totalMeasure*u+0.5)/u;
	theForm.theMeasTotal.value = tMeas + " " + unitList[j];
	theForm.theMeasSegment.value = currentMeasure + " " + unitList[j];
*/

	tamPixel = Math.abs(maxx - minx) / mwidth;

	if (areaMeasure > 0) {
		var measTotal;
		var unitText;

			// cerrar el poligono para poder calcular el perimetro
		if (clickCount > 0) {
			var xD = Math.abs(clickPointX[clickCount-1] - clickPointX[0]);
			var yD = Math.abs(clickPointY[clickCount-1] - clickPointY[0]);
			mDistance = Math.sqrt(Math.pow(xD, 2) + Math.pow(yD, 2));
			perimeter = totalMeasure + mDistance;
		}
		errorAreaMeasure = perimeter * RAIZ_DE_DOS * tamPixel;

		if (getScaleAreaUnit() == "Km2") {
			measTotal = RoundDecimal(areaMeasure / 1000000, numDecimals);
			errorMeasureText = RoundDecimal(errorAreaMeasure / 1000000, numDecimals);
			unitText = "Km<sup>2</sup>";
		}else if (getScaleAreaUnit() == "Ha") {
			measTotal = RoundDecimal(areaMeasure / 10000, numDecimals);
			errorMeasureText = RoundDecimal(errorAreaMeasure / 10000, numDecimals);
			unitText = "Ha";
		} else {
			measTotal = RoundDecimal(areaMeasure, numDecimals);
			errorMeasureText = RoundDecimal(errorAreaMeasure, numDecimals);
			unitText = "m<sup>2</sup>";
		}

		document.getElementById("theAreaMedTotal").value = measTotal + " " + unitText;
		document.getElementById("medErrorArea").value = "± " + errorMeasureText + " " + unitText;
	} else {
		var unitText;
		if (getScaleAreaUnit() == "Km2") {
			unitText = "Km<sup>2</sup>";
		}else if (getScaleAreaUnit() == "Ha") {
			unitText = "Ha";
		} else {
			unitText = "m<sup>2</sup>";
		}
		document.getElementById("theAreaMedTotal").value = "0 " + unitText;
		document.getElementById("medErrorArea").value = "± 0 " + unitText;
	}
	//showLayer("areaMeasureBox");
}

function updateMapClicks() {
		// incluyo la actualizacion del pintado de puntos
	jg.clear();
	pixelPointPrev = null;
	firstPixelPoint = null;
	for (var ii = 0; ii < clickCount; ii++) {
		pixelPoint = getScreenXY(clickPointX[ii], clickPointY[ii]);
			// Pinto una marca mas gruesa para la medicion
		jg.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);

		if (ii > 0){
				// Pinto una linea entre mediciones
			jg.drawLine(pixelPoint[0], pixelPoint[1], pixelPointPrev[0], pixelPointPrev[1]);
		} else {
			firstPixelPoint = pixelPoint;
		}

			// al medir por áreas, cerrar el polígono si hay 3 puntos o más
		if (isAreaMeasuring) {
			if ((ii == clickCount-1) && (clickCount >= 3)) {
				jg.drawLine(pixelPoint[0], pixelPoint[1], firstPixelPoint[0], firstPixelPoint[1]);
			}
		}

			// actualizo el vector de puntos de pantalla
		screenClickPointX[ii] = pixelPoint[0];
		screenClickPointY[ii] = pixelPoint[1];

			// preparo el punto para la siguiente iteracion
		pixelPointPrev = pixelPoint;
	}

	jg.paint();

	if (valueRadius != -1) {
		var tamPixel = Math.abs(maxx - minx) / mwidth;
		jgCircle.setStroke(2);
		var numPixels = valueRadius / tamPixel;
		if ((numPixels > 0) && (numPixels*2 <= Math.max(mwidth, mheight))) {
			jgCircle.clear();
			var pixelPoint = getScreenXY(xRadius, yRadius);
			jgCircle.drawEllipse(pixelPoint[0]-numPixels, pixelPoint[1]-numPixels, numPixels*2, numPixels*2);

			if (numPixels*2>10) {
				jgCircle.drawLine(pixelPoint[0]-4, pixelPoint[1]-4, pixelPoint[0]+4, pixelPoint[1]+4);
				jgCircle.drawLine(pixelPoint[0]+4, pixelPoint[1]-4, pixelPoint[0]-4, pixelPoint[1]+4);
			}

			jgCircle.paint();
		} else {
				// es muy grande y no lo pinto
			jgCircle.clear();
			var pixelPoint = getScreenXY(xRadius, yRadius);

			if (numPixels*2>10) {
				jgCircle.drawLine(pixelPoint[0]-4, pixelPoint[1]-4, pixelPoint[0]+4, pixelPoint[1]+4);
				jgCircle.drawLine(pixelPoint[0]+4, pixelPoint[1]-4, pixelPoint[0]-4, pixelPoint[1]+4);
			}
			jgCircle.paint();
		}
	}

	if (customToolType == "GRANJAS") {
			// si no tengo aun, cojo el centro del mapa
			// sera aproximado, pero mas o menos suele servir
		if (xRadiusGranjas != null) {
			xRadiusGranjas = maxx - (Math.abs(maxx - minx) / 2);
			yRadiusGranjas = maxy - (Math.abs(maxy - miny) / 2);

			if (distanceList.length == 0) {
				distanceList[0] = 5000;
			}
		}
		var yaHayAspa = false;
		var tamPixel = Math.abs(maxx - minx) / mwidth;
		jgCircleGranjas.clear();
		for (var ii = 0; ii < distanceList.length; ii++) {
			var numPixels = Math.round(distanceList[ii] / tamPixel);
			if ((numPixels > 0) && (numPixels*2 <= Math.max(mwidth, mheight))) {
				var pixelPoint = getScreenXY(xRadiusGranjas, yRadiusGranjas);

				jgCircleGranjas.drawEllipse(pixelPoint[0]-numPixels, pixelPoint[1]-numPixels, numPixels*2, numPixels*2);

				if (! yaHayAspa) {
					if (numPixels*2>10) {
						jgCircleGranjas.drawLine(pixelPoint[0]-4, pixelPoint[1]-4, pixelPoint[0]+4, pixelPoint[1]+4);
						jgCircleGranjas.drawLine(pixelPoint[0]+4, pixelPoint[1]-4, pixelPoint[0]-4, pixelPoint[1]+4);
					}
					yaHayAspa = true;
				}

			}
		}
		jgCircleGranjas.paint();

	 // else: es muy grande y no lo pinto

		/*
	if ((origenX >= 0) && (origenX <= iWidth)) {
		if ((origenY >= 0) && (origenY <= iHeight)) {

				// pintamos un aspa en el centro
			jgCircle.setColor( colorListAspa );
	jgCircle.drawLine(origenX-4, origenY-4, origenX+4, origenY+4);
	jgCircle.drawLine(origenX+4, origenY-4, origenX-4, origenY+4);
		}
	}

	for (i = 0; i < distanceList.length; i++) {
		jgCircle.setColor( colorList[i%colorList.length] );
		
			// tengo que construir un circulo de radio distanceList[i]
			// centrado en el centro del mapa
			// pero ojo xq tengo que indicarle al jgCircle la esquina superior izquierda
		radioEnPx = distanceList[i] / tamPixel;
	
			// solo pintamos si no es demasiado grande y no se va a salir del mapa de vision
		if ((radioEnPx*2 <= iHeight) && (radioEnPx*2 <= iWidth)) {
			currentOrigenX = origenX - radioEnPx;
			currentOrigenY = origenY - radioEnPx;
			jgCircle.drawEllipse(currentOrigenX, currentOrigenY, radioEnPx*2, radioEnPx*2);
		}
	}
	jgCircle.paint();
		 
		 */
	}
}

function updateNotifications() {
	jgNotificar.clear();
	for (var idx = 0; idx < pointUserCount; idx++) {
		for (var qq = 0; qq < pointUserList[idx]['count']; qq++) {
			var theX = pointUserList[idx][(qq) + 'x'];
			var theY = pointUserList[idx][(qq) + 'y'];
			var pixelPoint = getScreenXY(theX, theY);
			jgNotificar.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);
		}
	}

	for (var idx = 0; idx < lineUserCount; idx++) {
		var theX = lineUserList[idx]['0x'];
		var theY = lineUserList[idx]['0y'];
		var pixelPoint = getScreenXY(theX, theY);

		jgNotificar.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);
		for (var qq = 2; qq <= lineUserList[idx]['count']; qq++) {
			var theX = lineUserList[idx][(qq-1) + 'x'];
			var theY = lineUserList[idx][(qq-1) + 'y'];
			var pixelPoint = getScreenXY(theX, theY);

			var theXprev = lineUserList[idx][(qq-2) + 'x'];
			var theYprev = lineUserList[idx][(qq-2) + 'y'];
			var pixelPointPrev = getScreenXY(theXprev, theYprev);

			jgNotificar.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);
			jgNotificar.drawLine(pixelPoint[0], pixelPoint[1], pixelPointPrev[0], pixelPointPrev[1]);
		}
	}

	for (var idx = 0; idx < polygonUserCount; idx++) {
		var screenListX = new Array();
		var screenListY = new Array();
		for (var qq = 0; qq < polygonUserList[idx]['count']; qq++) {
			var theX = polygonUserList[idx][(qq) + 'x'];
			var theY = polygonUserList[idx][(qq) + 'y'];
			var pixelPoint = getScreenXY(theX, theY);
			screenListX[qq] = pixelPoint[0];
			screenListY[qq] = pixelPoint[1];
			jgNotificar.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);
		}
		screenListX[polygonUserList[idx]['count']] = screenListX[0];
		screenListY[polygonUserList[idx]['count']] = screenListY[0];

		if (polygonUserList[idx]['count'] > 1) {
			jgNotificar.drawPolygon(screenListX, screenListY);
		}
	}
	jgNotificar.paint();
}

function updateGeorefMapClicks(divToPaint, rec, divToLabel) {
	divToPaint.clear();
	divToLabel.clear();
	pixelPointPrev = null;

	numData = rec.getVectorialDataCount();

	for (var jj = 0; jj < numData; jj++) {
		geom = rec.getGeometry(jj);

		textoG = rec.getAttribute(jj, 'text');

		if ((geom.getClass() == Geometry.LINEAR_RING) || (geom.getClass() == Geometry.LINE_STRING)) {

			for (var kk = 0; kk < geom.npoints; kk++) {
				pixelPoint = getScreenXY(geom.xpoints[kk], geom.ypoints[kk]);

					//pinto una marca más gruesa para el punto exacto
				divToPaint.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);

				if (kk > 0){
						// Pinto una linea entre mediciones
					divToPaint.drawLine(pixelPoint[0], pixelPoint[1], pixelPointPrev[0], pixelPointPrev[1]);
				}

					// preparo el punto para la siguiente iteracion
				pixelPointPrev = pixelPoint;
			}

				// pinto la etiqueta
			drawLabel(divToLabel, geom, textoG);
		} else {
				// supongo que siempre es punto si no es linea
			pixelPoint = getScreenXY(geom.getX(), geom.getY());
			divToPaint.fillRect(pixelPoint[0] - 1, pixelPoint[1] - 1, 3, 3);


			if (getTipoGeorref() == 'point') {
					// pinto la etiqueta si georeferencia puntos
				drawLabel(divToLabel, geom, textoG);
			}
		}
	}

	divToPaint.paint();
	divToLabel.paint();
}


/*

t_find = top.MapFrame;

function getIndexLayer(theId) {
	for (ii = 0; ii < t_find.LayerID.length; ii++) {
		if (t_find.LayerID[ii] == theId) {
			return ii;
		}
	}
	return -1;
}

	// probar primero con urbana y si no existe, probar con rustica
function sendRefCatU() {
	previousXMLMode = identifyXMLMode;
	identifyXMLMode = 10000;

		//5019 parcelas urbana
	theIndex = getIndexLayer(5019);
	t_find.LayerVisible[theIndex] = 1;
	t_find.setActiveLayer(theIndex);
	t_find.identify();

	identifyXMLMode = previousXMLMode;
}

function sendRefCatR() {
	previousXMLMode = identifyXMLMode;
	identifyXMLMode = 10001;

		//5021 parcelas rustica
		//5020 subparcelas rustica
	//theIndex = getIndexLayer(5021);
	theIndex = getIndexLayer(5020);
	t_find.LayerVisible[theIndex] = 1;
	t_find.setActiveLayer(theIndex);
	t_find.identify();

	identifyXMLMode = previousXMLMode;
}


function writeCookieValue(theReply, separator, cookieName) {
	beginPos = theReply.indexOf(separator);
	longSeparator = separator.length;

	if (beginPos != -1) {
		valueTemp = theReply.substring(beginPos+longSeparator);
		endPos = valueTemp.indexOf('"');
		valueFinal = valueTemp.substring(0, endPos);
		writePersistentCookie(cookieName, valueFinal, "months", 1);
	}
}



function receiveRefCatU(theReply) {
	separator = 'REFCAT="';
	beginPos = theReply.indexOf(separator);
		// 8 es por la longitud de separator
	if (beginPos != -1) {
		refCatTemp = theReply.substring(beginPos+8);
		endPos = refCatTemp.indexOf('"');
		refCatFinal = refCatTemp.substring(0, endPos);
	writePersistentCookie("refcat", refCatFinal, "months", 1);


		writeCookieValue(theReply, 'PARCELA="', 'parcela');
//		writeCookieValue(theReply, 'SUBPARCE="', 'poligono');

		writePersistentCookie("poligono", "0000", "months", 1);

		writeCookieValue(theReply, 'COORX="', 'coorX');
		writeCookieValue(theReply, 'COORY="', 'coorY');

		top.close();
	} else {
			// si no hay de urbana, probar con rustica
		sendRefCatR();
	}
}

function receiveRefCatR(theReply) {
	separator = 'REFCAT="';
	beginPos = theReply.indexOf(separator);
		// 8 es por la longitud de separator
	if (beginPos != -1) {
		refCatTemp = theReply.substring(beginPos+8);
		endPos = refCatTemp.indexOf('"');
		refCatFinal = refCatTemp.substring(0, endPos);
		writePersistentCookie("refcat", refCatFinal, "months", 1);

		writeCookieValue(theReply, 'PARCELA="', 'parcela');
		writeCookieValue(theReply, 'SUBPARCE="', 'poligono');
		writeCookieValue(theReply, 'COORX="', 'coorX');
		writeCookieValue(theReply, 'COORY="', 'coorY');


		top.close();
	} else {
			// si no hay rustica, devolver cadena vacia
		writePersistentCookie("refcat", "", "months", 1);
		top.close();
	}
}
*/

	newScaleUnit = "m";
	newScaleAreaUnit = "Ha";

	function onChangeLineScale() {
		combo = document.getElementById('cboLineScale');
		newScaleUnit = combo.options[combo.selectedIndex].value;
		updateDistanceMeasureBox();
	}

	function getScaleUnit() {
		return newScaleUnit;
	}

	function onChangeAreaScale() {
		combo = document.getElementById('cboAreaScale');
		newScaleAreaUnit = combo.options[combo.selectedIndex].value;
		updateAreaMeasureBox();
	}

	function getScaleAreaUnit() {
		return newScaleAreaUnit;
	}
	function limpiarProfile() {
		jg.clear();
		measurePointsX=[];
		measurePointsY=[];
		resetClick();
		
		clearProfile();
	}
	function limpiarMedicionDistancia() {
		jg.clear();
		measurePointsX=[];
		measurePointsY=[];
		resetClick();
		clearDistanceMeasureBox();
	}

	function limpiarMedicionArea() {
		jg.clear();
		measurePointsX=[];
		measurePointsY=[];
		resetClick();
		clearAreaMeasureBox();
	}

	function limpiarEdicion() {
		jgPoint.clear();
		jgLine.clear();
		jgPol.clear();
		jgNotificar.clear();
		jgPointLabel.clear();
		jgLineLabel.clear();
		jgPolLabel.clear();
	}

	function limpiarHistorico() {
		jgHistorico.clear();
		jgHistoricoLabel.clear();
	}

	function limpiarBufferCirculo() {
		jgCircle.clear();
	}

	function borrarBufferCirculo() {
		limpiarBufferCirculo();
		valueRadius = -1;
	}

	function limpiarBufferCirculoGranjas() {
		jgCircleGranjas.clear();
	}

	function borrarBufferCirculoGranjas() {
		limpiarBufferCirculoGranjas();
		valueRadiusGranjas = -1;
		if (customToolType == "GRANJAS") {
			customToolType = "";
		}
	}


	function onChangeGeorefType() {
		combo = document.getElementById('cboGeorefType');
		newValue = combo.options[combo.selectedIndex].value;
		showOnlyOneGeoref(newValue);
	}

	function showOnlyOneGeoref(id) {
		document.getElementById('georefPointOptions').style.display = 'none';
		document.getElementById('georefLineOptions').style.display = 'none';
		document.getElementById('georefPolyOptions').style.display = 'none';

		document.getElementById(id).style.display = 'block';
	}

	function getTipoGeorref() {
					/*
		combo = document.getElementById('cboGeorefType');
		newValue = combo.options[combo.selectedIndex].value;
		switch (newValue) {
			case 'georefPointOptions': return 'point';
			case 'georefLineOptions': return 'line';
			case 'georefPolyOptions': return 'polygon';
			default: return '';
		}
		*/
		return 'point';
	}


// convert coordinates into screen points
function getScreenXY(xIn, yIn) {
		pixelX = (maxx - minx) / mwidth;
		pixelY = (maxy - miny) / mheight;

		screenPoint = new Array();
		screenPoint[0] = Math.round((xIn-minx)/pixelX);
		screenPoint[1] = Math.round((maxy-yIn)/pixelY);
		return screenPoint;
}

// calculate distance to current scalebarunits
function calcDistance(mX, mY) {
	if (clickCount>0) {
		var xD = Math.abs(mX - clickPointX[clickCount-1]);
		var yD = Math.abs(mY - clickPointY[clickCount-1]);
		var mDistance = Math.sqrt(Math.pow(xD, 2) + Math.pow(yD, 2));

		currentMeasure = RoundDecimal(mDistance, numDecimals);
	}
}

/*
// convert the amounts to new units
function convertUnits(theDist1, mUnits, sUnits) {
	// Note: decimal are not hard coded to allow use with locales using commas instead of points.
	var theDist = parseFloat(theDist1);
	var mDistance = theDist;
	//alert(theDist);
	if (mUnits == "FEET") {
		if (sUnits=="MILES") {
			mDistance = theDist / 5280;
		} else if (sUnits == "METERS") {
			mDistance = theDist * (3048/10000);
		} else if (sUnits == "KILOMETERS") {
			mDistance = theDist * (3048/10000000);
		}
	} else {
		if (sUnits=="MILES") {
			mDistance = theDist * (6213711922/10000000000000);
		} else if (sUnits == "FEET") {
			mDistance = theDist * (3280839895/1000000000);
		} else if (sUnits == "KILOMETERS") {
			mDistance = theDist / 1000;
		}
	}
	var u = Math.pow(10, numDecimals);
	//alert(u);
	if (!isNav) mDistance = parseInt(mDistance * u + (5/10)) / u
	//alert(mDistance);
	return mDistance;
}
*/
