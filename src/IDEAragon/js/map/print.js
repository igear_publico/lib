
function showPrintDialog(){
	$("#printWin").dialog("open");
}

function doPrint(){
	$("#printDialog").dialog("close");
    document.printForm.xml.value = makeXMLsafeAcentos(getDataToPrint());
console.log(document.printForm.xml.value);
    document.printForm.xslt.value = printActionXSL;
    document.printForm.action = printActionURL;
    document.printForm.submit();
    
}

function pushWMSRequest(requests,layer, bbox, mapWidth,mapHeight){
	if (layer instanceof ol.layer.Group){
		layer.getLayers().forEach(function(sublayer, i) {
			 pushWMSRequest(requests,sublayer, bbox, mapWidth,mapHeight);

		});
	}
	else{
		if (layer.getVisible()){
		var source = layer.getSource();
		
		if ((source instanceof ol.source.ImageWMS)||(source instanceof ol.source.TileWMS)||(source instanceof ol.source.WMTS)||(source instanceof ol.source.XYZ)||(source instanceof ol.source.TileArcGISRest)){
			var params= new Object();
			var capa;
			try{
				params = source.getParams();
				capa = params.LAYERS;
			}
			catch(err){
				//sera WMTS
				try{
				capa = source.getLayer();
				}
				catch(err){ // es xyz o tilearcgisrest
					capa =layer.get("wms_layer");
				}
			}
			if (typeof capa == 'undefined'){
				capa =layer.get("wms_layer");
			}
			var url; 

			if((source instanceof ol.source.WMTS)||(source instanceof ol.source.XYZ)||(source instanceof ol.source.TileArcGISRest)){
				try{
				url = serviciosWMTS[getWMTSIdx(layer.get("wmts_pk"))].url_wms;
				}
				catch(err){
					url = layer.get("wms_url");
				}
			}
			else{
				url = (source instanceof ol.source.TileWMS ? source.getUrls()[0]:source.getUrl());
			}
			url = urlAbsoluta(url);
			
			if (url.indexOf("?")<0){
				url = url+"?";
			}
			if (url.indexOf("?")<url.length-1){
				url = url+"&";
			}
			url += "SERVICE=WMS&REQUEST=GetMap&EXCEPTIONS=application/vnd.ogc.se_blank&TRANSPARENT=true";
			url +="&VERSION=";
			if (params.VERSION){
				url +=params.VERSION;
			}
			else{
				url +="1.1.1";
			}
			url +="&FORMAT=";
			if (params.FORMAT){
				url +=params.FORMAT;
			}
			else{
				url +="image/png";
			}
			var projBbox = bbox;
			url +="&SRS=";
			if (params.SRS){
				url +=params.SRS;
			}
			else if ((source instanceof ol.source.XYZ)||(source instanceof ol.source.TileArcGISRest)){
				url +="EPSG:25830";
			}
			else if (source.getProjection() && (source.getProjection().getCode()!="EPSG:25830")){
				var extent = bbox.split(",");
				var projExtent= ol.proj.transformExtent([parseFloat(extent[0]),parseFloat(extent[1]),parseFloat(extent[2]),parseFloat(extent[3])], 'EPSG:25830', source.getProjection().getCode());
				projBbox=projExtent[0]+","+projExtent[1]+","+projExtent[2]+","+projExtent[3];
				url +=source.getProjection().getCode();
			}
			else{
				
				
				url +="EPSG:25830";
			}
			if (params.CQL_FILTER){
				url +="&CQL_FILTER="+encodeURIComponent(params.CQL_FILTER);
			}
			if (params.TIME){
				url +="&TIME="+encodeURIComponent(params.TIME);
			}
			requests.push(url+"&STYLES=&Layers=" + capa+
					 '&WIDTH=' + mapWidth + '&HEIGHT=' + mapHeight+
			 '&BBOX=' +projBbox);
				
		}
		else if(source instanceof ol.source.ImageStatic){
			var extent = bbox.split(",");
			var imgExtent = layer.get("extent");
			var pixelsMin=coords2px(mapWidth, mapHeight,extent[0], extent[1], extent[2], extent[3],imgExtent[0],imgExtent[1]).split(",");
			var pixelsMax=coords2px(mapWidth, mapHeight,extent[0], extent[1], extent[2], extent[3],imgExtent[2],imgExtent[3]).split(",");
			// se intercambian ymax e ymin pq las coordenadas van de abajo a arriba pero los pixels van de arriba a abajo
			requests.push(server+"/BD_GIS/getScaledImage.jsp?xmin="+pixelsMin[0]+"&xmax="+pixelsMax[0]+"&ymin="+pixelsMax[1]+"&ymax="+pixelsMin[1]+"&width="+mapWidth+"&height="+mapHeight+"&img="+encodeURIComponent(layer.get("url")));
			
		}
		}
	}
//	return requests;
}

function getPrintScale(printWidth, printHeight,minx,maxx){
	var resolucion = (maxx-minx)/printWidth;
	ancho = 100000/resolucion;
	var coef =Math.max(1,Math.floor(ancho/100));
	while ((ancho/coef < 140)&&(coef>1)){
		coef--;
	}
	
	anchura = ancho/coef;
	
	
	 scale5 = 100/coef;
	 scale_units="Km";
	if (scale5 <10){
		scale_units="m";
		scale5 = scale5*1000;
	}
	
	return urlAbsoluta(printScaleURL)+'?width='+printWidth+'&height='+printHeight+'&scaleWidth='+Math.round(anchura)+'&length='+Math.round(scale5)+'&units='+scale_units;

}

function getPrintOverview(newBboxPrint){
	var topLeftPx = coord2px_overview(newBboxPrint[0], newBboxPrint[3]);
	var bottomRightPx = coord2px_overview(newBboxPrint[2], newBboxPrint[1]);

	return '<situacionAragon><laUrl>'+urlAbsoluta(server)+'/lib/IDEAragon/themes/default/images/mapaOverview.gif</laUrl></situacionAragon><situacionAragon><laUrl>'+urlAbsoluta(printOverviewURL)+'?width='+printWidthOverview+'&height='+printHeightOverview+'&left='+topLeftPx["x"]+'&bottom='+bottomRightPx["y"]+'&right='+bottomRightPx["x"]+'&top='+topLeftPx["y"]+'</laUrl></situacionAragon>';


}

function getWMSRequests(bbox,mapWidth, mapHeight){
	var requests = [];
	mapOL.getLayers().forEach(function(layer, i) {
		
		pushWMSRequest(requests,layer, bbox, mapWidth,mapHeight);

	});

	return requests;
}
function getDataToPrint(){
	var newBboxPrint = checkDeformation(printMapWidth, printMapHeight);
	var extent = newBboxPrint.split(",");
	var wms_layers = getWMSRequests(newBboxPrint,printMapWidth, printMapHeight);
	var capas="";
	for (var i=0; i<wms_layers.length; i++){
		capas += "<mapa><laUrl>"+wms_layers[i]+"</laUrl></mapa>";
	}
	
	var titulo = document.getElementById("titleForPrint").value;
	
	// devolver el XML completo con la informaci�n necesaria para el PDF
	return "<imprimir><fecha>"+getFechaActual()+"</fecha><titulo>"+titulo+"</titulo>"+
	"<leyenda>"+printAppUrl4Legend+$("#leyenda img").attr("src")+"</leyenda>"+capas+
	"<mapa><laUrl>"+getPrintScale(printMapWidth, printMapHeight,extent[0],extent[2])+
	"</laUrl></mapa>"+getPrintOverview(extent)+
"</imprimir>";
}
