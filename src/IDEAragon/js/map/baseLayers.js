var fondosTextos=new Array();
var fondosNiveles=new Array();
var fondosWMS=new Array();
var fondosConOrto=new Array();
var fondosTranspOrto=new Array();
var fondosScaleOrto=new Array();

/**
 * Función para añadir los fondos cartograficos disponibles en el mapa
@param id: identificador del fondo. El que os parezca, sin espacios ni carácteres extraños (acentos, ñ, ....)
@param label: etiqueta que quereís que se muestre en el botón
@param icon: nombre del archivo de imagen que queréis que se muestre en el botón. Estará almacenado en /lib/IDEAragon/themes/default/images
@param wmsRequests: vector con las peticiones wms para obtener el fondo (lo normal es que sea una variable definida en wmsParam.js)
@param opciones: si se quiere que aparezca desplegable con opciones para el fondo (como en ortofoto), pasar el vector con las opciones. En caso contrario null.
@param conOrto: true si al representar el fondo hay que incluir la orto, false en caso contrario
@param transpOrto: porcentaje de trasnsparencia a aplicar a la orto (en caso de que se haya indicado conOrto=true).
@param scaleOrto: escala a partir de la que incluir la orto (en caso de que se haya indicado conOrto=true)
@param niveles: vector con las escalas en las que cambiar el texto de fondo o null si no se cambia el texto. Si hay opciones será un vector con las escalas para cada opción
@param textos: texto del fondo (si niveles=null) o vector con los textos correspondientes a cada nivel del vector indicado en el parámetro niveles. Si hay opciones será un vector con las etiquetas para cada opción
@param isDefault: indica si es el fondo a mostrar por defecto (true) o no (false)
@param selectedOption: si el fondo tiene opciones indica la opción seleccionada inicialmente. Indicar valores entre 0 (primera opción) y número de opciónes-1 (última opción)
*/
function addFondo(id, label, icon, wmsRequests, opciones, conOrto,transpOrto,scaleOrto, niveles, textos, isDefault, selectedOption){
	fondosWMS[id]=wmsRequests;
	fondosNiveles[id]=niveles;
	fondosTextos[id]=textos;
	fondosConOrto[id]=conOrto;
	fondosTranspOrto[id]=transpOrto;
	fondosScaleOrto[id]=scaleOrto;
	$("#fondos").append('<td><input type="radio"  id="'+id+'" name="radio" onclick="updateBaseLayer()"><label  for="'+id+'">'+label+'</label></td>');
	if (opciones &&  (opciones.length>0)){
		$("#fondosOptionsIcon").append('<td><img class="selectorImg" title="Mostrar opciones del fondo" src="/lib/IDEAragon/themes/default/images/down-arrow-mini.png" border="0"  onclick="javascript:showBaseLayerSelector(this,\''+id+'Selector_chosen\');"></img></td>');
		var options ="";
		for (var i=0; i<opciones.length; i++){
			options += '<option'+(selectedOption && selectedOption==i ? " selected":"")+'>'+opciones[i]+'</option>';
		}
		$("#fondosOptions").append('<td><select id="'+id+'Selector" class="baseLayerSelector customCBox" onchange="changeBaseLayerOption(\''+id+'\')">'+options+'</select></td>');
		$("#"+id+'Selector').chosen({ disable_search: true});
	}
	else{
		$("#fondosOptions").append('<td></td>');
		$("#fondosOptionsIcon").append('<td></td>');
	}
	$("#"+id).button({icons : {
		primary : id+"-icon"}
	});
	$(".ui-button .ui-icon."+id+"-icon").css ("background-image", "url(/lib/IDEAragon/themes/default/images/"+icon+")");
	
	if (isDefault){
		document.getElementById(id).checked=true;
		$("#"+id).button("refresh");
	}
	

	 $("#baseToolbar").css("width",30+(80*$("#fondos input").length));
}