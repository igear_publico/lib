fullLeft = null;
fullBottom = null;
fullRight = null;
fullTop = null;


var cuantasWheel = 0;
var xWheel = -1;
var yWheel = -1;
var tWheel = -1;

function wheelRaton(e) {
	if (tWheel != -1) {
		clearTimeout(tWheel);
	}
	var evt = window.event || e;
	var delta = evt.detail? evt.detail*(-120) : evt.wheelDelta; //check for detail first so Opera uses that instead of wheelDelta
	/*
		getXY(e);
		xWheel = theX;
		yWheel = theY;
	*/

		// ignoro donde hizo click y muevo por el centro del mapa
	xWheel = Math.round(mwidth / 2);
	yWheel = Math.round(mheight / 2);

	if (delta > 0) {
		cuantasWheel++;
	} else {
		cuantasWheel--;
	}
	tWheel = setTimeout("actualizaWheel()", 400);
}

function actualizaWheel() {
	var cuantasWheelAux = cuantasWheel;
	cuantasWheel = 0;
	if (cuantasWheelAux > 0) {
		fixedZoomIn(xWheel, yWheel, cuantasWheelAux);
	} else {
		fixedZoomOut(xWheel, yWheel, Math.abs(cuantasWheelAux));
	}
}

function boundingBox(x1, y1, x2, y2) {
	this.minx = x1;
	this.maxx = x2;
	this.miny = y1;
	this.maxy = y2;
}

function getFullMap(){
	getMap(fullLeft, fullBottom, fullRight, fullTop, false, true);
}

xmin_old = "";
xmax_old = "";
ymin_old = "";
ymax_old = "";

function backupExtent() {
	xmin_old = minx;
	xmax_old = maxx;
	ymin_old = miny;
	ymax_old = maxy;
}
function restoreExtent() {
	minx = xmin_old;
	maxx = xmax_old;
	miny = ymin_old;
	maxy = ymax_old;
}

function fixedZoomIn(pxX, pxY, numVeces) {
	backupExtent();

	var newPoint = getMapXY(pxX, pxY);

	growEnvelope(Math.pow(3/4, numVeces), newPoint[0], newPoint[1]);
	getMap(minx, miny, maxx, maxy, false);
}

function fixedZoomOut(pxX, pxY,numVeces) {
	backupExtent();

	var newPoint = getMapXY(pxX, pxY);

	growEnvelope(Math.pow(4/3, numVeces), newPoint[0], newPoint[1]);
	getMap(minx, miny, maxx, maxy, false);
}

function zoom(left, bottom, right, topp){
	backupExtent();
	if (activeTool == "zoomIn") {
		getExtentForZoomIn(left, bottom, right, topp);
	} else {
		getExtentForZoomOut(left, bottom, right, topp);
	}
	getMap(minx, miny, maxx, maxy, false);
}

function pan(ix, iy) {
	backupExtent();
	var dx = (maxx - minx)/mwidth;
	var mx = dx*ix;
	var my = dx*iy;
	minx += mx;
	maxx += mx;
	miny += my;
	maxy += my;
	getMap(minx, miny, maxx, maxy, true);
}

function panDirection(dir){
	backupExtent();
	shift(dir);
	getMap(minx, miny, maxx, maxy, true);
}

function shift(dir){
	backupExtent();
	var dx = maxx - minx;
	var dy = maxy - miny;

	switch(dir) {
		//NORTH
		case "north":
			miny += 0.3*dy;
			maxy += 0.3*dy;
		break;
		//south
		case "south":
			miny -= 0.3*dy;
			maxy -= 0.3*dy;
		break;
		//east
		case "east":
			minx += 0.3*dx;
			maxx += 0.3*dx;
		break;
		//west
		case "west":
			minx -= 0.3*dx;
			maxx -= 0.3*dx;
		break;
		//northeast
		case "ne":
			miny += 0.3*dy;
			maxy += 0.3*dy;
			minx += 0.3*dx;
			maxx += 0.3*dx;
		break;
		//northwest
		case "nw":
			miny += 0.3*dy;
			maxy += 0.3*dy;
			minx -= 0.3*dx;
			maxx -= 0.3*dx;
		break;
		//southeast
		case "se":
			miny -= 0.3*dy;
			maxy -= 0.3*dy;
			minx += 0.3*dx;
			maxx += 0.3*dx;
		break;
		//southwest
		case "sw":
			miny -= 0.3*dy;
			maxy -= 0.3*dy;
			minx -= 0.3*dx;
			maxx -= 0.3*dx;
		break;

	}
}

function getExtentForZoomIn(left, bottom, right, topp){
	var LLPoint = getMapXY(left, bottom);
	var URPoint = getMapXY(right, topp);
	minx = LLPoint[0];
	miny = LLPoint[1];
	maxx = URPoint[0];
	maxy = URPoint[1];
}


function getExtentForZoomOut(left, bottom, right, topp){
	var xDiff= maxx-minx;
	var yDiff= maxy-miny;

	var pwidth = right-left;
	var pheight = topp-bottom;
	var xRatio = mwidth / pwidth;
	var yRatio = mheight / pheight;
	var xAdd = xRatio * xDiff / 2;
	var yAdd = yRatio * yDiff / 2;
	minx = minx - xAdd;
	maxx = maxx + xAdd;
	miny = miny - yAdd;
	maxy = maxy + yAdd;
}

function getMapXY(xIn,yIn) {
	var newValues = new Array();

	var mouseX = xIn;
	var pixelX = (maxx-minx) / mwidth;
	var newX = pixelX * mouseX + minx;
	var mouseY = mheight - yIn;
	var pixelY = (maxy-miny) / mheight;
	var newY = (pixelY * mouseY) + miny;
	newValues[0] = newX;

	newValues[1] = newY;
	return newValues;
}

function growEnvelope(value, cx, cy){
	var dx = maxx - minx;
	var dy = maxy - miny;

	//var cx = (maxx + minx)/2.0;
	//var cy = (maxy + miny)/2.0;

	var dx1 = 0.5 * value * dx;
	var dy1 = 0.5 * value * dy;

	minx = cx - dx1;
	miny = cy - dy1;
	maxx = cx + dx1;
	maxy = cy + dy1;
}

function getMapWithCurrentExtent(avoidCollapse){
	if ((!avoidCollapse)&&(typeof TABLET_WIDTH !== 'undefined')&&(screen.width<=TABLET_WIDTH)){
		collapseTools();
	}
	getMap(minx, miny, maxx, maxy, true);
}

function contains(x, y, _minx, _miny, _maxx, _maxy) {
	return (x >= _minx &&
		y >= _miny &&
		x < _maxx &&
		y < _maxy);
}

function checkMaxScale(_minx, _maxx, _mwidth) {
		// ver si la nueva escala no se va demasiado
	if (getMapScale2(false, _minx, _maxx, _mwidth) > maxScaleAllowed) {
		return false;
	}
	return true;
}

function checkMaxBbox(_minx, _miny, _maxx, _maxy) {
	/*
	r1 = contains(_minx, _miny, limitLeft, limitBottom, limitRight, limitTop);
	r2 = contains(_minx, _maxy, limitLeft, limitBottom, limitRight, limitTop);
	r3 = contains(_maxx, _miny, limitLeft, limitBottom, limitRight, limitTop);
	r4 = contains(_maxx, _maxy, limitLeft, limitBottom, limitRight, limitTop);

alert(r1);
alert(r2);
alert(r3);
alert(r4);
	return (r1 || r2 || r3 || r4);
*/
	if (_minx > limitRight) {
		return false;
	}
	if (_maxx < limitLeft) {
		return false;
	}
	if (_miny > limitTop) {
		return false;
	}
	if (_maxy < limitBottom) {
		return false;
	}
	return true;
}

function checkDeformation(_minx, _miny, _maxx, _maxy, mwidth, mheight) {
	var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);

	var ratioTamano = mwidth / mheight;
	var diffX = _maxx - _minx;
	var diffY = _maxy - _miny;
	var ratioCoord = diffX / diffY;
//alert("ratioT" + ratioTamano + "," + ratioCoord)
	if (ratioCoord > ratioTamano) {
		// estirar la Y
	//	alert("estirar la Y")

		var newDiffY = diffX / ratioTamano;

		var gapY = newDiffY - diffY;

		if (gapY > 0) {
			newBBOX.miny = _miny - (gapY/2);
			newBBOX.maxy = _maxy + (gapY/2);
		} else {
			newBBOX.maxy = _miny - (gapY/2);
			newBBOX.miny = _maxy + (gapY/2);
		}
	} else if (ratioCoord < ratioTamano){
		// estirar la X
		//alert("estirar la X")

		var newDiffX = ratioTamano * diffY;

		var gapX = newDiffX - diffX;
		if (gapX > 0) {
			newBBOX.minx = _minx - (gapX/2);
			newBBOX.maxx = _maxx + (gapX/2);
		} else {
			newBBOX.maxx = _minx - (gapX/2);
			newBBOX.minx = _maxx + (gapX/2);
		}
	}
	return newBBOX;
}

/*
function checkDeformationPrint(_minx, _miny, _maxx, _maxy, mwidth, mheight) {
	var ratioTamano = mwidth / mheight;
	var diffX = _maxx - _minx;
	var diffY = _maxy - _miny;
	var ratioCoord = diffX / diffY;
//alert("ratioT" + ratioTamano + "," + ratioCoord)
	if (ratioCoord > ratioTamano) {
		// estirar la Y
	//	alert("estirar la Y")

		var newDiffY = diffX / ratioTamano;

		var gapY = newDiffY - diffY;
		minyPrint = _miny - (gapY/2);
		maxyPrint = _maxy + (gapY/2);
		minxPrint = _minx;
		maxxPrint = _maxx;

	} else if (ratioCoord < ratioTamano){
		// estirar la X
		//alert("estirar la X")

		var newDiffX = ratioTamano * diffY;

		var gapX = newDiffX - diffX;
		minx = _minx - (gapX/2);
		maxx = _maxx + (gapX/2);
		minyPrint = _miny;
		maxyPrint = _maxy;
	}
}
*/

//this for zoom to scale functionality
function doZoomScale(newScale) {
	var tempFactor = 96;
	var inchPerMeter = 39.4;

	distanceInches = (newScale) / tempFactor;
	distanceOnePixel = distanceInches / inchPerMeter;
	xDistance = distanceOnePixel * mwidth;
	yDistance = distanceOnePixel * mheight;

	xMidPoint = minx + ((maxx - minx)/2);
	yMidPoint = maxy - ((maxy - miny)/2);

	newLeft = xMidPoint - (xDistance/2);
	newRight = xMidPoint + (xDistance/2);
	newTop = yMidPoint + (yDistance/2);
	newBottom = yMidPoint - (yDistance/2);

	getMap(newLeft, newBottom, newRight, newTop, true);
}
//end zoom to scale functionality
