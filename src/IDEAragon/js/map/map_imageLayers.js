function genericLoadLayers(map,map_projection, layerList, prefix, zIdx, opacity) {
	//layerList = new Array();
	var maxLayerCount = eval(prefix + "_LAYERS");
	for (var i = 0; i < maxLayerCount; i++) {
		var layer = new ol.layer.Image({
					title: eval(prefix + "_TITLE_" + i),
					source: new ol.source.ImageWMS({
							params: {"LAYERS": eval(prefix + "_LAYER_" + i),
								"VERSION": "1.1.1",
								"TRANSPARENT": true,
								"FORMAT": "image/png"},
							url: eval(prefix + "_WMS_URL_" + i),
							projection: map_projection
					})
		});
		layer.setZIndex(zIdx);
		layer.setOpacity(opacity);
		map.addLayer(layer);
		layerList[i] = layer;
	}
}
function getPrintWMS(newBboxPrint,printMapWidth, printMapHeight){
	var wms_layers = getWMSRequests(newBboxPrint,printMapWidth, printMapHeight);
	var capas="";
	for (var i=0; i<wms_layers.length; i++){
		capas += "<mapa><laUrl>"+wms_layers[i]+"</laUrl></mapa>";
	}
	return capas;
}
function getWMSRequests(bbox,mapWidth, mapHeight){
	var requests = [];
	map.getLayers().forEach(function(layer, i) {
		
		pushWMSRequest(requests,layer, bbox, mapWidth,mapHeight);

	});

	return requests;
}

function pushWMSRequest(requests,layer, bbox, mapWidth,mapHeight){
	if (layer instanceof ol.layer.Group){
		layer.getLayers().forEach(function(sublayer, i) {
			 pushWMSRequest(requests,sublayer, bbox, mapWidth,mapHeight);

		});
	}
	else{
		if (layer.getVisible()){
		var source = layer.getSource();
		if ((source instanceof ol.source.TileWMS)||(source instanceof ol.source.ImageWMS)){
			var params = source.getParams();
			var url = (source instanceof ol.source.TileWMS ? source.getUrls()[0]:source.getUrl());
			url = urlAbsoluta(url);
			if (url.indexOf("?")<0){
				url = url+"?";
			}
			url += "SERVICE=WMS&REQUEST=GetMap&TRANSPARENT=true";
			url +="&VERSION=";
			if (params.VERSION){
				url +=params.VERSION;
			}
			else{
				url +="1.1.1";
			}
			url +="&FORMAT=";
			if (params.FORMAT){
				url +=params.FORMAT;
			}
			else{
				url +="image/png";
			}
			url +="&SRS=";
			if (params.SRS){
				url +=params.SRS;
			}
			else{
				url +="EPSG:25830";
			}
			requests.push(url+"&STYLES=&Layers=" + params.LAYERS+
					 '&WIDTH=' + mapWidth + '&HEIGHT=' + mapHeight+
			 '&BBOX=' +bbox);
				
		}
		}
	}
}