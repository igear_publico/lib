////////////////////////////////////////////////////////////
// Fichero: geometry.js
// Paquete: commonTools/geometry
// Fecha creaci�n:
// Fecha �ltima modificaci�n: 07-11-2003
// Autor: David Portol�s Rodr�guez (dportoles)
// Versi�n: 1.0.8
// Descripci�n: Contiene las clases Geometry... y java.awt.geom.*
////////////////////////////////////////////////////////////

function Geometry(srs) {
	// INTERFAZ: getBounds():Rectangle2D_Double
	this.srs = srs || "EPSG:4326";

	this.getSrs = getSrs;
	this.setSrs = setSrs;

	function getSrs() {
		return this.srs;
	}

	function setSrs(newSrs) {
		this.srs = newSrs;
	}
}
Geometry.POINT = 0;
Geometry.LINE_STRING = 1;
Geometry.LINEAR_RING = 2;

function Point2D_Double(x,y) {
	this.x = x || 0;
	this.y = y || 0;

	this.getX = getX;
	this.setX = setX;
	this.getY = getY;
	this.setY = setY;
	this.setLocation = setLocation;

	function getX() {
		return this.x;
	}

	function setX(newX) {
		this.x = newX;
	}

	function getY() {
		return this.y;
	}

	function setY(newY) {
		this.y = newY;
	}

	function setLocation(newX, newY) {
		this.setX(newX);
		this.setY(newY);
	}
}
Point2D_Double.prototype = new Geometry;

function Rectangle2D_Double(x, y, width, height) {
	this.x = x || 0;
	this.y = y || 0;
	this.width = width || 0;
	this.height = height || 0;

	this.getX = getX;
	this.setX = setX;
	this.getY = getY;
	this.setY = setY;
	this.getWidth = getWidth;
	this.setWidth = setWidth;
	this.getHeight = getHeight;
	this.setHeight = setHeight;

	this.setRect = setRect;
	this.isEmpty = isEmpty;
	this.contains = contains;

	function getX() {
		return this.x;
	}

	function setX(newX) {
		this.x = newX;
	}

	function getY() {
		return this.y;
	}

	function setY(newY) {
		this.y = newY;
	}

	function getWidth() {
		return this.width;
	}

	function setWidth(newWidth) {
		this.width = newWidth;
	}

	function getHeight() {
		return this.height;
	}

	function setHeight(newHeight) {
		this.height = newHeight;
	}

	function setRect(newX, newY, newWidth, newHeight) {
		this.setX(newX);
		this.setY(newY);
		this.setWidth(newWidth);
		this.setHeight(newHeight);
	}

	function isEmpty() {
		return (this.width <= 0.0) || (this.height <= 0.0);
	}

	function contains(x, y) {
		x0 = this.getX();
		y0 = this.getY();
		return (x >= x0 &&
			y >= y0 &&
			x < x0 + this.getWidth() &&
			y < y0 + this.getHeight());
	}
}
Rectangle2D_Double.prototype = new Geometry;

function Point(x,y) {
		// llamar al constructor del padre; equivale al m�todo de Java super()
	this._super = Point2D_Double;
	this._super(x, y);

	this._bounds = null; //new Rectangle2D_Double;

	this.getBounds = getBounds;
	this.getClass = getClass;

	function getBounds() {
		if ((this._bounds == null) ||(this._bounds.x != this.x)||(this._bounds.y != this.y)) {
			this._bounds = new Rectangle2D_Double(x,y,0,0);
		}
		return this._bounds;
	}

	function getClass() {
		return Geometry.POINT;
	}
}
Point.prototype = new Point2D_Double;

function Curve() {
	// Interfaz, define 3 metodos:
	// double getLength();
	// GeoPoint getStartPoint();
	// GeoPoint getEndPoint();
}
Curve.prototype = new Geometry;

function Surface() {
	// Interfaz, define 2 metodos:
	// double getArea();
	// double getPerimeter();
}
Surface.prototype = new Geometry;

function LineString(xpoints, ypoints, npoints) {
	/**
	* N�mero de puntos del LineString.
	*/
	this.npoints = npoints || 0;

	/**
	* Coordenadas X de los puntos del LineString.
	*/
	this.xpoints = xpoints || new Array(0);

	/**
	* Coordenadas Y de los puntos del LineString.
	*/
	this.ypoints = ypoints || new Array(0);

	/**
	* Rect�ngulo que engloba al LineString.
	*/
	this._bounds = null; //new Rectangle2D_Double;

	this.getLength = getLength;
	this.getStartPoint = getStartPoint;
	this.getEndPoint = getEndPoint;
	this.addPoint = addPoint;
	this.getBounds = getBounds;
	this.setBounds = setBounds;
	this.move = move;
	this._calculateBounds = _calculateBounds;
	this.getClass = getClass;

	/**
	 * Sin par�metros:
	 * Calcula longitud del LineString.
	 * @return Longitud del LineString
	 *
	 * Con par�metros:
	 * Calcula la longitud de un tramo dado del GeoLineString. Un tramo es
	 * el segmento que une dos puntos consecutivos en un GeoLineString. El primer
	 * tramo de un GeoLineString es el 0 y el �ltimo es el (this.npoints - 2).
	 * @param tramo Tramo dado del GeoLineString
	 * @return Longitud del tramo del GeoLineString
	 */

	function getLength(tramo) {
		if (tramo) {
			if ((tramo >= 0) && (tramo <= this.npoints - 2)) {
				return Math.sqrt((this.xpoints[tramo + 1] - this.xpoints[tramo])
																					* (this.xpoints[tramo + 1] - this.xpoints[tramo])
												+
												 (this.ypoints[tramo + 1] - this.ypoints[tramo])
																					* (this.ypoints[tramo + 1] - this.ypoints[tramo]));
			} else {
				return 0.0;
			}
		} else {
			length = 0.0;

			for (i=0; i <= this.npoints - 2; i++) {
				length += this.getLength(i);
			}

			return length;
		}
	}

	/**
	 * Obtener punto de comienzo del GeoLineString.
	 * @return Punto de comienzo del GeoLineString
	 */
	function getStartPoint() {
		return new Point(this.xpoints[0], this.ypoints[0]);
	}

	/**
	 * Obtener punto final del GeoLineString.
	 * @return Punto final del GeoLineString
	 */
	function getEndPoint() {
		return new Point(this.xpoints[this.npoints - 1], this.ypoints[this.npoints - 1]);
	}

	/**
	 * A�ade un punto a este GeoLineString.
	 * Si ya se ha realizado alguna operaci�n que calcule _bounds en este
	 * GeoLineString, como <code> getBounds </code> o <code> contains </code>,
	 * entonces este m�todo actualiza _bounds.
	 * @param x La coordenada X del punto
	 * @param y La coordenada Y del punto
	 */
	function addPoint(x, y) {
		/* Esto debe ser por usar Java
		if (this.npoints == this.xpoints.length) {
			double tmp[];
			tmp = new double[this.npoints * 2];
			System.arraycopy(this.xpoints, 0, tmp, 0, this.npoints);
			this.xpoints = tmp;
			tmp = new double[this.npoints * 2];
			System.arraycopy(this.ypoints, 0, tmp, 0, this.npoints);
			this.ypoints = tmp;
		}
		*/
		this.xpoints[this.npoints] = x;
		this.ypoints[this.npoints] = y;
		this.npoints++;
		if (this._bounds != null) {
			if (x < this._bounds.x) {
				this._bounds.width = this._bounds.width + (this._bounds.x - x);
				this._bounds.x = x;
			} else {
				this._bounds.width = Math.max(this._bounds.width, x - this._bounds.x);
			}

			if (y < this._bounds.y) {
				this._bounds.height = this._bounds.height + (this._bounds.y - y);
				this._bounds.y = y;
			} else {
				this._bounds.height = Math.max(this._bounds.height, y - this._bounds.y);
			}
		}
	}

	/**
	 * Devuelve la caja m�mima (bounding box) que envuelve este GeoLineString.
	 * Esta caja es el rect�ngulo m�s peque�o cuyos lados son paralelos a los
	 * ejes X e Y del espacio de coordenadas, y que puede contener
	 * completamente a este GeoLineString.
	 * @return Un rect�ngulo que define la caja que envuelve este GeoLineString
	 */
	function getBounds() {
		if (this._bounds == null) {
			this._bounds = _calculateBounds(this.xpoints, this.ypoints, this.npoints);
		}
		return this._bounds;
	}

	/**
	 * Establece un nuevo bounding box para este GeoLineString.
	 * @param x Coordenada X izquierda del nuevo bounding box
	 * @param y Coordenada Y superior del nuevo bounding box
	 * @param w Anchura del nuevo bounding box
	 * @param h Altura del nuevo bounding box
	 */
	function setBounds(x, y, w, h) {
		this._bounds = new Rectangle2D_Double(x, y, w, h);
	}


	/**
	 * Mueve el GeoLineString, moviendo cada punto la cantidad pasada como
	 * par�metro.
	 * @param incx Desplazamiento del GeoLineString en X
	 * @param incy Desplazamiento del GeoLineString en Y
	 */
	function move(incx, incy)	{
		for (i = 0; i < this.npoints; i++) {
			this.xpoints[i] += incx;
			this.ypoints[i] += incy;
		}
		this._bounds = _calculateBounds(this.xpoints, this.ypoints, this.npoints);
	}


	/**
	 * Calcula la caja que contiene a este GeoLineString.
	 * Mete el resultado en _bounds.
	 * @param xpoints Un array de coordenadas X
	 * @param ypoints Un array de coordenadas Y
	 * @param npoints N�mero de puntos del GeoLineString
	 */
	function _calculateBounds(xpoints, ypoints, npoints) {
		boundsMinX = Number.MAX_VALUE;
		boundsMinY = Number.MAX_VALUE;
		boundsMaxX = -Number.MAX_VALUE;
		boundsMaxY = -Number.MAX_VALUE;

		if (npoints > 0) {
			boundsMinX = xpoints[0];
			boundsMinY = ypoints[0];
			boundsMaxX = xpoints[0];
			boundsMaxY = ypoints[0];
		}
		for (i = 1; i < npoints; i++) {
			x = xpoints[i];
			boundsMinX = Math.min(boundsMinX, x);
			boundsMaxX = Math.max(boundsMaxX, x);
			y = ypoints[i];
			boundsMinY = Math.min(boundsMinY, y);
			boundsMaxY = Math.max(boundsMaxY, y);
		}
		this._bounds = new Rectangle2D_Double(boundsMinX, boundsMinY,
																		 boundsMaxX - boundsMinX,
																		 boundsMaxY - boundsMinY);
		return this._bounds;
	}

	function getClass() {
		return Geometry.LINE_STRING;
	}
}
LineString.prototype = new Curve;

function Line(x1, y1, x2, y2) {
	this.npoints = 2;

	this.xpoints = new Array(2);
	this.xpoints[0] = x1;
	this.xpoints[1] = x2;

	this.ypoints = new Array(2);
	this.ypoints[0] = y1;
	this.ypoints[1] = y2;

	this.addPoint = addPoint;

	/**
	 * Este m�todo no hace nada. As� se impide crear GeoLines con m�s de
	 * dos puntos.
	 */
	function addPoint() {
	}
}
Line.prototype = new LineString;

function LinearRing(xpoints, ypoints, npoints) {
		// llamar al constructor del padre; equivale al m�todo de Java super()
	this._super = LineString;
	this._super(xpoints ,ypoints, npoints);

	this.contains = contains;
	this.isClockWise = isClockWise;
	this.getClass = getClass;

	function contains(x, y) {
		if (this.getBounds().contains(x, y)) {
			hits = 0;
			ySave = 0;

			// Find a vertex that is not on the halfline
			var i = 0;
			while ((i < this.npoints) && (this.ypoints[i] == y)) {
				i++;
			}

			// Walk the edges of the polygon
			for (var n = 0; n < this.npoints; n++) {
				j = (i + 1) % this.npoints;
				dx = this.xpoints[j] - this.xpoints[i];
				dy = this.ypoints[j] - this.ypoints[i];

				// Ignore horizontal edges completely
				if (dy != 0) {
					// Check to see if the edge intersects
					// the horizontal halfline through (x, y)
					rx = x - this.xpoints[i];
					ry = y - this.ypoints[i];

					// Deal with edges starting or ending on the halfline
					if ((this.ypoints[j] == y) && (this.xpoints[j] >= x)) {
						ySave = this.ypoints[i];
					}
					if ((this.ypoints[i] == y) && (this.xpoints[i] >= x)) {
						if ((ySave > y) != (ypoints[j] > y)) {
							hits--;
						}
					}

					// Tally intersections with halfline
					s = ry / dy;
					if ((s >= 0.0) && (s <= 1.0) && ((s * dx) >= rx)) {
						hits++;
					}
				}
				i = j;
			}
			// Inside if number of intersections odd
			return (hits % 2) != 0;
		}
		return false;
	}

 /**
	* Determinar si los v�rtices de este GeoLinearRing est�n en orden clockwise
	* (direcci�n de las manecillas del reloj) o anti-clockwise (al rev�s).
	* @return Cierto si los v�rtices de este GeoLinearRing est�n en orden clockwise
	*/
	function isClockWise() {
		N = this.npoints;
		i = 0;
		j = 0;
		area = 0;

		for (i=0;i<N;i++) {
			j = (i + 1) % N;
			area += this.xpoints[i] * this.ypoints[j];
			area -= this.ypoints[i] * this.xpoints[j];
		}

		return (area < 0);
	}

	function getClass() {
		return Geometry.LINEAR_RING;
	}
}
LinearRing.prototype = new LineString;

////////////////////////////////////////////////////////////
// Clase: Polygon
// Descripci�n: Representa a un pol�gono (listado de v�rtices)
//							con posibles agujeros (interiorRings)
////////////////////////////////////////////////////////////
function Polygon(exteriorRing, interiorRings) {
		/**
		* Frontera externa del GeoPolygon.
		*/
	this.exteriorRing = exteriorRing;

		/**
		* Fronteras internas del GeoPolygon.
		*/
	this.interiorRings = interiorRings || new Array(0);

	this.numInteriorRings = this.interiorRings.length || 0;

	this.contains = contains;

	this.getExteriorRing = getExteriorRing;
	this.getInteriorRings = getInteriorRings;
	this.getNumInteriorRings = getNumInteriorRings;
	this.getArea = getArea;
	this._getRingArea = _getRingArea;
	this.getPerimeter = getPerimeter;
	this.getBounds = getBounds;

	/**
	 * Obtener la frontera externa del pol�gono.
	 * @return Una referencia a la frontera externa del pol�gono
	 */
	function getExteriorRing() {
		return this.exteriorRing;
	}

	/**
	 * Obtener el n�mero de fronteras internas del pol�gono.
	 * @return N�mero de fronteras internas del pol�gono
	 */
	function getNumInteriorRings() {
		return this.numInteriorRings;
	}

	/**
	 * Obtener las fronteras internas del pol�gono.
	 * @return Una referencia a las fronteras internas del pol�gono
	 */
	function getInteriorRings() {
		return this.interiorRings;
	}

	/**
	 * Calcula el �rea del pol�gono. El �rea ser� la contenida en la frontera
	 * externa menos las contenidas en las fronteras internas (agujeros).
	 * @return �rea del pol�gono
	 */
	function getArea() {
		extArea = _getRingArea(this.exteriorRing);

		for (var i = 0; i < this.interiorRings.length; i++) {
			extArea -= _getRingArea(this.interiorRings[i]);
		}

		return extArea;
	}

	function _getRingArea(ring) {
		N = ring.npoints;
		i = 0;
		j = 0;
		area = 0;

		for (i=0;i<N;i++) {
			j = (i + 1) % N;
			area += ring.xpoints[i] * ring.ypoints[j];
			area -= ring.ypoints[i] * ring.xpoints[j];
		}

		area /= 2;
		return(area < 0 ? -1*area : area);
	}

	/**
	* Devuelve el rect�ngulo envolvente de este GeoPolygon.
	* @return El rect�ngulo envolvente de este GeoPolygon
	*/
	function getBounds() {
		return this.exteriorRing.getBounds();
	}

 /**
	 * Calcula el per�metro del pol�gono. Este per�metro ser� la longitud
	 * de su frontera externa.
	 * @return Per�metro del pol�gono
	 */
	function getPerimeter() {
		return this.exteriorRing.getLength();
	}
}
Polygon.prototype = new Surface;
