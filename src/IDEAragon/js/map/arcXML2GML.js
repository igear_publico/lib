function getGMLPolygon(arcXML){
	//var gml = arcXML.replace(/POLYGON/g,"gml:outerBoundaryIs").replace(/RING/g,"gml:LinearRing").replace(/COORDS/g,"gml:coordinates");
 
	//<RING><POINT x="678715,9403" y="4596183,2117"/>
	var gml = arcXML[0].replace(/<RING>/g,"").replace(/<\/RING>/g,"").replace(/COORDS/g,"gml:coordinates");
	gml="<gml:coordinates>"+getGMLPointCoords(gml)+"</gml:coordinates>";
	return '<gml:Polygon srsName="http://www.opengis.net/gml/srs/epsg.xml#25830"><gml:outerBoundaryIs><gml:LinearRing>'
	+gml+'</gml:LinearRing></gml:outerBoundaryIs></gml:Polygon>';
}

function getGMLLine(arcXML){
	//var gml = arcXML.replace(/<POLYLINE>/g,"").replace(/<\/POLYLINE>/g,"").replace(/<PATH>/g,"").replace(/<\/PATH>/g,"").replace(/COORDS/g,"gml:coordinates");
	
	/*<POLYLINE>
	<PATH>
	<POINT x="679822,8075" y="4591709,2293"/>
	<POINT x="679829,1199" y="4591690,7293"/>*/
	
	var gml = arcXML[0].replace(/<POLYLINE>/g,"").replace(/<\/POLYLINE>/g,"").replace(/<PATH>/g,"").replace(/<\/PATH>/g,"").replace(/COORDS/g,"gml:coordinates");
	gml="<gml:coordinates>"+getGMLPointCoords(gml)+"</gml:coordinates>";
	return '<gml:LineString srsName="http://www.opengis.net/gml/srs/epsg.xml#25830">'+gml+'</gml:LineString>';
}

function getGMLPointCoords(arcXMLPoints){
	var gml = arcXMLPoints.replace(/<POINT /g,"");
	gml=gml.replace(/,/g,".");
	gml=gml.replace(/x=\"/g,"");
	gml=gml.replace(/\" y=\"/g,",");
	gml=gml.replace(/\"\/>/g," ");
	return gml;
}
function getGMLPoint(arcXML){
				/*
	var gml = arcXML.replace(/<MULTIPOINT>/g,"").replace(/<\/MULTIPOINT>/g,"").replace(/COORDS/g,"gml:coordinates");
	gml=gml.replace(/,/g,".").replace(/\s/g,",");
	return '<gml:Point srsName="http://www.opengis.net/gml/srs/epsg.xml#25830">'+gml+'</gml:Point>';
*/
				// lo que tengo realmente es: "<POINT x="726727,5315" y="4525963,5604"/>"
	var gml = arcXML[0].replace(/<MULTIPOINT>/g,"").replace(/<\/MULTIPOINT>/g,"").replace(/POINT/g,"gml:coordinates");
	gml=gml.replace(/,/g,".");
	gml=gml.replace(/x=\"/g,">");
	gml=gml.replace(/\" y=\"/g,",");
	gml=gml.replace(/\"\/>/g,"</gml:coordinates>");
	return '<gml:Point srsName="http://www.opengis.net/gml/srs/epsg.xml#25830">'+gml+'</gml:Point>';
}
