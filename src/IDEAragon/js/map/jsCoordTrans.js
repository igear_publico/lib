////////////////////////////////////////////////////////////
// Fichero: jsCoordTrans.js
// Paquete:
// Fecha creaci�n: 15-05-2003
// Fecha �ltima modificaci�n: 30-05-2003
// Autor: David Portol�s Rodr�guez (dporto)
// Versi�n: 1.0
// Descripci�n: Contiene las clases (Seg�n paquetes)
//	org.geotools.units:
//	-------------------
//		Unit, Unit_metre, Unit_degree
// org.geotools.cs:
// ----------------
//		WGS84ConversionInfo, Info, Ellipsoid, DatumType, Datum
//		HorizontalDatum, Spheroid, Projection, Ellipsoid_FlattenedSphere
//		DatumType_classic
// org.geotools.ct:
// ----------------
//		Point2D_Double, CoordinatePoint, MapProjection, MapProjection_inverse
//		TransverseMercatorProjection
////////////////////////////////////////////////////////////


//////////////////////////////////////////
//	Paquete: org.geotools.units
//////////////////////////////////////////


	// ESTA CLASE ESTA MUY MAL HECHA!!!!!
function Unit(unit) {
	this.unit = unit;

	this.canConvert = canConvert;
	this.equals = equals;

	////// !!!!!!!!!!!!
	function canConvert() {
		return true;
	}

	function equals(aUnit) {
		return (aUnit.unit == this.unit);
	}
}

function Unit_metre() {
	return new Unit('m');
}

function Unit_degree() {
	return new Unit('�');
}

//////////////////////////////////////////
//	Paquete: org.geotools.cs
//////////////////////////////////////////

function WGS84ConversionInfo() {
		// Bursa Wolf shift in meters.
	this.dx = -1;
		// Bursa Wolf shift in meters.
	this.dy = -1;
		// Bursa Wolf shift in meters.
	this.dz = -1;
		// Bursa Wolf rotation in arc seconds.
	this.ex = -1;
		// Bursa Wolf rotation in arc seconds.
	this.ey = -1;
		// Bursa Wolf rotation in arc seconds.
	this.ez = -1;
		// Bursa Wolf scaling in parts per million.
	this.ppm = -1;
		// Human readable text describing intended region of transformation.
	this.areaOfUse = '';

	this.equals = equals;

	function equals(that) {
		if ((this.dx == that.dx) &&
				(this.dy == that.dy) &&
				(this.dz == that.dz) &&
				(this.ex == that.ex) &&
				(this.ey == that.ey) &&
				(this.ez == that.ez) &&
				(this.ppm == that.ppm) &&
				(this.areaOfUse == that.areaOfUse)) {
			return true;
		}
		return false;
	}
}

function Info(name) {
	this.name = name; // String

	this.getName = getName();
	this.ensureAngularUnit = ensureAngularUnit;
	this.ensureLinearUnit = ensureLinearUnit;

	function getName() {
		return this.name;
	}

	function ensureLinearUnit(unit) {
		if (!new Unit_metre().canConvert(unit)) {
			alert("Error: Unit::ensureLinearUnit");
		}
	}

	function ensureAngularUnit(unit) {
		if (!new Unit_degree.canConvert(unit)) {
			alert("Error: Unit::ensureAngularUnit");
		}
	}
}

function Ellipsoid(name, semiMajorAxis, semiMinorAxis, inverseFlattening, ivfDefinitive, unit) {
	this._super = Info;
	this._super(name);

	this.unit = unit;
	this.semiMajorAxis = check("semiMajorAxis", semiMajorAxis);
	this.semiMinorAxis = check("semiMinorAxis", semiMinorAxis);
	this.inverseFlattening = check("inverseFlattening", inverseFlattening);
	this.ivfDefinitive = ivfDefinitive;
	this.check = check;
	this.equals = equals;

	this.ensureLinearUnit(unit);

	this.getSemiMinorAxis = getSemiMinorAxis;
	this.getSemiMajorAxis = getSemiMajorAxis;

	function getSemiMinorAxis() {
		return this.semiMinorAxis;
	}

	function getSemiMajorAxis() {
		return this.semiMajorAxis;
	}

	this.createEllipsoid = createEllipsoid;

	function check(name, value) {
		if (value>0) {
			return value;
		}
		alert("Error Ellipsoid::check: " + name);
	}

	function createEllipsoid(name, semiMajorAxis, semiMinorAxis, unit) {
		if (semiMajorAxis == semiMinorAxis) {
			return new Spheroid(name, semiMajorAxis, false, unit);
		} else {
			return new Ellipsoid(name, semiMajorAxis, semiMinorAxis,
					semiMajorAxis/(semiMajorAxis-semiMinorAxis), false, unit);
		}
	}

	function equals(anEllips) {
		if (this.semiMajorAxis == anEllips.semiMajorAxis) {
			if (this.semiMinorAxis == anEllips.semiMinorAxis) {
				if (this.inverseFlattening == anEllips.inverseFlattening) {
					return true;
				}
			}
		}
		return false;
	}
}
Ellipsoid.prototype = Info;

function Ellipsoid_FlattenedSphere(name, semiMajorAxis, inverseFlattening, unit) {
	if (!isFinite(inverseFlattening)) {
		return new Spheroid(name, semiMajorAxis, true, unit);
	} else {
		return new Ellipsoid(name, semiMajorAxis, semiMajorAxis*(1-1/inverseFlattening), inverseFlattening, true, unit);
	}
}

function Spheroid(name, radius, ivfDefinitive, unit) {
	this._super = Ellipsoid;
	this._super(name, this.check("radius", radius), radius, Infinity, ivfDefinitive, unit);
}
Spheroid.prototype = Ellipsoid;

function Ellipsoid_WGS84() {
		// (Ellipsoid) pool.canonicalize(createFlattenedSphere("WGS84", 6378137.0, 298.257223563, Unit.METRE));
		// paso del canonicalize...
	return new Ellipsoid_FlattenedSphere("WGS84", 6378137.0, 298.257223563, new Unit_metre());
}

function DatumType(name, value, key) {
	this.name = name; // String
	this.value = value; // int
	this.key = key; //int
}

function DatumType_classic() {
		// public static final int CS_HD_Classic 1001
		// public static final int CLASSIC = 5;
		//return new DatumType("CLASSIC", CS_HD_Classic, ResourceKeys.CLASSIC);
	return new DatumType("CLASSIC", 1001, 5);
}

function DatumType_geocentric() {
		//	public static final int CS_HD_Geocentric = 1002;
		//	public static final int GEOCENTRIC = 15;
		//	public static final Horizontal GEOCENTRIC = new Horizontal("GEOCENTRIC", CS_DatumType.CS_HD_Geocentric, ResourceKeys.GEOCENTRIC);
	return new DatumType("GEOCENTRIC", 1002, 15);
}

function Datum(name, datumType) {
	this._super = Info;
	this._super(name);

	this.datumType = datumType;

	this.getDatumType = getDatumType;

	function getDatumType() {
		return this.datumType;
	}
}
Datum.prototype = Info;


function HorizontalDatum(name, type, ellipsoid, parameters) {
	this._super = Datum;
	this._super(name, type);

	this.ellipsoid = ellipsoid;
		// supuestamente, hacer un clone de los parameters!!!!
	this.parameters = parameters;
	this.equals = equals;

	this.getEllipsoid = getEllipsoid;
	this.equals = equals;

	function getEllipsoid() {
		return this.ellipsoid;
	}

	function equals(aHorizDatum) {
		if (this.getEllipsoid().equals(aHorizDatum.getEllipsoid())) {
			if (this.parameters.equals(aHorizDatum.parameters)) {
				return true;
			}
		}
		return false;
	}
}
HorizontalDatum.prototype = Datum;


function Projection(name, classification, semiMinor, semiMajor, falseNorthing, falseEasting, centralMeridian, centralLatitude, scaleFactor) {
	this._super = Info;
	this._super(name);

	this.classification = classification || "";

	this.falseNorthing = falseNorthing;
	this.falseEasting = falseEasting;
	this.centralMeridian = centralMeridian;
	this.centralLatitude = centralLatitude;
	this.semiMinor = semiMinor;
	this.semiMajor = semiMajor;
	this.scaleFactor = scaleFactor;

	this.getFalseNorthing = getFalseNorthing;
	this.getFalseEasting = getFalseEasting;
	this.getCentralMeridian = getCentralMeridian;
	this.getCentralLatitude = getCentralLatitude;
	this.getSemiMinor = getSemiMinor;
	this.getSemiMajor = getSemiMajor;
	this.getScaleFactor = getScaleFactor;
	this.getClass = getClass;

	function getFalseNorthing() {
		return this.falseNorthing;
	}

	function getFalseEasting() {
		return this.falseEasting;
	}

	function getCentralMeridian() {
		return this.centralMeridian;
	}

	function getCentralLatitude() {
		return this.centralLatitude;
	}

	function getSemiMinor() {
		return this.semiMinor;
	}

	function getSemiMajor() {
		return this.semiMajor;
	}

	function getScaleFactor() {
		return this.scaleFactor;
	}

	function getClass() {
		return this.classification;
	}
}

Projection.prototype = Info;

//////////////////////////////////////////
//	Paquete: org.geotools.ct
//////////////////////////////////////////

function Point2D_Double(x,y) {
	this.x = x || 0;
	this.y = y || 0;

	this.getX = getX;
	this.getY = getY;
	this.setLocation = setLocation;

	function getX() {
		return this.x;
	}

	function getY() {
		return this.y;
	}

	function setLocation(newX, newY) {
		this.x = newX;
		this.y = newY;
	}
}

function CoordinatePoint(dim) {
	this.ord = new Array(dim);
	this.getOrd = getOrd;
	this.setLocation = setLocation;

	function getOrd() {
		return this.getOrd;
	}

	function setLocation(x,y) {
		this.ord[0] = x;
		this.ord[1] = y;
	}
}


function MapProjection(projection) {
	this.semiMajor = projection.getSemiMajor();
	this.semiMinor = projection.getSemiMinor();
	this.centralMeridian = degrees2radians(projection.getCentralMeridian());

	this.centralLatitude = degrees2radians(projection.getCentralLatitude());
	this.scaleFactor = projection.getScaleFactor();
	this.falseEasting = projection.getFalseEasting();
	this.falseNorthing = projection.getFalseNorthing();
	this.isSpherical = (this.semiMajor == this.semiMinor);
	this.es = 1.0 - (this.semiMinor * this.semiMinor)/(this.semiMajor * this.semiMajor);
	this.e = Math.sqrt(this.es);

	this.MAX_ERROR = 1;
	this.EPS=1.0E-6;
	this.TOL=1E-10;

	this.transform = transform;
	this.inverseTransform = inverseTransform;
	this.transform2ptos = transform2ptos;
	this.inverseTransform2ptos = inverseTransform2ptos;

	function transform(src, srcOffset, dest, dstOffset, numPts) {
		//alert("AQUIE ESTA LA TRANSFORM 11");
		reverse = (src==dest && srcOffset<dstOffset &&
				srcOffset+(numPts << 1)>dstOffset);
		if (reverse) {
			srcOffset += 2*numPts;
			dstOffset += 2*numPts;
		}
		point=new Point2D_Double();
		firstException=null;
		while (--numPts>=0) {
			point.x = src[srcOffset++];
			point.y = src[srcOffset++];
			this.transform2ptos(point, point);
			dest[dstOffset++] = point.x;
			dest[dstOffset++] = point.y;
			if (reverse) {
				srcOffset -= 4;
				dstOffset -= 4;
			}
		}
		if (firstException != null) {
			throw firstException;
		}
	}

	function inverseTransform(src, srcOffset, dest, dstOffset, numPts) {
		//alert("inverseTransform 888");

		reverse = (src==dest && srcOffset<dstOffset &&
				srcOffset+(numPts << 1)>dstOffset);
		if (reverse) {
			srcOffset += 2*numPts;
			dstOffset += 2*numPts;
		}

		point=new Point2D_Double();
		firstException=null;
		while (--numPts>=0) {
			point.x = src[srcOffset++];
			point.y = src[srcOffset++];
			this.inverseTransform2ptos(point, point);
			dest[dstOffset++] = point.x;
			dest[dstOffset++] = point.y;

			if (reverse) {
				srcOffset -= 4;
				dstOffset -= 4;
			}
		}
		if (firstException != null) {
			throw firstException;
		}
	}

	function transform2ptos(ptSrc, ptDst) {
		//alert("AQUIE ESTA LA TRANSFORM 33 (2ptos)");

		x=ptSrc.getX();
		y=ptSrc.getY();
		ptDst = this.transformUTM(degrees2radians(x), degrees2radians(y), ptDst);
		//alert("PX: " + ptDst.getX() + ", " + "PY; " + ptDst.getY());
		return ptDst;
	}

	function inverseTransform2ptos(ptSrc, ptDst) {
		//alert("inverseTransform2ptos");
		x0 = ptSrc.getX();
		y0 = ptDst.getY();
		ptDst = this.inverseTransformUTM(x0, y0, ptDst);
		x = radians2degrees(ptDst.getX());
		y = radians2degrees(ptDst.getY());
		//alert("X: " + x + ", " + "Y; " + y);
		ptDst.setLocation(x,y);
		return ptDst;
	}
}

function MapProjection_inverse(projection) {
	this._super = MapProjection;
	this._super(projection);

	this.transform_inverse = transform_inverse;

	function transform_inverse(source, srcOffset, dest, dstOffset, length) {
		//alert("AQUIE ESTA LA TRANSFORM 44");
		this.inverseTransform(source, srcOffset, dest, dstOffset, length);
	}

}
MapProjection_inverse.prototype = MapProjection;

function TransverseMercatorProjection (projection) {
	this._super = MapProjection_inverse;
	this._super(projection);

	FC1= 1.00000000000000000000000, // 1/1
	FC2= 0.50000000000000000000000, // 1/2
	FC3= 0.16666666666666666666666, // 1/6
	FC4= 0.08333333333333333333333, // 1/12
	FC5= 0.05000000000000000000000, // 1/20
	FC6= 0.03333333333333333333333, // 1/30
	FC7= 0.02380952380952380952380, // 1/42
	FC8= 0.01785714285714285714285; // 1/56

	C00= 1.0,
	C02= 0.25,
	C04= 0.046875,
	C06= 0.01953125,
	C08= 0.01068115234375,
	C22= 0.75,
	C44= 0.46875,
	C46= 0.01302083333333333333,
	C48= 0.00712076822916666666,
	C66= 0.36458333333333333333,
	C68= 0.00569661458333333333,
	C88= 0.3076171875;

	EPS10=1e-10, EPS11=1e-11;

	this.ak0 = this.semiMajor*this.scaleFactor;

	this.esp = (this.semiMajor * this.semiMajor) / (this.semiMinor * this.semiMinor) - 1.0;
	this.en0 = C00 - this.es * (C02 + this.es * (C04 + this.es * (C06 + this.es * C08)));
	this.en1 = this.es * (C22 - this.es * (C04 + this.es * (C06 + this.es * C08)));
	this.en2 = (t = this.es * this.es) * (C44 - this.es * (C46 + this.es * C48));
	this.en3 = (t *= this.es) * (C66 - this.es * C68);
	this.en4 = t * this.es * C88;

	this.getZone = getZone;
	this.mlfn = mlfn;
	this.transformUTM = transformUTM;
	this.inverseTransformUTM = inverseTransformUTM;

	function getZone(centralLongitudeZone1, zoneWidth) {
		alert("TransverseMercatorProjection::getZone: comprobar que la funcionalidad es correcta");
		zoneCount = Math.abs(360/zoneWidth);
		t = centralLongitudeZone1 - 0.5*zoneWidth;
		t = radians2degrees(centralMeridian) - t;
		t = Math.floor(t/zoneWidth + this.EPS);		// Nombre de zones entre la longitudes centrale et la longitude 1.
		t -= zoneCount*Math.floor(t/zoneCount);		// Si n�gatif, ram�ne dans l'intervale 0 � (zoneCount-1).
		t_int = Math.floor(t);
		return (t_int)+1;

		//			zoneCount = Math.abs(360/zoneWidth);
		//			t;
		//			t = centralLongitudeZone1 - 0.5*zoneWidth; // Longitude du d�but de la 1�re zone.
		//			t = Math.toDegrees(centralMeridian) - t;	// Nombre de degr�s de longitudes entre la longitude centrale et la longitude 1.
		//			t = Math.floor(t/zoneWidth + EPS);				// Nombre de zones entre la longitudes centrale et la longitude 1.
		//			t -= zoneCount*Math.floor(t/zoneCount);		// Si n�gatif, ram�ne dans l'intervale 0 � (zoneCount-1).
		//			return ((int) t)+1;
	}


	function mlfn(phi,sphi,cphi) {
		cphi *= sphi;
		sphi *= sphi;
		return this.en0 * phi - cphi *
			(this.en1 + sphi *
			(this.en2 + sphi *
			(this.en3 + sphi *
			(this.en4))));
	}

	function transformUTM(x, y, ptDst) {
//		alert("UTM::transform()");
//		alert("x: " + x + " y: " + y);
		if (Math.abs(y) > (Math.PI/2 - this.EPS)) {
			alert("ERROR: TransverseMercatorProjection::transform ERROR_POLE_PROJECTION");
		}
		y -= this.centralLatitude;
		x -= this.centralMeridian;
		sinphi = Math.sin(y);
		cosphi = Math.cos(y);
		if (this.isSpherical) {
			// Spherical model.
			b = cosphi * Math.sin(x);
			if (Math.abs(Math.abs(b) - 1.0) <= EPS10) {
				alert("ERROR: TransverseMercatorProjection::transform ERROR_VALUE_TEND_TOWARD_INFINITY");
			}
			yy = cosphi * Math.cos(x) / Math.sqrt(1.0-b*b);
			x = 0.5*this.ak0 * Math.log((1.0+b)/(1.0-b)); // 8-1
			if ((b=Math.abs(yy)) >= 1.0) {
				if ((b-1.0) > EPS10) {
					alert("ERROR: TransverseMercatorProjection::transform ERROR_VALUE_TEND_TOWARD_INFINITY 2");
				} else {
					yy = 0.0;
				}
			}
			else {
				yy = Math.acos(yy);
			}
			if (y<0) {
				yy = -yy;
			}
			y = this.ak0 * yy;
		} else {
			// Ellipsoidal model.
			t = Math.abs(cosphi)>EPS10 ? sinphi/cosphi : 0;
			t *= t;
			al = cosphi*x;
			als = al*al;
			al /= Math.sqrt(1.0 - this.es * sinphi*sinphi);
			n = this.esp * cosphi*cosphi;

			// NOTE: meridinal distance at central latitude is always 0
			y = this.ak0*(this.mlfn(y, sinphi, cosphi) + sinphi*al*x*
					FC2 * ( 1.0 +
						FC4 * als * (5.0 - t + n*(9.0 + 4.0*n) +
							FC6 * als * (61.0 + t * (t - 58.0) + n*(270.0 - 330.0*t) +
								FC8 * als * (1385.0 + t * ( t*(543.0 - t) - 3111.0))))));
			x = this.ak0*al*(FC1 + FC3 * als*(1.0 - t + n +
						FC5 * als * (5.0 + t*(t - 18.0) + n*(14.0 - 58.0*t) +
							FC7 * als * (61.0+ t*(t*(179.0 - t) - 479.0 )))));
		}
		x += this.falseEasting;
		y += this.falseNorthing;
		if (ptDst!=null) {
			ptDst.setLocation(x,y);
			return ptDst;
		} else {
			return new Point2D_Double(x,y);
		}
	}

	function inverseTransformUTM(x, y, ptDst) {
		//alert("UTM::inversetransform()");
		x -= this.falseEasting;
		y -= this.falseNorthing;

		if (this.isSpherical) {
			// Spherical model.
			t = Math.exp(x/this.ak0);
			d = 0.5 * (t-1/t);
			t = Math.cos(y/this.ak0);
			phi = Math.asin(Math.sqrt((1.0-t*t)/(1.0+d*d)));
			y = y<0.0 ? -phi : phi;
			x = (Math.abs(d)<=EPS10 && Math.abs(t)<=EPS10) ? this.centralMeridian :
				Math.atan2(d,t) + this.centralMeridian;
		} else {
			// Ellipsoidal model.
			y_ak0 = y/this.ak0;
			k = 1.0-this.es;
			phi = y_ak0;
			for (i=10; true;) { // rarely goes over 5 iterations
				if (--i < 0) {
					alert("ERROR: TransverseMercatorProjection::inverseTransform ERROR_NO_CONVERGENCE");
				}
				s = Math.sin(phi);
				t = 1.0 - this.es * (s*s);

				t = (this.mlfn(phi, s, Math.cos(phi)) - y_ak0) / ( k * t * Math.sqrt(t));
				phi -= t;
				if (Math.abs(t)<EPS11) {
					break;
				}
			}
			if (Math.abs(phi) >= (Math.PI/2)) {
				y = y<0.0 ? -(Math.PI/2) : (Math.PI/2);
				x = this.centralMeridian;
			} else {
				sinphi = Math.sin(phi);
				cosphi = Math.cos(phi);
				t = Math.abs(cosphi)>EPS10 ? sinphi/cosphi : 0.0;
				n = this.esp * cosphi*cosphi;
				con = 1.0 - this.es * sinphi*sinphi;
				d = x*Math.sqrt(con)/this.ak0;
				con *= t;
				t *= t;
				ds = d*d;
				y = phi - (con*ds / (1.0 - this.es)) *
					FC2 * (1.0 - ds *
							FC4 * (5.0 + t*(3.0 - 9.0*n) + n*(1.0 - 4*n) - ds *
								FC6 * (61.0 + t*(90.0 - 252.0*n + 45.0*t) + 46.0*n - ds *
									FC8 * (1385.0 + t*(3633.0 + t*(4095.0 + 1574.0*t))))));

				x = d*(FC1 - ds * FC3 * (1.0 + 2.0*t + n -
							ds*FC5*(5.0 + t*(28.0 + 24* t + 8.0*n) + 6.0*n -
								ds*FC7*(61.0 + t*(662.0 + t*(1320.0 + 720.0*t))))))/cosphi + this.centralMeridian;
			}
		}
		y += this.centralLatitude;
		if (ptDst!=null) {
			ptDst.setLocation(x,y);
			return ptDst;
		} else {
			return new Point2D_Double(x,y);
		}
	}
}
TransverseMercatorProjection.prototype = MapProjection_inverse;

function MatrixTransform(numCol, numRow, elt) {
	this.numCol = numCol || 0;
	this.numRow = numRow|| 0;
	this.elt = elt || new Array(); //double[];

	this.transformMatrix = transformMatrix;

	function transformMatrix(srcPts, srcOff, dstPts, dstOff, numPts) {
		srcOffB = srcOff;
		dstOffB = dstOff;
		numPtsB = numPts;

		inputDimension = this.numCol-1; // The last ordinate will be assumed equals to 1.
		outputDimension = this.numRow-1;
		buffer = new Array(); //double[this.numRow];
		if (srcPts==dstPts) {
			// We are going to write in the source array. Checks if
			// source and destination sections are going to clash.
			upperSrc = srcOffB + numPtsB*inputDimension;
			if (upperSrc > dstOffB) {
				if (inputDimension >= outputDimension ? dstOffB > srcOffB :
						dstOffB + numPtsB*outputDimension > upperSrc) {
					// If source overlaps destination, then the easiest workaround is
					// to copy source data. This is not the most efficient however...
					srcPts = new Array(); //double[numPtsB*inputDimension];

						// como esto no existe en Javascript, lo copio a pelo!
					//System.arraycopy(dstPts, srcOffB, srcPts, 0, srcPts.length);
					// equivale a copiar srcPts.length elementos, empezando en la pos srcOffB, del vector dstPts
					// en el vector srcPts, empezando en 0
					ii = 0;
					for (kk = srcOffB; kk<=(srcPts.length+srcOffB); kk++) {
						srcPts[ii] = dstPts[kk];
						ii++;
					}
					//!!!!!!!!!!!!!!!!!
					// Problemas con el arraycopy xq no existe en JS!!!!
					//!!!!!!!!!!!!!!!!!

					srcOffB = 0;
				}
			}
		}
		while (--numPtsB>=0) {
			mix=0;
			for (var j=0; j<this.numRow; j++) {
				sum = this.elt[mix + inputDimension];
				for (var i=0; i<inputDimension; i++) {
					sum += srcPts[srcOffB+i]*this.elt[mix++];
				}
				buffer[j] = sum;
				mix++;
			}
			w = buffer[outputDimension];
			for (var j=0; j<outputDimension; j++) {
				// 'w' is equals to 1 if the transform is affine.
				dstPts[dstOffB++] = buffer[j]/w;
			}
			srcOffB += inputDimension;
		}
	}
}

function GeocentricTransform (ellipsoid) {
	semiMajor = ellipsoid.getSemiMajorAxis();
	semiMinor = ellipsoid.getSemiMinorAxis();
	//units = ellipsoid.getAxisUnit();

	this.MAX_ERROR = 0.01;
	this.COS_67P5 = 0.38268343236508977;
	this.AD_C = 1.0026000;
		// habria q convertir a Metros, pero supongo q siempre lo estara...
	this.a = semiMajor;		// Unit.METRE.convert(semiMajor, units)
	this.b = semiMinor;		// Unit.METRE.convert(semiMinor, units)
	this.a2 = (this.a * this.a);
	this.b2 = (this.b * this.b);
	this.e2 = (this.a2 - this.b2) / this.a2;
	this.ep2 = (this.a2 - this.b2) / this.b2;
	this.hasHeight = false;

	// Habria q hacer m�s comprobaciones pero supongo q todo ha ido bien...

	this.getDimSource = getDimSource;
	this.transformGeocentric = transformGeocentric;
	this.transformGeocentric_inverse = transformGeocentric_inverse;

	function getDimSource() {
		return 2;	// De momento, siempre ser�n coordenadas en DOS dimensiones
	}


	/**
	 * Implementation of geodetic to geocentric conversion. This
	 * implementation allows the caller to use height in computation.
	 */
	function transformGeocentric(srcPts, srcOff, dstPts, dstOff, numPts, hasHeight) {
		numPtsB = numPts;

		step = 0;
		dimSource = this.getDimSource();
		this.hasHeight |= (dimSource>=3);
			// ojo q son vectores!
			/* LO QUITO XQ PARECE Q NO ENTRA NUNCA...
			COMPROBAR SI NO ENTRA NUNCA EN NINGUN CASO!!!!!!!!!!!!!!!!!!!!!!!
		if (srcPts==dstPts && srcOff<dstOff && srcOff+numPtsB*dimSource>dstOff) {
			step = -dimSource;
			srcOff -= (numPtsB-1)*step;
			dstOff -= (numPtsB-1)*step;
		}
		*/
		error = null;
		while (--numPtsB >= 0) {
			L = degrees2radians(srcPts[srcOff++]); // Longitude
			P = degrees2radians(srcPts[srcOff++]); // Latitude
			h = hasHeight ? srcPts[srcOff++] : 0; // Height above the ellipsoid (m)

			cosLat = Math.cos(P);
			sinLat = Math.sin(P);
			rn = this.a / Math.sqrt(1 - this.e2 * (sinLat*sinLat));

			dstPts[dstOff++] = (rn + h) * cosLat * Math.cos(L); // X: Toward prime meridian
			dstPts[dstOff++] = (rn + h) * cosLat * Math.sin(L); // Y: Toward East
			dstPts[dstOff++] = (rn * (1-this.e2) + h) * sinLat; // Z: Toward North
			srcOff += step;
			dstOff += step;
		}

	}


	/**
	 * Converts geocentric coordinates (x, y, z) to geodetic coordinates
	 * (longitude, latitude, height), according to the current ellipsoid
	 * parameters. The method used here is derived from "An Improved
	 * Algorithm for Geocentric to Geodetic Coordinate Conversion", by
	 * Ralph Toms, Feb 1996.
	 */
	function transformGeocentric_inverse(srcPts, srcOff, dstPts, dstOff, numPts) {
		numPtsB = numPts;
		step = 0;
		dimSource = this.getDimSource();
		hasHeight = (dimSource>=3);
		computeHeight = hasHeight;
		/*
		OJO!!!! HAY Q COMPROBAR SI SE ENTRA POR AQUI ALGUNA VEZZZZZ!!!!!!!!!!
		if (srcPts==dstPts && srcOff<dstOff && srcOff+numPts*dimSource>dstOff) {
			step = -dimSource;
			srcOff -= (numPts-1)*step;
			dstOff -= (numPts-1)*step;
		}
		*/

		while (--numPtsB >= 0) {
			x = srcPts[srcOff++]; // Toward prime meridian
			y = srcPts[srcOff++]; // Toward East
			z = srcPts[srcOff++]; // Toward North

			// Note: The Java version of 'atan2' work correctly for x==0.
			//			 No need for special handling like in the C version.
			//			 No special handling neither for latitude. Formulas
			//			 below are generic enough, considering that 'atan'
			//			 work correctly with infinities (1/0).

			// Note: Variable names follow the notation used in Toms, Feb 1996
			W2 = x*x + y*y										;								// square of distance from Z axis
			W = Math.sqrt(W2);																// distance from Z axis
			T0 = z * this.AD_C;																// initial estimate of vertical component
			S0 = Math.sqrt(T0*T0 + W2);												// initial estimate of horizontal component
			sin_B0 = T0 / S0;																	// sin(B0), B0 is estimate of Bowring aux variable
			cos_B0 = W / S0;																	// cos(B0)
			sin3_B0 = sin_B0 * sin_B0 * sin_B0;								// cube of sin(B0)
			T1 = z + this.b * this.ep2 * sin3_B0;							// corrected estimate of vertical component

			sum = W - this.a*this.e2*(cos_B0*cos_B0*cos_B0);	// numerator of cos(phi1)
			S1 = Math.sqrt(T1*T1 + sum * sum);								// corrected estimate of horizontal component
			sin_p1 = T1 / S1;																	// sin(phi1), phi1 is estimated latitude
			cos_p1 = sum / S1;																// cos(phi1)

				// OJO CON ESTAS FUNCIONES RARAS...
			longitude = radians2degrees(Math.atan2(y, x));
			latitude = radians2degrees(Math.atan(sin_p1 / cos_p1));

			dstPts[dstOff++] = longitude;
			dstPts[dstOff++] = latitude;

			if (computeHeight) {
				rn = this.a/Math.sqrt(1-this.e2*(sin_p1*sin_p1)); // Earth radius at location
				if (cos_p1 >= this.COS_67P5) height = W / +cos_p1 - rn;
				else if (cos_p1 <= (-1*this.COS_67P5)) height = W / -cos_p1 - rn;
				else height = z / sin_p1 + rn*(this.e2 - 1.0);
				if (this.hasHeight) {
					dstPts[dstOff++] = height;
				}

				/* SI NO SE CUMPLE, MALA SUERTE :)

				// If assertion are enabled, then transform the
				// result and compare it with the input array.
				double distance;
				assert MAX_ERROR > (distance=checkTransform(new double[]
							{x,y,z, longitude, latitude, height})) : distance;
							*/
			}
			srcOff += step;
			dstOff += step;
		}
	}
}
