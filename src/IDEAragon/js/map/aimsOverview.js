/*
var xminOverview = 569301;
var ymaxOverview = 4755088;
var xmaxOverview = 810739;
var yminOverview = 4413376;
*/
var xminOverview = 530000;
var ymaxOverview = 4760000;
var xmaxOverview = 855000;
var yminOverview = 4410000;

var tamPixelOverviewX = (xmaxOverview - xminOverview) / 130;
var tamPixelOverviewY = (ymaxOverview - yminOverview) / 140;

function coord2px_overview(_theX, _theY) {
	var result = new Array();

	var offsetX = _theX - xminOverview;
	var offsetY = ymaxOverview - _theY;

	result["x"] = Math.round(offsetX / tamPixelOverviewX);
	result["y"] = Math.round(offsetY / tamPixelOverviewY);

	if (result["x"] <= 0) {
		result["x"] = 0;
	}

	if (result["y"] <= 0) {
		result["y"] = 0;
	}

	if (result["x"] >= 130) {
		result["x"] = 130;
	}

	if (result["y"] >= 140) {
		result["y"] = 140;
	}
	return result;
}

function updateMarcaOverview() {
	var topLeftPx = coord2px_overview(minx, maxy);
	var bottomRightPx = coord2px_overview(maxx, miny);
	var diffX = bottomRightPx["x"] - topLeftPx["x"];
	var diffY = bottomRightPx["y"] - topLeftPx["y"];

	document.getElementById('marcaOverview').style.left = topLeftPx["x"] + "px";
	document.getElementById('marcaOverview').style.top = topLeftPx["y"] + "px";
	document.getElementById('marcaOverview').style.width = diffX + "px";
	document.getElementById('marcaOverview').style.height = diffY + "px";

	var mitadX = Math.round(topLeftPx["x"] + (diffX/2));
	var mitadY = Math.round(topLeftPx["y"] + (diffY/2));

	if (topLeftPx["y"] > 2) {
		document.getElementById('marcaOverviewN').style.display = "block";
		document.getElementById('marcaOverviewN').style.left = mitadX + "px";
		document.getElementById('marcaOverviewN').style.top = "0px";
		document.getElementById('marcaOverviewN').style.width = "1px";
		document.getElementById('marcaOverviewN').style.height = (topLeftPx["y"]-1) + "px";
	} else {
		document.getElementById('marcaOverviewN').style.display = "none";
	}

	if (bottomRightPx["y"] < 138) {
			// ojo que al sumar 1, concatena y se desborda
		var aux = bottomRightPx["y"];
		aux++;

		document.getElementById('marcaOverviewS').style.display = "block";
		document.getElementById('marcaOverviewS').style.left = mitadX + "px";
		document.getElementById('marcaOverviewS').style.top = (aux++) + "px";
		document.getElementById('marcaOverviewS').style.width = "1px";
		document.getElementById('marcaOverviewS').style.height = (140-bottomRightPx["y"]-1) + "px";
	} else {
		document.getElementById('marcaOverviewS').style.display = "none";
	}

	if (topLeftPx["x"] > 2) {
		document.getElementById('marcaOverviewW').style.display = "block";
		document.getElementById('marcaOverviewW').style.left = "0px";
		document.getElementById('marcaOverviewW').style.top = mitadY + "px";
		document.getElementById('marcaOverviewW').style.width = (topLeftPx["x"]-1) + "px";
		document.getElementById('marcaOverviewW').style.height = "1px";
	} else {
		document.getElementById('marcaOverviewW').style.display = "none";
	}

	if (bottomRightPx["x"] < 128) {
			// ojo que al sumar 1, concatena y se desborda
		var aux = bottomRightPx["x"];
		aux++;

		document.getElementById('marcaOverviewE').style.display = "block";
		document.getElementById('marcaOverviewE').style.left = (aux++) + "px";
		document.getElementById('marcaOverviewE').style.top = mitadY + "px";
		document.getElementById('marcaOverviewE').style.width = (130-bottomRightPx["x"]-2) + "px";
		document.getElementById('marcaOverviewE').style.height = "1px";
	} else {
		document.getElementById('marcaOverviewE').style.display = "none";
	}
}

function getXY_overview(e) {
	var posVentanaX = parseInt(document.getElementById('overviewWin').style.left);
		// 20px por la barra del titulo
	var posVentanaY = parseInt(document.getElementById('overviewWin').style.top) + 20;
	if (isNav) {
		theX=e.pageX - posVentanaX;
		theY=e.pageY - posVentanaY;
	} else {
		theX=event.clientX + document.body.scrollLeft - posVentanaX;
		theY=event.clientY + document.body.scrollTop - posVentanaY;
	}

	return false;
}

function click_overview(e) {
	getXY_overview(e);
	if ((theX >= 0) && (theY >= 0)) {
		var newX = xminOverview + (theX * tamPixelOverviewX);
		var newY = ymaxOverview - (theY * tamPixelOverviewY);

		//var layerOverview = document.getElementById('layerOverviewId')
		doRecenterXYWithScale(newX, newY, getMapScale(false));
	}
}
