var axlSelection="";
var theHiddenURL = "";
var theHiddenURLGeneralAragon = "";
//need to define loadXML function for non IE browser which do not have loadXML implemented
//we use loadXML (can't use load method) to load ArcXML response because the response from ArcIMS is in plain text format

if(!isIE){
	Document.prototype.loadXML = function (s) {
			// parse the string to a new doc
		var doc2 = (new DOMParser()).parseFromString(s, "text/xml");

			// remove all initial children
		while (this.hasChildNodes()) {
			this.removeChild(this.lastChild);
		}

			// insert and import nodes
		for (var i = 0; i < doc2.childNodes.length; i++) {
			this.appendChild(this.importNode(doc2.childNodes[i], true));
		}
	};
}

function clearMap() {
	document.getElementById('theImage').style.display = "none";
	document.getElementById('theAcetateEscala').style.display = "none";
	document.getElementById('theAcetateMarca').style.display = "none";

	jg.clear();
	limpiarEdicion();
	limpiarHistorico();
	limpiarBufferCirculo();
	limpiarBufferCirculoGranjas();

	//bufferCircleWin.hide();
	//jgCircle.clear();
	//valueRadius = -1;
}

function clearWMS(wmsIdx) {
	//document.getElementById(wmsList[wmsIdx]["id"]).src = "images/pixel.gif";
	if (mapOL && (wmsIdx>1)){// no es fondo
		var capasOL = mapOL.getLayers();
		for (var i=0; i<capasOL.getLength(); i++){
			var capaOL = capasOL.item(i);
			if (capaOL.get("idxWMS")==wmsIdx){
				
				capaOL.setVisible(false);
				break;
			}
		}
	}
	else{
	document.getElementById(wmsList[wmsIdx]["id"]).style.display = "none";
	}
}

function updateWMS(wmsIdx) {
	if(document.getElementById('bgFotoShowId') && (! document.getElementById('bgFotoShowId').checked)) {
		if (wmsIdx == idxWMSFondoFoto) {
			if (wmsIdx != 0) {
				return;
			}
		}
	}
	wmsObj = wmsList[wmsIdx];
	
	var layers =  getInitialParamCaseInsensitive(wmsObj["url"], "layers=",true).split(",");
	
	var cql_filter="";
	for (var i=0; i<layers.length; i++){
		var layer= layers[i];
		if(filterLayerList[layer]){
			if (cql_filter==""){
				for (var j=0; j<i; j++){
					cql_filter+="INCLUDE;";
				}
			}
			else{
				cql_filter+=";";
			}
			var selectionWhere=filterLayerList[layer];
			var partes = selectionWhere.split("=");
			var igual = selectionWhere.indexOf("=");
			selectionWhere = partes[0].toLowerCase()+selectionWhere.substr(igual);
			cql_filter+=selectionWhere;
		}
		else{
			if (cql_filter!=""){
				cql_filter+=";INCLUDE";
			}
		}
	}
	if (mapOL && (wmsIdx>1)){// no es fondo
		var capasOL = mapOL.getLayers();
		for (var i=0; i<capasOL.getLength(); i++){
			var capaOL = capasOL.item(i);
			try{
			if (capaOL.get("idxWMS")==wmsIdx){
				if (cql_filter!=""){
					capaOL.getSource().set("cql_filter",cql_filter);
				}
			
				if ((typeof idxWMS_invisible!='undefined')&&(wmsIdx==idxWMS_invisible)&&(capaOL instanceof ol.layer.Group || capaOL.getSource().getUrl() !=  wmsObj["url"])){
					
					capasOL.removeAt(i);
					capasOL.insertAt(i,createWMSLayer(wmsIdx));
				}
				
				capaOL.setVisible(true);
				break;
			}
			}catch(err){
				console.log(err);
			}
		}
	}
	else{
		urlWMS = wmsObj["url"] + '&WIDTH=' + mwidth + '&HEIGHT=' + mheight;
		urlWMS += '&BBOX=' + minx + ',' + miny + ',' + maxx + ',' + maxy;
	if (cql_filter!=""){
		urlWMS+="&CQL_FILTER="+cql_filter;
	}
	
	
	document.getElementById(wmsObj["id"]).src = urlWMS;
	document.getElementById(wmsObj["id"]).style.zindex = wmsObj["zIdx"];

	loadingWMSId = "wmsLoading" + wmsIdx;
		// compruebo si esta visible ese elemento (puede estar dentro de una carpeta plegada)
	if (document.getElementById(loadingWMSId)) {
		//setTimeout("muestraWMScomoVisible('" + loadingWMSId + "')", 100);
		document.getElementById(loadingWMSId).style.visibility = "visible";
	}
	if(document.getElementById(wmsList[wmsIdx]["id"]).style.display =="none"){
		document.getElementById(wmsList[wmsIdx]["id"]).style.display ="block";
	}
	}
}

/*
function muestraWMScomoVisible(theId) {
	document.getElementById(theId).style.visibility = "visible";
}
*/
function updateAcetateEscala(mustRefreshScale) {
	http = getHTTPObject();
	if ((http != null)) {
		var axlAcetate = getMapRequestAcetateEscala(minx, miny, maxx, maxy);
		if (mustRefreshScale) {
			http.onreadystatechange = printResponseAcetateEscalaRefreshScale;
		} else {
			http.onreadystatechange = printResponseAcetateEscalaNoRefreshScale;
		}
		http.open("POST", url, true);
		http.send(axlAcetate);
	}
}

function updateAcetateMarca(theXMarca, theYMarca, theXMarcaBis, theYMarcaBis) {
	console.log("Función a eliminar. aimsXMLMap.js updateAcetateMarca(theXMarca, theYMarca, theXMarcaBis, theYMarcaBis)");
	/*http = getHTTPObject();
	if ((http != null)) {
		var axlAcetate = getMapRequestAcetateMarca(minx, miny, maxx, maxy, theXMarca, theYMarca, theXMarcaBis, theYMarcaBis);
		http.onreadystatechange = printResponseAcetateMarca;
		http.open("POST", url, true);
		http.send(axlAcetate);
	}*/
}

function checkExtent(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, unitScaleBar, layerVisibility, avoidCheckMaxScale) {
	if (!avoidCheckMaxScale){
	if (_mapSizeX) {
		if (! checkMaxScale(_minx, _maxx, _mapSizeX)) {
			return "1";
		}
	} else {
		if (! checkMaxScale(_minx, _maxx, mwidth)) {
			return "1";
		}
	}
	}
	if (! checkMaxBbox(_minx, _miny, _maxx, _maxy)) {
		return "2";
	}
	return "";
}

function getMap(_minx, _miny, _maxx, _maxy, avoidRefreshScale, avoidCheckMaxScale) {
	minx = _minx;
	miny = _miny;
	maxx = _maxx;
	maxy = _maxy;

	http = getHTTPObject();
	if ((http != null)) {
		var resultCh = checkExtent(_minx, _miny, _maxx, _maxy, null, null,null, null,avoidCheckMaxScale);

		if (resultCh == "1") {
				// La escala es muy lejana
			getFullMap();
		} else if (resultCh == "2") {
				// no hacer petición, lo dejamos como está
			//getMapWithCurrentExtent();
			resetAfterPan();
			restoreExtent();
		} else {
			
			var  newBbox =checkDeformation(_minx, _miny, _maxx, _maxy, mwidth, mheight);
			minx = newBbox.minx;
			maxx = newBbox.maxx;
			miny = newBbox.miny;
			maxy = newBbox.maxy;
			
			if ((typeof mapOL !='undefined') && (mapOL!=null)){
				updateMapOL();
			}
	//		var axl = getMapRequest(xmin,ymin,xmax,ymax,null,null,null,null, true);

			for (var ii = 0; ii <wmsList["count"]; ii++) {
				clearWMS(ii);
			}

			clearMap();

			/*http.open("POST", url, true);
			if (debugOn) {
				alert("sending :" + axl);
			}*/
			muestraLayerLoading();
			if (!avoidRefreshScale){
				newMapScale = getMapScale(false);
				setNewScale(newMapScale);
				updateZoomBar(newMapScale);
			}
			rememberExtent(minx, miny, maxx, maxy);
			
		/*	if (avoidRefreshScale) {
				http.onreadystatechange = printResponseNoRefreshScale;
			} else {
				http.onreadystatechange = printResponseRefreshScale;
			}
			axlSelection=axl;
			http.send(axl);*/

			for (var ii = 0; ii <wmsList["count"]; ii++) {
				if (wmsList[ii]["visib"]) {
					updateWMS(ii);
				}
			}
			var mustRefreshScale =!avoidRefreshScale;
			afterMapRefresh(mustRefreshScale);
			updateMarcaOverview();
			ocultaLayerLoading();

			 manageFotograma(minx,miny,maxx,maxy);
		}
	}
}

function manageFotograma(minx,miny,maxx,maxy){
	
}
function getMapHidden(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, _theDPI, layerVisibilityList, tipoImagen) {
	http = getHTTPObject();
	if ((http != null)) {

		var axl = getMapRequestPrint(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, _theDPI, layerVisibilityList, tipoImagen);

		http.open("POST", url, true);
		http.onreadystatechange = eval("printResponseHidden" + tipoImagen);
		muestraLayerLoading();

		http.send(axl);
	}
}


function getXMLDocResult() {
	isWorking = false;
	var result = http.responseText;

		//var result = http.responseXML;	//fails
	if (debugOn) {
		alert(result);
	}

	var xmlDoc;

	if(document.implementation && document.implementation.createDocument) {
		// MOZILLA
		xmlDoc = document.implementation.createDocument("", "", null);
		if (xmlDoc.loadXML){// no es IE10
			xmlDoc.async="false";
			xmlDoc.loadXML(result);
			return xmlDoc;
		}
		

	} 
	if (window.ActiveXObject){
		//IE
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async="false";
		xmlDoc.loadXML(result);
	}
	
	
	return xmlDoc;

}

function correctPNG(imgMapaId, _theURL) {
	if (!mapContainerCreated){
		setTimeout(new function(){correctPNG(imgMapaId,_theURL);},1000);
	};
	var imgMapa = document.getElementById(imgMapaId);
	if (isIE6) {
		//imgMapa.style.filter ="progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'" + _theURL + "\', sizingMethod='scale') Alpha(opacity=65);";
		imgMapa.style.filter ="progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'" + _theURL + "\', sizingMethod='scale');";
		imgMapa.src = _theURL;
		//imgMapa.style.zindex = 100;
	} else {
		imgMapa.src = _theURL;
	}
}

function printResponseRefreshScale(){
	printResponse(true, false);
}

function printResponseNoRefreshScale(){
	printResponse(false, false);
}

function printResponseHidden(){
	printResponse(false, true, "");
}

function printResponseHiddenSituacion(){
	printResponse(false, true, "Situacion");
}

function printResponseHiddenAragon(){
	printResponse(false, true, "Aragon");
}

var timeoutVar = null;
function printResponse(mustRefreshScale, isHidden, tipoImagen){
	if (http.readyState == 4) {
		if (http.status == 200) {
			var xmlDoc = getXMLDocResult();
			if (xmlDoc.getElementsByTagName("OUTPUT")){
			var theURL = xmlDoc.getElementsByTagName("OUTPUT").item(0).getAttribute("url");
			if (isHidden) {
				if (tipoImagen == "Aragon") {
					eval("theHiddenURLGeneral" + tipoImagen + " = '" + theURL + "';");
				} else {
					theHiddenURL = theURL;
				}
			} else {
				$("#theImage").load(function() {
						muestraVisible('theImage');
						if (timeoutVar != null) {
							clearTimeout(timeoutVar);
						}
						timeoutVar = null;
					}
				);
				correctPNG('theImage', theURL);

					// si pasados 4000 mseg no se ha cargado, mostrar
					// dependerá de cuánto le cueste cargar
					// provocará que el mapa vectorial "parpadee" (Se ve el anterior y enseguida el nuevo), pero...
					// podría no notificarse el load() por problema en la compatibilidad del navegador o por la cache
					// con otras imágenes (escala...) también ocurre pero pasa desapercibido)
				timeoutVar = setTimeout("muestraVisible('theImage');", 4000);

				var env = xmlDoc.getElementsByTagName("ENVELOPE").item(0);
				minx = parseFloat(env.getAttribute("minx"));
				miny = parseFloat(env.getAttribute("miny"));
				maxx = parseFloat(env.getAttribute("maxx"));
				maxy = parseFloat(env.getAttribute("maxy"));
					/*
				if (mustChangeMaxScaleAllowed) {
					maxScaleAllowed = getMapScale(false);
					mustChangeMaxScaleAllowed = false;

					fullLeft = parseFloat(env.getAttribute("minx"));
					fullBottom = parseFloat(env.getAttribute("miny"));
					fullRight = parseFloat(env.getAttribute("maxx"));
					fullTop = parseFloat(env.getAttribute("maxy"));
				}
					*/
				//alert(minx +","+ miny +","+ maxx+","+maxy);

				if (mustRefreshScale) {
					newMapScale = getMapScale(false);
					setNewScale(newMapScale);
					updateZoomBar(newMapScale);
				}
				rememberExtent(minx, miny, maxx, maxy, theURL);

				updateAcetateEscala(mustRefreshScale, isHidden);

				updateMarcaOverview();
			}
			}
		} else {
			alert("Error retreiving data");
		}
	}
	if ((axlSelection!='')&&(queryUrl!="")){

		if (idSelectionLayer != '') {
			if (selectionWhere != '') {
				the_url=queryUrl.replace("&CustomService=Query","");
				if (the_url != url){
					http = getHTTPObject();
					http.open("POST", the_url, true);
					if (!mustRefreshScale) {
						http.onreadystatechange = printResponseNoRefreshScale;
					} else {
						http.onreadystatechange = printResponseRefreshScale;
					}
					var axl = axlSelection;
					axlSelection="";
					http.send(axl);

				}
			}
		}
		axlSelection="";
	}
}

function muestraVisible(theId) {
	document.getElementById(theId).style.display = "block";
}

function printResponseAcetateEscalaRefreshScale(){
	printResponseAcetateEscala(true);
}

function printResponseAcetateEscalaNoRefreshScale(){
	printResponseAcetateEscala(false);
}

function printResponseAcetateEscala(mustRefreshScale){
	console.log("Función a eliminar. aimsXMLMap.js printResponseAcetateEscala(mustRefreshScale)");
	if (http.readyState == 4) {
		if (http.status == 200) {
			var xmlDoc = getXMLDocResult();
			var theURL = xmlDoc.getElementsByTagName("OUTPUT").item(0).getAttribute("url");
			//document.getElementById('theAcetateEscala').src = theURL;

			correctPNG('theAcetateEscala', theURL);
			setTimeout("muestraVisible('theAcetateEscala')", 200);

			/*
			if ((findXCoordUsr != "") && (findYCoordUsr != "")) {
				updateAcetateMarca(findXCoordUsr, findYCoordUsr);
			} else {
				ocultaLayerLoading();
			}
			*/
			updateAcetateMarca(findXCoordUsr, findYCoordUsr, xCoordUsr, yCoordUsr);

			afterMapRefresh(mustRefreshScale);
		} else {
			alert("Error retreiving data");
		}
	}
}

function printResponseAcetateMarca(){
	if (http.readyState == 4) {
		if (http.status == 200) {
			var xmlDoc = getXMLDocResult();
			var theURL = xmlDoc.getElementsByTagName("OUTPUT").item(0).getAttribute("url");
			//document.getElementById('theAcetateMarca').src = theURL;
			//document.getElementById('theAcetateMarca').style.display = 'block';

			correctPNG('theAcetateMarca', theURL);
			setTimeout("muestraVisible('theAcetateMarca')", 200);

			ocultaLayerLoading();
		} else {
			alert("Error retreiving data");
		}
	}
}

minxArray = new Array();
maxxArray = new Array();
minyArray = new Array();
maxyArray = new Array();
theURLArray = new Array();
scalesArray = new Array();
idxArray = -1;
function rememberExtent(_minx, _miny, _maxx, _maxy/*, _theURL*/) {
	if (idxArray >= 0) {
		if ((minxArray[idxArray] == _minx) && (maxxArray[idxArray] == _maxx) && (minyArray[idxArray] == _miny) && (maxyArray[idxArray] == _maxy)) {
			// es justo el anterior, no guardar
			return;
		}
	}
	idxArray++;
	minxArray[idxArray] = _minx;
	maxxArray[idxArray] = _maxx;
	minyArray[idxArray] = _miny;
	maxyArray[idxArray] = _maxy;
	//theURLArray[idxArray] = _theURL;
	scalesArray[idxArray] = document.getElementById('scaleTextBox').value;
}

function previousExtent() {
	if (idxArray > 0) {
		idxArray--;
		minx = minxArray[idxArray];
		maxx = maxxArray[idxArray];
		miny = minyArray[idxArray];
		maxy = maxyArray[idxArray];
		//document.getElementById('theImage').src = theURLArray[idxArray];
	//	correctPNG('theImage', theURLArray[idxArray]);

		setNewScale(scalesArray[idxArray]);
		updateZoomBar(scalesArray[idxArray]);

		updateMapClicks();
//		updateAcetateEscala(true);
		if ((typeof mapOL !='undefined') && (mapOL!=null)){
			updateMapOL();
		
		}
		for (var ii = 0; ii <wmsList["count"]; ii++) {
			clearWMS(ii);
		}

		for (var ii = 0; ii <wmsList["count"]; ii++) {
			if (wmsList[ii]["visib"]) {
				updateWMS(ii);
			}
		}
		 manageFotograma(minx,miny,maxx,maxy);
	} else {
		alert("No hay extent anterior");
	}
}

function afterMapRefresh(mustRefreshScale) {
	if ((typeof customQueryPoint !='undefined')&&customQueryPoint) {
		var pxUsr = convertCoordToPixel(customQueryPoint_X, customQueryPoint_Y);
		identify(pxUsr[0], pxUsr[1]);
		customQueryPoint = false;
	}

	updateMapClicks();
	updateNotifications();
	if (isGeoreferencing) {
		updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
		//updateGeorefMapClicks(jgLine, lineRecordset, jgLineLabel);
		//updateGeorefMapClicks(jgPol, polRecordset, jgPolLabel);
	}
	if (isHistorico) {
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);
	}
	theHiddenURLGeneralAragon = "";
	//var theURL = document.getElementById("theImage").src;
}

/*
function getURL(theReply) {
	var theURL = "";
	var startpos = 0;
	var endpos = 0;

	var pos = theReply.indexOf("OUTPUT");
	var pos1 = theReply.indexOf("url", pos);
	pos1 += 5;
	var pos2 = theReply.indexOf("\"", pos1);
	theURL = theReply.substring(pos1,pos2);


	return theURL;
}
*/

function getMapRequest(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, unitScaleBar, layerVisibility,avoidCheckMaxScale) {
	if (!avoidCheckMaxScale){
	if (_mapSizeX) {
		if (! checkMaxScale(_minx, _maxx, _mapSizeX)) {
			return "1";
		}
	} else {
		if (! checkMaxScale(_minx, _maxx, mwidth)) {
			return "1";
		}
	}
}
	if (! checkMaxBbox(_minx, _miny, _maxx, _maxy)) {
		return "2";
	}

	var newBbox;
	if (_mapSizeX && _mapSizeY) {
		newBbox = checkDeformation(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY);
	} else {
		newBbox = checkDeformation(_minx, _miny, _maxx, _maxy, mwidth, mheight);
	}
	minx = newBbox.minx;
	maxx = newBbox.maxx;
	miny = newBbox.miny;
	maxy = newBbox.maxy;

	var axl = getCabeceraArcXML(minx, miny, maxx, maxy, _mapSizeX, _mapSizeY, false, layerVisibility);

	axl += getArcXMLSelection();
	axl += getArcXMLSelectionEsquema();
	/*
	axl += '<LAYER type="featureclass" name="Elementos seleccionados" visible="true">\n';
	//axl += '<DATASET fromlayer="BusMun" />\n';
	//axl += '<SPATIALQUERY where="CARTO.T02_LIMITEMUNICIPAL_2007.D_MUNI_INE LIKE UPPER(&apos;%Sabi%&apos;)"/>\n';
	axl += '<DATASET fromlayer="Municipio" />\n';
	axl += '<SPATIALQUERY where="Limite Municipal.D_MUNI_INE LIKE UPPER(&apos;%Sabi%&apos;)"/>\n';
	axl += '<SIMPLERENDERER>\n';
	axl += '<SIMPLEPOLYGONSYMBOL fillcolor="255,128,0" filltype="solid" transparency="0,5" boundarycolor="255,255,255" />\n';
	axl += '</SIMPLERENDERER>\n';
	axl += '</LAYER>\n';
*/

	axl += '  </GET_IMAGE> </REQUEST></ARCXML>\n';
	return axl;
}

function getMapRequestPrint(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, _theDPI, layerVisibilityList, tipoImagen) {
	var newBbox;
	if (_mapSizeX && _mapSizeY) {
		newBbox = checkDeformation(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY);
	} else {
		newBbox = checkDeformation(_minx, _miny, _maxx, _maxy, mwidth, mheight);
	}
	var __minx = newBbox.minx;
	var __maxx = newBbox.maxx;
	var __miny = newBbox.miny;
	var __maxy = newBbox.maxy;
	var axl = getCabeceraArcXML(__minx, __miny, __maxx, __maxy, _mapSizeX, _mapSizeY, false, layerVisibilityList, _theDPI);

	if (tipoImagen != "Aragon") {
		var newMapScale = -1;
		var ancho_mapa = mwidth;
		
		if (_mapSizeX) {
			ancho_mapa =_mapSizeX;
			newMapScale = getMapScale2(false, __minx, __maxx, _mapSizeX);
		} else {
			newMapScale = getMapScale2(false, __minx, __maxx, mwidth);
		}
		if (newMapScale >= 100000) {
			scaleunits = "kilometers";
		} else {
			scaleunits = "meters";
		}
		axl += getArcXMLScaleBar(scaleunits, ancho_mapa-285,10);
	}

	if (tipoImagen == "Aragon") {
		axl += getArcXMLOverviewExtension();
	}

	if (document.getElementById('printSeleccion')) {
		if (document.getElementById('printSeleccion').checked) {
			axl += getArcXMLSelection();
			axl += getArcXMLSelectionEsquema();
		}
	}

	if (document.getElementById('printMediciones')) {
		if (document.getElementById('printMediciones').checked) {
			if (clickCount > 0) {
				var	axl2 = ' <LAYER type="acetate" name="marcasMedicion">\n';
				for (var ii = 0; ii < clickCount; ii++) {
					axl2 += '  <OBJECT units="database">\n';
					axl2 += '  <POINT coords="' + clickPointX[ii] + ' ' + clickPointY[ii] + '" >';
					axl2 += '  <SIMPLEMARKERSYMBOL color="255,0,0" type="circle" width="3" />';
					axl2 += '  </POINT>';
					axl2 += '  </OBJECT>\n';
				}
				axl2 += '</LAYER>\n';

				axl2 += '<LAYER type="acetate" name="lineasMedicion" id="acetate">';
				axl2 += '  <OBJECT units="database">';
				axl2 += '    <SIMPLELINESYMBOL color="255,0,0" width="1" />';
				axl2 += '    <POLYLINE>';
				axl2 += '      <PATH>';
				axl2 += '        <COORDS>';
	
				for (var ii = 0; ii < clickCount; ii++) {
					axl2 += clickPointX[ii] + ' ' + clickPointY[ii] + ';';
					if (isAreaMeasuring) {
						if ((ii == clickCount-1) && (clickCount >= 3)) {
							axl2 += clickPointX[0] + ' ' + clickPointY[0];
						}
					}
				}
				axl2 += '				 </COORDS>';
				axl2 += '      </PATH>';
				axl2 += '    </POLYLINE>';
				axl2 += '  </OBJECT>';
				axl2 += '</LAYER>';
	
				axl += axl2;
			}
		}
	}

	if (document.getElementById('printGeorref')) {
		if (document.getElementById('printGeorref').checked) {
			axl += getArcXMLPointRecordset(pointRecordset, 'text');
		}
	}
	if (document.getElementById('printHistorico')) {
		if (document.getElementById('printHistorico').checked) {
			axl += getArcXMLPointRecordset(historicoRecordset);
		}
	}

	if (valueRadius != -1) {
		axl += bufferDrawCircleAroundPt(valueRadius, xRadius, yRadius);	
	}

	if (customToolType == "GRANJAS") {
		if (xRadiusGranjas != null) {
			for (var ii = 0; ii < distanceList.length; ii++) {
				axl += bufferDrawCircleAroundPt(distanceList[ii], xRadiusGranjas, yRadiusGranjas);
			}		
		}
	}

	axl += '  </GET_IMAGE> </REQUEST></ARCXML>\n';
	return axl;
}

function getMapRequestAcetateMarca(_minx, _miny, _maxx, _maxy, _xMarca, _yMarca, _xMarcaBis, _yMarcaBis){
/*	var axl = getCabeceraArcXML(_minx, _miny, _maxx, _maxy, null, null, true);

	axl += getArcXMLAcetateMarca(_xMarca,  _yMarca, _xMarcaBis,  _yMarcaBis);

	axl += '    </GET_IMAGE> </REQUEST></ARCXML>\n';
	return axl;*/
}

var filterLayerList = new Array();

function getCabeceraArcXML(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, allLayersFalse, layerVisibility, _theDPI) {
	var axl = '<?xml version="1.0" encoding="UTF-8"?>\n';
	axl += '<ARCXML version="1.1">\n';
	axl += ' <REQUEST>\n';
	axl += '  <GET_IMAGE>\n';
	axl += '   <PROPERTIES>\n';
	axl += '    <ENVELOPE minx="' + _minx + '" miny="' + _miny +'" maxx="' + _maxx +'" maxy="' + _maxy + '" />\n';
	if (_theDPI) {
		if (_mapSizeX && _mapSizeY) {
			axl += '  <IMAGESIZE height="' + _mapSizeY + '" width="' + _mapSizeX + '" dpi="' + _theDPI + '" scalesymbols="false" />\n';
		} else {
			axl += '  <IMAGESIZE height="' + mheight + '" width="' + mwidth + '" dpi="' + _theDPI + '" scalesymbols="false" />\n';
		}
	} else {
		if (_mapSizeX && _mapSizeY) {
			axl += '  <IMAGESIZE height="' + _mapSizeY + '" width="' + _mapSizeX + '" />\n';
		} else {
			axl += '  <IMAGESIZE height="' + mheight + '" width="' + mwidth + '" />\n';
		}
	}
	axl += '    <LAYERLIST>\n';

	if (allLayersFalse) {
		for (var i=0;i<layerCount;i++) {
			axl += '<LAYERDEF id="' + LayerID[i] + '" visible="false" />\n';
		}
	} else {
		var layerVisibArray = LayerVisible;

		if (layerVisibility) {
			layerVisibArray = layerVisibility;
		}
		for (var i=0; i<layerCount; i++) {
			if (layerVisibArray[i] == 1) {
				var theFilter = "";
				if (filterLayerList[LayerID[i]]) {
					theFilter = '<QUERY where="' + filterLayerList[LayerID[i]] + '" ></QUERY>';
				}
				axl += '<LAYERDEF id="' + LayerID[i] + '" visible="true" >\n' + theFilter + '\n</LAYERDEF>';
			} else {
				axl += '<LAYERDEF id="' + LayerID[i] + '" visible="false" />\n';
			}
		}
	}
	axl += '    </LAYERLIST>\n';
	axl += '   </PROPERTIES>\n';

	return axl;
}

function getMapRequestAcetateEscala(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY) {
	var axl = getCabeceraArcXML(_minx, _miny, _maxx, _maxy, _mapSizeX, _mapSizeY, true);

	var newMapScale = -1;
	if (_mapSizeX) {
		newMapScale = getMapScale2(false, _minx, _maxx, _mapSizeX);
	} else {
		newMapScale = getMapScale2(false, _minx, _maxx, mwidth);
	}
	if (newMapScale >= 100000) {
		scaleunits = "kilometers";
	} else {
		scaleunits = "meters";
	}

	axl += getArcXMLScaleBar(scaleunits);

	axl += '    </GET_IMAGE> </REQUEST></ARCXML>\n';
	return axl;
}

function getArcXMLScaleBar(scaleunits,xpos, ypos) {
/*
	axl += '<LAYER type="acetate" name="theNorthArrow">\n';
	axl += '<OBJECT units="pixel">\n';
	axl += '<NORTHARROW type="6" size="30" coords="50 50" shadow="32,32,32" angle="0" antialiasing="True" overlap="False" />\n';
	axl += '</OBJECT>\n';
	axl += '</LAYER>\n';
*/

/*
	axl += '	<LAYER type="acetate" name="theScaleBar">\n';
	axl += '<OBJECT units="pixel">\n';
	axl += '<SCALEBAR screenlength="209" coords="3 3" fontcolor="0,0,0" fontstyle="Regular" barcolor="128,128,128" mapunits="meters" scaleunits="kilometers" antialiasing="True" fontsize="9" barwidth="5" overlap="False" />\n';
	axl += '</OBJECT>\n';
	axl += '</LAYER>\n';
*/
	var position = 10;
	var yposition = 50;
	if ((xpos)&&(xpos>=0)){
		position = xpos;
	}
	if ((ypos)&&(ypos>=0)){
		yposition = ypos;
	}
	var yposition2=yposition+10;
	var axl = '<LAYER type="acetate" name="Escala">\n';
	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="0.0" bartransparency="1.0" fontcolor="0,122,180" coords="'+position+' '+yposition+'" barcolor="0,122,180" fontsize="11" screenlength="250" barwidth="5" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="0,122,180" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="0.0" bartransparency="1.0" fontcolor="206,206,206" coords="'+position+' '+yposition+'" barcolor="206,206,206" fontsize="11" screenlength="175" barwidth="5" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="206,206,206" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="0.0" bartransparency="1.0" fontcolor="0,122,180" coords="'+position+' '+yposition+'" barcolor="0,122,180" fontsize="11" screenlength="100" barwidth="5" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="0,122,180" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="0.0" bartransparency="1.0" fontcolor="206,206,206" coords="'+position+' '+yposition+'" barcolor="206,206,206" fontsize="11" screenlength="25" barwidth="5" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="206,206,206" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="1.0" bartransparency="0.0" fontcolor="245,243,244" coords="'+position+' '+yposition2+'" barcolor="192,0,0" fontsize="11" fontstyle="bold" screenlength="250" barwidth="0" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="54,54,54" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="1.0" bartransparency="0.0" fontcolor="245,243,244" coords="'+position+' '+yposition2+'" barcolor="255,255,255" fontsize="11" fontstyle="bold" screenlength="175" barwidth="0" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="54,54,54" overlap="False"/></OBJECT>\n';

	axl	+= '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="1.0" bartransparency="0.0" fontcolor="245,243,244" coords="'+position+' '+yposition2+'" barcolor="192,90,90" fontsize="11" fontstyle="bold" screenlength="100" barwidth="0" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="54,54,54" overlap="False"/></OBJECT>\n';

	axl += '<OBJECT units="pixel"><SCALEBAR precision="0" texttransparency="1.0" bartransparency="0.0" fontcolor="245,243,244" coords="'+position+' '+yposition2+'" barcolor="255,255,255" fontsize="12" fontstyle="bold" screenlength="25" barwidth="0" mapunits="meters" antialiasing="true" scaleunits="' + scaleunits + '" outline="54,54,54" overlap="False"/></OBJECT>\n';
	axl += '</LAYER>\n';

	return axl;
}

function getArcXMLAcetateMarca(xMarca, yMarca,xMarcaBis, yMarcaBis) {
	var axl = '';

	axl += '<LAYER type="acetate" name="Texto Fondo" id="TextoFondo">';
	axl += '<OBJECT units="pixel">';
	axl += '<TEXT coords="10 20" label="' + getTextoFondo() + '">';
	axl += '<TEXTMARKERSYMBOL font="Verdana" fontstyle="bold" fontsize="14" outline="54,54,54" fontcolor="255,255,255" />';
	axl += '</TEXT>';
	axl += '</OBJECT>';
	axl += '</LAYER>';

	if ((xMarca != "") && (yMarca != "")) {
		axl += ' <LAYER type="acetate" name="allTheClicks">\n';
		axl += '  <OBJECT units="database">\n';
		axl += '   <POINT coords="' + xMarca + ' ' + yMarca +'">\n';
			//axl += '<SIMPLEMARKERSYMBOL  type="Circle" color="255,0,0" width="10" />\n';
		axl += '    <RASTERMARKERSYMBOL overlap="true" image="' + chinchetaQueryImagePath + '" url="' + chinchetaQueryURL + '" size="20,20" />\n';
		axl += '   </POINT>\n';
		axl += '  </OBJECT>\n';
		axl += '</LAYER>\n';
	}
	
	if ((xMarcaBis != "") && (yMarcaBis != "")) {
		axl += ' <LAYER type="acetate" name="allTheClicks">\n';
		axl += '  <OBJECT units="database">\n';
		axl += '   <POINT coords="' + xMarcaBis + ' ' + yMarcaBis +'">\n';
			//axl += '<SIMPLEMARKERSYMBOL  type="Circle" color="255,0,0" width="10" />\n';
		axl += '    <RASTERMARKERSYMBOL overlap="true" image="' + chinchetaInfoImagePath + '" url="' + chinchetaInfoURL + '" size="20,20" />\n';
		axl += '   </POINT>\n';
		axl += '  </OBJECT>\n';
		axl += '</LAYER>\n';
	}

	return axl;
}

function getArcXMLSelection() {
	var axl = '';

	if (idSelectionLayer != '') {
		if (selectionWhere != '') {
			axl += ' <LAYER type="featureclass" name="selection">\n';
			axl += '  <DATASET fromlayer="' + idSelectionLayer + '" />\n';
			axl += '  <QUERY where="' + selectionWhere + '" />\n';
			axl += '  <SIMPLERENDERER>\n';
			if (selectionType == "poin" ) {
					//axl += '<SIMPLEMARKERSYMBOL  type="Circle" color="' + selectionColorPoint + '" width="10" />\n';
				axl += '<RASTERMARKERSYMBOL overlap="true" image="' + chinchetaQueryImagePath + '" url="' + chinchetaQueryURL + '" size="20,20" />\n';
			} else if (selectionType == "line" ) {
				axl += '<SIMPLELINESYMBOL  type="SOLID" color="' + selectionColorLine + '" width="3" />\n';
			} else {
				if (isIE6) {
					//axl += '<SIMPLEPOLYGONSYMBOL  fillcolor="' + selectionColorPoly + '" filltype="diagcross" transparency="' + selectionTransparentLevel +'" boundarycolor="' + selectionBoundaryColorPoly+ '" />\n';
					axl += '<SIMPLEPOLYGONSYMBOL filltransparency="0" boundarycolor="' + selectionBoundaryColorPoly+ '" boundarywidth="' + selectionBoundaryWidthPoly+ '" />\n';
				} else {
					//axl += '<SIMPLEPOLYGONSYMBOL  fillcolor="' + selectionColorPoly + '" filltype="solid" transparency="' + selectionTransparentLevel +'" boundarycolor="' + selectionBoundaryColorPoly+ '" />\n';
					axl += '<SIMPLEPOLYGONSYMBOL filltransparency="0" boundarycolor="' + selectionBoundaryColorPoly+ '" boundarywidth="' + selectionBoundaryWidthPoly+ '" />\n';
				}
			}

			axl += '  </SIMPLERENDERER>\n';
			axl += '</LAYER>\n';
		}
	}

	return axl;
}

function getArcXMLSelectionEsquema() {
	var axl = '';
	
	if ((typeof selectionWhereEsquema !='undefined')&&(selectionWhereEsquema != '')) {
		for (var ii =0; ii < listaIdSelectionLayerEsquema.length; ii++) {
			axl += ' <LAYER type="featureclass" name="selection'+ ii + '">\n';
			axl += '  <DATASET fromlayer="' + listaIdSelectionLayerEsquema[ii] + '" />\n';
			axl += '  <QUERY where="' + selectionWhereEsquema + '" />\n';
			axl += '  <SIMPLERENDERER>\n';
			if (selectionTypeEsquema == "poin" ) {
					//axl += '<SIMPLEMARKERSYMBOL  type="Circle" color="' + selectionColorPoint + '" width="10" />\n';
				axl += '<RASTERMARKERSYMBOL overlap="true" image="' + chinchetaQueryImagePath + '" url="' + chinchetaQueryURL + '" size="20,20" />\n';
			} else if (selectionType == "line" ) {
				axl += '<SIMPLELINESYMBOL  type="SOLID" color="' + selectionColorLine + '" width="3" />\n';
			} else {
				if (isIE6) {
					axl += '<SIMPLEPOLYGONSYMBOL  fillcolor="0,0,255" filltype="solid" transparency="' + selectionTransparentLevel +'" boundarycolor="255,255,255" />\n';
				} else {
					axl += '<SIMPLEPOLYGONSYMBOL  fillcolor="0,0,255" filltype="solid" transparency="' + selectionTransparentLevel +'" boundarycolor="255,255,255" />\n';
				}
			}

			axl += '  </SIMPLERENDERER>\n';
			axl += '</LAYER>\n';
		}
	}

	return axl;
}

function getCoords(x1, y1, x2, y2) {
	var theCoords = x1 + " " + y1 + ";";
	theCoords += x1 + " " + y2 + ";";
	theCoords += x2 + " " + y2 + ";";
	theCoords += x2 + " " + y1 + ";";

	return theCoords;
}

function getLayerPolygon(x1, y1, x2, y2, tipo) {
	var theString = '<LAYER type="ACETATE" id="extensionOverview' + tipo + '" name="extensionOverview' + tipo + '">\n';
	theString += '<OBJECT units="database">\n';

	var theCoords = getCoords(x1, y1, x2, y2);

	theString += '<POLYGON coords="'+theCoords+'">\n';
	theString += '<SIMPLEPOLYGONSYMBOL fillcolor="255,0,0" filltype="fdiagonal" transparency="0,5" boundarycolor="255,0,0" />\n';
	theString += '</POLYGON>\n';
	theString += '</OBJECT>\n';
	theString += '</LAYER>\n';
	return theString;
}

function getArcXMLOverviewExtension() {
	var theString = '';

	theString += getLayerPolygon(minxPrint, minyPrint, maxxPrint, maxyPrint, '');
	var midX = (maxxPrint + minxPrint) / 2;
	var midY = (maxyPrint + minyPrint) / 2;
	theString += getLayerPolygon(midX, maxyPrint, midX, ymaxOverview, 'N');
	theString += getLayerPolygon(xminOverview, midY, minxPrint, midY, 'W');
	theString += getLayerPolygon(maxxPrint, midY, xmaxOverview, midY, 'E');
	theString += getLayerPolygon(midX, yminOverview, midX, minyPrint, 'S');

	/*
	theString = '<LAYER type="ACETATE" id="extensionOverview" name="extensionOverviewN">\n';
	theString += '<OBJECT units="database">\n';
	var theCoords = minxPrint + " " + minyPrint + ";";
	theCoords += minxPrint + " " + maxyPrint + ";";
	theCoords += maxxPrint + " " + maxyPrint + ";";
	theCoords += maxxPrint + " " + minyPrint + ";";

	theString += '<POLYGON coords="'+theCoords+'">\n';
	theString += '<SIMPLEPOLYGONSYMBOL fillcolor="255,0,0" filltype="fdiagonal" transparency="0.5" boundarycolor="255,0,0" />\n';
	theString += '</POLYGON>\n';
	theString += '</OBJECT>\n';
	theString += '</LAYER>\n';
*/
	return theString;
}

function getArcXMLPointRecordset(_recordset, _nombreCampoTexto) {
	var	numData = _recordset.getVectorialDataCount();
	var axl = '';
	if (numData > 0) {
		axl = ' <LAYER type="acetate" name="theClicks">\n';
		for (var jj = 0; jj < numData; jj++) {
			aux = _recordset.getGeometry(jj);
			texto = '';
			if (_nombreCampoTexto) {
				texto = _recordset.getAttribute(jj, _nombreCampoTexto);
			} else {
				texto = RoundDecimal(aux.getX(),2) + ', ' + RoundDecimal(aux.getY(),2);
			}

			axl += '  <OBJECT units="database">\n';
			axl += '   <TEXT coords="' + aux.getX() + ' ' + aux.getY() + '" label="' + texto +'">\n';
				//axl2 += '<SIMPLEMARKERSYMBOL  type="Circle" color="255,0,0" width="10" />\n';
			axl += '    <TEXTMARKERSYMBOL font="Arial" fontstyle="bold" fontsize="14" fontcolor="255,0,0" valignment="bottom"/>\n';
			axl += '   </TEXT>\n';
			axl += '  </OBJECT>\n';
			axl += '  <OBJECT units="database">\n';
	    axl += '  <POINT coords="' + aux.getX() + ' ' + aux.getY() + '" >';
      axl += '  <SIMPLEMARKERSYMBOL color="255,0,0" />';
      axl += '  </POINT>';
			axl += '  </OBJECT>\n';
				//	w.document.writeln(aux.getX() + "\t" + aux.getY());
		}
		axl += '</LAYER>\n';
	}
	return axl;
}

function bufferDrawCircleAroundPt(radiusDistance, buffX, buffY){
	if (debugOn) {
		alert("[function bufferDrawCircleAroundPt]\nradiusDistance="+radiusDistance+"\nbuffX="+buffX+"\nbuffY="+buffY);
	}

	var theCoords="";
	var increment=0.0;
	firstTime=true;
	//distance = radiusDistance * 5280;
	distance = radiusDistance;

	x = buffX;
	y = buffY;
	while (increment<=359.0) {
		var x1 = x + (distance * Math.cos(increment*Math.PI/180));
		var y1 = y + (distance * Math.sin(increment*Math.PI/180));
		theCoords=theCoords+String(x1)+" ";
		theCoords=theCoords+String(y1)+";";
		if (firstTime) {
			firstString=theCoords;
			firstTime=false;
		}
		increment=increment+1;
	}
	var lastString = firstString.substring(0,firstString.length - 1);
	//alert(lastString);
	theCoords=theCoords+lastString;

	// ArcXML to draw circle object
	var theString = '<LAYER type="ACETATE" id="selRadiusCircle" name="Circle">\n';
	theString += '<OBJECT units="database">\n';
	theString += '<POLYGON coords="'+theCoords+'">\n';
	theString += '<SIMPLEPOLYGONSYMBOL fillcolor="255,0,0" filltype="fdiagonal" fillinterval="6" transparency="1,0" boundarycolor="255,0,0" boundarywidth="3" />\n';
	theString += '</POLYGON>\n';
	theString += '</OBJECT>\n';
	theString += '</LAYER>\n';
	// ArcXML to draw point object in center of circle
	theString += '<LAYER type="ACETATE" id="ptInCircle" name="PointInCircle">\n';
	theString +='<OBJECT units="database">\n';
	theString += '<POINT coords="'+x+' '+y+'">\n';
	theString += '<SIMPLEMARKERSYMBOL color="255,0,0" outline="255,0,0" type="cross" transparency="1,0" width="5" />\n';
	theString += '</POINT>\n';
	theString += '</OBJECT>\n';
	theString += '</LAYER>\n';

	if (debugOn) {
		alert("[function bufferDrawCircleAroundPt]\ntheString="+theString);
	}
	return theString;
}		// end of function bufferDrawCircleAroundPt

function getPoligonoSeleccionadoArcXML(tipoImagen, sinFondo) {
	var axl = "";
	axl += '<LAYER type="acetate" name="poligonoSeleccionado" id="poligonoSeleccionado">';
	axl += '  <OBJECT units="database">';
	if (tipoImagen == "") {
		if (sinFondo) {
			axl += '		<SIMPLEPOLYGONSYMBOL fillcolor="0,0,255" filltype="gray" filltransparency="0" boundarytransparency="1" boundarycolor="255,168,50"  boundarywidth="' + selectionBoundaryWidthPoly +'" />\n';
		} else {
			axl += '		<SIMPLEPOLYGONSYMBOL fillcolor="255,0,0" filltype="gray" transparency="1" boundarycolor="255,128,0"  boundarywidth="3" />\n';
		}
		axl += coordsPoligono;
	} else if (tipoImagen == "Aragon") {
		var xCentroidePoligono = ((maxxPoligono - minxPoligono) / 2) + minxPoligono;
		var yCentroidePoligono = ((maxyPoligono - minyPoligono) / 2) + minyPoligono;
		axl += '    <POINT coords="' + xCentroidePoligono + ' ' + yCentroidePoligono + '">\n';
		axl += '       <SIMPLEMARKERSYMBOL color="255,0,0" outline="255,0,0" type="cross" transparency="1.0" width="5" />\n';
		axl += '    </POINT>\n';
	} else if (tipoImagen == "Situacion") {
		var diffXPoligono = (maxxPoligono - minxPoligono);
		var diffYPoligono = (maxyPoligono - minyPoligono);

		if ( (diffXPoligono < 1000) || (diffXPoligono <1000) ) {
				// es pequeña, pinto un punto
			var xCentroidePoligono = ((maxxPoligono - minxPoligono) / 2) + minxPoligono;
			var yCentroidePoligono = ((maxyPoligono - minyPoligono) / 2) + minyPoligono;
			axl += '    <POINT coords="' + xCentroidePoligono + ' ' + yCentroidePoligono + '">\n';
			axl += '       <SIMPLEMARKERSYMBOL color="255,0,0" outline="255,0,0" type="cross" transparency="1.0" width="5" />\n';
			axl += '    </POINT>\n';
		} else {
			axl += '		<SIMPLEPOLYGONSYMBOL fillcolor="255,0,0" filltype="gray" transparency="1" boundarycolor="255,128,0"  boundarywidth="3" />\n';
			axl += coordsPoligono;
		}
	}
	axl += '</OBJECT>\n';
	axl += '</LAYER>\n';
	return axl;
}
