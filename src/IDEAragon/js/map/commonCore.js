
function Recordset(attributesNamesList) {
		//vector de los nombres de los atributos
	this.attributesNamesList = attributesNamesList || new Array();

		//vector de geometrías
	this.geometryList = new Array();
		//matriz de los atributos
	this.attributesList = new Array();
	this.nextPosition = 0;

	this.getGeometryList = getGeometryList;
	this.getGeometry = getGeometry;
	this.getAttributesList= getAttributesList;
	this.getAttribute = getAttribute;
	this.getAttributeNamesList = getAttributeNamesList;
	this.setAttributeNamesList = setAttributeNamesList;

	this.addVectorialData = addVectorialData;
	this.removeVectorialData = removeVectorialData;
	this.clearVectorialData = clearVectorialData;
	this.getVectorialDataCount = getVectorialDataCount;

	function getAttributeNamesList() {
		return this.attributesNamesList;
	}

	function setAttributeNamesList(newAttributesNamesList) {
		this.attributesNamesList = newAttributesNamesList;
	}

	function getGeometryList() {
		return this.geometryList;
	}

	function getGeometry(index) {
		return this.geometryList[index];
	}

	function getAttributesList(index) {
		return this.attributesList[index];
	}

	function getAttribute(index, nameAttribute) {
		indexCol = -1;
		for (var i=0; i<this.getAttributeNamesList().length; i++) {
				// en i está el nombre de la columna del recordset
			if (this.getAttributeNamesList()[i] == nameAttribute) {
				indexCol = this.getAttributeNamesList()[i];
			}
		}
		if (indexCol != -1) {
			return this.attributesList[index][indexCol];
		} else {
			return null;
		}
	}

	function addVectorialData(newGeometry, newAttributesList) {
		this.geometryList[this.nextPosition] = newGeometry;
		this.attributesList[this.nextPosition] = newAttributesList;
		this.nextPosition++;
	}

	function removeVectorialData(index) {
		this.geometryList[index] = null;
		this.attributesList[index] = null;
		for (var i = index; i < this.nextPosition-1; i++) {
			this.geometryList[i] = this.geometryList[i+1];
			this.attributesList[i] = this.attributesList[i+1];
		}
		this.nextPosition--;
	}
	function clearVectorialData() {
		this.nextPosition = 0;
	}

	function getVectorialDataCount() {
		return this.nextPosition;
	}
}
