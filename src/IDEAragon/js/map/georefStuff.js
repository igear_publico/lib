var originador = null;

function changeOriginador() {
	if (originador == null) {
		originador = '';
	}
	originador = window.prompt("Introduzca su nombre de usuario", originador);
	if (originador == null) {
		originador = '';
	}
}

/*
function initGeorefPoint() {
	if (customGeoref) {
		_op = null;
		if (top.opener) {
			_op = top.opener;
		} else if (opener) {
			_op = opener;
		} else if (window.opener) {
			_op = window.opener;
		}

		if (_op) {
			if (!(_op.closed)) {
				alert("Haga click sobre el mapa en el punto deseado");
				if (typeCustomGeoref == 1) {
					parent.MapFrame.clickFunction('georefPointCustom1');
				} else {
					parent.MapFrame.clickFunction('georefPointCustom2');
				}
			} else {
				alert("La ventana principal se ha cerrado y no es posible continuar");
			}
		} else {
				alert("No se encuentra la ventana principal");
		}
	} else {
		if (originador == '') {
			originador = window.prompt("Introduzca su nombre de usuario", "");
		}
		parent.MapFrame.clickFunction('georefPoint');
		if (originador == null) {
			originador = '';
		}
	}
}

function initGeorefLine() {
	parent.MapFrame.clickFunction('georefLine');
}

function initGeorefPol() {
	parent.MapFrame.clickFunction('georefPol');
}
*/

function endGeoref() {
		// reseteo para que vuelva a empezar
	clickPointX = new Array();
	clickPointY = new Array();
	screenClickPointX = new Array();
	screenClickPointY = new Array();
	clickCount = 0;
	resetClick();
}

function finPointGeoref() {
	textoG = window.prompt("Introduzca el texto asociado", "");
	if ((textoG != null) && (textoG != '')) {
		attrib = new Array();
		attrib['text'] = textoG;
		attrib['escala'] = getMapScale(false);
		attrib['originador'] = originador;
		attrib['fecha'] = new Date();
		attrib['SRS'] = "EPSG:25830";

		gg = new Point(clickPointX[0], clickPointY[0]);
		pointRecordset.addVectorialData(gg, attrib);
	}
	updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);

	endGeoref();
}

function finLineGeoref() {
	if (clickCount >= 2) {
		textoG = window.prompt("Introduzca el texto asociado", "");
		if ((textoG != null) && (textoG != '')) {
			attrib = new Array();
			attrib['text'] = textoG;
			attrib['escala'] = getMapScale(false);
			attrib['originador'] = originador;
			attrib['fecha'] = new Date();
			attrib['SRS'] = "EPSG:25830";

			gg = new LineString();
			for (var ii = 0; ii < clickCount; ii++) {
				gg.addPoint(clickPointX[ii], clickPointY[ii]);
			}
			lineRecordset.addVectorialData(gg, attrib);

			updateGeorefMapClicks(jgLine, lineRecordset, jgLineLabel);
		}
	} else {
		alert("Para generar una línea debe haber al menos 2 puntos");
	}
	endGeoref();
}

function finPolGeoref() {
	if (clickCount >= 3) {
		textoG = window.prompt("Introduzca el texto asociado", "");
		if ((textoG != null) && (textoG != '')) {
			attrib = new Array();
			attrib['text'] = textoG;
			attrib['escala'] = getMapScale(false);
			attrib['originador'] = originador;
			attrib['fecha'] = new Date();
			attrib['SRS'] = "EPSG:25830";

			gg = new LinearRing();
			for (var ii = 0; ii < clickCount; ii++) {
				gg.addPoint(clickPointX[ii], clickPointY[ii]);
			}

				// repetir el primer punto para que sepa que el poligono esta cerrado
			gg.addPoint(clickPointX[0], clickPointY[0]);
			polRecordset.addVectorialData(gg, attrib);
			updateGeorefMapClicks(jgPol, polRecordset, jgPolLabel);
		}
	} else {
		alert("Para generar un polígono debe haber al menos 3 puntos");
	}

	endGeoref();
}

function finGeom() {
	switch(getTipoGeorref()) {
		case 'point':		break;
		case 'line':		finLineGeoref();
										break;
		case 'polygon':	finPolGeoref();
										break;
		default:				break;
	}
}

function deleteAllGeom(tipoGeom) {
	if (window.confirm("¿Está seguro de que desea borrar todos los elementos?")) {
			// reseteo variables
		clickPointX = new Array();
		clickPointY = new Array();
		screenClickPointX = new Array();
		screenClickPointY = new Array();
		clickCount = 0;

			// borro recordset
		if (tipoGeom == Geometry.LINEAR_RING) {
			polRecordset.clearVectorialData();
			updateGeorefMapClicks(jgPol, polRecordset, jgPolLabel);
		} else if (tipoGeom == Geometry.LINE_STRING) {
			lineRecordset.clearVectorialData();
			updateGeorefMapClicks(jgLine, lineRecordset, jgLineLabel);
		} else {
			pointRecordset.clearVectorialData();
			updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
		}
		resetClick();
	}
}

function dosDig(x) {
	if (x < 10) {
		return "0" + x;
	} else {
		return x;
	}
}

function exportGeoref(tipoGeom) {
	var d = new Date();
	var fechaHora = "_" + d.getFullYear() + dosDig(d.getMonth()+1) + dosDig(d.getDate()) + "_" + dosDig(d.getHours()) + "_" + dosDig(d.getMinutes());

	if (tipoGeom == Geometry.LINEAR_RING) {
		//generateFileExcel(polRecordset, "/BD_GIS/SITAR_georref_pol.jsp");
		generateFileExcel(pointRecordset, "/export/georrefPol_IDEAragon" + fechaHora + ".xls");
	} else if (tipoGeom == Geometry.LINE_STRING) {
		//generateFileExcel(lineRecordset, "/BD_GIS/SITAR_georref_lin.jsp");
		generateFileExcel(pointRecordset, "/export/georrefLin_IDEAragon" + fechaHora + ".xls");
	} else {
		//generateFileExcel(pointRecordset, "/BD_GIS/SITAR_georref_pun.jsp");
		generateFileExcel(pointRecordset, "/export/georref_IDEAragon" + fechaHora + ".xls");
	}
}

function exportGeorefCSV(tipoGeom) {
	if (tipoGeom == Geometry.LINEAR_RING) {
		generateCSVFile(polRecordset);
	} else if (tipoGeom == Geometry.LINE_STRING) {
		generateCSVFile(lineRecordset);
	} else {
		generateFileCSV(pointRecordset);
	}
}

function generateCSVFile(recordset) {
	w = window.open('', '');
	numData = recordset.getVectorialDataCount();

	w.document.writeln("<html><body>");
	for (var jj = 0; jj < numData; jj++) {
		aux = recordset.getGeometry(jj);
		w.document.writeln(aux.getX() + "\t" + aux.getY());
	}
	w.document.writeln("</body></html>");
	w.document.close();
}

	// funciones para generar los ficheros Excel/CSV
function num2Coord(aux) {
	coord = String(RoundDecimal(aux,3));
	if (coord.indexOf(".") != -1) {
		posPunto = coord.indexOf(".");
		st1 = coord.substr(0, posPunto);
		st2 = coord.substr(posPunto+1);
	
	coord = st1 + st2 + "E-" + st2.length;
	}
	return coord;
}

function generateItemCSV(x, y, text, id, escala, originador, fecha, srs) {
		// el campo id es para relacionar las geometrias que forman el polígono
	coordX = num2Coord(x);
	coordY = num2Coord(y);
	//return (coordX + "\t" + coordY + "\t" + text + "\t" + id);
	return ((id !=null? id + ";" :"")+ text + ";" + coordX + ";" + coordY + ";" + escala + ";" + (originador !=null? originador + ";" :"") + fecha + ";" + srs);
}

function generateFileCSV(rec) {



	//w = window.open();
	numData = rec.getVectorialDataCount();
	//w.document.writeln("<html><body>");
	var csv = "Id;Texto;Coord X;Coord Y;Escala;Creador;Fecha;SRS\n";
	for (var jj = 0; jj < numData; jj++) {
		aux = rec.getGeometry(jj);

		textoG = rec.getAttribute(jj, 'text');
		escala = rec.getAttribute(jj, 'escala');
		originador = rec.getAttribute(jj, 'originador');
		fecha = rec.getAttribute(jj, 'fecha');
		srs = rec.getAttribute(jj, 'SRS');

		if ((aux.getClass() == Geometry.LINEAR_RING) || (aux.getClass() == Geometry.LINE_STRING)) {
			for (var kk = 0; kk < aux.npoints; kk++) {
				csv += generateItemCSV(aux.xpoints[kk], aux.ypoints[kk], textoG, jj, escala, originador, fecha, srs)+"\n";
			}
		} else {
			// supongo que siempre es punto si no es linea
			csv += generateItemCSV(aux.getX(), aux.getY(), textoG, jj, escala, originador, fecha, srs)+"\n";
		}
	}
	try{
		var blob = new Blob([csv], {type: "application\/octet-stream"});
		var url = URL.createObjectURL(blob);
		var a = document.getElementById('enlace');
		a.href = window.URL.createObjectURL(blob);
		a.download = "fichero.csv";

		a.click();
	}
	catch(err){
		var w=window.open();
		w.document.writeln("<html><body>");
		var lines = csv.split("\n");
		for (var i=0; i<lines.length; i++){
			w.document.writeln(lines[i]);
		}

		w.document.writeln("</body></html>");
		w.document.close();
	}


}

function generateItemExcel(x, y, text, id, escala, originador, fecha, srs) {
		// el campo id es para relacionar las geometrias que forman el polígono
	coordX = num2Coord(x);
	coordY = num2Coord(y);
	//return ("<tr><td>" + coordX + "</td><td>" + coordY + "</td><td>" +text + "</td><td>" + id + "</td></tr>");
	return ("<tr>"+(id != null ? "<td>" + id + "</td>":"")+"<td>" + text + "</td><td>" + coordX + "</td><td>" + coordY + "</td><td>" + escala + "</td>"+(id != null ? "<td>" + originador + "</td>" : "")+"<td>" + fecha + "</td><td>" + srs + "</td></tr>");
}


function getExcelTable(rec,georref) {
	numData = rec.getVectorialDataCount();

	//excelTablevar = '<table border=1><tr style="background-color:#FFCC99;"><th width="250">Coord X</th><th width="250">Coord Y</th><th width="300">Texto</th><th width="100">Id</th></th>';
	excelTablevar = '<table border="1" class="textoInv"><tr style="background-color:#FFCC99;">'+(georref ? '<th width="100">Id</th>':'')+'<th width="300">Texto</th><th width="250">Coord X</th><th width="250">Coord Y</th><th width="150">Escala</th>'+(georref ? '<th width="150">Creador</th>':'')+'<th width="350">Fecha</th><th width="250">SRS</th></tr>';

	for (var jj = 0; jj < numData; jj++) {
		aux = rec.getGeometry(jj);

		textoG = rec.getAttribute(jj, 'text');
		escala = rec.getAttribute(jj, 'escala');
		
			originador = rec.getAttribute(jj, 'originador');

		fecha = rec.getAttribute(jj, 'fecha');
		srs = rec.getAttribute(jj, 'SRS');

		if (georref && ((aux.getClass() == Geometry.LINEAR_RING) || (aux.getClass() == Geometry.LINE_STRING)) ){
			for (var kk = 0; kk < aux.npoints; kk++) {
				excelTablevar += (generateItemExcel(aux.xpoints[kk], aux.ypoints[kk], textoG, jj, escala, originador, fecha, srs));
			}
		} else {
				// supongo que siempre es punto si no es linea
			excelTablevar +=(generateItemExcel(aux.getX(), aux.getY(), textoG,(georref? jj : null), escala, (georref ? originador : null), fecha, srs));
		}
	}
	excelTablevar = excelTablevar + "</table>";
	return excelTablevar;
}

function generateFileExcel(rec, actionName) {
	document.excelForm.resultsTable.value = getExcelTable(rec, true);
	document.excelForm.target = "_blank";
	document.excelForm.action = actionName;
	document.excelForm.submit();
}

	// final de funciones para generar los ficheros Excel/CSV

	// código para poder clickar en algun punto y hacer algo

function deleteClickByText(tipoGeom) {
	textoG = window.prompt("Introduzca el texto de la geometría a borrar", "");

	if ((textoG != null) && (textoG != '')) {
		if (tipoGeom == Geometry.LINEAR_RING) {
			deleteAGeomByText(polRecordset, textoG);
			updateGeorefMapClicks(jgPol, polRecordset, jgPolLabel);
		} else if (tipoGeom == Geometry.LINE_STRING) {
			deleteAGeomByText(lineRecordset, textoG);
			updateGeorefMapClicks(jgLine, lineRecordset, jgLineLabel);
		} else {
			deleteAGeomByText(pointRecordset, textoG);
			updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
		}
	}
}

function deleteAGeomByText(rec, text) {
	numData = rec.getVectorialDataCount();

	indiceData = 0;
	finalizado = false;
	while (indiceData < numData) {
		if (rec.getAttribute(indiceData, 'text') == text) {
			rec.removeVectorialData(indiceData);
			numData--;

			// no incrementar indiceData porque el borrado reorganiza el vector y la posicion
			// indiceData se habra rellenado con el siguiente
		} else {
			indiceData++;
		}
	}
}

// IMPORTANTE: LLAMAR A metodo paint() tras utilizarlo!!!!
function drawLabel(divToLabel, geom, texto) {
	xCenter = 0;
	yCenter = 0;

		// Calcular el pto central donde va a ir la etiqueta
	if (geom.getClass() == Geometry.LINE_STRING) {
			// poner una etiqueta en el centro de la linea
			// si hay numero impar de puntos, lo pongo en el punto central
			// sino, lo pongo en el medio entre los dos puntos centrales
		numPuntos = geom.npoints;
		indexCenter = Math.floor(numPuntos/2);
		if ((numPuntos % 2) == 0) {
			x0 = geom.xpoints[indexCenter-1];
			y0 = geom.ypoints[indexCenter-1];
			x1 = geom.xpoints[indexCenter];
			y1 = geom.ypoints[indexCenter];

			xOffset = x1 - x0;
			yOffset = y1 - y0;

			xCenter = x0 + (xOffset/2);
			yCenter = y0 + (yOffset/2);
		} else {
			xCenter = geom.xpoints[indexCenter];
			yCenter = geom.ypoints[indexCenter];
		}
	} else {
			// poner una etiqueta en el centro de cada polígono
		rectangle = geom.getBounds();
			// calculo el centro del polígono
		xCenter = rectangle.getX() + (rectangle.getWidth() / 2);
		yCenter = rectangle.getY() + (rectangle.getHeight() / 2);
	}
	pixelLabelPoint = getScreenXY(xCenter, yCenter);
	divToLabel.drawString(texto,pixelLabelPoint[0],pixelLabelPoint[1]);
}


function deleteLastGeorefPoint() {
	numData = pointRecordset.getVectorialDataCount();
	if (numData > 0) {
		pointRecordset.removeVectorialData(numData-1);
	} else {
		alert("No hay puntos para borrar");
	}

	updateGeorefMapClicks(jgPoint, pointRecordset, jgPointLabel);
}


// georef stuff
attributesNames = new Array();
attributesNames[0] = "text";
attributesNames[1] = "escala";
attributesNames[2] = "originador";
attributesNames[3] = "fecha";
attributesNames[4] = "SRS";

pointRecordset = new Recordset(attributesNames);
lineRecordset = new Recordset(attributesNames);
polRecordset = new Recordset(attributesNames);
historicoRecordset = new Recordset(attributesNames);


function pintaGeorrefZones(hspc,vspc,iWidth,iHeight) {
	var content = '<DIV style="width:100%;height:100%;visibility:visible;" id="georefPointGeomDiv"></DIV>';
	content += '<DIV style="width:100%;height:100%;visibility:visible;" id="georefPointLabelDiv"></DIV>';
	createLayer("georefPointDiv",hspc,vspc,iWidth,iHeight,true,content,999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="georefLineGeomDiv"></DIV>';
	content += '<DIV style="width:100%;height:100%;visibility:visible;" id="georefLineLabelDiv"></DIV>';
	createLayer("georefLineDiv",hspc,vspc,iWidth,iHeight,true,content,999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="georefPolyGeomDiv"></DIV>';
	content += '<DIV style="width:100%;height:100%;visibility:visible;" id="georefPolyLabelDiv"></DIV>';
	createLayer("georefPolyDiv",hspc,vspc,iWidth,iHeight,true,content,999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="notificarGeomDiv"></DIV>';
	createLayer("notificarDiv",hspc,vspc,iWidth,iHeight,true,content, 999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="georefHistoricoGeomDiv"></DIV>';
	content += '<DIV style="width:100%;height:100%;visibility:visible;" id="georefHistoricoLabelDiv"></DIV>';
	createLayer("georefHistoricoDiv",hspc,vspc,iWidth,iHeight,true,content,999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="circleGeomDiv"></DIV>';
	createLayer("circleDiv",hspc,vspc,iWidth,iHeight,true,content,999);

	content = '<DIV style="width:100%;height:100%;visibility:visible;" id="circleGranjasGeomDiv"></DIV>';
	createLayer("circleGranjasDiv",hspc,vspc,iWidth,iHeight,true,content,999);
}

function updateGeorrefZones(doc, hspc,vspc,iWidth,iHeight) {
	doc.getLayer("georefPointDiv").width = iWidth + "px";
	doc.getLayer("georefPointDiv").height = iHeight + "px";
	doc.getLayer("georefLineDiv").width = iWidth + "px";
	doc.getLayer("georefLineDiv").height = iHeight + "px";
	doc.getLayer("georefPolyDiv").width = iWidth + "px";
	doc.getLayer("georefPolyDiv").height = iHeight + "px";
	doc.getLayer("georefHistoricoDiv").width = iWidth + "px";
	doc.getLayer("georefHistoricoDiv").height = iHeight + "px";
	doc.getLayer("notificarDiv").width = iWidth + "px";
	doc.getLayer("georefPolyDiv").height = iHeight + "px";
}

function updateOtherZones(doc, hspc,vspc,iWidth,iHeight) {
	doc.getLayer("circleDiv").width = iWidth + "px";
	doc.getLayer("circleDiv").height = iHeight + "px";
	doc.getLayer("circleGranjasDiv").width = iWidth + "px";
	doc.getLayer("circleGranjasDiv").height = iHeight + "px";
}


/*
function historicoCoords() {
	parent.MapFrame.clickFunction('historico');
}
*/

function finHistorico() {
	textoG = window.prompt("Introduzca el texto asociado", "");
	if ((textoG != null) && (textoG != '')) {
		attrib = new Array();
		attrib['text'] = textoG;
		attrib['escala'] = getMapScale(false);
		attrib['fecha'] = new Date();
		attrib['SRS'] = "EPSG:25830";

		gg = new Point(clickPointX[0], clickPointY[0]);
		historicoRecordset.addVectorialData(gg, attrib);
	}
	
	txt = textoG + "("+RoundDecimal(clickPointX[0], 2) + ", " + RoundDecimal(clickPointY[0], 2)+")";

	updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);

	addNewPointHistorico(txt);

// reseteo para que vuelva a empezar
	clickPointX = new Array();
	clickPointY = new Array();
	screenClickPointX = new Array();
	screenClickPointY = new Array();
	clickCount = 0;
	resetClick();

}

historicoClicksCount = 0;
function addNewPointHistorico(txt) {
	//alert(document.getElementById('historicoSelect').options.length);
	document.getElementById('historicoSelect').options[historicoClicksCount] = new Option(txt, historicoClicksCount);
	historicoClicksCount++;
}

function exportaHistorico() {
	numData = historicoRecordset.getVectorialDataCount();

	var csv = "Texto;Coord X;Coord Y;Escala;Fecha;SRS\n";
	for (var jj = 0; jj < numData; jj++) {
		aux = historicoRecordset.getGeometry(jj);

		textoG = historicoRecordset.getAttribute(jj, 'text');
		escala = historicoRecordset.getAttribute(jj, 'escala');
		fecha = historicoRecordset.getAttribute(jj, 'fecha');
		srs = historicoRecordset.getAttribute(jj, 'SRS');
		
				csv += generateItemCSV(aux.getX(), aux.getY(), textoG,null, escala,null, fecha, srs)+"\n";
		
	}
	try{
	var blob = new Blob([csv], {type: "application\/octet-stream"});
	var url = URL.createObjectURL(blob);
	var a = document.getElementById('enlace');
	a.href = window.URL.createObjectURL(blob);
    a.download = "fichero.csv";
    a.click();
	}
	catch(err){
		var w=window.open();
		w.document.writeln("<html><body>");
		var lines = csv.split("\n");
		for (var i=0; i<lines.length; i++){
			w.document.writeln(lines[i]);
		}
		
		w.document.writeln("</body></html>");
		w.document.close();
	}
}

function exportaHistoricoExcel() {
	document.excelForm.resultsTable.value = getExcelTable(historicoRecordset,false);
	document.excelForm.target = "_blank";
	
	var d = new Date();
	var fechaHora = "_" + d.getFullYear() + dosDig(d.getMonth()+1) + dosDig(d.getDate()) + "_" + dosDig(d.getHours()) + "_" + dosDig(d.getMinutes());

	//document.excelForm.action = "/BD_GIS/SITAR_georref_pun.jsp";
	document.excelForm.action = "/export/georref_IDEAragon"  + fechaHora + ".xls";
	document.excelForm.submit();
}


function borraHistorico() {
	if (window.confirm("¿Está seguro de que desea borrar todos los elementos?")) {
		historicoRecordset.clearVectorialData();
		updateGeorefMapClicks(jgHistorico, historicoRecordset, jgHistoricoLabel);

			// reseteo para que vuelva a empezar
		clickPointX = new Array();
		clickPointY = new Array();
		screenClickPointX = new Array();
		screenClickPointY = new Array();
		clickCount = 0;

		resetClick();

		historicoClicksCount = 0;
		combo = document.getElementById('historicoSelect');
		if (combo.options.length > 0) {
			combo.options.length = null;
		}
	}
}

	/*	function toggleLabel(idLabel, idSpan) {
			obj = document.getElementById(idLabel);
			if (obj.style.visibility=="visible") {
				hideZone(idLabel);
				document.getElementById(idSpan).innerHTML = 'Mostrar etiquetas';
			} else {
				document.getElementById(idSpan).innerHTML = 'Ocultar etiquetas';
				showZone(idLabel);
			}
		}*/

