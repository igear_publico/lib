var findXCoordUsr = "";
var findYCoordUsr = "";

function doXYQuery(_theX, _theY, _theSRS) {
		// convertir del srs elegido a 25830
	if (_theSRS != "EPSG:25830") {
		if (_theSRS == "EPSG:4326") {
				// Todo va por el servicio WCTS salvo el 4326 porque no lo ofrecen
			destino = coordTransformProj4s(_theSRS, "EPSG:25830", _theX, _theY);
			xWCTS = destino.ord[0];
			yWCTS = destino.ord[1];
		} else {
			pendingFuncWCTS = "performXYQuery();";
			coordTransformIGN(_theSRS, "EPSG:25830", _theX, _theY);
		}
	} else {
			// No hace falta convertir
		xWCTS = _theX;
		yWCTS = _theY;
		performXYQuery();
	}
}

function performXYQuery() {
	var theX = xWCTS;
	var theY = yWCTS;

	if ((theX <= limitLeft) || (theY <= limitBottom) || (theX >= limitRight) || (theY >= limitTop)) {
		alert("La coordenada está fuera del rango del visor");
	} else {
		findXCoordUsr = theX;
		findYCoordUsr = theY;

		if (getMapScale(false) > 25000) {
			doRecenterXYWithScale(theX, theY, 25000);
		} else {
			doRecenterXYWithScale(theX, theY, getMapScale(false));
		}
	}
}

function doRecenterXYWithScale(_theX, _theY, _scale) {
	minx = parseFloat(_theX);
	maxx = parseFloat(_theX);
	miny = parseFloat(_theY);
	maxy = parseFloat(_theY);
	doZoomScale(_scale);
	setNewScale(_scale);
	updateZoomBar(_scale);
}

function abrirAyudaMunicipios(provTxt) {
	switch(provTxt) {
		case "Huesca":		detailedQueryWin.loadcontent("municipios_catastrales_huesca.html");
											detailedQueryWin.settitle("Listado de municipios catastrales de Huesca");
											detailedQueryWin.show();
											break;
		case "Teruel":		detailedQueryWin.loadcontent("municipios_catastrales_teruel.html");
											detailedQueryWin.settitle("Listado de municipios catastrales de Teruel");
											detailedQueryWin.show();
											break;
		case "Zaragoza":	detailedQueryWin.loadcontent("municipios_catastrales_zaragoza.html");
											detailedQueryWin.settitle("Listado de municipios catastrales de Zaragoza");
											detailedQueryWin.show();
											break;
		default:					break;
	}
}

function ayudaMunicipios_sigpac() {
	combo_laProvincia_sigpac = document.getElementById('laProvincia_sigpac');
	var provTxt = combo_laProvincia_sigpac.options[combo_laProvincia_sigpac.selectedIndex].value;
	abrirAyudaMunicipios(provTxt);
}

function ayudaMunicipios_parceR() {
	combo_laProvincia_parceR = document.getElementById('laProvincia_parceR');
	var provTxt = combo_laProvincia_parceR.options[combo_laProvincia_parceR.selectedIndex].value;
	abrirAyudaMunicipios(provTxt);
}

function ayudaProvincia() {
	alert("El 22 es el código correspondiente a Huesca.\nEl 44 es el código correspondiente a Teruel.\nEl 50 es el código correspondiente a Zaragoza.");
}


function getQueryPortal(munVia, portalBajo, portalAlto) {
	theQuery = 'C_MUN_VIA=&apos;' + munVia + '&apos; AND NUMERO &gt;=' + portalBajo + ' AND NUMERO &lt;=' + portalAlto;
	return theQuery;
}

function getQueryCallejero(theValue, theLoc, exact) {
	theQuery ='( ';

	var operadorLogico;

	if (exact) {
		operadorLogico = "AND";
	} else {
		operadorLogico = "OR";
	}

	isFirst = true;
	words = theValue.split(' ');
	if (words.length > 0) {
		for (i=0; i<words.length; i++) {
			if (! isCommonWord(words[i])) { // quito la particula
				if (isFirst) {
					theQuery += '(FON_VIA LIKE carto.f_fonetica(&apos;%' + words[i]+ '%&apos;))';
					isFirst = false;
				} else {
					theQuery += operadorLogico + '(FON_VIA LIKE carto.f_fonetica(&apos;%' + words[i]+ '%&apos;))';
				}
			}
		}
	}

	theQuery += ') ';

	if (theLoc) {
		theQuery += 'AND (FON_MUN LIKE carto.f_fonetica(&apos;%' + theLoc + '%&apos;))';
	}
	return theQuery;
}

