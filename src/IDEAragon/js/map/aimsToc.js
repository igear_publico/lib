var tocVisible = true;
var tocContent="";

function displayToc(){
	tocContent = getLayersContent();
	for (var i=0;i<layerCount; i++){
		tocContent +=" <tr valign=\"middle\">\n";
		tocContent +=" <td></td>\n";
		tocContent +="   <td><img src=\"/lib/IDEAragon/themes/default/images/linelastnode.gif\" border=\"0\"></td>\n";
		tocContent +="     <td colspan=\"1\" NOWRAP>\n";
		tocContent +="		<input type=\"checkbox\" value=\"\" onclick=\"setVisibility(" + i + ");getMapWithCurrentExtent();\"";
		if(LayerVisible[i] == 1) tocContent += " CHECKED ";
		tocContent += "/>\n";
		tocContent +="          <input type=\"radio\" name=\"activelyr\" value=\"\" onclick=\"setActiveLayer(" + i + ")\"";
		if(i== ActiveLayerIndex) tocContent += " CHECKED ";
		tocContent += "/> ";
		tocContent += LayerName[i] + "</td></tr>\n";

	}
	tocContent += "</table>\n";
	//alert(tocContent);
	//updateContent("tocWin",tocContent);

/*	if (hasTocInitialOpened) {
		tocWin.show();
	}*/

	toc.divToc = document.getElementById("toc_control");
	//tocWin.setcontent(toc.writeHTML());
	$("#toc_control").append(toc.writeHTML());
	 $( "#transpTocSlider" ).slider({
			 min: 0,
			 max: 100,
			 step: 1,
			 slide: function( event, ui ) {
				// $(".tocLayer").css("opacity",(100-ui.value)/100);
				 mapOL.getLayerGroup().setOpacity((100-ui.value)/100);
			 },
			 change: function( event, ui ) {
				// $(".tocLayer").css("opacity",(100-ui.value)/100);
				 mapOL.getLayerGroup().setOpacity((100-ui.value)/100);
				 showTranspWarning(ui.value) ;
				 
				 
			 }
			 });
	refreshTOCAccordion();
	return false;
}


function getLayersContent(){

	var lyrsContent="";

	lyrsContent += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
	lyrsContent += "  <tr valign=\"middle\">\n";
	lyrsContent += "    <td><a href=\"#\" onclick=\"toggleVisibility();\" >";
		if(tocVisible) lyrsContent += "<img src=\"/lib/IDEAragon/themes/default/images/folderopened.gif\" border=\"0\"></a>\n";
		else lyrsContent += "<img src=\"/lib/IDEAragon/themes/default/images/folderclosed.gif\" border=\"0\"></a>\n";
	lyrsContent += "    </td>\n";
	lyrsContent += "    <td colspan=\"2\" NOWRAP>   Layers</td></tr>\n";
	return lyrsContent;
}
function toggleVisibility(){
	tocVisible = ! tocVisible;
	var lyrsContent = getLayersContent();
	if(!tocVisible) {
		//updateContent("tocWin", lyrsContent);
		tocWin.setcontent(lyrsContent);
	} else {
		displayToc();
	}
}

function setVisibility(lyrIndex){
	if(LayerVisible[lyrIndex] == 1) LayerVisible[lyrIndex] = 0;
	else LayerVisible[lyrIndex] = 1;
}

function setActiveLayer(lyrIndex){
	ActiveLayerIndex = lyrIndex;
	ActiveLayer = LayerID[lyrIndex];
}

function updateContent(name, content) {
	var theObj = document.getElementById(name);
	if (theObj!=null) {
		theObj.innerHTML = content;
	}
	return false;
}
