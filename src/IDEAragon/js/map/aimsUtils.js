// Funcion para redondear un numero decimal a d decimales
function RoundDecimal(num,d) {
	var m = Math.pow(10,d);
	var num2 = num * m;
	return (Math.round(num2) / m) ;
}

function degrees2radians(degrees) {
	return (degrees * Math.PI / 180);
}

function radians2degrees(radians) {
	return (radians*180/Math.PI);
}

function isEnterKey(evt) {
	if (!evt) {
		return (evt.keyCode == 13);
	} else if (!evt.keyCode) {
		return (evt.which == 13);
	}
	return (evt.keyCode == 13);
}

// get the coords at mouse position
function getMouse(e) {
	/*
	if (visorDisabled()) {
		return false;
	}

	//if ((hasOVMap) && (ovIsVisible) && (ovMapIsLayer) && !isOverviewTitleZone(mouseX,mouseY)) {
	if (!isSpecialMapZone(mouseX,mouseY)) {
		if ((mouseX>iWidth) || (mouseY>iHeight) || (mouseX<=0) ||(mouseY<=0) || ((hasOVMap) && (ovIsVisible) && (ovMapIsLayer) && OVMapContains(mouseX,mouseY))) {
			chkMouseUp(e);
		} else {
			mouseStuff();
		}
	}
*/
	getXY2(e);
	mouseStuff();
	return false;
}

function mouseStuff() {
				/*
	if ((zooming) || (selectBox)) {
		x2=mouseX;
		y2=mouseY;
		setClip();
	} else if (panning) {
		x2=mouseX;
		y2=mouseY;
		panMouse();
	}
	pixelX = xDistance / iWidth;
	mapX = pixelX * mouseX + eLeft;
	var theY = iHeight - mouseY;
	pixelY = yDistance / iHeight;
	mapY = pixelY * theY + eBottom;

	if ((toolMode==20) || (toolMode==2006)) {
		calcDistance(mapX,mapY);
	}
	//else if (showXYs) {
	if (showXYs) {
		var u = Math.pow(10,numDecimals);
		var uX = Math.round(mapX * u) / u;
		var uY= Math.round(mapY * u) / u;
		var mouseString = msgList[52] + uX + " , " + uY + " -- " + msgList[53] + mouseX + " , " + mouseY;
		if (showScalePercent) mouseString = mouseString;
		//if (showScalePercent) mouseString = mouseString + " -- " + msgList[54] + mapScaleFactor;
		//window.status = mouseString;

		// incluido coordsZone (dportoles)
		setNewCoords(uX, uY);

	}
					*/

	numDecimals = 2;
	var coordActual = getMapXY(theX, theY);
	coordActualX = coordActual[0];
	coordActualY = coordActual[1];

	setNewCoords(coordActualX, coordActualY);

	if (isDistanceMeasuring) {
		movement_distanceMeasure(coordActualX, coordActualY);
	}
	
}


