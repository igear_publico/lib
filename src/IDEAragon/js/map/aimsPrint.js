var theHiddenURL = "";
var minxPrint = "";
var minyPrint = "";
var maxxPrint = "";
var maxyPrint = "";
var minxPrintAragon = "";
var minyPrintAragon = "";
var maxxPrintAragon = "";
var maxyPrintAragon = "";

var theTitlePrint = "";

var tt;

function coords2px(printWidth, printHeight,minx, miny, maxx, maxy,xIn, yIn) {
	pixelX = (maxx - minx) / printWidth;
	pixelY = (maxy - miny) / printHeight;

	return Math.round((xIn-minx)/pixelX)+","+Math.round((maxy-yIn)/pixelY);
}

function obtenGeneralAragon() {
	if (theHiddenURLGeneralAragon == "") {
		var layerVisibilityList = getLayerVisibilityListAragon(true);
		minxPrintAragon = xminOverview;
		maxxPrintAragon = xmaxOverview;
		minyPrintAragon = yminOverview;
		maxyPrintAragon = ymaxOverview;

		getMapHidden(minxPrintAragon, minyPrintAragon, maxxPrintAragon, maxyPrintAragon, printWidthAragon, printHeightAragon, null, layerVisibilityList, "Aragon", true, null, null, null);

		checkMapInformeGeneralAragon();
	} else {
		eval(pendingFunction);
	}
}

function getLayerVisibilityListAragon(esInformeGeneral, i, j, k) {				
	var layerVisibilityList = new Array();
	for (var ii=0;ii<layerCount;ii++) {
		if (LayerName[ii] == "Aragon") {
			layerVisibilityList[ii] = true;
		} else if (LayerName[ii] == "Limites Autonomico de Aragon") {
			layerVisibilityList[ii] = true;
		} else {
			layerVisibilityList[ii] = false;
		}
	}
	return layerVisibilityList;
}

function doPrint() {
	theHiddenURL = "";
		// generar nuevo mapa con dimensiones, dpi y escala apropiada
		// lo pedimos y nos quedamos esperando a que llegue
	theTitlePrint = document.getElementById('titleForPrint').value;

	var cboDPI = document.getElementById('cboPrintDPI');
	dpiSelected = cboDPI.options[cboDPI.selectedIndex].value;

	if (dpiSelected == "96") {
		printMapWidth = printWidth;
		printMapHeight = printHeight;
	} else {
		printMapWidth = Math.round((printWidth*300)/96);
		printMapHeight = Math.round((printHeight*300)/96);
	}
	var newBboxPrint = checkDeformation(minx, miny, maxx, maxy, printWidth, printHeight);
	minxPrint = newBboxPrint.minx;
	maxxPrint = newBboxPrint.maxx;
	minyPrint = newBboxPrint.miny;
	maxyPrint = newBboxPrint.maxy;
	//	getMapHidden(minxPrint, minyPrint, maxxPrint, maxyPrint, printWidth, printHeight, dpiSelected);

//	getMapHidden(minxPrint, minyPrint, maxxPrint, maxyPrint, printWidth, printHeight, 96, null, "");
	//printOptionsWin.hide();
	$( "#printWin" ).dialog( "close" );
	//checkMapPrint();
	generatePrintPage();

	setActiveTool("zoomIn");
}


function checkMapPrint() {
	if (theHiddenURL == "") {
		tt = setTimeout("checkMapPrint()",100);
	} else {
		if (tt) {
			clearTimeout(tt);
		}
		obtenGeneralAragon();
	}
}

function checkMapInformeGeneralAragon() {
	if (theHiddenURLGeneralAragon == "") {
		tt = setTimeout("checkMapInformeGeneralAragon()", 100);
	} else {
		if (tt) {
			clearTimeout(tt);
		}
		eval(pendingFunction);
	}
}

function getFechaActual() {
	var fecha=new Date();

	return fecha.getDate() + "/" +	(fecha.getMonth() +1) + "/" + fecha.getFullYear();
}

function getPrintMarkers(printWidth, printHeight,minxPrint,minyPrint,maxxPrint, maxyPrint){

	var fts = markers.getSource().getFeatures();
	var urls="";
		for (var i=0;i<fts.length;i++){
			if (fts[i].get('layer') && (fts[i].get('layer').length>0)){
				urls+='<mapa><laUrl>'+urlAbsoluta(idearagonWMS + '&WIDTH=' + printWidth + '&HEIGHT=' + printHeight+ '&BBOX=' + minxPrint + ',' + minyPrint + ',' + maxxPrint + ',' + maxyPrint+
						"&STYLES=busqueda&Layers="+fts[i].get('layer')+
				'&CQL_FILTER='+fts[i].get('cql_filter'))+'</laUrl></mapa>';
				
			}
			else{// punto
				try{
				var coords = fts[i].getGeometry().getCoordinates();
				
				var pixels=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,coords[0],coords[1]);
				urls += '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?icon='+urlAbsoluta(fts[i].getStyle().getImage().getSrc())+'&coords='+pixels+'&width='+printWidth+'&height='+printHeight+ '</laUrl></mapa>';
				}
				catch(err){// por si no fuera un punto con icono
					console.log(err);
				}

			}
		
		}
	return urls;
	
}
function getPrintPoints(recordset,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
	var numData = recordset.getVectorialDataCount();
	var request="";
	if (numData > 0) {
		
		for (var jj = 0; jj < numData; jj++) {
			aux = recordset.getGeometry(jj);
			
			var pixels=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,aux.getX(),aux.getY());
			request += '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?label='+encodeURIComponent(recordset.attributesList[jj].text)+'&color='+jg.color.replace("#","")+'&coords='+pixels+'&width='+printWidth+'&height='+printHeight+'&drawWidth=5'+ '</laUrl></mapa>';
		
		}

		
	}	
	return request;
}

/** se utiliza en visor para imprimir capas geoBOA **/
function getPrintSpecialLayers(printWidth, printHeight,minxPrint, minyPrint, maxxPrint , maxyPrint){
	
}
function generatePrintPage() {
	var idContent = '<imprimir>';
	idContent += '<fecha>' + getFechaActual() + '</fecha>';
	idContent += '<titulo>' + theTitlePrint + '</titulo>';
	//idContent += '<img id="theNorthArrowPrint" src="images/brujula.gif" style="position:absolute;top:' + (printHeight+25) + 'px;left:' + (printWidth-50) + 'px;z-index:500;"/>';

		// habra que reordenarla segun el apilamiento zindex
	var listaAPedir = new Array();
	var pedirCount = 0;
	listaAPedir[pedirCount] = new Array();
/*	listaAPedir[pedirCount]["URL"] = theHiddenURL;
	listaAPedir[pedirCount]["ZIDX"] = 10000;
	pedirCount++;*/

	var printHQWidth = Math.round((printWidth*300)/96);
	var printHQHeight = Math.round((printHeight*300)/96);
	for (var ii = 0; ii <wmsList["count"]; ii++) {
		if (wmsList[ii]["visib"]) {
			var wmsObj = wmsList[ii];
			var urlWMS = wmsObj["url"];
			if (wmsObj["supportHQ"]) {
				urlWMS += '&WIDTH=' + printHQWidth + '&HEIGHT=' + printHQHeight;
			} else {
				urlWMS += '&WIDTH=' + printWidth + '&HEIGHT=' + printHeight;
			}
			urlWMS += '&BBOX=' + minxPrint + ',' + minyPrint + ',' + maxxPrint + ',' + maxyPrint;

				// wmsList[ii]["zIdx"] 

			listaAPedir[pedirCount] = new Array();
			listaAPedir[pedirCount]["URL"] = urlWMS;
			listaAPedir[pedirCount]["ZIDX"] = wmsList[ii]["zIdx"];
			pedirCount++;
		}
	}

		// ordeno segun el metodo de la burbuja, como son pocos, no afecta el rendimiento
	for (var idx = (pedirCount-1); idx>=0; idx--) {
		var valorMaxHastaAhora = listaAPedir[idx]["ZIDX"];
		var posMaxHastaAhora = idx;

			//busco el maximo de todos los que quedan
		for (var j = 0; j < idx; j++) {
			if (listaAPedir[j]["ZIDX"] > valorMaxHastaAhora) {
				posMaxHastaAhora = j;
				valorMaxHastaAhora = listaAPedir[j]["ZIDX"];
			}
		}
			//pongo al final el maximo de los que quedan
		var aux = listaAPedir[idx];
		listaAPedir[idx] = listaAPedir[posMaxHastaAhora];
		listaAPedir[posMaxHastaAhora] = aux;
	}

	for (var j = 0; j < pedirCount; j++) {
		idContent += '<mapa><laUrl>' + urlAbsoluta(listaAPedir[j]["URL"]) + '</laUrl></mapa>';
	}
	idContent += getPrintSpecialLayers(printWidth, printHeight,minxPrint, minyPrint, maxxPrint , maxyPrint);
	if (mapOL){
	idContent += getPrintMarkers(printWidth, printHeight,minxPrint, minyPrint, maxxPrint , maxyPrint);
	}


	if (isHistorico) {
		idContent += getPrintPoints(historicoRecordset,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint)
	}
		if (isDistanceMeasuring||isAreaMeasuring) {
			var coordsX;
			var coordsY;
			if (measurePointsX.length > 0){
				coordsX=measurePointsX;
				coordsY=measurePointsY;
			}
			else{
				coordsX=clickPointX;
				coordsY=clickPointY;
			}
			
			if (coordsX.length > 0) {
				var pixels="";
				for (var jj = 0; jj < coordsX.length; jj++) {
				
					if (jj>0){
						pixels+=",";
					}
					 pixels+=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,coordsX[jj],coordsY[jj]);
				
				}
				if (isAreaMeasuring && (jj>1)){
					pixels+=","+coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,coordsX[0],coordsY[0]);
				}
				idContent += '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?color='+jg.color.replace("#","")+'&coords='+pixels+'&width='+printWidth+'&height='+printHeight+'&drawWidth=2'+ '</laUrl></mapa>';

			}
		}
	

	if (isGeoreferencing) {
		idContent += getPrintPoints(pointRecordset,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint)
	}
	idContent += '<mapa><laUrl>' +getPrintScale(printWidth, printHeight,minx,maxx)+ '</laUrl></mapa>';
	var topLeftPx = coord2px_overview(minxPrint, maxyPrint);
	var bottomRightPx = coord2px_overview(maxxPrint, minyPrint);

	idContent += '<situacionAragon><laUrl>'+urlAbsoluta(server)+'/lib/IDEAragon/themes/default/images/mapaOverview.gif</laUrl></situacionAragon><situacionAragon><laUrl>'+urlAbsoluta(printOverviewURL)+'?width='+printWidthOverview+'&height='+printHeightOverview+'&left='+topLeftPx["x"]+'&bottom='+bottomRightPx["y"]+'&right='+bottomRightPx["x"]+'&top='+topLeftPx["y"]+'</laUrl></situacionAragon>';

 $("#legend_panel img").each( function() {
	 var label="";
	 var legenderror=false;
	 if (this.className){
		 if (this.className.indexOf("legendError")<0){
		 if ($("p."+this.className).length>0){
			 label = "<label>"+$("p."+this.className)[0].innerHTML+"</label>";
		 }
		 }
		 else{
			 legenderror=true;
		 }
		 
	 }
	 if (!legenderror){
		idContent +='<leyenda><laUrl>' + cambiaIDEAragonPorSitar(this.src) + '</laUrl>'+label+'</leyenda>';
	 }
	});
	
	idContent +='</imprimir>';
	idContent = makeXMLsafeAcentos(idContent);

	ocultaLayerLoading();

	if (debugOn) {
		alert(idContent);
	}
	console.log(idContent);
	
	document.printForm.xml.value = idContent;
	document.printForm.xslt.value = printActionXSL;
	document.printForm.action = printActionURL;
	document.printForm.submit();
}

function loadedWMSPrint(ii) {
}

function cambiaIDEAragonPorSitar(theUrl) {
	return theUrl.replace("http:/"+"/idearagon.aragon.es", "http:/"+"/sitar.aragon.es");
}

