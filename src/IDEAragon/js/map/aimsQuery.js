var resultCount = -1;
var initialRecord = -1;
var nomCalleToFind = "";
var numPortalToFind = -1;
var cviaPortalToFind = -1;
var locCallejeroToFind = "";
var txtToQuery = "";
var txtToQueryNoExact = "";
var featureCountLimit = 30;

var queryUrl = "";

var bestMinx, bestMiny, bestMaxx, bestMaxy;
var dataCandidatos = new Array();

function initialQuery(findQuery, activeLyr, newQueryURL) {
	ActiveLayer = activeLyr;

	var axl = getIdRequestQuery(findQuery, false, 0, "");

	if (newQueryURL) {
		queryUrl = newQueryURL;
	} else {
		queryUrl = defaultQueryUrl;
	}

	if (debugOn) {
		alert("consultando a la URL: " + queryUrl);
	}
	http = getHTTPObject();
	if ((http != null)) {
		http.open("POST", queryUrl, true);
		http.onreadystatechange = parseInitialResultQuery;

		isWorking = true;
		muestraLayerLoading();
		if (debugOn) {
			alert("enviando la consulta: \n" + axl);
		}

		http.send(axl);
	}
}

function query(txt, txtNoExact, activeLyr, isExact, newQueryURL) {
	resultCount = -1;
	initialRecord = 1;
	nomCalleToFind = "";
	numPortalToFind = -1;
	cviaPortalToFind = -1;
	locCallejeroToFind = "";
	txtToQuery = txt;
	txtToQueryNoExact = txtNoExact;

	if (newQueryURL) {
		queryUrl = newQueryURL;
	} else {
		queryUrl = defaultQueryUrl;
	}

	ActiveLayer = activeLyr;

	if (isExact) {
		sendIdRequestQuery(txt, true, initialRecord, false, isExact, false, "");
	} else {
		sendIdRequestQuery(txtNoExact, true, initialRecord, false, isExact, false, "");
	}
}

function queryToponimo(theValue, theMuni) {
	/*document.getElementById("texto").value=theValue+(theMuni !="" ? ", "+theMuni :"");
	buscar();*/
	var url = '/SimpleSearchService/typedSearchService?texto='+theValue+(theMuni!=null && theMuni!="" ? "&muni="+theMuni:"")+"&type=toponimo&app="+APP_NAME;
	$.ajax({
		url: url,
		type: 'GET',
		success:  function(datos) {
			var json = $.xmlToJSON(datos);
		
			//alert(json.Body[0].searchResult[0].type[0].Text);
			var pos=0;
			if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){

				alert("No se pudo localizar la calle indicada");
			}
			for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
				var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
				var resultados ="";
				if ((busquedaLine.Count[0].Text!="0")&&(busquedaLine.Count[0].Text!="-1")){
					num_results = num_results+parseInt(busquedaLine.Count[0].Text);
					if(typeof busquedaLine.List != 'undefined'){
						resultados = busquedaLine.List[0].Text;


					
						var lineas=resultados.split("\n");
						for (var i=0;i<lineas.length;i++){
							valores = lineas[i].split("#");
							
							var coords = valores[2].split(":");
							if (coords.length ==4){
								bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[2])+1000)+":"+(parseInt(coords[3])+1000);
							}
							else if (coords.length==2){
								bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[0])+1000)+":"+(parseInt(coords[1])+1000)+
								"&MARKERX="+coords[0]+"&MARKERY="+coords[1];
							}
							else{
								bbox="";
							}


							if (bbox!=""){
								params="BOX="+bbox;
								showSearchResultOL(params);
								
							}
							else{
								alert("No se puede ubicar el topónimo");
							}
							
						}
					}
				}
			}
 
			},
		error:  function() { alert("error "+url);},
		complete: function(xmlHttpRequest) { 
			showSearching(false);},
			async:false,
		processData: false,
		contentType: "text/xml; charset=\"utf-8\"",
	
			// timeout: timeoutvalue-200
	});
}

function queryCallejero(calle, loc, numportal, isExact) {
/*	document.getElementById("texto").value=calle+(numportal !="" ? " "+numportal:"")+", "+loc;
	buscar();*/
	var type = "BusCallUrb";
	var texto = calle;
	if (numportal !=""){
		type="TroidesV";
		texto = texto+" "+numportal;
	}
	var url = '/SimpleSearchService/typedSearchService?texto='+texto+(loc!=null && loc!="" ? "&muni="+loc:"")+"&type="+type+"&app="+APP_NAME;
	$.ajax({
		url: url,
		type: 'GET',
		success:  function(datos) {
			var json = $.xmlToJSON(datos);
		
			//alert(json.Body[0].searchResult[0].type[0].Text);
			var pos=0;
			if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){

				alert("No se pudo localizar la calle indicada");
			}
			for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
				var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
				var resultados ="";
				if ((busquedaLine.Count[0].Text!="0")&&(busquedaLine.Count[0].Text!="-1")){
					num_results = num_results+parseInt(busquedaLine.Count[0].Text);
					if(typeof busquedaLine.List != 'undefined'){
						resultados = busquedaLine.List[0].Text;


					
						var lineas=resultados.split("\n");
						for (var i=0;i<lineas.length;i++){
							valores = lineas[i].split("#");
							

							if (type =="TroidesV"){
								showPortalResultOL(valores[3],numportal);
							}
							else{
							params="ACTIVELAYER=BusCallUrb&QUERY=objectid="+valores[3];
							showSearchResultOL(params);
							}
							
						}
					}
				}
			}
 
			},
		error:  function() { alert("error "+url);},
		complete: function(xmlHttpRequest) { 
			showSearching(false);},
			async:false,
		processData: false,
		contentType: "text/xml; charset=\"utf-8\"",
	
			// timeout: timeoutvalue-200
	});
}

function locatePortal(numVia, numportal, isExact) {
	muestraLayerLoading();
	cviaPortalToFind = numVia;
	numPortalToFind = numportal;
	if (isExact) {
		txtToQuery = getQueryPortal(numVia, numportal, numportal);
	} else {
		txtToQuery = getQueryPortal(numVia, numportal-5, (numportal-0)+5);
	}

	ActiveLayer = "TroidesV";
	sendIdRequestQuery(txtToQuery, true, initialRecord, true, isExact, true, "NUMERO");
}

function sendIdRequestQuery(findQuery, countOnly, beginRecord, isCallejero, isExact, isPortal, fieldOrderBy) {
	var axl = getIdRequestQuery(findQuery, countOnly, beginRecord, fieldOrderBy);
	if (debugOn) {
		alert("consultando a la URL: " + queryUrl);
	}
	http = getHTTPObject();
	if ((http != null)) {
		http.open("POST", queryUrl, true);
		if (isCallejero) {
			if (isPortal) {
				if (countOnly) {
					if (isExact) {
						http.onreadystatechange = parseResultQueryForCountPortalExacto;
					} else {
						http.onreadystatechange = parseResultQueryForCountPortalNoExacto;
					}
				} else {
					http.onreadystatechange = parseResultQueryPortal;
				}
			} else {
				if (countOnly) {
					if (isExact) {
						http.onreadystatechange = parseResultQueryForCountCallejeroExacto;
					} else {
						http.onreadystatechange = parseResultQueryForCountCallejeroNoExacto;
					}
				} else {
					http.onreadystatechange = parseResultQueryCallejero;
				}
			}
		} else {
			if (countOnly) {
				if (isExact) {
					http.onreadystatechange = parseResultQueryForCountExacto;
				} else {
					http.onreadystatechange = parseResultQueryForCountNoExacto;
				}
			} else {
				http.onreadystatechange = parseResultQuery;
			}
		}
		isWorking = true;
		muestraLayerLoading();
		if (debugOn) {
			alert("enviando la consulta: \n" + axl);
		}
		//axl2 = unescape(axl);
		//axlSafe = makeXMLsafe(axl2);
		http.send(axl);
	}
}

function getIdRequestQuery(findQuery, countOnly, beginRecord, fieldOrderBy) {
	//var axl = '<?xml version="1.0" encoding="Cp1252"?>';
	//var axl = '<?xml version="1.0" encoding="ISO-8859-1"?>';
	var axl = '<?xml version="1.0" encoding="UTF-8"?>';

	//var axl='';

	axl += '<ARCXML version="1.1">\n<REQUEST>\n<GET_FEATURES geometry="false" outputmode="xml" checkesc ="true" ';
	if (countOnly) {
		axl += 'envelope="false" skipfeatures="true">\n';
	} else {
		axl += 'envelope="true" skipfeatures="false" beginrecord="' + beginRecord + '" featurelimit="' + featureCountLimit + '">\n';
	}
	axl += '<LAYER id="' + ActiveLayer + '" />';
	if (fieldOrderBy) {
		axl += '<SPATIALQUERY subfields="' + getFields(ActiveLayer) + '" where="' + findQuery + '" order_by="ORDER BY ' + fieldOrderBy + '" />';
	} else {
		axl += '<SPATIALQUERY subfields="' + getFields(ActiveLayer) + '" where="' + findQuery + '" />';
	}
	axl += '</GET_FEATURES>';
	axl += '</REQUEST>';
	axl += '</ARCXML>';
	return axl;
}

function parseResultQueryForCountExacto() {
	parseResultQueryForCount(true);
}

function parseResultQueryForCountNoExacto() {
	parseResultQueryForCount(false);
}

function parseResultQueryForCount(esExacto) {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;

			resultCount = getCountQuery(result);
			if (resultCount != 0) {
				if (esExacto) {
					sendIdRequestQuery(txtToQuery, false, initialRecord, false, false, false, "");
				} else {
					sendIdRequestQuery(txtToQueryNoExact, false, initialRecord, false, false, false, "");
				}
			} else {
				if (esExacto) {
					if (debugOn) {
						alert("busco por palabras independientes");
					}
					query(txtToQuery, txtToQueryNoExact, ActiveLayer, false);
				} else {
					alert("No se encontraron coincidencias");
				}
			}
		} else {
			alert("Error retreiving data");
		}
	}
}

function parseResultQueryForCountCallejeroExacto() {
	parseResultQueryForCountCallejero(true);
}
function parseResultQueryForCountCallejeroNoExacto() {
	parseResultQueryForCountCallejero(false);
}

function parseResultQueryForCountCallejero(esExacto) {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			resultCount = getCountQuery(result);
			if (debugOn) {
				alert("num result " + resultCount);
			}
			if (resultCount == 0) {
				if (! esExacto) {
					if (debugOn) {
						alert("busco por palabras independientes");
					}
					queryCallejero(nomCalleToFind, locCallejeroToFind, numPortalToFind, false);
				} else {
					alert("No se localiza el nombre de vía");
				}
			} else {
				if (debugOn) {
					alert("hay al menos 1 calle, busco para hacer join");
				}
				txtToQuery = getQueryCallejero(nomCalleToFind, locCallejeroToFind, esExacto);

				ActiveLayer = "BusCallUrb";
				sendIdRequestQuery(txtToQuery, false, initialRecord, true, false, false, "");
			}
		} else {
			alert("Error retreiving data");
		}
	}
}


function parseResultQueryForCountPortalExacto() {
	parseResultQueryForCountPortal(true);
}
function parseResultQueryForCountPortalNoExacto() {
	parseResultQueryForCountPortal(false);
}

function parseResultQueryForCountPortal(esExacto) {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			resultCount = getCountQuery(result);
			if (debugOn) {
				alert("num result " + resultCount);
			}
			if (resultCount == 0) {
				if (esExacto) {
					if (debugOn) {
						alert("busco por los portales cercanos de esa calle");
					}
					locatePortal(cviaPortalToFind, numPortalToFind, false);
				} else {
					alert("No se localiza ningún número de portal cercano al solicitado. Se muestra la calle completa.");
					if (((bestMaxx - bestMinx) > 0.5) || ((bestMaxy - bestMiny) > 0.5)) {
							// es un poligono o linea
						getMap(bestMinx, bestMiny, bestMaxx, bestMaxy);
					} else {
							// o es un punto, o una geometria muy pequeña (menos de medio metro)
						doRecenterXYWithScale(bestMinx, bestMiny, 1500);
					}
				}
			} else {
				if (debugOn) {
					alert("hay al menos 1 portal, busco para hacer join");
				}
				txtToQuery = "";
				if (esExacto) {
					txtToQuery = getQueryPortal(cviaPortalToFind , numPortalToFind, numPortalToFind);
				} else {
					txtToQuery = getQueryPortal(cviaPortalToFind , numPortalToFind-5, (numPortalToFind-0)+5);
				}

				ActiveLayer = "TroidesV";
				sendIdRequestQuery(txtToQuery, false, initialRecord, true, true, true, "");
			}
		} else {
			alert("Error retreiving data");
		}
	}
}

function parseResultQuery() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			if (debugOn) {
				alert("RESULTADO:" + result);
			}
			processResultQuery(result, false, false);
		} else {
			alert("Error retreiving data");
		}
	}
}


function parseResultQueryCallejero() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			if (debugOn) {
				alert("RESULTADO:" + result);
			}
			processResultQuery(result, true, false);
		} else {
			alert("Error retreiving data");
		}
	}
}

function parseResultQueryPortal() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			if (debugOn) {
				alert("RESULTADO:" + result);
			}
			processResultQuery(result, true, true);
		} else {
			alert("Error retreiving data");
		}
	}
}

function parseInitialResultQuery() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			var result = http.responseText;
			if (debugOn) {
				alert("RESULTADO:" + result);
			}
			resultCount = getCountQuery(result);

			processResultQuery(result, false, false, (bboxString != ""));

				// quedaba pendiente pedir el mapa con el nuevo extent
//			getServiceInfo();
		} else {
			alert("Error retreiving data");
		}
	}
}
function getCountQuery(theReply) {
	var theCount;
	var startpos = 0;
	var endpos = 0;
	var pos = theReply.indexOf("FEATURECOUNT");
	var pos1 = theReply.indexOf("count", pos + 12);
	if (pos1 != -1) {
		pos1 += 7;
		var pos2 = theReply.indexOf("\"", pos1);
		theCount = parseFloat(theReply.substring(pos1, pos2));
	} else {
		theCount=0;
	}
	//alert(theCount);
	//ocultaLayerLoading();
	return theCount;
}

var idSelectionLayer = "";
var selectionWhere = "";
var selectionType = "";

function processResultQuery(theReply, isCallejero, isPortal, noRecenter) {
	//TODO : Use XML DOM parser instead to parse the response

	//var selectedData = "";
	var endpos = 1;
	reply = theReply;

	var notHideLayer = false;

		// mirar si hay 0, 1, o >1 resultados para actuar
	var posIni = reply.indexOf("<ENVELOPE ", endpos);
	var posFin = reply.indexOf("<ENVELOPE ", posIni+1);
	if (posIni == -1) {
			// no hay resultados
		alert("No se encontraron coincidencias");
	} else {
		if (resultCount == 1) {
				// hay uno, ir directamente
			if (isCallejero && ! isPortal && numPortalToFind != "") {

				if (debugOn) {
					alert("hay 1 calle candidata");
				}
				var posFie = reply.indexOf("<FIELDS ", endpos);
				startposFie = posFie + 8;
				endposFie = reply.indexOf('" />', startposFie);
				selectedDataFie = reply.substring(startposFie, endposFie);
				cviaIndex = -1;
				if (selectedDataFie != "") {
					var fValue1 = getFieldValuesQuery(selectedDataFie);
					var fName1 = getFieldNamesQuery(selectedDataFie);

					for (var ii=0;ii<fName1.length;ii++) {
						tempString = fName1[ii];
						if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
							nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
						} else {
							nombreCampo = tempString;
						}
						if (nombreCampo == "C_MUN_VIA") {
							cviaIndex = ii;
						}
					}
				}

				if (debugOn) {
					alert("munvia" + fValue1[cviaIndex]);
				}

				posEnv = reply.indexOf("<ENVELOPE ", endpos);
				startposEnv = posEnv + 10;
				endposEnv = reply.indexOf('"/>', startposEnv);
				envelopeData = reply.substring(startposEnv, endposEnv);
				bbox = new Array();
				if (envelopeData != "") {
					var fValueEnv = getFieldValuesQuery(envelopeData);
					var fNameEnv = getFieldNamesQuery(envelopeData);
					parseEnvelope(fNameEnv, fValueEnv, bbox);
				}
				//document.location = "visor.htm?BBOX=" + bbox["minx"] + ":" + bbox["miny"] + ":" + bbox["maxx"] + ":" + bbox["maxy"];

				idSelectionLayer = ActiveLayer;
				posId = reply.indexOf(nombreCampoId + '="', endpos);
				startposId = posId + nombreCampoId.length+2;
				endposId = reply.indexOf('"', startposId);
				theId = reply.substring(startposId, endposId);
				selectionWhere = nombreCampoId + '=' + theId;
				selectionType = LayerTypeById[ActiveLayer.toString().toUpperCase()];

				bestMinx = parseFloat(bbox["minx"]);
				bestMiny = parseFloat(bbox["miny"]);
				bestMaxx = parseFloat(bbox["maxx"]);
				bestMaxy = parseFloat(bbox["maxy"]);

				notHideLayer = true;

				locatePortal(fValue1[cviaIndex], numPortalToFind, true);
			} else {
				var posFie = reply.indexOf("<FIELDS ", endpos);
				startposFie = posFie + 8;
				endposFie = reply.indexOf('" />', startposFie);
				selectedDataFie = reply.substring(startposFie, endposFie);

				var scaleIdx = -1;

				if (selectedDataFie != "") {
					var fValue1 = getFieldValuesQuery(selectedDataFie);
					var fName1 = getFieldNamesQuery(selectedDataFie);

					for (var ii=0;ii<fName1.length;ii++) {
						tempString = fName1[ii];
						if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
							nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
						} else {
							nombreCampo = tempString;
						}
						if (nombreCampo == "ESCALA") {
							scaleIdx = ii;
						}
					}
				}

				idSelectionLayer = ActiveLayer;
				posId = reply.indexOf(nombreCampoId + '="', endpos);
				startposId = posId + nombreCampoId.length+2;
				endposId = reply.indexOf('"', startposId);
				theId = reply.substring(startposId, endposId);
				selectionWhere = nombreCampoId + '=' + theId;
				selectionType = LayerTypeById[ActiveLayer.toString().toUpperCase()];

				if (noRecenter) {
					getMap(customMinx, customMiny, customMaxx, customMaxy);
				} else {
					posEnv = reply.indexOf("<ENVELOPE ", endpos);
					startposEnv = posEnv + 10;
					endposEnv = reply.indexOf('"/>', startposEnv);
					envelopeData = reply.substring(startposEnv, endposEnv);
					bbox = new Array();
					if (envelopeData != "") {
						var fValueEnv = getFieldValuesQuery(envelopeData);
						var fNameEnv = getFieldNamesQuery(envelopeData);
						parseEnvelope(fNameEnv, fValueEnv, bbox);
					}
					//document.location = "visor.htm?BBOX=" + bbox["minx"] + ":" + bbox["miny"] + ":" + bbox["maxx"] + ":" + bbox["maxy"];

					minx = parseFloat(bbox["minx"]);
					miny = parseFloat(bbox["miny"]);
					maxx = parseFloat(bbox["maxx"]);
					maxy = parseFloat(bbox["maxy"]);

					if (maxy <= 200000) {
						updateSeleccionCandidatos(reply, isCallejero, isPortal);
					} else {
						if (((maxx - minx) > 0.5) || ((maxy - miny) > 0.5)) {
								// es un poligono o linea
							getMap(minx, miny, maxx, maxy);
						} else {
								// o es un punto, o una geometria muy pequeña (menos de medio metro)
							if (isPortal) {
								doRecenterXYWithScale(minx, miny, 1000);
							} else {
								var scaleLevel = 2500;
								if (scaleIdx != -1) {
									if (! isNaN(fValue1[scaleIdx])) {
										if (fValue1[scaleIdx] > 0) {
											scaleLevel = fValue1[scaleIdx];
										}
									}
								}
								doRecenterXYWithScale(minx, miny, scaleLevel);
							}
						}
					}
				}
			}
		} else {
				// hay varios, permitir al usuario elegir
			updateSeleccionCandidatos(reply, isCallejero, isPortal);
		}
	}

	if (! notHideLayer) {
		ocultaLayerLoading();
	}
}

function filtraResultados(resultList) {
	var resultListFiltrado = new Array();
	var idx = 0;
	var esValido = false;
	for (var ii = 0; ii < resultList.length; ii++) {
		var obj = resultList[ii]['address_components'];
		for (var jj = 0; jj < obj.length; jj++) {
			if (obj[jj]["long_name"] == "Aragón") {
				if (obj[jj]["types"][0] == "administrative_area_level_1") {
					esValido = true;
				}
			}
			if (esValido) {
				resultListFiltrado[idx] = resultList[ii];
				idx++;
				esValido = false;
			}
		}
	}
	return resultListFiltrado;
}

function updateSeleccionCandidatos(reply, isCallejero, isPortal) {
	idContent = getHTMLUserSelection(reply, isCallejero, isPortal);

	$("#tabs").tabs( "enable", 1);
	$("#tabs").tabs("option", "active", 1);
	document.getElementById("info").innerHTML = idContent;
	
	$("#searchTab").append("<div id=\"resultados_tabla\"></div>");
	$("#searchTab").append("<div id=\"resultados_pag\"></div>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pag1\" disabled onclick=\"showCandidatos(1)\"></button>");
	$("#resultados_pag").append("<button class=\"pag_button\"id=\"pagPrev\" disabled onclick=\"showCandidatos_pre()\"></button>");
	$("#resultados_pag").append("<span id=\"pags\">P&aacute;gina 0 de 0</span>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pagSig\" disabled onclick=\"showCandidatos_sig()\"></button>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pagUlt\" disabled onclick=\"showCandidatos_ult()\"></button>");
	initButtonsBuscador();
	var columns=new Array();
	var columns_names= new Array();
	dataCandidatos=new Array();
	$("#userSelectionTable tr").each(function( index ) {
		  if (index==0){
			 var columnas =this.childNodes;
			 columns.push({id: "id", name: "", field: "id",minWidth: VER_COLUMN_WIDTH,width:VER_COLUMN_WIDTH,maxWidth: VER_COLUMN_WIDTH, sortable:false});
			 for (var i=0; i<columnas.length; i++){
				 var header = columnas[i].childNodes[0].innerHTML;
				 columns_names.push(header.toLowerCase());
				 columns.push({id: header.toLowerCase(), name: header, field: header.toLowerCase(),sortable:false});
			 }
			 columns.push({id: "ver", name: "Ver", field: "ver",minWidth: VER_COLUMN_WIDTH,width:VER_COLUMN_WIDTH,maxWidth: VER_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
		  }
		  else{
			  var row=new Array();
			 
			  var columnas =this.childNodes;
			  row["id"]=index;
			  for (var i=0; i<columnas.length; i++){
				  var cell = columnas[i].childNodes[0];
				  if (cell.className == 'itemEnlace'){
					  row["ver"]=cell.href;
					  row[columns_names[i]]=cell.firstChild.textContent;
				  }
				  else{
					  row[columns_names[i]]=cell.textContent;
				  }
			  }
			  dataCandidatos.push(row);
		  }
	});
	

	           	var options = {
			editable: false,
			enableCellNavigation: true,
			enableColumnReorder: true,
			forceFitColumns:true
	};  
	if(parseInt($(".pag_button").css("height"))>39){ // es móvil
	 options = {
			editable: false,
			enableCellNavigation: true,
			enableColumnReorder: true,
			forceFitColumns:true,
			rowHeight: 40
	};
	}
	

	$(function () {
		dataView = new Slick.Data.DataView();

		grid = new Slick.Grid("#resultados_tabla", dataView, columns, options);
		//  setTableColumnWidth();
		grid.resizeCanvas();


		/* dataView.onRowsChanged.subscribe(function(e,args) {
		    grid.invalidateRows(args.rows);
		    grid.render();
		});*/
		//  var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));
		grid.onSort.subscribe(function (e, args) {
			var comparer = function(a, b) {
				return a[args.sortCol.field] > b[args.sortCol.field];
			};

			// Delegate the sorting to DataView.
			// This will fire the change events and update the grid.
			//  alert( dataView.getItem(1)["id"]);
			view = grid.getData();

			view.sort(comparer, args.sortAsc);

			//	 alert( dataView.getItem(1)["id"]);

		});

		grid.render();
		showCandidatos(1);
		
	});

}

function showCandidatos(pagina){
	inicio = (pagina-1) * page_size ;
	pagina_actual = pagina;
	dataView.beginUpdate();
	dataView.setItems(dataCandidatos.slice(inicio, inicio+ page_size));
	dataView.endUpdate();
	grid.updateRowCount();
	grid.invalidate();
	grid.render();
	var num_results = dataCandidatos.length;
	if (num_results==1){
		document.getElementById("info").innerHTML="1 resultado encontrado";
	}
	else if (num_results>0)
		document.getElementById("info").innerHTML=num_results+" resultados encontrados";
	else
		document.getElementById("info").innerHTML="No se encontraron resultados para la búsqueda realizada";
	
	numPags = Math.ceil(num_results/page_size);
	document.getElementById("pags").innerHTML="P&aacute;gina "+pagina_actual+" de "+numPags;
	$( "#pag1" ).button( "option", "disabled", pagina_actual==1 );
	$( "#pagPrev" ).button( "option", "disabled", pagina_actual==1 );
	$( "#pagSig" ).button( "option", "disabled", pagina_actual==numPags);
	$( "#pagUlt" ).button( "option", "disabled", pagina_actual==numPags );
}
function showCandidatos_pre(){
	showCandidatos(pagina_actual -1);
}

function showCandidatos_sig(){
	showCandidatos(pagina_actual +1);
}

function showCandidatos_ult(){
	var num_results = dataCandidatos.length;
	ult_pagina = Math.ceil(num_results/page_size);
	if (ult_pagina>pagina_actual){
		showCandidatos(ult_pagina);
	}
	else{
		alert("Ya está en la última página");
	}
}
function getHTMLCabeceraCandidato() {
	var idContent = "";
	idSelectionLayer = ActiveLayer;

		// pongo los nombres de campos como columnas (si hay mas de uno)
	var listaFieldNames = mainFieldName[idSelectionLayer].split(' ');

	if (queryResultsFieldName[idSelectionLayer]) {
		listaFieldNames = queryResultsFieldName[idSelectionLayer].split(' ');
	}

	if (listaFieldNames.length ==1){
		idContent += "<tr>";
		idContent += "<td><b>";
		idContent += "Nombre";
		idContent += "</b></td>";
		idContent += "</tr>";
	}
	else if (listaFieldNames.length >1) {
		idContent += "<tr>";
		if (moreDetails[idSelectionLayer]) {
			idContent += "<td style='width:20px'></td>";
		}
		for (var jj = 0; jj < listaFieldNames.length; jj++) {
			idContent += "<td><b>";
			if (fieldAliasList2[idSelectionLayer][listaFieldNames[jj]]) {
				idContent += fieldAliasList2[idSelectionLayer][listaFieldNames[jj]];
			} else {
				idContent += listaFieldNames[jj];
			}
			idContent += "</b></td>";
		}
		idContent += "</tr>";
	}

	return idContent;
}

function getHTMLCandidato(reply, isCallejero, isPortal) {
	var idContent = "";
	var endpos = 1;
	var posEnv = reply.indexOf("<ENVELOPE ", endpos);
	var posFie = reply.indexOf("<FIELDS ", endpos);

	var evenOdd;
	var ii = 0;

	idSelectionLayer = ActiveLayer;

	while (posEnv!=-1) {
		if ((ii%2) == 0) {
			evenOdd = "evenRow";
		} else {
			evenOdd = "oddRow";
		}
		ii++;

		startposEnv = posEnv + 10;
		endposEnv = reply.indexOf('" />', startposEnv);
		envelopeData = reply.substring(startposEnv, endposEnv);
		bbox = new Array();
		if (envelopeData != "") {
			var fValueEnv = getFieldValuesQuery(envelopeData);
			var fNameEnv = getFieldNamesQuery(envelopeData);
			parseEnvelope(fNameEnv, fValueEnv, bbox);
		}

		posId = reply.indexOf(nombreCampoId + '="', endpos);
		startposId = posId + nombreCampoId.length+2;
		if (posId == -1) {
			posId = reply.indexOf(nombreCampoId2 + '="', endpos);
			startposId = posId + nombreCampoId2.length+2;
		}

		endposId = reply.indexOf('"', startposId);
		theId = reply.substring(startposId, endposId);
		selectionType = LayerTypeById[ActiveLayer.toString().toUpperCase()];

		startposFie = posFie + 8;
		endposFie = reply.indexOf('" />', startposFie);
		selectedDataFie = reply.substring(startposFie, endposFie);
		if (selectedDataFie != "") {
			var fValue1 = getFieldValuesQuery(selectedDataFie);
			var fName1 = getFieldNamesQuery(selectedDataFie);

			idContent += displayIdResultQuery(evenOdd, fName1, fValue1, bbox, theId, isCallejero, isPortal, numPortalToFind, idSelectionLayer, false, 0);
		}

		reply = reply.substring(endposFie);
		posEnv = reply.indexOf("<ENVELOPE ", 1);
		posFie = reply.indexOf("<FIELDS ", 1);
	}
	return idContent;
}

/**
 * devuelve vector de resultados. Cada componente del vector contiene los datos de una fila.
 * @param reply
 * @param isCallejero
 * @param isPortal
 * @returns {String}
 */
function getHTMLUserSelection(reply, isCallejero, isPortal) {
	var idContent = '';

	if (resultCount >= featureCountLimit) {
		alert('Se han encontrado más de ' + featureCountLimit + ' resultados.\nSi no ha obtenido el resultado deseado, redefina el criterio de la búsqueda.')
	}
	idContent += "<table id='userSelectionTable' align='center' width='90%' cellpadding='5' cellspacing='0' class='item' style='display:none'>";

	idContent += getHTMLCabeceraCandidato();
	idContent += getHTMLCandidato(reply, isCallejero, isPortal);

	idContent += "</table>";

	return idContent;
}

// get a list of field names from the returned record
function getFieldNamesQuery(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fName1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		fName1[f] = v[0];
	}
	return fName1;

}

// get a list field values from the returned record
function getFieldValuesQuery(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fValue1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		if ((v[1]=="") || (v[1]==null)) v[1] = "&nbsp;";
		fValue1[f] = v[1];
	}
	return fValue1;
}

function parseEnvelope(fName1, fValue1, bbox) {
	for (var ii=0;ii<fValue1.length;ii++) {
		nombreCampo = fName1[ii];
		valorCampo = fValue1[ii];
		bbox[nombreCampo] = valorCampo;
	}
}

function changeId(_theId) {
	selectionWhere = nombreCampoId +'=' + _theId;
	$("#tabs").tabs( "enable", 0);
	$("#tabs").tabs("option", "active", 0);
}

function displayIdResultQuery(evenOdd, fName1, fValue1, bbox, _id, isCallejero, isPortal, numPortalToFind, idLayer) {
	var idContent = '';

	var listaIndexMainField = new Array();
	var listaFieldNames = mainFieldName[idLayer].split(' ');

	if (queryResultsFieldName[idLayer]) {
		listaFieldNames = queryResultsFieldName[idLayer].split(' ');
	}

	var cviaIndex = -1;
	//var clickableIndex = -1;
	var moreDetailsCampo = moreDetails[idLayer] || "";
	var moreDetailsIdx = -1;

	var scaleIdx = -1;

	for (var ii=0;ii<fName1.length;ii++) {
		tempString = fName1[ii];
		if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
			nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
		} else {
			nombreCampo = tempString;
		}
		if (isIncluded(listaFieldNames, nombreCampo, listaFieldNames.length)) {
			listaIndexMainField[nombreCampo] = ii;
		}
		if (isCallejero) {
			if (nombreCampo == "C_MUN_VIA") {
				cviaIndex = ii;
			}
		}
		if (nombreCampo == "ESCALA") {
			scaleIdx = ii;
		}
		if (moreDetailsCampo == nombreCampo) {
			moreDetailsIdx = ii;
		}
	}

	var idEnlace = _id;

	idContent += "<tr>";

	_minx = parseFloat(bbox["minx"]);
	_miny = parseFloat(bbox["miny"]);
	_maxx = parseFloat(bbox["maxx"]);
	_maxy = parseFloat(bbox["maxy"]);

	isFirst = true;
	for (var jj = 0; jj < listaFieldNames.length; jj++) {
		if (isFirst) {
			isFirst = false;

				// rellenar aqui la columna de "ver mas detalles"
			if (moreDetailsIdx != -1) {
				if ((fValue1[moreDetailsIdx] != "") && (fValue1[moreDetailsIdx] != "&nbsp;")) {
					idContent += "<td class='item' style='width:20px'>";

					idContent += "<a id='detalles" + idEnlace + "Link' class='itemEnlace' href='javascript:toggleZoneCarpeta(\"detalles" + idEnlace + "\", \"detalles\")' title='Mostrar detalles'>";
					idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/old_icon_closed.gif' border='0'></img></a>";
					idContent += "</td>";
				} else {
					idContent += "<td>";
					idContent += "</td>";
				}
			}

			var generateLink = true;

			if (_maxy <= 200000) {
				generateLink = false;
			}

			if (generateLink) {
				idContent += "<td class='item'>";
				idContent += "<a class='itemEnlace' href='javascript:";

				if (isCallejero && !isPortal && numPortalToFind != "") {
					idContent += "locatePortal(\"" + fValue1[cviaIndex] + "\"," + numPortalToFind + ", true);";
				} else {
					if (((_maxx - _minx) > 0.5) || ((_maxy - _miny) > 0.5)) {
							// es un poligono o linea
						idContent += "changeId(" + _id + ");getMap(" + _minx + "," + _miny + ",";
						idContent += _maxx + "," + _maxy + ");";
					} else {
							// o es un punto, o una geometria muy pequeña (menos de medio metro)
						if (isPortal) {
							idContent += "changeId(" + _id + ");doRecenterXYWithScale(" + _minx + "," + _miny + ", 1000);";
						} else {
							var scaleLevel = 15000;
							if (scaleIdx != -1) {
								if (! isNaN(fValue1[scaleIdx])) {
									if (fValue1[scaleIdx] > 0) {
										scaleLevel = fValue1[scaleIdx];
									}
								}
							}
							idContent += "changeId(" + _id + ");doRecenterXYWithScale(" + _minx + "," + _miny + ", " + scaleLevel + ");";
						}
					}
				}
				mainFieldText = corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]);
				idContent += "' title='" + mainFieldText + "'>" + mainFieldText + "</a>";
			} else {
				idContent += "<td class='item'>";
				mainFieldText = corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]);
				idContent += mainFieldText;
			}
		} else {
			idContent += "<td class='item'>";
			mainFieldText = corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]);
			idContent += mainFieldText;
		}
		idContent += "</td>";
	}

	idContent += "</tr>";

	if (moreDetailsIdx != -1) {
		if ((fValue1[moreDetailsIdx] != "") || (fValue1[moreDetailsIdx] != "&nbsp;")) {
			idContent += "<tr id='detalles" + idEnlace + "Table' style='display:none'>";
			idContent += "<td class='item' style='width:20px;visibility:hidden;'>";
			idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/old_icon_closed.gif' border='0'></img>";
			idContent += "</td>";
			idContent += "<td colspan='" + (listaFieldNames.length) + "'><div style='width:" + ((queryResultsWin.sx*0.9)-40) + "px'>";
			idContent += corrigeCaracteresExtranos(fValue1[moreDetailsIdx]);
			idContent += "</div>";
			idContent += "</td>";
			idContent += "</tr>";
		}
	}

	return idContent;
}
