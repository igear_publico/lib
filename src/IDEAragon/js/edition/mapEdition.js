var editingBuffer500000=1000;
var editingBuffer100000=500;
var editingBuffer10000=100;
var editingBuffer=50;


function info(){
	if (document.getElementById("infoTool").checked){
		closeMeasureArea();
		closeMeasurePath();
		if (singleclickEvent){
			mapOL.unByKey(singleclickEvent);
		}
		try{

			if (drawControl){
				drawControl.setActive(false);
			}
			if (formDialog && !formDialog.dialog('isOpen')){
				formDialog.dialog( "destroy" );
			}
		}
		catch(err){
			//alert(err);
		}
		var capa = toc.getEditingLayer();
		if ((capa!=null)&&(capa.get("editable"))){
			selectControl.setActive(true);
		}
		else{
			singleclickEvent= mapOL.on('singleclick', function(evt) {
				var capa = toc.getEditingLayer();
				if(capa==null){
					alert("No hay ninguna capa seleccionada para edición");
					return;
				}
				var viewResolution = /** @type {number} */ (mapOL.getView().getResolution());
				var url = capa.getSource().getGetFeatureInfoUrl(
						evt.coordinate, viewResolution, 'EPSG:25830',
						{'INFO_FORMAT': 'text/html'});
				if (url) {

					$('<div  class="innerWindow"></div>')
					.html(	'<iframe seamless id=gfiIframe src="' + url + '"></iframe>')
					.dialog({
						autoOpen: true,
						close: function(){
							$( this ).dialog( "destroy" );
						},


						title: capa.get('caption'),
						modal: false,
						width:850,
						height:650
					});


				}
			});
		}

		showSelTool(true,"info-icon");
	}
	else{

		showSelTool(false,"info-icon");
	}


}

function newFt(){
	if (document.getElementById("georref").checked){
		closeMeasureArea();
		closeMeasurePath();
		if (singleclickEvent){
			mapOL.unByKey(singleclickEvent);
		}
		if((document.getElementById("dialogFtForm") ==null)||(typeof document.getElementById("dialogFtForm")=='undefined')){
			newFeature();
		}
		else{

			if (editingGeom!=null){
				activateUpdateGeorref(editingGeom);
			}
			else{
				formDialog.dialogExtend("restore");
			}
		}
		showSelTool(true,"edit-icon");
	}
	else{

		showSelTool(false,"edit-icon");
	}
}


function removeWFSLayer(){
	if (wfs_layer){
		mapOL.removeLayer(wfs_layer);
		wfs_layer=null;
	}

}
function showWFSLayer(layer){
	var source = layer.getSource();
	var layerId = source.getParams().LAYERS;
	try{
		if ($("#dialogDownloadForm").dialog( "isOpen" )){
			$("#layerId").val(layerId);
		}
	}catch(err){}
	var projection = new ol.proj.Projection({code:"EPSG:25830"});
	
	var vectorSource = new ol.source.Vector({
		format: new ol.format.GeoJSON({defaultDataProjection: projection, featureProjection:projection}) ,
		url: function(extent) {
			var serviceURL;
			try{
				serviceURL=source.getUrls()[0];
			}
			catch(err){
				serviceURL=source.getUrl();
			}
			return serviceURL+'?service=WFS&' +
			'version=1.0.0&request=GetFeature&typename='+source.getParams().LAYERS +
			'&outputFormat=application/json&srsname=EPSG:25830&' + (source.getParams().CQL_FILTER?"CQL_FILTER="+source.getParams().CQL_FILTER:'&bbox=' + extent.join(',') + ',EPSG:25830')
			;
		},
		strategy: ol.loadingstrategy.bbox
	});
	var fillColor = 'rgba(255,255,255,0.01)';
	if( layer.get("wfsColor")){
		fillColor = layer.get("wfsColor");
	}
	var fill = new ol.style.Fill({
		color: fillColor
	});
	var stroke = new ol.style.Stroke({
		color:fillColor,
		width: 5
	});

	/*if (layer.get("clustered")){
		vectorSource =   new ol.source.Cluster({
	        distance: clusteringDistance,
	        source: vectorSource
	      });
	}*/
	wfs_layer = new ol.layer.Vector({
		source: vectorSource,
		style: new ol.style.Style({
			image: new ol.style.Circle({
				fill: fill,
				stroke: stroke,
				radius: 5
			}),

			stroke: stroke,
			fill: fill
		}),

	});
	var minScale = layer.get("minScale");
	if (minScale){
		minScale=minScale+1;
		wfs_layer.setMaxResolution(minScale/DOTS_PER_M);
	}
	mapOL.addLayer(wfs_layer);
	if (singleclickEvent){
		mapOL.unByKey(singleclickEvent);
	}
	if (selectControl){
		mapOL.removeInteraction(selectControl);
	}
	selectControl = new ol.interaction.Select({multi:(layer.get("clustered")?true:false)});
	selectControl.setActive(true);
	mapOL.addInteraction(selectControl);

	var selected = selectControl.getFeatures();
	var titulo = layer.get('caption').split("<");
	if (layer.get("clustered")){
		selectControl.on('select',function(evt){
			if (selected.getLength()>1){
				showFtSelectionDialog(layerId,titulo[0],selected);
			}
			else if (selected.getLength()==1){
				showEditionForm(layerId,titulo[0],selected.item(0),true);
			}
			document.getElementById("infoTool").checked=true;
			$("#infoTool").button("refresh");
		});
		
	}
	else{
	selected.on('add', function(evt) {
		
		var feature = evt.element;
		showEditionForm(layerId,titulo[0],feature,true);
		document.getElementById("infoTool").checked=true;
		$("#infoTool").button("refresh");
	});
	}
}

function showFtSelectionDialog(layerId, caption, features){
	var ftList='<ul>';
	for (var i=0; i<features.getLength(); i++){
		var atributos = features.item(i).getProperties();
		ftList +='<li><a href="#" onclick="javascript:showFtEditionForm(\''+layerId+'\',\''+caption+'\','+i+')">'+atributos.scientificname+' ('+atributos.fecha+')</a></li>';
	}
	ftList +='</ul>';
	var dialogExtendOptions = {

			"minimizable" : true

	};
	$('<div id="dialogFtSelect" class="innerWindow"></div>')
	.html(	'<p>Seleccione el elemento que desea consultar/editar</p>'+ftList
			)
	.dialog({
		autoOpen: true,
		title: 'Seleccionar',
		width:400,
		maxHeight:600,
		//modal: true,
		 buttons: [
		          
		           {
			             text: "Cerrar",
			             click: function() {
			            	 
			               $( this ).dialog( "close" );
			               selectControl.getFeatures().clear();
			             }
			           }
		         ],
		close: function(){
			
			$( this ).dialog( "destroy" );
			activateInfoTool();}
	
	}).dialogExtend(dialogExtendOptions);
}
function calculateDistance(p1,p2){
	return Math.sqrt(Math.pow(p1[0]-p2[0],2)+Math.pow(p1[1]-p2[1],2));
}

function appendLine(geom1,geom2,reverse1, reverse2,attach){
	if (geom1.getType()=="MultiLineString"){
		if (geom2.getType()=="MultiLineString"){
			var lineStrings = geom2.getLineStrings();
			for (var i=0; i<lineStrings.length; i++){
				geom1.appendLineString(lineStrings[i]);
			}	
		}
		else{
			geom1.appendLineString(geom2);
		}
		return geom1;
	}
	else if (geom2.getType()=="MultiLineString"){ // la geom1 no es multi pero la 2 sí
		geom2.appendLineString(geom1);
		return geom2;
	}
	if(!reverse1 && !reverse2){
			var coords = geom2.getCoordinates();
			for (var i=(attach ? 0 : 1); i<coords.length; i++){
				geom1.appendCoordinate(coords[i]);
			}
		
		return geom1;
	}
	else if(!reverse1 && reverse2){
		var coords = geom2.getCoordinates();

		for (var i=coords.length-(attach ? 1: 2); i>=0; i--){
			geom1.appendCoordinate(coords[i]);
		}
		return geom1;
	}
	else if(reverse1 && !reverse2){
		var newGeom = new ol.geom.LineString([]);
		var coords = geom1.getCoordinates();
		for (var i=coords.length-1; i>=0; i--){
			newGeom.appendCoordinate(coords[i]);
		}
		coords = geom2.getCoordinates();
		for (var i=(attach ? 0: 1); i<coords.length; i++){
			newGeom.appendCoordinate(coords[i]);
		}
		return newGeom;
	}
	else{
		return appendLine(geom2,geom1,false,false,attach);
	}
}

function checkMinScale(capa){
	var result = true;
	if (capa && document.getElementById("infoTool").checked){
		var minScale = capa.get("minScale");
		if (minScale){
			result = parseInt(document.getElementById('scaleTextBox').value)<=parseInt(minScale);
			if (!result){
				alert("La capa "+capa.get("caption")+" solo puede ser editada a escalas superiores a 1:"+minScale);
				document.getElementById("zoomRectTool").checked=true;
				$("#zoomRectTool").button("refresh");
				showSelTool(true,"zoom-rect-icon");
			}

		}
	}
	return result;
}


function loadGPXGeom(data, gpxLayer){
	gpxLayer.getSource().clear(true);
	var xmlData = data.replace(/(\r\n|\n|\r)/gm,"").trim();

	var format =  new ol.format.GPX();
	var features = format.readFeatures(xmlData,{
		featureProjection: new ol.proj.Projection({code:"EPSG:25830"}),
		dataProjection: new ol.proj.Projection({code:"EPSG:4326"})
	});
	gpxLayer.getSource().addFeatures(features);

	// forzar refresco para que se pinte la capa

	showLoading(false);
}
function loadGPX(data, layerName, formGeom, type) {
	if (formGeom){
		if (document.getElementById("formCoordsIcon").checked){
			document.getElementById("formCoordsIcon").checked=false;
			$("#formCoordsIcon").button("refresh");
			activateUpdateGeorref(type);
		}
		selectControl.getFeatures().clear();
		loadGPXGeom(data, markers);
		var features = markers.getSource().getFeatures();
		if(features.length>0){
			onChangeGeom(features[0],type);
		}

	}
	else{
		if ((typeof gpxLayer == 'undefined')||(gpxLayer==null)){
			var source = new ol.source.Vector();


			gpxLayer = new ol.layer.Vector({
				source: source
			});

			mapOL.addLayer(gpxLayer);

		}
		loadGPXGeom(data, gpxLayer);
	}

}


function activateModifyDrawnGeom(type){
	var features = new ol.Collection(drawingLayer.getSource().getFeatures()) ;
	setEditionStyle(features);
	modifyControl = new ol.interaction.Modify({
		features:features,
		deleteCondition: function(event) {
			return ol.events.condition.shiftKeyOnly(event) &&
			ol.events.condition.singleClick(event);
		}
	});

	modifyControl.on('modifyend',function(e){
		var features = e.features.getArray();
		for (var i=0;i<features.length;i++){
			onChangeGeom(features[i],features[i].getGeometry().getType());
		}
	});
	mapOL.addInteraction(modifyControl);
	if (snapControl){
		mapOL.removeInteraction(snapControl);
	}
	if (type=='LineString'){
		snapControl = new ol.interaction.Snap({
			source: wfs_layer.getSource()
		});
		mapOL.addInteraction(snapControl);
	}
}
function activateDrawGeom(type){
	if (modifyControl){
		mapOL.removeInteraction(modifyControl);
	}
	if (snapControl){
		mapOL.removeInteraction(snapControl);
	}

	if(drawControl){

		drawingLayer.getSource().clear(true);
		mapOL.removeLayer(drawingLayer);
		mapOL.removeInteraction(drawControl);

	}
	var drawCollection=new ol.Collection();
	var source = new ol.source.Vector({features: drawCollection});


	/*drawCollection.on('add', function(evt) {
				setTimeout(controlDoubleClickZoom(true),1000);
		       });*/
	drawingLayer = new ol.layer.Vector({
		source: source
	});
	//drawingLayer.setMap(map);
	mapOL.addLayer(drawingLayer);
	drawControl = new ol.interaction.Draw({
		source: drawingLayer.getSource(),
		type: type
	});
	drawControl.on('drawstart',
			function(evt) {
		// set sketch
		drawingLayer.getSource().clear(true);
		controlDoubleClickZoom(false);

	}, this);
	drawControl.on('drawend',
			function(evt) {

		onChangeGeom(evt.feature,evt.feature.getGeometry().getType());

		drawControl.setActive(false);



		if (evt.feature.getGeometry().getType()!='Point'){
			setTimeout( function(){activateModifyDrawnGeom(evt.feature.getGeometry().getType());},1000);
		}
		else{
			try{
				document.getElementById("formCoordsIcon").checked=false;
				$("#formCoordsIcon").button("refresh");
			}
			catch(err){}
		}
		setTimeout( function(){controlDoubleClickZoom(true);},1000);

	}, this);
	mapOL.addInteraction(drawControl);
	if (type=='LineString'){
		snapControl = new ol.interaction.Snap({
			source:  wfs_layer.getSource()
		});
		mapOL.addInteraction(snapControl);
		snapControl.setActive(true);
	}

	if (type=='Point'){
		alert("Pinche sobre el mapa en la posición que desee situar el punto");
	}
}

function controlDoubleClickZoom(active){
	//Find double click interaction
	var interactions = mapOL.getInteractions();
	for (var i = 0; i < interactions.getLength(); i++) {
		var interaction = interactions.item(i);                          
		if (interaction instanceof ol.interaction.DoubleClickZoom) {
			interaction.setActive(active);
		}
	}
}


function setEditionStyle(features){
	features.forEach(function(feature, i) {

		feature.setStyle([new ol.style.Style({
			stroke: new ol.style.Stroke({
				color: '#3399CC',
				width: 5
			}),
			fill: new ol.style.Fill({
				color: 'rgba(255,255,255,0)'
			})
		}),new ol.style.Style({
			image: new ol.style.Circle({
				radius: 5,
				fill: new ol.style.Fill({

					color: '#3399CC'
				}),
				stroke: new ol.style.Stroke({
					width:1.5,
					color: 'white'
				})
			}),
			geometry: function(feature) {
				var tipo = feature.getGeometry().getType();
				// return the coordinates of the first ring of the polygon
				if ((tipo=='MultiLineString')||(tipo=='MultiPolygon')){
					var coordinates = feature.getGeometry().getCoordinates();
					var multiPointCoords =[];
					for (var i=0; i<coordinates.length; i++){

						var polyCoords = coordinates[i];
						if (Array.isArray(polyCoords[0][0])){						
							for (var j=0; j<polyCoords.length; j++){

								multiPointCoords= multiPointCoords.concat(polyCoords[j]);
							}
						}
						else{ // realmente no es multi
							multiPointCoords= multiPointCoords.concat(polyCoords);
						}

					}
					return new ol.geom.MultiPoint(multiPointCoords);
				}
				else if (tipo=='Polygon'){
					var polyCoords = feature.getGeometry().getCoordinates();
					var multiPointCoords =[];
					for (var j=0; j<polyCoords.length; j++){
						multiPointCoords= multiPointCoords.concat(polyCoords[j]);
					}


					return new ol.geom.MultiPoint(multiPointCoords);
				}
				else{
					return new ol.geom.MultiPoint(feature.getGeometry().getCoordinates());
				}

			}
		})]);
	});
}
function activateUpdateGeorref(type){
	if($("#REQUEST").val()=="INSERT"){
		if (document.getElementById("formCoordsIcon").checked){
			activateDrawGeom(type);
		}
		else{
			if (drawControl){
				drawControl.setActive(false);
			}
			if (modifyControl){
				modifyControl.setActive(false);
			}
		}
	}
	else{
		if (document.getElementById("formCoordsIcon").checked){


			if(($("#REQUEST").val()!="OVERWRITE")&&(selectControl.getFeatures().getLength()>0)){// si es overwrite ya est� pintada en markers
				markers.getSource().clear();
				markers.getSource().addFeature(selectControl.getFeatures().item(0).clone());
				markers.setVisible(true);
			}
			selectControl.getFeatures().clear();
			markers.setZIndex(wfs_layer.getZIndex()+1);
			var features = new ol.Collection(markers.getSource().getFeatures()) ;
			if (type !='Point'){
				setEditionStyle(features);
			}

			modifyControl = new ol.interaction.Modify({
				features:features,
				deleteCondition: function(event) {
					return ol.events.condition.shiftKeyOnly(event) &&
					ol.events.condition.singleClick(event);
				}
			});

			modifyControl.on('modifyend',function(e){
				var features = e.features.getArray();
				for (var i=0;i<features.length;i++){
					onChangeGeom(features[i],type);
				}
			});
			mapOL.addInteraction(modifyControl);
			if (snapControl){
				mapOL.removeInteraction(snapControl);
			}
			if (type=='LineString'){
				snapControl = new ol.interaction.Snap({
					source: wfs_layer.getSource()
				});
				mapOL.addInteraction(snapControl);
				contextMenuAddPoint = new ContextMenu({
					width: 150,
					default_items: false, //default_items are (for now) Zoom In/Zoom Out
					items: [
					        {
					        	text: 'Añadir punto',
					        	// classname: 'some-style-class', // add some CSS rules
					        	callback: function(e){ 
					        		addVertice(e);


					        	}

					        }
					        ]
				});
				mapOL.addControl(contextMenuAddPoint);
				contextMenuAddPoint.disable();
				contextMenuAddPoint.on('beforeopen', function(evt){

					var feature = mapOL.forEachFeatureAtPixel(evt.pixel, function(ft, l){
						return ft;
					},null,function(layer){
						return layer==markers;
					});
					contextMenuAddPoint.disable();
					if (feature) { // open only on features
						contextMenuAddPoint.disable();
					}
					else{
						contextMenuAddPoint.enable();
					}
				});
				contextMenuCorte = new ContextMenu({
					width: 150,
					default_items: false, //default_items are (for now) Zoom In/Zoom Out
					items: [
					        {
					        	text: 'Partir por aquí',
					        	// classname: 'some-style-class', // add some CSS rules
					        	callback: function(e){ 
					        		var feature = mapOL.forEachFeatureAtPixel(mapOL.getPixelFromCoordinate(e.coordinate), function(ft, l){
					        			return ft;
					        		},null,function(layer){
					        			return layer=markers;
					        		});
					        		var coords = e.coordinate;
					        		var point = feature.getGeometry().getClosestPoint(coords);

					        		var corte = 	new ol.Feature({
					        			geometry:  new ol.geom.Point(point)});
					        		corte.setStyle(new ol.style.Style({
					        			image: new ol.style.RegularShape({
					        				radius: 7,
					        				points:4,
					        				fill: new ol.style.Fill({

					        					color: 'red'
					        				}),
					        				stroke: new ol.style.Stroke({
					        					width:1.5,
					        					color: 'white'
					        				})
					        			})}));
					        		var source = markers.getSource();
					        		var fts = source.getFeatures();
					        		for (var i=0; i<fts.length; i++){
					        			if (fts[i].get("esCorte")){
					        				source.removeFeature(fts[i]);
					        			}
					        		}

					        		corte.set("esCorte",true);
					        		source.addFeature(corte);
					        		onChangeGeom(feature,feature.getGeometry().getType());

					        	}

					        }
					        ]
				});
				mapOL.addControl(contextMenuCorte);
				contextMenuCorte.on('beforeopen', function(evt){

					var feature = mapOL.forEachFeatureAtPixel(evt.pixel, function(ft, l){
						return ft;
					},null,function(layer){
						return layer==markers;
					});
					contextMenuCorte.disable();
					if (feature) { // open only on features
						contextMenuCorte.enable();
					}
					else{
						contextMenuCorte.disable();
					}
				});
				contextMenuUnion = new ContextMenu({
					width: 150,
					default_items: false, //default_items are (for now) Zoom In/Zoom Out
					items: [

					        {
					        	text: 'Unir',

					        	callback: function(e){ 
					        		var feature = mapOL.forEachFeatureAtPixel(mapOL.getPixelFromCoordinate(e.coordinate), function(ft, l){
					        			return ft;
					        		},null,function(layer){
					        			return layer=wfs_layer;
					        		});  
					        		var editFeature = markers.getSource().getFeatures()[0];
					        		var geom1 = editFeature.getGeometry();
					        		var geom2 = feature.getGeometry();
					        		delFeatures.push(feature.get("objectid"));
					        		var geom1First = geom1.getFirstCoordinate();
					        		var geom2First = geom2.getFirstCoordinate();
					        		var geom1Last = geom1.getLastCoordinate();
					        		var geom2Last = geom2.getLastCoordinate();
					        		if ((geom1First[0]==geom2Last[0])&&(geom1First[1]==geom2Last[1])){
					        			editFeature.setGeometry(appendLine(geom2,geom1,false, false,false));
					        		}
					        		else if ((geom1Last[0]==geom2First[0])&&(geom1Last[1]==geom2First[1])){
					        			editFeature.setGeometry(appendLine(geom1,geom2,false, false,false));
					        		}
					        		else if  ((geom1Last[0]==geom2Last[0])&&(geom1Last[1]==geom2Last[1])){
					        			editFeature.setGeometry(appendLine(geom1,geom2,false, true,false));
					        		}
					        		else if ((geom1First[0]==geom2First[0])&&(geom1First[1]==geom2First[1])){
					        			editFeature.setGeometry(appendLine(geom1,geom2,true, false,false));
					        		}
					        		else{
					        			var dis1 = calculateDistance(geom1.getFirstCoordinate(),geom2.getLastCoordinate());
					        			var dis2 = calculateDistance(geom2.getFirstCoordinate(),geom1.getLastCoordinate());
					        			var dis3 = calculateDistance(geom1.getFirstCoordinate(),geom2.getFirstCoordinate());
					        			var dis4 = calculateDistance(geom2.getLastCoordinate(),geom1.getLastCoordinate());
					        			if ((dis1<=dis2)&&(dis1<=dis3)&&(dis1<=dis4)){
					        				editFeature.setGeometry(appendLine(geom2,geom1,false, false,true));
					        			}
					        			else if ((dis2<=dis1)&&(dis2<=dis3)&&(dis2<=dis4)){
					        				editFeature.setGeometry(appendLine(geom1,geom2,false, false,true));
					        			}
					        			else if ((dis3<=dis1)&&(dis3<=dis2)&&(dis3<=dis4)){
					        				editFeature.setGeometry(appendLine(geom1,geom2,true, false,true));
					        			}
					        			else{
					        				editFeature.setGeometry(appendLine(geom1,geom2,false, true,true));
					        			}
					        		}
					        		onChangeGeom(editFeature,type);
					        	}

					        },

					        {
					        	text: 'Añadir punto',
					        	// classname: 'some-style-class', // add some CSS rules
					        	callback: function(e){ 
					        		addVertice(e);


					        	}

					        }

					        ]
				});
				mapOL.addControl(contextMenuUnion);
				contextMenuUnion.on('beforeopen', function(evt){
					var feature = mapOL.forEachFeatureAtPixel(evt.pixel, function(ft, l){
						return ft;
					},null,function(layer){
						return layer==wfs_layer;
					});
					var editFeature = mapOL.forEachFeatureAtPixel(evt.pixel, function(ft, l){
						return ft;
					},null,function(layer){
						return layer==markers;
					});
					if (!editFeature && feature) { // open only on features
						contextMenuUnion.enable();
					} else {
						contextMenuUnion.disable();
					}
				});

			}
		}
		else{
			if (modifyControl){
				mapOL.removeInteraction(modifyControl);
			}
			if (contextMenuUnion){
				mapOL.removeControl(contextMenuUnion);
			}
			if (contextMenuCorte){
				mapOL.removeControl(contextMenuCorte);
			}
		}
	}
}

function addVertice(e){
	var coords = e.coordinate;
	var editFeature = markers.getSource().getFeatures()[0];
	var geom1 = editFeature.getGeometry();
	var geom1First = geom1.getFirstCoordinate();
	var geom1Last = geom1.getLastCoordinate();
	var distanceFirst = calculateDistance(geom1First,coords);
	var distanceLast = calculateDistance(geom1Last, coords);
	if (distanceFirst>distanceLast){
		geom1.appendCoordinate(coords);
		editFeature.setGeometry(geom1);
	}
	else{
		var newGeom = new ol.geom.LineString([]);
		newGeom.appendCoordinate(coords);
		var coordsLine = geom1.getCoordinates();

		for (var i=0; i<coordsLine.length; i++){
			newGeom.appendCoordinate(coordsLine[i]);
		}
		editFeature.setGeometry(newGeom);
	}

	onChangeGeom(editFeature,editFeature.getGeometry().getType());
}