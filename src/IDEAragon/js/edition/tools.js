var resized=false;

function downloadData(){
	var editingLayer = getEditingLayer();
	if (editingLayer == null){
		
		return;
	}
	
	var shpDialog=$('<div id="dialogDownloadForm" class="innerWindow"></div>')
	.html(	'<form id="downloadShpForm" name="downloadShpForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="EXPORT"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+editingLayer.getSource().getParams().LAYERS+'"></input>'
			+(editingLayer.getSource().getParams().CQL_FILTER?'<input type="hidden" id="cql_filter" name="cql_filter" value="'+editingLayer.getSource().getParams().CQL_FILTER+'"></input>':"")
			+'<input type="hidden" id="filter" name="filter" value="0"></input>'
			+'<input type="hidden" id="value" name="value" value=""></input>'+
			
			
			'<table><tr><td>Formato:</td><td><select class="content" id="format" name="format" ><option value="SHP_ESRI" selected="">Shapefile (proyección ArcGIS)</option><option value="SHP">Shapefile</option><option value="KML">KML</option><option value="GPX">GPX-GDB</option></select></td></tr>'+
			'<td>Huso:</td><td><select class="content" id="huso" name="huso" ><option value="30" selected="">30</option><option value="31">31</option></select></td></tr>'+
			'<tr><td>	<input type="radio" checked id="todo" name="filtro" onclick="setDownloadFilter(0)"><label  for="todo">Capa completa</label></td><td></td></tr>'+
			'<tr><td><input type="radio"  id="provi" name="filtro" onclick="activateAutocomplete(\'Provi\')"><label  for="provi">Provincia:</label></td>'+
			'<td><input class="autoCompleteFilter" id="autoCompleteProvi" type="text"></td></tr>'+
			'<tr><td><input type="radio"  id="comarca" name="filtro" onclick="activateAutocomplete(\'Comarca\')"><label  for="comarca">Comarca:</label></td>'+
			'<td><input class="autoCompleteFilter" id="autoCompleteComarca" type="text"></td></tr>'+
			'<tr><td><input type="radio"  id="municipio" name="filtro" onclick="activateAutocomplete(\'Municipio\')"><label  for="municipio">Municipio:</label></td>'+
			'<td><input  class="autoCompleteFilter" id="autoCompleteMunicipio" type="text"></td></tr>'+
			'<tr><td><input  type="radio"  id="bbox" name="filtro" onclick="activateBboxFilter()"><label  for="bbox">Selecci&oacute;n en mapa</label></td><td></td></tr></table>'+
'<input id="descargar" type="submit" onclick="mostrarDescargando()" value="Descargar">'+
	'</form>')
	.dialog({
		autoOpen: true,
		title: 'Descargar datos',
		width:400,
		modal: false,
		close: function(){
			clearExportBbox();
			$( this ).dialog( "destroy" );}
	});
	
	
}

function mostrarDescargando(){
	alert("En breve se iniciará la descarga de los datos solicitados");
}

function activateBboxFilter(){
	setDownloadFilter(9);
	dragBoxControl = new ol.interaction.DragBox({
		
		  style: new ol.style.Style({
		    stroke: new ol.style.Stroke({
		      color: [0, 0, 255, 1]
		    })
		  })
		});

		        /* add the DragBox interaction to the map */
	mapOL.addInteraction(dragBoxControl);

		dragBoxControl.on('boxend', function (evt) {
			
		        var coords = dragBoxControl.getGeometry().getCoordinates();
		       
		        var feature = new ol.Feature({
					  geometry: new ol.geom.Polygon(coords),
					 
					  name: 'Bbox'
					});
		        markers.getSource().addFeature(feature);
		       
		        var coordArray = feature.getGeometry().getCoordinates()[0];
		        var jsonGeom = '{"type":"Polygon","coordinates":[[';
		        for (var i=0; i< coordArray.length; i++){
		        	if (i>0){
		        		jsonGeom +=",";
		        	}
		        	jsonGeom= jsonGeom+"["+coordArray[i][0]+","+coordArray[i][1]+"]";
		        	
		        }
		        jsonGeom +=']],"crs":{"type":"name","properties":{"name":"EPSG:25830"}}}';
		        $("#value").val(jsonGeom);
		});
}

function clearExportBbox(){
	try{
		mapOL.removeInteraction(dragBoxControl);
		}
		catch(err){
			
		}
		removeMarkers();
}
function setDownloadFilter(value){
	clearExportBbox();
	$("#filter").val(value);
}


/**
 * carga las unidades correspondientes en los filtros geográficos de la exportación a SHP
 */
function activateAutocomplete(codEsquema){
	
	setDownloadFilter(9);
	$(".autoCompleteFilter").each(function(index, element) {
	   $(this).val("");
	} );
	cUnitList = new Object();
	dUnitList = new Object();
	
	var unidades;
var capa;
var campo;
	
	if (codEsquema=="Municipio"){
	
		if (typeof municipios=="undefined"){
			// Obtener la lista de municipios del servicio BD_GIS correspondiente

			municipios =muniDefault.split("\n")	;
			muniDefault = null;
			municipios.sort();
		}
		unidades = municipios;
		capa = muniLayer;
		campo = muniId;
	}
	else if (codEsquema=="Comarca"){
		if (typeof comarcas=="undefined"){
			
			comarcas =comarcaDefault.split("\n")	;
			comarcaDefault=null;
			comarcas.sort();
			
		}
		unidades = comarcas;
		capa = comarcaLayer;
		campo = comarcaId;
	}
	else if (codEsquema=="Provi"){
		if (typeof provincias=="undefined"){
			
			provincias =provinciasDefault.split("\n")	;
			provinciasDefault = null;
			provincias.sort();
			
		}
		capa = provinciasLayer;
		campo = provinciasId;
		unidades = provincias;
	}
	// se ordenan por el nombre de forma ascendente
		customarrayUnitJQ=new Array();
		for (var i=0; i<unidades.length; i++){
			if (unidades[i].length>0){
				var atr = unidades[i].split(";");
				
					addU(atr[0],parseInt(atr[1],10));
					
					customarrayUnitJQ.push(atr[0]);

				
			}
		}
		
	initAutocomplete(customarrayUnitJQ, codEsquema, capa, campo);
}
	
	

function addU(dunidad, cunidad, c_comarca) {
	cUnitList[dunidad]=cunidad;
	dUnitList[cunidad]=dunidad;
	if (c_comarca){
		comarcaUnitList[dunidad]=c_comarca;
	}

}

function initAutocomplete(customarrayUnitJQ, esquema, capa, campo){

	$( "#autoComplete"+esquema ).autocomplete({
		select: function( event, ui ) {
			showLoading(true);
			$("#value").val("");
			$.ajax({
				url: queryUrlRef+'?service=WFS&' +
				'version=1.0.0&request=GetFeature&typename='+capa+'&' +
				'outputFormat=application/json&srsname=EPSG:25830&' +
				'CQL_FILTER='+campo+"="+cUnitList[ui.item.value],
				type: 'GET',
				 contentType: "application/text;charset=utf-8",
				success: function(data) {
					
					
					if (data.totalFeatures<=0){
						alert("No se ha podido obtener la extensión de "+ui.item.value);
					}
					else if (data.totalFeatures==1) {
						$("#value").val(JSON.stringify(data.features[0].geometry).replace("}",
								',"crs":{"type":"name","properties":{"name":"EPSG:25830"}}}'));
					}
					showLoading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("No se ha podido obtener el el "+esquema+" indicado "+textStatus +"\n "+errorThrown);
					
					showLoading(false);
				}
			});
		},
		source: function(request, response) {

			var results = $.ui.autocomplete.filter(customarrayUnitJQ, request.term);
			response(results.slice(0, 10));
		}
	});

}

function showProposals() {
	if (document.getElementById("props_tabla")==null) {
		document.getElementById("valTodas").checked=true;
		initProposals("validationTab",10);
		getProposals(1);
	}
	
}
function showOwnProposals() {
	if (document.getElementById("own_props_tabla")==null) {
		document.getElementById("notifTodas").checked=true;
		initOwnProposals("infoTab",10);
		getOwnProposals(1);
	}

}

/**
 * ajusta la altura de las pesta�as al espacio disponible en la ventana
 */

function isMobile(){
	// obtener la altura y anchura de la ventana
	if (document.body && document.body.offsetHeight) {
	//	winH = document.body.offsetHeight;
		winW = document.body.offsetWidth;
	}
	if (document.compatMode == 'CSS1Compat' && document.documentElement
			&& document.documentElement.offsetWidth) {
	//	winH = document.documentElement.offsetHeight;
		winW = document.documentElement.offsetWidth;
	}
	if (window.innerWidth && window.innerHeight) {
	//	winH = window.innerHeight;
		winW = window.innerWidth;
	}
	return (winW<=474);
}
function adaptHeight() {

	// obtener la altura y anchura de la ventana
	if (document.body && document.body.offsetHeight) {
		winH = document.body.offsetHeight;
		winW = document.body.offsetWidth;
	}
	if (document.compatMode == 'CSS1Compat' && document.documentElement
			&& document.documentElement.offsetWidth) {
		winH = document.documentElement.offsetHeight;
		winW = document.documentElement.offsetWidth;
	}
	if (window.innerWidth && window.innerHeight) {
		winH = window.innerHeight;
		winW = window.innerWidth;
	}

	
	// establecer el tama�o para el div principal, para que ocupe el espacio
	// entre la barra de herramientas y el final de la ventana
	
	var altura = winH - $("#toolbar").height()  - $("#foot_int").height() - $("#mapPanel").position().top;
	var addTabsH=30;
	if(winW>474){ // es m�vil
		altura = altura -100;
	}
	else{
		altura = altura -20;
	}
	if (altura < MIN_HEIGHT){
		altura = MIN_HEIGHT;
	}
	
	$("#main").css('height',
			altura);
	// ajustar las pesta�as al tam�o de su contenedor
	$("#tabs").css('height',altura+addTabsH);

	// Ajustar la altura del acordeon de herramientas a la de su contenedor
	$("#toolsContainer").css('height',altura-5);
	refreshTOCAccordion();
	refreshLegendAccordion();
	

	// ajsutar el tama�o del contenido de las pesta�as
	var tabPanelsHeight =$("#toolsContainer").height()/* - $("#mapPanel").position().top+5*/;

	$("#mapPanel").css('height', altura);
	$("#searchTab").css('height', altura);
	$("#infoTab").css('height', altura);
	$("#validationTab").css('height', altura);
	//$("#resultados_tabla").css("height",altura-50);
	//$("#mdPanel").css('height', altura);
	resized=true;
}

/**
 * reajustar el tama�o de las pesta�as
 */
function resize() {
	// ajustar la altura de las pesta�as
	adaptHeight();
	// ajustar el contentenido de la pesta�a que se est� mostrando
	switch ($("#tabs").tabs("option", "active")) {
	case 0:// mapa
		// ajustar el mapa
		resizeMap();
		// programar que se ajuste la tabla cuando se active su pesta�a
	//	$("#tabs").tabs("option", "activate", onActivateTable);
		break;
	}
}
function init(){
	
	info();
	showLoading(false);
	 
}
function checkInitialParams(){
	
}

/**
 * inicializar las pesta�aas
 */
function initTabs() {
	$("#tabs").tabs({
			disabled:[1,2, 3],
			  activate: function( event, ui ) {
				  if (ui.newPanel[0].id=="infoTab"){
					  showOwnProposals();
				  }
				  else if (ui.newPanel[0].id=="validationTab"){
					  showProposals();
				  }
			  }
	});
	// deshabilito las pesta�as, para que se activen cuando se carguen sus
	// contenidos
	//$("#tabs").tabs("disable");
	// visualizar de inicio la primera pesta�a (mapa)


	$("#tabs").tabs("option", "active", 0);
	// ajustar el tama�o de las pesta�as a la altura de la ventana
	setTimeout(adaptHeight,500);
};