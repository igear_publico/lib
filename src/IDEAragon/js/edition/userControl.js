var encryptKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDMtnFB5Pn1m2RKiVMe0Im5AjfMQQ0LBelz3tz8U2BMrJCb1tYx2FbDsRAJ9SLRiTXQb8rLeGGqGymaRKE7qHZ1t5sPzSiAISTdX9piEHcwj+QGuKJlgYRqf5DRO6+UB+UhBuGUSxlbotR/RgBGtTFu2CUoOCAeicOwPAxkYiRkrwIDAQAB";
var tokenId;
var debugOn=false;

function inputKeyUpLogin(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
    	compruebaUsr();
    }
}
/**
 * 
 * @param email_path
 * @param modal_win
 */
function initUserControl(email_path, modal_win){
	var htmlUsr = '<div><table width="100%" cellpadding="1" cellspacing="1" style="table-layout:fixed;">';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="login">Nombre de usuario:</label></td><td>';
	htmlUsr += '<input onFocus="act(\'login\')" onBlur="deac(\'login\')" onkeyup="inputKeyUpLogin(event)" id="login" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="passwd">Contrase&ntilde;a:</label></td><td>';
	htmlUsr += '<input type="password" onFocus="act(\'passwd\')" onkeyup="inputKeyUpLogin(event)" onBlur="deac(\'passwd\')" id="passwd" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '<tr><td colspan="2" align="center"><hr/>Si no dispone de usuario, solic&iacute;telo mediante el siguiente <a target="_blank" href="resources/impacces.pdf">impreso</a></td></tr>';
	htmlUsr += '</table></div>';

	dialogLog = $('<div id="loginDialog"></div>')
		.html(htmlUsr)
		.dialog({
			autoOpen: false,
			closeOnEscape: false,
			open: function(event, ui) { $(".ui-dialog[aria-describedby='loginDialog'] .ui-dialog-titlebar-close").hide(); },
			title: 'Registro de usuarios',
			modal: modal_win,
			width: "300"
		});
}

function dialogLogin(hayCancelar) {
	if (hayCancelar) {
		dialogLog.dialog( "option", "buttons", [ { id:"btnEnviar", text: "Enviar", click: function() { compruebaUsr(); } }, { id:"btnCancelar",text: "Cancelar", click: function() { $( this ).dialog( "close" ); } } ] );
	}	else {
		dialogLog.dialog( "option", "buttons", [ { id:"btnEnviar", text: "Enviar", click: function() { compruebaUsr(); } } ] );
	}
	dialogLog.dialog("open");
}

function compruebaUsr() {
	var newUsuario = document.getElementById('login').value;
	var newPasswd = document.getElementById('passwd').value;

	if ((newUsuario == "") || (newPasswd == "")) {
		var dialogUsuario= $('<div></div>')
		.html('<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 1em 1em;"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;margin-bottom: .3em;"></span><strong>Error</strong></p></div><br/>Debe introducir obligatoriamente los campos de nombre de usuario y contrase&ntilde;a.<br/></div>')
		.dialog({
			autoOpen: true,
			buttons: {
				Aceptar: function() {
					$( this ).dialog( "close" );
					act('login');
					document.getElementById('login').focus();
				}
			},
			title: 'Error',
			modal: false
		});
	} else {
		
		checkUsr();
	}
}
function checkUsr(service) {
	var newUsuario = document.getElementById('login').value;
	var newPasswd = encryptPwd(document.getElementById('passwd').value);
	
	var url = cartoUpdateURL+'?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(newPasswd);

	if (debugOn) {
		window.open(url);
	}
	$("#btnEnviar").button( "disable");
	$("#btnCancelar").button( "disable");
	$.ajax({
		url: url,
		type: 'GET',
		
		success: function(data) {
					var partes = data.split("#");
					var pos = data.indexOf("#");
					var rol = partes[0];
					tokenId=partes[1];	
					//enviarCambio();
					document.getElementById("nomUsrConnected").innerHTML = "Conectado como usuario " + newUsuario;
					document.getElementById("connection").innerHTML = "Cambiar de usuario";
					document.getElementById("connection").title = "Cambiar de usuario";
					$( "#infoTool" ).button( "enable" );
					$( "#georref" ).button( "enable" );
					$( "#impShp" ).button( "enable" );
					$( "#impGPX" ).button( "enable" );
					$("#tabs").tabs("enable", 2);
					if (rol==1){// validador
						$("#tabs").tabs("enable", 3);
					}
					$( "#toc_window" ).accordion( 'option','active',0 );
					 refreshTOCAccordion();
					 checkInitialParams(newUsuario);
					dialogLog.dialog("close");
		},
		error: function(data, textStatus, errorThrown) {
			if (debugOn) {
				alert('Error validando el usuario. State: ' + data.readyState + ", Status:" + data.status);
			}
			if (textStatus =='timeout'){
			
					alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
				
			}
			else if (data.status==403){
				$dialogRecibido = $('<div></div>')
				.html('<div>Usuario y contrase&ntilde;a no v&aacute;lidos</div>')
				.dialog({
					autoOpen: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					},
					title: 'Error',
					modal: true
			});
			}
			else{
			creaVentanaAviso("No se ha podido validar su usuario. Por favor, int&eacute;ntelo m&aacute;s adelante.\n", true);
			console.log("No se pudo validar el usuario. Respuesta servicio:"+data.responseText);
			}
			$("#btnEnviar").button( "enable");
			$("#btnCancelar").button( "enable");
		},
		timeout:15000
	});
}

function validateToken() {
	

	var url =cartoUpdateURL+ '?REQUEST=VALIDATETOKEN&TOKEN=' + tokenId;

	if (debugOn) {
		window.open(url);
	}
	var esValido=false;
	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			esValido=true;
					
		},
		error: function(data, textStatus, errorThrown) {
			
			if (debugOn) {
				alert('Token inválido. State: ' + data.readyState + ", Status:" + data.status);
			}
			
		},
		async: false
	});
	return esValido;
}