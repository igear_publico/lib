function buscarSOAP(pagina){

	showSearching(true);
	inicio = (pagina-1) * page_size + 1;
	literal = encodeURI(document.getElementById("texto").value.toLowerCase());
	var maxParam="";
	if (!mostrarTodos && (typeof MAX_RESULTS != 'undefined')){
		maxParam="&maxResults="+MAX_RESULTS;
	}
	var url = cartoUpdateURL+'?REQUEST=SEARCH&TOKEN='+tokenId+'&texto='+literal+"&inicio="+inicio+"&total="+page_size+maxParam+"&app="+APP_NAME;
	pagina_actual = pagina;
	$.ajax({
		url: url,
		type: 'GET',
		success:  function(data) { /*try{*/addTableData(data);updatePages(); 
},
		error:  function() { alert("error "+url);},
		complete: function(xmlHttpRequest) { 
			showSearching(false);},
			processData: false,
			contentType: "text/xml; charset=\"utf-8\"",
			async: false
			
	});
}	