﻿
var hayCambiosForm=false;
var formDialog;
var grid_prop, grid_own_prop;
var data_prop = [];
var data_own_prop = [];
var dataView_prop, dataView_own_prop;
var page_size_prop = 10;
var pagina_actual_prop=1;
var pagina_actual_own_prop=1;
var num_props = 0;
var num_own_props = 0;
var orderField="fechaProp";
var orderAsc=true;
var orderFieldOwn="fechaProp";
var orderAscOwn=true;
var editingGeom=null;
var cartoUpdateTimeout=30000;
var hayCambiosCoords=false;
var delFeatures=[];

function fillFormDate(atributos){
	try {

		var fecha_str=atributos.fecha_mod;
		setDateField("fecha_mod",fecha_str);
		
	}
	catch(err) {
		console.log( "No hay valor para la fecha o no es válido:"+atributos.fecha_mod + ". "+err);
	}
}
function parseWFSDate(fecha_str){

	try{

		return fecha_str.substr(0,10);
	}
	catch(err){
		console.log("Fecha incorrecta:"+fecha_str+". "+err);
	}

}

function setDateField(id,value){
	var parsed=parseWFSDate(value);
	$("#"+id).val(parsed);
	/*if ($("#"+id).val()!=parsed){
		document.getElementById(id).value=parseArcXMLDateChrome(value);
	}*/
}

function checkInteger(e){
	
	if((e.key == ",")||(e.key == ".")) {
		alert("Este campo no admite decimales");
		return e.preventDefault();
	}
	else{
		fireFormChanged();
	}
}
function checkComboError(id, value, msg) {
	if ((value==null)||((value=="")&&(value!=0))){
		return "";
	}
	try {
		document.getElementById(id).value=value;
		if (document.getElementById(id).value!=value){
			return (msg + value + ". ");
		}
	}
	catch(err) {
		return (msg + value + ". ");
	}
	return "";
}
function getEditingLayer(){

	var editingLayer = toc.getEditingLayer();
	if (editingLayer == null){
		alert("No hay ninguna capa seleccionada para edición");
		return null;
	}
	if (!editingLayer.get('editable')){
		alert("La capa seleccionada ("+editingLayer.get('caption')+") no es editable");
		return null;
	}
	return editingLayer;
}


function newFeature(){
	var editingLayer = getEditingLayer();
	if (editingLayer == null){

		return;
	}
	showEditionForm(editingLayer.getSource().getParams().LAYERS,editingLayer.get('caption'),null,true);
}

function saveEditionForm(){
	//var continueSavingForm=false;

	if (hayCambiosForm){
		var msg="";
		if(delFeatures.length>0){
			msg="Esta operación implica la solicitud de borrado de los objetos fusionados.\n";
		}
		return confirm(msg+"¿Está usted seguro de guardar los cambios realizados?");


	}
	else{
		alert("No se han realizado modificaciones. No se guardará ningún cambio");


		return false;
	}

}


function closeEditionForm(){
	if (hayCambiosForm){
		$('<div  class="innerWindow"></div>')
		.html(	'<p>¿Está usted seguro de cerrar el formulario y perder los cambios realizados?</p>'+
		'<p>Pulse Sí para cerrar el formulario y NO para mantenerlo abierto</p>')
		.dialog({
			autoOpen: true,
			close: function(){
				$( this ).dialog( "destroy" );
			},
			buttons: [
			          {
			        	  text: "SÍ",
			        	  click: function() {

			        		  $( this ).dialog( "close" );
			        		  formDialog.dialog( "close" );
			        		  formDialog.dialog( "destroy" );
			        	  }
			          },
			          {
			        	  text: "NO",
			        	  click: function() {
			        		  $( this ).dialog( "close" );
			        	  }
			          }],

			          title: 'Cerrar formulario',
			          modal: true
		});

	}
	else{
		formDialog.dialog( "close" );
		formDialog.dialog( "destroy" );
	}
}

function fireEditedCoords(){
	hayCambiosCoords=true;
	fireFormChanged();
}

function fireFormChanged(cambioGeom){
	if (!cambioGeom){
		editingGeom=null;
	}
	hayCambiosForm=true;
}

function sendDeleteRequest(layerId,objectid,comentario,ambito){
	$.ajax({
		url: cartoUpdateURL+"?REQUEST=DELETE&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectid+"&comentario="+escape(comentario)+ambito,
		type: 'POST',
		timeout:cartoUpdateTimeout,

		//async:false,
		success:  function(data){
			alert("Se ha tramitado la solicitud de borrado de la feature");


			formDialog.dialog( "close" );
			formDialog.dialog( "destroy" );

		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			if (textStatus=="timeout"){
				alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
			}	
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se ha podido tramitar la solicitud de borrado "+textStatus +"\n "+errorThrown);}
		}


	});
}
function deleteFeature(layerId,objectid){
	var ambito="";
	if (layerId=='PUNTOS_AGUA'){
		ambito="&ambito="+$("#ambito").val();
	}

	var comentario = $("#comentario").val();
	if ((comentario == null)||(comentario.length<=0)){
		alert("Debe rellenar el comentario sobre su propuesta de borrado");
		return;
	}
	$('<div  class="innerWindow"></div>')
	.html(	'<p>¿Está usted seguro de borrar esta feature?</p>'+
	'<p>Pulse Sí para para borrar la feature y NO para mantenerla</p>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: [
		          {
		        	  text: "SÍ",
		        	  click: function() {

		        		  sendDeleteRequest(layerId,objectid,comentario,ambito);
		        		  $( this ).dialog( "close" );
		        	  }
		          },
		          {
		        	  text: "NO",
		        	  click: function() {
		        		  $( this ).dialog( "close" );
		        	  }
		          }],

		          title: 'Cerrar formulario',
		          modal: true
	});



}

function highlightProposal(pk_prop){
	$(".slick-cell.r0").each(function( index ) {
		if($( this ).text()==pk_prop){
			$( this ).parent().addClass("greyRow");
		}
		else{
			$( this ).parent().removeClass("greyRow");
		}
	});

}
function showProposedFeature(layerId,title,objectid,editable, comentario,pk_prop, onlyMap,onlyZoom, revisable){
	highlightProposal(pk_prop);
	$.ajax({
		url: service+'?service=WFS&' +
		'version=1.0.0&request=GetFeature&typename='+layerId+'&' +
		'outputFormat=application/json&srsname=EPSG:25830&' +
		'CQL_FILTER=objectid='+objectid,
		type: 'GET',
		success:  function(data){
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
			if (features.length>0){
				$("#tabs").tabs("option", "active", 0);
				if (!onlyMap){
					showEditionForm(layerId,title,features[0],editable, comentario,pk_prop,revisable);
				}
				var geom = features[0].getGeometry();
				var buffer=defaultBuffer;
				if (geom.getType()=='Point') {
					buffer=bufferPoints;
				}
				else {
					features[0].setStyle([new ol.style.Style({stroke:new ol.style.Stroke({width:5, color:'white'})}),
					                      new ol.style.Style({stroke:new ol.style.Stroke({width:3, color:'blue'})})]);
				}
				mapOL.getView().fit(ol.extent.buffer( features[0].getGeometry().getExtent(),buffer), mapOL.getSize()); 
				if (!onlyZoom){
					var source = markers.getSource();
					source.clear(true);
					source.addFeature(features[0]);
					source.refresh();	
				}

			}
			else{
				if(onlyMap && !onlyZoom){
					// suponemos que es una validación de borrado, hacemos zoom a la feature eliminada
					showProposedFeature(layerId+"_TEMP",title,objectid,editable, comentario,pk_prop, onlyMap, true, revisable);
				}
				else{
					alert("No se ha encontrado el objeto con objectid "+objectid+" en la capa "+layerId);
				}
			}
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			alert("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
		}


	});	

}

function initFormButtons(){
	$("#formCoordsIcon").button({
		text:false,
		icons : {

			primary : "edit-icon"
		}
	});
}
function showFtEditionForm(layerId,title,selectedIdx){
	var selected = selectControl.getFeatures();
	showEditionForm(layerId,title,selected.item(selectedIdx),true);
}
function showEditionForm(layerId,title,feature,editable, comentario,pk_prop, revisable){
	if((document.getElementById("dialogFtForm") !=null)&&(typeof document.getElementById("dialogFtForm")!='undefined')){
		alert("Ya se está editando una feature. Para editar otra diferente cierre primero el formulario");
		return;
	}
	delFeatures=[];
	if (selectControl){
		selectControl.setActive(false);
	}
	hayCambiosForm=false;
	hayCambiosCoords=false;
	var dialogExtendOptions = {

			"minimizable" : true

	};

	var parsedLayerId = layerId.replace("_TEMP","");
	var requestOp="INSERT";
	var autoOpenDialog=true;
	var atributos ;
	var objectid ="";
	var enableGuardar=true;
	if (feature){
		atributos = feature.getProperties();
		objectid = atributos.objectid;

		autoOpenDialog=false;
		enableGuardar=false;
		if (parsedLayerId==layerId){
			requestOp="UPDATE";
		}
		else{
			requestOp="OVERWRITE";
		}

	}
	else if ((layerForm[layerId].indexOf('id="gmlPoint"')>0)&&(layerForm[layerId].indexOf('id="gmlPolygon"')>0)&&(layerForm[layerId].indexOf('id="gmlLine"')>0)){ // el usuario tiene que elegir el tipo de geometría
		autoOpenDialog=false;
		$('<div></div>')
		.html('<div class="ui-widget"><p>¿Qué tipo de geometría quiere crear?</p></div>')
		.dialog({
			autoOpen: true,
			buttons: {
				Punto: function() {
					activateDrawGeom('Point');
					autoOpenDialog=false;
					$("#formPolygon").remove();
					$("#formLine").remove();
					$( this ).dialog( "destroy" );
					
				},
				Línea:function() {
					activateDrawGeom('LineString');
					$("#formPoint").remove();
					$("#formPolygon").remove();
					$( this ).dialog( "destroy" );
					formDialog.dialog("open");
				},
				Polígono:function() {
					activateDrawGeom('Polygon');
					$("#formPoint").remove();
					$("#formLine").remove();
					$( this ).dialog( "destroy" );
					formDialog.dialog("open");
				}

			},
			close: function(ev, ui) {
				$( this ).dialog( "destroy" );
			},
			width: 300,
			title: 'Tipo de geometría',
			modal: true
		});
	}
	else if (layerForm[layerId].indexOf('id="gmlPoint"')>0){ // se va a dar de alta una feature con geometr�a puntual
		// no abrir el formulario hasta que se ubique el punto
		activateDrawGeom('Point');
		autoOpenDialog=false;
	}
	else if (layerForm[layerId].indexOf('id="gmlLine"')>0){ // se va a dar de alta una feature con geometr�a lineal
		activateDrawGeom('LineString');
	}
	else if (layerForm[layerId].indexOf('id="gmlPolygon"')>0){ // se va a dar de alta una feature con geometr�a poligonal
		activateDrawGeom('Polygon');
	}
	var ancho = (isMobile()?MOBILE_WIDTH:1000);
	formDialog= $('<div id="dialogFtForm" class="innerWindow"></div>')
	.html(	'<form id="ftEditionForm" target="_blank" name="editionForm" autocomplete="false" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="'+requestOp+'">'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'">'
			+'<input type="hidden" id="layerId" name="layerId" value="'+parsedLayerId+'">'
			+'<input type="hidden" class="objectId" id="objectId" name="objectId" value="'+objectid+'">'
			+'<input type="hidden" id="corte" name="corte">'
			+'<input type="hidden" id="clave" name="clave" '+(requestOp=='OVERWRITE'?'value="'+pk_prop+'"':'')+'>'
			+ '<div style="float:right;"><a href="javascript:printForm()"><img border="0" src="/lib/IDEAragon/themes/default/images/imprimir.png" title="Imprimir" alt="Imprimir"></a></div>'
			+ '<div id="saveShpButton" style="float:right;'+(requestOp=="UPDATE"?'':'visibility:hidden;')+'"><a href="javascript:exportShp()"><img border="0" src="/lib/IDEAragon/themes/default/images/save.png" title="Exportar a SHP" alt="Exportar a SHP"></a></div>'
			+ '<div id="showPhotosButton" class="attachedButton" style="float:right;'+(requestOp=="UPDATE" || requestOp=="OVERWRITE"?'':'visibility:hidden;')+
			'"><span id="photosCount" class="attachCount"></span><a href="javascript:showPhotos(\''+layerId+'\')"><img id="showPhotosImg" border="0" src="/lib/IDEAragon/themes/default/images/picture.png" title="Fotos" alt="Fotos"></a></div>'
			+ '<div id="showDocsButton" class="attachedButton" style="float:right;'+(requestOp=="UPDATE"|| requestOp=="OVERWRITE"?'':'visibility:hidden;')+
			'"><span id="docsCount" class="attachCount"></span><a href="javascript:showDocs(\''+layerId+'\')"><img id="showDocsImg" border="0" src="/lib/IDEAragon/themes/default/images/File.png" title="Documentos" alt="Documentos"></a></div>'

			+	layerForm[parsedLayerId]+		
			((editable ||(requestOp=="INSERT")) ? '<button id="deleteFt" name="deleteFt" '+(requestOp!="UPDATE"?'style="display:none" ':'')+'onclick="deleteFeature(\''+layerId+'\',\''+objectid+'\')">Borrar</button>'
					+'<button name="closeFormBtn" onclick="closeEditionForm()">Cerrar</button>'
					+(revisable?'<button id="revisarBtn" name="revisarBtn" onclick="showRevisionDialog('+pk_prop+',\''+parsedLayerId+'\')">Revisar</button>':'')
					+'<input '+(enableGuardar?"":"disabled")+' id="guardarBtn" type="submit" value="Guardar cambios">'+
					'<ul id="comentarioTbl" class="blockComent"><li class="label">Comentario para el validador (*)</li><li class="field"><textarea autocomplete="off" style="width:100%; class="content" id="comentario" name="comentario"  cols="75" maxlength="254"  required ></textarea></li></ul>':'')

					+'</form>')
					.dialog({
						autoOpen: autoOpenDialog,
						open: function(){
							$("#ftEditionForm input").attr("autocomplete", "off");
							$("#ftEditionForm textarea").attr("autocomplete", "off");
							$("#ftEditionForm select").attr("autocomplete", "off");
							initFormButtons();
							if(drawControl && drawControl.getActive()){
								document.getElementById("formCoordsIcon").checked=true;
								$("#formCoordsIcon").button("refresh");
							}


						},
						close: function(){

							formDialog.dialog( "destroy" );
							editingGeom=null;
							/*limpiarEdicion();
			limpiarMarcaMapa();*/
							removeMarkers();
							/*if (selectionWhere != "") {
				selectionWhere = "";
				getMapWithCurrentExtent();
			}*/


							activateInfoTool();
							//}
						},

						width: ancho,
						title: 'Editar '+title,
						modal: false
					}).dialogExtend(dialogExtendOptions);
	if (feature){
		checkEditable(layerId,objectid);
	}

	setPhotosCount(parsedLayerId, objectid);
	setDocsCount(parsedLayerId, objectid);
	$("#slider1").responsiveSlides({
		auto: false,
		pager:true
	});

	$('button[name="deleteFt"], button[name="closeFormBtn"], button[name="revisarBtn"]').on('click', function (e) {
		e.preventDefault();

	});
	$("#ftEditionForm").submit(function(){
		if (/*($("#ftEditionForm .invalidField").length-$("#ftEditionForm label.invalidField").length)>0*/!$("#ftEditionForm").valid() ){
			alert("Revise los campos inválidos (marcados en rojo)");
			$("#ftEditionForm").validate();
			var sliderIdx=0;
			while ($("#rslides1_s"+sliderIdx).length>0){
				if (($("#rslides1_s"+sliderIdx+" .invalidField").length-$("#rslides1_s"+sliderIdx+" label.invalidField").length)>0){

					$('li.rslides1_s'+(sliderIdx+1)).find('a').trigger('click');
					sliderIdx=9999;
				}
				sliderIdx++;
			}

		}
		try{
			var puntos = markers.getSource().getFeatures();
			if((puntos.length>0))
				for (var i=0;i<puntos.length;i++){
					if (puntos[i].get('esCorte')){
						$("#corte").val(puntos[i].getGeometry().getCoordinates().join(","));
						break;
					}

				}
		}
		catch(err){
			alert("Error al dividir la línea");
		}
		if (hayCambiosCoords){
			try{
				var coords = $("#pointCoords").val().split(";");
				var x=Number(coords[0]);
				var y=Number(coords[1]);
				//	edit_point(x,y);
				//	fireCoordsChanged(x,y);
			}
			catch(err){
				alert("Coordenadas inválidas");
			}
		}
		onSubmit();
		 $('.sendDisabled').prop('disabled', false);
	});
	$('#ftEditionForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#ftEditionForm').ajaxSubmit({
				timeout:cartoUpdateTimeout,
				beforeSubmit: function(a,f,o) {
					var guardar = saveEditionForm();
					document.getElementById("guardarBtn").disabled = guardar;
					 $('.sendDisabled').prop('disabled', true);
					return guardar;
				},
				success: function(data) { 

					if ((data != null)&&(data.length>0)){
						var lines =  $.trim(data).split("\n");
						if (lines.length>0){
							$("#objectId").val(lines[0]);
							if (lines.length>1){
								$("#id").val(lines[1]);
							}
						}


					}
					for (var i=0; i<delFeatures.length; i++){
						sendDeleteRequest($("#layerId").val(),delFeatures[i],"Borrada por fusión con otro objeto","");

					}
					alert("Se ha tramitado la solicitud de los cambios realizados");
					hayCambiosForm=false;
					hayCambiosCoords=false;
					refreshLayer(parsedLayerId);
					/*		if (requestOp=='OVERWRITE'){
						$("#revisarBtn").css("display","initial");
					}*/

					$("#REQUEST").val(""); // los cambios en el formulario ser�an ya de modificaci�n de feature
					/*$("#deleteFt").css("display","inline");
					$("#comentario").val("");*/
					$("#saveShpButton").css("visibility","visible");
					$("#showPhotosButton").css("visibility","visible");
					$("#showDocsButton").css("visibility","visible");

					$("#deleteFt").css("display","none");
					$("#guardarBtn").css("display","none");

					$("#comentarioTbl").css("display","none");

				},
				error: function(jqXHR,  textStatus,  errorThrown) { 

					if (textStatus =='timeout'){
						alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
					}
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else{
						var msg = jqXHR.responseText;
						try{
							msg = msg.substring(msg.indexOf("<h1>")+4,msg.indexOf("</h1>"))
						}
						catch(err){
							
						}
						alert("No se pudo actualizar la feature ("+msg+")");
						console.log("No se pudo actualizar la feature. Respuesta servicio:"+jqXHR.responseText);

					}
					document.getElementById("guardarBtn").disabled = false;

					$('#ftEditionForm input').not('.disabled').removeAttr('disabled');
				}
			});
		}
	});
	$( "#ftEditionForm" ).tooltip();
	if (feature){
		fillFormData(layerId,atributos);
		if (comentario){
			$("#comentario").val(comentario);
		}
		//showSearchResult("QUERYSERVICE="+service+"&ACTIVELAYER="+layerId+"&QUERY=OBJECTID="+objectid+"&BOX="+box);
		formDialog.dialog("open");
	}
}
function refreshLayer(parsedLayerId){
	if ((parsedLayerId=='INCENDIOS')||(parsedLayerId=='INCENDIOS_ACTIVOS')){
		try{
			var layerIdx = cartoforLayerIdx[parsedLayerId];
			var layer = toc.getOLLayer(layerIdx);
			var source = layer.getSource();
			var params = source.getParams();
			params.t = new Date().getMilliseconds();
			source.updateParams(params);
			if (wfs_layer){
				if (toc.editingLayer==layerIdx){
					removeWFSLayer();
					showWFSLayer(layer);
				}
			}
			
			}catch(err){
				// entrará para formularios que no corresponden a una capa GIS (e.g.: situacion operativa incendios)
				console.log(err);
			}
	}
}
function onSubmit(){
	// a implementar en instancia correspondiente si se quiere hacer alguna operación antes del submit
}
function onChangeGeom(feature,type){
	try{
		var format = new ol.format.WKT();
		var wkt = format.writeGeometry(feature.getGeometry());

		if (type == 'Point'){
			document.getElementById("gmlPoint").value = wkt;
			var coords = feature.getGeometry().getCoordinates();
			if (feature.getGeometry().getType()=='MultiPoint'){
				coords = coords[0];
			}
			document.getElementById("pointCoords").value =  coords[0]+";"+ coords[1];
			fireCoordsChanged( coords[0], coords[1]);
			/*if(document.getElementById("formPolygon")){
				$("#formPolygon").css("display","none");
				$("#formPoint").css("display","block");
			}*/
		}
		else if ((type == 'LineString')||(type == 'MultiLineString')){
			document.getElementById("gmlLine").value = wkt;
		}
		else {
			document.getElementById("gmlPolygon").value = wkt;
		}

		fireFormChanged(true);

	}
	catch(error){
		alert ("Error editando la geometría. "+error);
	}


}


function fireCoordsChanged(x,y){
	if ($("#dialogFtForm").dialog( "isOpen" )==false){
		$("#dialogFtForm").dialog("open");
	}
}


/**
 * inicializa los botones de JQuery
 */
function initButtonsProposals() {

	$("#prop_pagSig").button({
		icons : {
			primary : "next-icon"
		}
	});

	$("#prop_pagPrev").button({
		icons : {
			primary : "prev-icon"
		}
	});
	$("#prop_pagUlt").button({
		icons : {
			primary : "last-icon"
		}
	});

	$("#prop_pag1").button({
		icons : {
			primary : "first-icon"
		}
	});

}
function initButtonsOwnProposals() {

	$("#own_prop_pagSig").button({
		icons : {
			primary : "next-icon"
		}
	});

	$("#own_prop_pagPrev").button({
		icons : {
			primary : "prev-icon"
		}
	});
	$("#own_prop_pagUlt").button({
		icons : {
			primary : "last-icon"
		}
	});

	$("#own_prop_pag1").button({
		icons : {
			primary : "first-icon"
		}
	});

}
function getProposals_pre(){
	getProposals(pagina_actual_prop -1,getValidationState());
}

function getProposals_sig(){
	getProposals(pagina_actual_prop +1,getValidationState());
}

function getProposals_ult(){
	var ult_pagina = Math.ceil(num_props/page_size_prop);
	if (ult_pagina>pagina_actual_prop){
		getProposals(ult_pagina,getValidationState());
	}
	else{
		alert("Ya está en la última página");
	}
}

function getOwnProposals_pre(){
	getOwnProposals(pagina_actual_own_prop -1,getNotifiedState());
}

function getOwnProposals_sig(){
	getOwnProposals(pagina_actual_own_prop +1,getNotifiedState());
}

function getOwnProposals_ult(){
	var ult_pagina = Math.ceil(num_own_props/page_size_prop);
	if (ult_pagina>pagina_actual_own_prop){
		getOwnProposals(ult_pagina,getNotifiedState());
	}
	else{
		alert("Ya está en la última página");
	}
}
function initProposals(divId, pageSize){
	$("#"+divId).append("<div id=\"props_tabla\"></div>");
	$("#"+divId).append("<div id=\"props_pag\"></div>");
	$("#props_pag").append("<button class=\"pag_button\" id=\"prop_pag1\" disabled onclick=\"getProposals(1)\"></button>");
	$("#props_pag").append("<button class=\"pag_button\"id=\"prop_pagPrev\" disabled onclick=\"getProposals_pre()\"></button>");
	$("#props_pag").append("<span id=\"prop_pags\">P&aacute;gina 0 de 0</span>");
	$("#props_pag").append("<button class=\"pag_button\" id=\"prop_pagSig\" disabled onclick=\"getProposals_sig()\"></button>");
	$("#props_pag").append("<button class=\"pag_button\" id=\"prop_pagUlt\" disabled onclick=\"getProposals_ult()\"></button>");
	initButtonsProposals();
	page_size_prop = pageSize;
	var columns = [
{id: "id", name: "Id.", field: "id",minWidth: 55,width:55,maxWidth: 50,  sortable:true},
{id: "fechaprop", name: "Fecha notificación", field: "fechaprop",minWidth: 140,width:140,maxWidth: 140, sortable:true},
{id: "notificador", name: "Autor", field: "notificador",minWidth: 80,maxWidth: 100, sortable:true},
{id: "capa", name: "Capa", field: "capa",minWidth: 80,maxWidth: 100, sortable:true},
{id: "featureid", name: "Objeto", field: "featureid",minWidth: 80,sortable:true},
{id: "op", name: "Operación", field: "op",minWidth: 80,maxWidth: 100, sortable:true},
{id: "comentario", name: "Descripción", field: "comentario",minWidth: 80,sortable:true},
{id: "estado", name: "Estado", field: "estado",minWidth: 80, maxWidth: 100,sortable:true},
{id: "fecharev", name: "Fecha revisión", field: "fecharev",minWidth: 120,width:120,maxWidth: 120, sortable:true},
{id: "ver", name: "Ver", field: "ver",minWidth: 40,width:40,maxWidth: 40, sortable:false, formatter:verFormatter, cssClass:"centered"},
{id: "revisar", name: "Revisar", field: "revisar",minWidth: 60,width:60,maxWidth: 60, sortable:false, formatter:revisarFormatter, cssClass:"centered"}];
	var options = {
			editable: false,
			enableCellNavigation: true,
			enableColumnReorder: true,
			forceFitColumns:true
	};  
	if(parseInt($(".pag_button").css("height"))>39){ // es m�vil
		options = {
				editable: false,
				enableCellNavigation: true,
				enableColumnReorder: true,
				forceFitColumns:true,
				rowHeight: 40
		};
	}
	$(function () {
		dataView_prop = new Slick.Data.DataView();

		grid_prop = new Slick.Grid("#props_tabla", dataView_prop, columns, options);
		grid_prop.registerPlugin(new Slick.AutoTooltips());
		//  setTableColumnWidth();
		grid_prop.resizeCanvas();
		grid_prop.onSort.subscribe(function (e, args) {
			if (args.sortCol.field=="id"){
				orderField="pk";
			}
			else{
				orderField=args.sortCol.field;
			}
			orderAsc=args.sortAsc;
			getProposals(pagina_actual_prop, getValidationState());

		});

		grid_prop.render();

	});

}
function initOwnProposals(divId, pageSize){
	$("#"+divId).append("<div id=\"own_props_tabla\"></div>");
	$("#"+divId).append("<div id=\"own_props_pag\"></div>");
	$("#own_props_pag").append("<button class=\"pag_button\" id=\"own_prop_pag1\" disabled onclick=\"getOwnProposals(1)\"></button>");
	$("#own_props_pag").append("<button class=\"pag_button\"id=\"own_prop_pagPrev\" disabled onclick=\"getOwnProposals_pre()\"></button>");
	$("#own_props_pag").append("<span id=\"own_prop_pags\">P&aacute;gina 0 de 0</span>");
	$("#own_props_pag").append("<button class=\"pag_button\" id=\"own_prop_pagSig\" disabled onclick=\"getOwnProposals_sig()\"></button>");
	$("#own_props_pag").append("<button class=\"pag_button\" id=\"own_prop_pagUlt\" disabled onclick=\"getOwnProposals_ult()\"></button>");
	initButtonsOwnProposals();
	page_size_prop = pageSize;
	var columns = [
{id: "id", name: "Id.", field: "id",minWidth: 55,width:55,maxWidth: 50,  sortable:true},
{id: "fechaprop", name: "Fecha notificación", field: "fechaprop",minWidth: 140,width:140,maxWidth: 140, sortable:true},
{id: "capa", name: "Capa", field: "capa",minWidth: 80,maxWidth: 100, sortable:true},
{id: "featureid", name: "Objeto", field: "featureid",minWidth: 80,sortable:true},
{id: "comentario", name: "Descripción", field: "comentario",minWidth: 80,sortable:true},
{id: "estado", name: "Estado", field: "estado",minWidth: 80, maxWidth: 100,sortable:true},
{id: "fecharev", name: "Fecha revisión", field: "fecharev",minWidth: 120,width:120,maxWidth: 120, sortable:true},
{id: "comentario_validador", name: "Comentario validador", field: "comentario_validador",minWidth: 80,sortable:true},
{id: "ver", name: "Ver", field: "ver",minWidth: 40,width:40,maxWidth: 40, sortable:false, formatter:verFormatter, cssClass:"centered"}];
	var options = {
			editable: false,
			enableCellNavigation: true,
			enableColumnReorder: true,
			forceFitColumns:true
	};  
	if(parseInt($(".pag_button").css("height"))>39){ // es m�vil
		options = {
				editable: false,
				enableCellNavigation: true,
				enableColumnReorder: true,
				forceFitColumns:true,
				rowHeight: 40
		};
	}
	$(function () {
		dataView_own_prop = new Slick.Data.DataView();

		grid_own_prop = new Slick.Grid("#own_props_tabla", dataView_own_prop, columns, options);
		grid_own_prop.registerPlugin(new Slick.AutoTooltips());
		grid_own_prop.resizeCanvas();
		grid_own_prop.onSort.subscribe(function (e, args) {
			if (args.sortCol.field=="id"){
				orderFieldOwn="pk";
			}
			else{
				orderFieldOwn=args.sortCol.field;
			}
			orderAscOwn=args.sortAsc;
			getOwnProposals(pagina_actual_own_prop,getNotifiedState());

		});

		grid_own_prop.render();

	});

}
function getProposals(page, estado, service){
	showSearching(true);
	var inicio = (page-1) * page_size_prop + 1;


	pagina_actual_prop = page;
	var estado_param="";
	if ((typeof estado!='undefined')&&(estado!=null)){
		estado_param="&estado="+estado;
	}

	$.ajax({
		url: cartoUpdateURL+"?REQUEST=GET_ALL_PROPOSALS&TOKEN="+tokenId+"&inicio="+inicio+"&total="+page_size_prop+"&orderBy="+orderField+"&sortAsc="+orderAsc+estado_param,
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(data){

			$("#tabs").tabs( "enable", 3);
			//	$("#tabs").tabs("option", "active", 2);
			addPropTableData(data, false);
			updatePropPages();
			showSearching(false);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			showSearching(false);
			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido obtener las propuestas "+textStatus +"\n "+errorThrown);}
		}


	});	
}

function getOwnProposals(page,estado,service){
	showSearching(true);
	var inicio = (page-1) * page_size_prop + 1;


	pagina_actual_own_prop = page;
	var estado_param="";
	if ((typeof estado!='undefined')&&(estado!=null)){
		estado_param="&estado="+estado;
	}


	$.ajax({
		url: cartoUpdateURL+"?REQUEST=GET_OWN_PROPOSALS&TOKEN="+tokenId+"&inicio="+inicio+"&total="+page_size_prop+"&orderBy="+orderFieldOwn+"&sortAsc="+orderAscOwn+estado_param,
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(data){

			$("#tabs").tabs( "enable", 2);

			addPropTableData(data, true);
			updateOwnPropPages();
			showSearching(false);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			showSearching(false);
			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido obtener las propuestas "+textStatus +"\n "+errorThrown);}
		}


	});	
}

function updatePropPages(){
	var numPags = Math.ceil(num_props/page_size_prop);
	if (numPags==0){
		pagina_actual_prop=0;
	}
	document.getElementById("prop_pags").innerHTML="P&aacute;gina "+pagina_actual_prop+" de "+numPags;
	$( "#prop_pag1" ).button( "option", "disabled", pagina_actual_prop<=1 );
	$( "#prop_pagPrev" ).button( "option", "disabled", pagina_actual_prop<=1 );
	$( "#prop_pagSig" ).button( "option", "disabled", pagina_actual_prop==numPags);
	$( "#prop_pagUlt" ).button( "option", "disabled", pagina_actual_prop==numPags );

}

function updateOwnPropPages(){
	var numPags = Math.ceil(num_own_props/page_size_prop);
	if (numPags==0){
		pagina_actual_own_prop=0;
	}
	document.getElementById("own_prop_pags").innerHTML="P&aacute;gina "+pagina_actual_own_prop+" de "+numPags;
	$( "#own_prop_pag1" ).button( "option", "disabled", pagina_actual_own_prop<=1 );
	$( "#own_prop_pagPrev" ).button( "option", "disabled", pagina_actual_own_prop<=1 );
	$( "#own_prop_pagSig" ).button( "option", "disabled", pagina_actual_own_prop==numPags);
	$( "#own_prop_pagUlt" ).button( "option", "disabled", pagina_actual_own_prop==numPags );

}


function addPropTableData(datos, own){

	var escaped = $.trim(datos.replace(/[\n]/g, "\\n").replace(/[\r]/g, "\\r"));
	var json =  JSON.parse(escaped);

	if (own){
		data_own_prop = [];
		num_own_props = json.total;
	}
	else{
		data_prop = [];
		num_props = json.total;
	}


	for ( var j = 0; j < json.propuestas.length; j++) {

		var d = (own ? (data_own_prop[j] = {}):(data_prop[j] = {}));
		var prop = json.propuestas[j];
		prop.CAPA = prop.CAPA.replace("_TEMP","");
		//var capa = toc.findItemByAxlID(prop.CAPA);
		var objectid = prop.OBJECTID;
		if (prop.OP=="UPDATE"){
			objectid = prop.NEW_OBJECTID;
		}


		d["id"] = prop.PK;
		d["fechaprop"] = prop.FECHAPROP_STR;
		d["capa"] = prop.CAPA;//capa.caption ;
		if (prop.FEATUREID=="null"){
			d["featureid"] ="" ;
		}	
		else			{
			d["featureid"] = prop.FEATUREID ;
		}

		d["comentario"] = prop.COMENTARIO ;

		d["estado"] = prop.ESTADO ;
		if(prop.ESTADO.indexOf("Pendiente")<0){
			d["fecharev"] = prop.FECHAREV_STR;
		}
		if(own){
			if (prop.COMENTARIO_VALIDADOR=="null"){
				d["comentario_validador"] ="" ;
			}	
			else			{
				d["comentario_validador"] = prop.COMENTARIO_VALIDADOR ;
			}
		}
		else{
			if (prop.NOTIFICADOR=="null"){
				d["notificador"] ="" ;
			}	
			else			{
				d["notificador"] = prop.NOTIFICADOR;
			}
			d["op"] = prop.OP.replace("INSERT","CREACIÓN").replace("UPDATE","MODIFICACIÓN").replace("DELETE","BORRADO");
		}
		if (own){	
			d["ver"] = "\"javascript:showProposedFeature('"+prop.CAPA+((prop.ESTADO=='Validado' && prop.OP!='DELETE') || (prop.OP=='DELETE' && prop.ESTADO!='Validado')?"":"_TEMP")+"','"+prop.CAPA+"','"+objectid+ "',"+(prop.ESTADO!='Descartado' && prop.OP!='DELETE')+","+ (prop.ESTADO=='Validado'?null:"'"+d["comentario"]+"'")+",'"+prop.PK+"',false,false, false)\"";

		}
		else{
			d["ver"] = "\"javascript:showProposedFeature('"+prop.CAPA+((prop.ESTADO=='Validado' && prop.OP!='DELETE') || (prop.OP=='DELETE' && prop.ESTADO!='Validado')?"":"_TEMP")+"','"+prop.CAPA+"','"+objectid+ "',"+(prop.ESTADO!='Descartado')+","+ (prop.ESTADO=='Validado'?null:"'"+d["comentario"]+"'")+",'"+prop.PK+"',false,false,true)\"";

		}

		if (!own){
			d["revisar"] = "\"javascript:showRevisionDialog('"+prop.PK+ "','"+prop.CAPA+"','"+objectid+ "')\"";
		}
		//	d["situar"] = "\"javascript:showFeatureInMap('"+prop.CAPA+"','"+objectid+ "')\"";


	}

	if (own){
		dataView_own_prop.beginUpdate();
		dataView_own_prop.setItems(data_own_prop);
		dataView_own_prop.endUpdate();
		grid_own_prop.updateRowCount();
		grid_own_prop.invalidate();
		grid_own_prop.render();
		var countId="infoCount";
		if (num_own_props==1){
			document.getElementById(countId).innerHTML="1 resultado encontrado";
		}
		else if (num_own_props>0){

			document.getElementById(countId).innerHTML=num_own_props+" resultados encontrados";

		}
		else
			document.getElementById(countId).innerHTML="No hay ninguna propuesta";
	}


	else{
		dataView_prop.beginUpdate();
		dataView_prop.setItems(data_prop);
		dataView_prop.endUpdate();
		grid_prop.updateRowCount();
		grid_prop.invalidate();
		grid_prop.render();
		var countId="validationCount";
		if (num_props==1){
			document.getElementById(countId).innerHTML="1 resultado encontrado";
		}
		else if (num_props>0){

			document.getElementById(countId).innerHTML=num_props+" resultados encontrados";

		}
		else
			document.getElementById(countId).innerHTML="No hay ninguna propuesta";
	}

}



function getValidationState(){
	if (document.getElementById("valPendientes").checked){
		return 0;
	}
	else if (document.getElementById("valValidadas").checked){
		return 1;
	}
	else if (document.getElementById("valDescartadas").checked){
		return 2;
	}
	return null;
}
function getNotifiedState(){
	if (document.getElementById("notifPendientes").checked){
		return 0;
	}
	else if (document.getElementById("notifValidadas").checked){
		return 1;
	}
	else if (document.getElementById("notifDescartadas").checked){
		return 2;
	}
	return null;
}

function showRevisionDialog( pk, capa, objectid){
	highlightProposal(pk);
	var ancho = (isMobile()?MOBILE_WIDTH:600);
	var revDialog=$('<div class="innerWindow"></div>')
	.html(	'<form id="revisionForm" target="_blank" name="revisionForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="clave" name="clave" value="'+pk+'"></input>'+

			'<table><tr><th>Acci&oacute;n </th><td><select id="REQUEST" name="REQUEST" required>'+
			'<option value="VALIDATE">Validar</option>'+
			'<option value="REJECT">Descartar</option>'+

			'</select></td></tr>'+
			'<tr><th>Comentario </th><td><input class="content" type="text" id="comentario" name="comentario" size="80" maxlength="254"  required ></input></td></tr></table>'+
			'<input id="guardarRev" type="submit" value="Guardar">'+
	'</form>')
	.dialog({
		autoOpen: false,
		title: 'Revisar propuesta '+pk,
		width:ancho,
		modal: true,
		close: function(){
			$( this ).dialog( "destroy" );}
	});


	$('#revisionForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#revisionForm').ajaxSubmit({
				timeout:60000,
				success: function(data) { 

					alert("Se han guardado los cambios correctamente");
					if(($("#REQUEST").val()=='VALIDATE')||($("#REQUEST").val()=='OVERWRITE')){
						try{
						var layerIdx = cartoforLayerIdx[capa];
						var layer = toc.getOLLayer(layerIdx);
						var source = layer.getSource();
						var params = source.getParams();
						params.t = new Date().getMilliseconds();
						source.updateParams(params);
						if (wfs_layer){
							if (toc.editingLayer==layerIdx){
								removeWFSLayer();
								showWFSLayer(layer);
							}
						}
						if($("#REQUEST").val()=='VALIDATE'){
						showProposedFeature(capa,null, objectid, false,null,pk,true);
						}
						}catch(err){
							// entrará para formularios que no corresponden a una capa GIS (e.g.: situacion operativa incendios)
							console.log(err);
						}
					}
					getProposals(pagina_actual_prop,getValidationState());

					revDialog.dialog( "close" );
					//revDialog.dialog( "destroy" );


				},
				error: function(jqXHR,  textStatus,  errorThrown) { 
					if (textStatus=="timeout"){
						alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
					}	
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else{
						alert("No se pudieron guardar los cambios ("+errorThrown+")");
						console.log("No se pudieron guardar los cambio. Respuesta servicio:"+jqXHR.responseText);
					}

				}
			});
		}
	});


	revDialog.dialog( "open" );
}

function revisarFormatter(row, cell, value, columnDef, dataContext){
	return (value != null && value.length>0 ? "<a href="+value+"  ><img  src='/lib/IDEAragon/themes/default/images/review.png'></a>" : "");

}

function loadSHP() {
	var layer = toc.getEditingLayer();

	if  (layer == null){
		alert("No hay ninguna capa seleccionada para edición");
		return;
	}
	if (!layer.get("editable")){
		alert("La capa seleccionada no es editable");
		return
	}
	var ancho = (isMobile()?MOBILE_WIDTH:600);
	var shpDialog=$('<div class="innerWindow"></div>')
	.html(	'<form id="shpForm" target="_blank" accept-charset="utf-8" enctype="multipart/form-data" name="shpForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="INSERT_SHP"></input>'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+layer.getSource().getParams().LAYERS+'"></input>'+

			'<table><tr><th>Fichero .shp </th><td><input  required id="fileSHP" type="file" name="fileSHP" /></td></tr>'+
			'<tr><th>Fichero .dbf </th><td><input required id="fileDBF" type="file" name="fileDBF" /></td></tr>'+
			'<tr><th>Fichero .shx </th><td><input  required id="fileSHX" type="file" name="fileSHX" /></td></tr>'+
			'<tr><th>Comentario </th><td><input class="content" type="text" id="comentario" name="comentario" size="80" maxlength="254"  required ></input></td></tr></table>'+
			'<input id="guardarRev" type="submit" value="Guardar">'+
	'</form>')
	.dialog({
		autoOpen: false,
		title: 'Carga de shapefile',
		width:ancho,
		modal: true
	});

	/*$("form#shpForm").submit(function() {

	    var formData = new FormData($(this)[0]);

	    $.post($(this).attr("action"), formData, function(data) {
	        alert(data);
	    });

	    return false;
	});*/
	$('#shpForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#shpForm').ajaxSubmit({
				timeout:60000,
				success: function(data) { 

					alert("Se han guardado los cambios correctamente");
					shpDialog.dialog( "close" );
					shpDialog.dialog( "destroy" );

				},
				error: function(jqXHR,  textStatus,  errorThrown) { 
					if (textStatus=="timeout"){
						alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
					}	
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else{
						alert("No se pudieron guardar los cambios ("+errorThrown+")");
						console.log("No se pudieron guardar los cambios. Respuesta servicio:"+jqXHR.responseText);
					}

				}
			});
		}
	});


	shpDialog.dialog( "open" );
}

function exportShp(){
	if(($("#objectId").val()==null)||($("#objectId").val()=="")){
		alert("Para realizar la exportación debe guardar previamente la propuesta de inserción");
	}
	else{
		var url =  cartoUpdateURL+"?REQUEST=EXPORT&TOKEN="+tokenId+"&layerId="+$("#layerId").val()+"&value="+$("#objectId").val()+"&format=SHP_ESRI&filter=8";
		var form = $('<form></form>').attr('action', url).attr('method', 'post');
		//send request
		form.appendTo('body').submit().remove();

	}

}

function setPhotosCount(layerId, objectId) {
	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&tipo=IMG",
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));

			document.getElementById("photosCount").innerHTML=json.archivos.length;
			$("#photosCount").css("display","block");

		}
	});
}
function setDocsCount(layerId, objectId) {
	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId,
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));

			document.getElementById("docsCount").innerHTML=json.archivos.length;
			$("#docsCount").css("display","block");
		}
	});
}	

function refreshPhotos(layerId,objectId){

	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&tipo=IMG",
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));

			$("#photosCarousel li").remove();
			for ( var j = 0; j < json.archivos.length; j++) {


				var foto = json.archivos[j];
				$("#photosCarousel ul").append(  ' <li><img src="'+foto.url+'" alt=""></li>');

			}
			$('.jcarousel-pagination').jcarouselPagination('reloadCarouselItems');
			$('.jcarousel').jcarousel('reload');
			//$('.jcarousel').jcarousel('scroll', -1);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido refrescar las fotos "+textStatus +"\n "+errorThrown);}
		}


	});	
}
function showPhotos(layer) {
	var objectId = $(".objectId")[0].value;
	showSearching(true);
	if(document.getElementById("guardarBtn")){
		editable = !document.getElementById("guardarBtn").disabled;
		}
		else{
			editable=false;
		}

	var layerId =  layer.replace("_TEMP","");

	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&tipo=IMG",
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));
			var imgs="";

			for ( var j = 0; j < json.archivos.length; j++) {


				var foto = json.archivos[j];
				imgs+=  ' <li><img src="'+foto.url+'" alt=""></li>';

			}
			$('<div id="photosWin" class="innerWindow"></div>')
			.html(	'<div class="jcarousel-wrapper">'+
					' <div id="photosCarousel" class="jcarousel">'+
					'<ul>'+imgs+

					'</ul>'+
					'</div>'+

					'<a href="#" class="jcarousel-control-prev">&lsaquo;</a>'+
					'<a href="#" class="jcarousel-control-next">&rsaquo;</a>'+

					'<p class="jcarousel-pagination">'+

					'</p>'+
					'</div>'+
					'<div id="photosButtons">'+(editable ?'<button id="newPhoto" onclick="addPhoto(\''+layerId+'\')">A&ntilde;adir foto</button>'+
							'<button id="newPhotos" onclick="attachZip(\''+layerId+'\',\'IMG\')">A&ntilde;adir lote</button>':'')+
							'<button id="downloadPhoto" onclick="downloadPhoto()">Descargar foto</button>'+
							'<button id="downloadPhotos" onclick="downloadPhotos(\''+layerId+'\',\')">Descargar todas</button>'+
							(($.inArray( 3, $("#tabs").tabs( "option", "disabled" ))>=0)?'':(editable ?'<button id="delPhoto" onclick="deletePhoto(\''+layerId+'\')">Eliminar foto</button>':''))+
			'</div>')
			.dialog({
				open: function( event, ui ) {
					$('.jcarousel').jcarousel();

					$('.jcarousel-control-prev')
					.on('jcarouselcontrol:active', function() {
						$(this).removeClass('inactive');
					})
					.on('jcarouselcontrol:inactive', function() {
						$(this).addClass('inactive');
					});
					$('.jcarousel-control-prev').jcarouselControl({
						target: '-=1'
					});

					$('.jcarousel-control-next')
					.on('jcarouselcontrol:active', function() {
						$(this).removeClass('inactive');
					})
					.on('jcarouselcontrol:inactive', function() {
						$(this).addClass('inactive');
					});
					$('.jcarousel-control-next').jcarouselControl({
						target: '+=1'
					});

					$('.jcarousel-pagination')
					.on('jcarouselpagination:active', 'a', function() {
						$(this).addClass('active');
					})
					.on('jcarouselpagination:inactive', 'a', function() {
						$(this).removeClass('active');
					})
					.jcarouselPagination();
				},
				close: function(){
					$( this ).dialog( "destroy" );
				},
				autoOpen: true,
				title: 'Fotos',
				width:'auto',
				//width:(big ? 1400:800),
				modal: true
			});
			showSearching(false);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			showSearching(false);
			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido obtener las fotos "+textStatus +"\n "+errorThrown);}
		}


	});	




}


function addPhoto(layerId){
	var ancho = (isMobile()?MOBILE_WIDTH:600);
	var objectId = $(".objectId")[0].value;
	var newPhotoDialog=$('<div class="innerWindow"></div>')
	.html(	'<form id="photoForm"  accept-charset="utf-8" enctype="multipart/form-data" name="photoForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="ATTACH_FILE"></input>'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+layerId+'"></input>'
			+'<input type="hidden" id="objectId" name="objectId" value="'+objectId+'"></input>'
			+'<input type="hidden" id="tipo" name="tipo" value="IMG"></input>'+
			'<input  required id="photoFile" type="file" name="photoFile" accept=".jpg, .png, .jpeg, .gif, .bmp" />'+
			'<input id="guardarPhoto" type="submit" value="A&ntilde;adir">'+
			'<div id="uploading" style="display:none;" ><img border="0" src="/lib/IDEAragon/themes/default/images/loading.gif" alt="Cargando" title="Cargando"></img></div>'+
	'</form>')
	.dialog({
		autoOpen: false,
		title: 'Nueva foto',
		width:ancho,
		modal: true
	});


	$('#photoForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#photoForm').ajaxSubmit({
				timeout:60000,
				beforeSubmit: function(a,f,o) {
					document.getElementById("uploading").style="display:block;";
					var fileName = document.getElementById("photoFile").value.toLowerCase();
					var exts=[".jpg", ".png", ".jpeg", ".gif", ".bmp"];
					return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);

				},
				success: function(data) { 
					document.getElementById("uploading").style="display:none;";

					alert("Se ha generado correctamente la propuesta de incorporación de la foto");
					newPhotoDialog.dialog( "close" );
					newPhotoDialog.dialog( "destroy" );
					var resp = data.split("#");
					$(".objectId").each(function( index ) {
						$( this )[index].value = resp[1];
					});
					$("#REQUEST").val("OVERWRITE");
					$("#clave").val(resp[2]);
					$('.jcarousel ul').append(' <li><img src="'+resp[0]+'" alt=""></li>');
					$('.jcarousel-pagination').jcarouselPagination('reloadCarouselItems');
					$('.jcarousel').jcarousel('reload');
					$('.jcarousel').jcarousel('scroll', -1);
					refreshLayer(layerId);
				},
				error: function(jqXHR,  textStatus,  errorThrown) { 
					document.getElementById("uploading").style="display:none;";
					if (textStatus=="timeout"){
						alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
					}	
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else if (jqXHR.status==412){
						alert("El archivo es demasiado grande");

					}
					else{
						alert("No se pudo añadir la foto ("+errorThrown+")");
						console.log("No se pudo añadir la foto. Respuesta servicio:"+jqXHR.responseText);
					}

				}
			});
		}
	});


	newPhotoDialog.dialog( "open" );
}
function downloadPhoto(){


	var foto = $('.jcarousel').jcarousel('target');
	var enlace =  foto[0].children[0].src.replace('thumbnails/','');
	if (enlace.indexOf(location.host)>0){
		$('#photosButtons').append('<a id="link_foto_donwload" href="'+enlace+'" download></a>');
		document.getElementById('link_foto_donwload').click();
		$('#link_foto_donwload').remove();
	}
	else{
		window.open(enlace,'foto');
	}


}
function downloadPhotos(layerId,objectId){
	window.location.href = cartoUpdateURL+"?REQUEST=DOWNLOAD_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&tipo=IMG";
}
function deletePhoto(layerId){
	var objectId = $(".objectId")[0].value;
	var foto = $('.jcarousel').jcarousel('target');
	if (foto){
		$('<div  class="innerWindow"></div>')
		.html(	'<p>¿Está usted seguro de borrar esta foto?</p>'+
		'<p>Pulse SÍ para para borrar la foto y NO para mantenerla</p>')
		.dialog({
			autoOpen: true,
			close: function(){
				$( this ).dialog( "destroy" );
			},
			buttons: [
			          {
			        	  text: "SÍ",
			        	  click: function() {

			        		  $.ajax({
			        			  url: cartoUpdateURL+"?REQUEST=DELETE_FILE&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&tipo=IMG"+"&url="+escape(foto[0].lastChild.src),
			        			  type: 'POST',
			        			  timeout:cartoUpdateTimeout,

			        			  //async:false,
			        			  success:  function(data){
			        				  foto.remove();
			        				  $('.jcarousel-pagination').jcarouselPagination('reloadCarouselItems');
			        				  $('.jcarousel').jcarousel('reload');
			        				  alert("Se ha generado correctamente la propuesta de eliminación de La foto");


			        			  },
			        			  error:  function(  jqXHR,  textStatus,  errorThrown) { 
			        				  if (textStatus=="timeout"){
			        					  alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
			        				  }	
			        				  else if (jqXHR.status==403){
			        					  alert("Operación no permitida. Conéctese con un usuario válido");

			        				  }
			        				  else{
			        					  alert("No se ha podido eliminar la foto "+textStatus +"\n "+errorThrown);}
			        			  }


			        		  });	
			        		  $( this ).dialog( "close" );
			        	  }
			          },
			          {
			        	  text: "NO",
			        	  click: function() {
			        		  $( this ).dialog( "close" );
			        	  }
			          }],

			          title: 'Cerrar formulario',
			          modal: true
		});

	}
	else{
		alert("No hay ninguna foto");
	}

}

function refreshDocs(layerId, objectId) {

	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId,
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));

			$("#docsList li").remove();
			for ( var j = 0; j < json.archivos.length; j++) {


				var doc = json.archivos[j];
				$("#docsList").append(  ' <li id="doc'+j+'"><a target="_blank" href="'+doc.url+'" alt="">'+doc.descripcion+'</a>'+
						(($.inArray( 3, $("#tabs").tabs( "option", "disabled" ))>=0)?'':'<a href="javascript:deleteDoc(\''+layerId+'\',\''+objectId+'\',\''+doc.URL+'\',\'doc'+j+'\')">	<img src="http://localhost/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png" alt="Pulse para eliminar el documento"></a>')+'</li>');

			}

		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			showSearching(false);
			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido obtener los documentos "+textStatus +"\n "+errorThrown);}
		}


	});	




}
function showDocs(layer) {
	var objectId = $(".objectId")[0].value;
	var layerId =  layer.replace("_TEMP","");
	if(document.getElementById("guardarBtn")){
	editable = !document.getElementById("guardarBtn").disabled;
	}
	else{
		editable=false;
	}
	showSearching(true);
	var ancho = (isMobile()?MOBILE_WIDTH:800);
	$.ajax({
		url:  cartoUpdateURL+"?REQUEST=GET_ATTACHED_FILES&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId,
		type: 'POST',
		timeout:cartoUpdateTimeout,
		contentType: "application/text;charset=iso-8859-1",
		success:  function(datos){
			var json =  JSON.parse($.trim(datos));
			var docs="";

			for ( var j = 0; j < json.archivos.length; j++) {


				var doc = json.archivos[j];
				docs+=  ' <li id="doc'+j+'"><a target="_blank" href="'+doc.url+'" alt="">'+doc.descripcion+'</a>'+
				(($.inArray( 3, $("#tabs").tabs( "option", "disabled" ))>=0)?'':(editable?'<a href="javascript:deleteDoc(\''+layerId+'\',\''+objectId+'\',\''+doc.URL+'\',\'doc'+j+'\')">	<img src="http://localhost/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png" alt="Pulse para eliminar el documento"></a>':''))+'</li>';

			}
			$('<div class="innerWindow"></div>')
			.html(
					'<div><ul id="docsList">'+docs+

					'</ul>'+
					'</div>'+

					'<div id="docsButtons">'+(editable?'<button id="newDoc" onclick="addDoc(\''+layerId+'\',\''+objectId+'\')">A&ntilde;adir documento</button>'+
							'<button id="newDocs" onclick="attachZip(\''+layerId+'\',\''+objectId+'\')">A&ntilde;dir lote</button>':'')+
			'</div>')
			.dialog({

				close: function(){
					$( this ).dialog( "destroy" );
				},
				autoOpen: true,
				title: 'Documentos',
				width:ancho,
				modal: true
			});
			showSearching(false);
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			showSearching(false);
			if (textStatus =='timeout'){

				alert("Se ha superado el tiempo de espera para obtener la información solicitada");

			}
			else if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido obtener los documentos "+textStatus +"\n "+errorThrown);}
		}


	});	




}
function attachZip(layerId,tipo){
	var ancho = (isMobile()?MOBILE_WIDTH:600);
	var objectId = $(".objectId")[0].value;
	var newZipDialog=$('<div class="innerWindow"></div>')
	.html(	'<form id="zipForm"  accept-charset="utf-8" enctype="multipart/form-data" name="zipForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="ATTACH_FILES"></input>'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+layerId+'"></input>'
			+'<input type="hidden" id="objectId" name="objectId" value="'+objectId+'"></input>'
			+'<input type="hidden" id="tipo" name="tipo" value="'+tipo+'"></input>'+
			'<input  required id="zipFile" type="file" name="zipFile" />'+
			'<input id="guardarZip" type="submit" value="A&ntilde;adir">'+
			'<div id="uploading" style="display:none;" ><img border="0" src="/lib/IDEAragon/themes/default/images/loading.gif" alt="Cargando" title="Cargando"></img></div>'+
	'</form>')
	.dialog({
		autoOpen: false,
		close: function(){	newZipDialog.dialog( "destroy" );},
		title: 'Añadir lote',
		width:ancho,
		modal: true
	});


	$('#zipForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#zipForm').ajaxSubmit({
				timeout:60000,
				beforeSubmit: function(a,f,o) {
					document.getElementById("uploading").style="display:block;";
					var fileName = document.getElementById("zipFile").value.toLowerCase();
					var exts=[".zip"];
					var correcto = (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
					if (correcto){
						return correcto;
					}
					else{
						document.getElementById("uploading").style="display:none;";
						alert("El archivo debe ser un .zip");   
					}


				},
				success: function(data) { 
					document.getElementById("uploading").style="display:none;";

					alert("Se han generado correctamente la propuesta de incorporación de los archivos del lote");
					var resp = data.split("#");
					$(".objectId").each(function( index ) {
						$( this )[index].value =resp[0];
					});
					$("#REQUEST").val("OVERWRITE");
					$("#clave").val(resp[1]);
					newZipDialog.dialog( "destroy" );
					if(tipo=='IMG'){				
						refreshPhotos(layerId,resp[0]);
					}
					else{
						refreshDocs(layerId,resp[0]);
					}
				},
				error: function(jqXHR,  textStatus,  errorThrown) { 
					document.getElementById("uploading").style="display:none;";
					if (textStatus=="timeout"){
						alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
					}	
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else if (jqXHR.status==412){
						alert("El lote contiene archivos con extensión no permitida");

					}
					else if (jqXHR.status==413){
						alert("El archivo es demasiado grande");

					}
					else{
						alert("No se pudo añadir el lote ("+errorThrown+")");
						console.log("No se pudo añadir el lote. Respuesta servicio:"+jqXHR.responseText);
					}

				}
			});
		}
	});


	newZipDialog.dialog( "open" );

}

function addDoc(layerId, objectId){
	var ancho = (isMobile()?MOBILE_WIDTH:600);
	var newDocDialog=$('<div class="innerWindow"></div>')
	.html(	'<form id="docForm"  accept-charset="utf-8" enctype="multipart/form-data" name="photoForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="ATTACH_FILE"></input>'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+layerId+'"></input>'
			+'<input type="hidden" id="objectId" name="objectId" value="'+objectId+'"></input>'
			+'<div><label for="descripcion">Descripci&oacute;n del documento:</label><input size="60" maxlength="500" id="descripcion" name="descripcion" required></input></div>'+
			'<div><input  required id="docFile" type="file" name="docFile"/></div>'+
			'<input id="guardarDoc" type="submit" value="Añadir">'+
			'<div id="uploading" style="display:none;" ><img border="0" src="/lib/IDEAragon/themes/default/images/loading.gif" alt="Cargando" title="Cargando"></img></div>'+
	'</form>')
	.dialog({
		autoOpen: false,
		title: 'Nuevo documento',
		width:ancho,
		modal: true
	});


	$('#docForm').validate({
		errorClass: "invalidField",
		submitHandler: function() {
			$('#docForm').ajaxSubmit({
				timeout:60000,
				beforeSubmit: function(a,f,o) {
					document.getElementById("uploading").style="display:block;";
if (withoutExtRestriction){
	return true
}else{
					var fileName = document.getElementById("docFile").value.toLowerCase();
					var exts=[".csv", ".txt", ".pdf", ".xls", ".xlsx", ".doc", ".docx", ".zip", ".rar"];
					if(! (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName)){
						alert('Formato de archivo no soportado. Las extensiones admitidas son:".csv", ".txt", ".pdf", ".xls", ".xlsx", ".doc", ".docx", ".zip", ".rar"');
						document.getElementById("uploading").style="display:none;";
						return false;
					}
					else{
						return true;
					}
}
				},
				success: function(data) {
					
					document.getElementById("uploading").style="display:none;";
					alert("Se ha generado correctamente la propuesta de incorporación del documento");
					var idx = $("#docsList li").length+1;
					$("#docsList").append(  ' <li id="doc'+idx+'"><a href="'+data+'" alt="">'+$("#descripcion").val()+'</a>'+
							(($.inArray( 3, $("#tabs").tabs( "option", "disabled" ))>=0)?'':'<a href="javascript:deleteDoc(\''+layerId+'\',\''+objectId+'\',\''+data+'\',\'doc'+idx+'\')">	<img src="http://localhost/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png" alt="Pulse para eliminar el documento"></a>')+'</li>');

					newDocDialog.dialog( "close" );
					newDocDialog.dialog( "destroy" );
					var resp = data.split("#");
					$(".objectId").each(function( index ) {
						$( this )[index].value = resp[1];
					});
					$("#REQUEST").val("OVERWRITE");
					$("#clave").val(resp[2]);
					refreshLayer(layerId);

				},
				error: function(jqXHR,  textStatus,  errorThrown) { 
					document.getElementById("uploading").style="display:none;";
					if (textStatus=="timeout"){
						alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
					}	
					else if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else if (jqXHR.status==412){
						alert("El archivo es demasiado grande");

					}
					else{
						alert("No se pudo añadir el documento ("+errorThrown+")");
						console.log("No se pudo añadir el documento. Respuesta servicio:"+jqXHR.responseText);
					}

				}
			});
		}
	});


	newDocDialog.dialog( "open" );
}

function deleteDoc(layerId, objectId,url,itemId){


	$('<div  class="innerWindow"></div>')
	.html(	'<p>¿Está usted seguro de borrar este documento?</p>'+
	'<p>Pulse SÍ para para borrar el documento y NO para mantenerl0</p>')
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: [
		          {
		        	  text: "SÍ",
		        	  click: function() {

		        		  $.ajax({
		        			  url: cartoUpdateURL+"?REQUEST=DELETE_FILE&TOKEN="+tokenId+"&layerId="+layerId+"&objectId="+objectId+"&url="+escape(url),
		        			  type: 'POST',
		        			  timeout:cartoUpdateTimeout,

		        			  //async:false,
		        			  success:  function(data){
		        				  $("#"+itemId).remove();
		        				  alert("Se ha generado correctamente la propuesta de eliminación del documento");


		        			  },
		        			  error:  function(  jqXHR,  textStatus,  errorThrown) { 
		        				  if (textStatus=="timeout"){
		        					  alert("Se ha superado el tiempo de espera para obtener el resultado de la operación solicitada");
		        				  }	
		        				  else if (jqXHR.status==403){
		        					  alert("Operación no permitida. Conéctese con un usuario válido");

		        				  }
		        				  else{
		        					  alert("No se ha podido eliminar el documento "+textStatus +"\n "+errorThrown);}
		        			  }


		        		  });	
		        		  $( this ).dialog( "close" );
		        	  }
		          },
		          {
		        	  text: "NO",
		        	  click: function() {
		        		  $( this ).dialog( "close" );
		        	  }
		          }],

		          title: 'Cerrar formulario',
		          modal: true
	});




}


function activateInfoTool(){
	document.getElementById("infoTool").checked=true;
	$("#infoTool").button("refresh");
	showSelTool(true,"info-icon");
	if (modifyControl){
		/*selectControl.getFeatures().forEach(function(feature, i) {
			feature.setStyle(null);
		});*/

		mapOL.removeInteraction(modifyControl);
		if (contextMenuUnion){
			mapOL.removeControl(contextMenuUnion);
		}
		if (contextMenuCorte){
			mapOL.removeControl(contextMenuCorte);
		}
		//wfs_layer.getSource().refresh();
	}
	
		
	if (selectControl){
		if((document.getElementById("dialogFtSelect") ==null)||(typeof document.getElementById("dialogFtSelect")=='undefined')){
			selectControl.getFeatures().clear();
			selectControl.setActive(true);
		}
	}
	if (drawControl){
		drawControl.setActive(false);
	}

	try{
		if (formDialog && !formDialog.dialog('isOpen')){
			formDialog.dialog( "destroy" );
		}
	}
	catch(err){}
}

function checkEditable(capa, objectid){
	$.ajax({
		url: cartoUpdateURL+"?REQUEST=PENDING&TOKEN="+tokenId+"&layerId="+capa+"&objectId="+objectid,
		type: 'GET',
		success:  function(data){
			if (data =="true"){
				alert("Este objeto tiene propuestas pendientes de validación por lo que no se puede modificar hasta que se resuelvan");
			}
			else{
				try{
				document.getElementById("guardarBtn").disabled=false;
				}
				catch(err){}
			}
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 
			if (jqXHR.status==403){
				alert("Operación no permitida. Conéctese con un usuario válido");

			}
			else{
				alert("No se han podido comprobar si el objeto tiene propuestas pendientes de validación "+textStatus +"\n "+errorThrown);}
		}


	});	
}
