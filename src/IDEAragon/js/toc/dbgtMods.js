//---------------------------------------------------------------
//dbgtMods.js v.1.5a
//Copyright (C) 2002,2003 David Bollinger (davebollinger@hotmail.com)

//Support code for the "dbGroupToc" modification - A grouped
//table of contents for ArcIMS 3.1+ HTML viewer sites.

//Notice: This code may be freely distributed, used and
//modified provided that this comment remains intact.
//---------------------------------------------------------------


//this module is optional.

//this module is intended to present a method for simulating
//inheritence in javascript whereby you can customize the
//TOC, GROUP and LAYER objects used by the TOC in order to achieve
//custom behaviour without having to modify dbgtCode.js itself.

//(note that this inheritence trick doesn't really work for
//primitive data types - you either know what that means or you
//don't, sorry, not sure how to further document that!)

//several "common" variants are provided below as a starting point.
//presented roughly in order of increasing complexity.

//the "sample usage" shown below are designed to work on the
//sample 'sanfrancisco' map toc definition, simply replace the
//corresponding code with the sample usage code to try out the
//variation.



//---------------------------------------------------------------------------
//TOCNR
//a toc with no "root" node, if you visually prefer to eliminate the
//root node and the first column of branch connecting lines
//sample usage:
//var toc = new TOCNR('LAYERS','unused root caption',true,'swatch_layers.gif');
//---------------------------------------------------------------------------
function TOCNR(title,caption,autoRefreshMap,swatch) {
	var toc = new TOC(title,caption,autoRefreshMap,swatch);
	inherit(this,toc);

	// override GROUP writeHTML for toc.root
	this.root.writeHTML = function(child_shim, sibling_shim) {
		var s = "";
		for (var i=0, n=this.items.length; i<n; i++) {
			s += this.items[i].writeHTML('','');
		}
		return s;
	};

	return this;
}

var wmsIdxList = new Array();



function LAYERWMS(name,caption,swatch,getlegendgraphic,legend,labelField,linkURL, idxWMS, fun, noMostrar,  usrIdAttribute) {
//	var lyr = new LAYER(name,caption,swatch,legend,labelField,linkURL,usrIdAttribute);
	var layer = createWMSLayer(idxWMS);
	try{
	var lyrSource = layer.getSource();

	lyrSource.on('imageloadstart', function(event) {
		
		//replace with your custom action
		loadingWMSId = "wmsLoading" + idxWMS;
		// compruebo si esta visible ese elemento (puede estar dentro de una carpeta plegada)
		if (document.getElementById(loadingWMSId)) {
			//setTimeout("muestraWMScomoVisible('" + loadingWMSId + "')", 100);
			document.getElementById(loadingWMSId).style.visibility = "visible";
		}
	});

	lyrSource.on('imageloadend', function(event) {
	
		//replace with your custom action
		hideLoadingWMS(idxWMS); 
	});

	lyrSource.on('imageloaderror', function(event) {
		
		//replace with your custom action
		hideLoadingWMS(idxWMS); 
	}); 
	}
	catch(err){
		//nada será grupo de capas (entrada con varios location)
	}
	var lyr = new LAYERWMSOL(name,caption,swatch,getlegendgraphic,legend,labelField,linkURL, layer,idxWMS, fun, noMostrar,  usrIdAttribute);
	inherit(this,lyr);

	return this;

}


function LAYERWMTS(name,caption,swatch,getlegendgraphic,legend,labelField,linkURL,url,idxWMS, capabilities, layerName,style, version,format,gridset, fun, noMostrar,  usrIdAttribute){
	var layer;
	var parser = new ol.format.WMTSCapabilities();
	var result = parser.read(capabilities);
	var options = ol.source.WMTS.optionsFromCapabilities(result,
			{layer: layerName, matrixSet: gridset, format:format});

	layer =new ol.layer.Tile({
		source: new ol.source.WMTS(options)
	});
	layer.set("idxWMS",idxWMS);
	 layer.setVisible(wmsList[idxWMS]["visib"]);
	

	var lyr = new LAYERWMSOL(name,caption,swatch,getlegendgraphic,legend,labelField,linkURL, layer,idxWMS, fun, noMostrar,  usrIdAttribute);
	inherit(this,lyr);
	
	return this;
}

function LAYERWMSOL(name,caption,swatch,getlegendgraphic,legend,labelField,linkURL, layerOL,idxWMS, fun, noMostrar,  usrIdAttribute) {
	var lyr = new LAYER(name,caption,swatch,legend,labelField,linkURL,usrIdAttribute);
	inherit(this,lyr);
	addOLLayer(layerOL,wmsList[idxWMS]["zIdx"]);
	this.layerOL = layerOL;
	this.idxWMS = idxWMS;
	this.noMostrar = noMostrar;
	this.glg = getlegendgraphic;
	wmsIdxList[name] = idxWMS;

	this.fun = fun;

	this.getVisible = function() {
		return wmsList[this.idxWMS]["visib"];
	};

	this.setVisible = function(value) {

		wmsList[this.idxWMS]["visib"] = (value == 1);
		//if ((this.parent!=null)&&(this.parent.iconOpened!=null)){
			if (wmsList[this.idxWMS]["visib"]){
				loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""),this.glg,this.layerOL);
			}
			else{
				removeLayerForLegend(this.tocid);
			}
		//}
	};

	this.toggleVisible = function() {
		wmsList[this.idxWMS]["visib"] = ! wmsList[this.idxWMS]["visib"];
		if ((this.parent!=null)&&(this.parent.iconOpened!=null)){
			if (wmsList[this.idxWMS]["visib"]){
				loadLayerForLegend(this.tocid,this.caption,(this.legend != null? this.legend:""),this.glg,this.layerOL);
			}
			else{
				removeLayerForLegend(this.tocid);
			}
		}
	};

	this.onVisibleClick = function() {

		this.toggleVisible();

		if (this.fun) {
			eval(this.fun + "(" + idxWMS + ")");
		}
	};

	this.init = function() {
		// para que no informe que no existe
	};



	this.writeHTML = function(child_shim, sibling_shim) {
		var s = '';
		if (this.visible) {
			if (! this.noMostrar) {
				// si no estamos a la escala, no pintar
				//if ((mapScaleFactor>=LayerMinScale[this.index]) && (mapScaleFactor<=LayerMaxScale[this.index])) {

				//-------------------
				// START OF LAYER ROW
				//-------------------
				var highlight = (ActiveLayerIndex == this.index) ? ' CLASS="Highlight"' : '';
				s += '<TR' + highlight + '><TD NOWRAP>';

				//-----------------
				// CONNECTING LINES
				//-----------------
				s += child_shim;
				//s += '<IMG class="imgToc" SRC="' + this.icon.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="" >';

				//--------------------
				// VISIBILITY CHECKBOX
				//--------------------

				if (wmsList[this.idxWMS]["visib"]) {
					s += '<a href="#" title="Pulse para ocultar"><IMG class="imgToc" SRC="' + _cache.iconVisible.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para ocultar" border="0"></a>';
				} else {
					s += '<a href="#" title="Pulse para mostrar"><IMG class="imgToc" SRC="' + _cache.iconHidden.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para mostrar" border="0"></a>';
				}

				//-------------
				// SWATCH IMAGE
				//-------------
				if (this.swatch != null) {
					s += '<IMG class="imgToc" SRC="' + this.swatch.src + '"' + _cache.strIconSize + ' onmousedown="toc.onSwatchClick(' + this.tocid + ');" alt="">';
				}

				//---------
				// LINK
				//---------

				/*if (this.linkURL != '') {
					s += '<a href="' + this.linkURL + '" title="Ver Leyenda y Metadato" target="_blank"><img class="imgToc" border="0" src="imagesTOC/icon_layerMD.gif"/></a>';
				} else {
					s += '<img class="imgToc" onmousedown="alert(\'Este contenido no está catalogado\');" border="0" src="imagesTOC/icon_layerNO_MD.gif"/>';
				}*/
				//--------------
				// LAYER CAPTION
				//--------------
				//s += '<A HREF="toc.onCaptionClick(' + this.tocid + ');">' + this.caption + '</A>';
				s += '&nbsp;<LABEL class="textToc">' + this.caption + '</LABEL>';


				//-------------
				// LABEL TOGGLE
				//-------------
				if (this.labelField != "") {
					var labelicon = (this.labelled) ? _cache.iconLabelOn.src : _cache.iconLabelOff.src;
					s += '<IMG class="imgToc" SRC="' + labelicon + '"' + _cache.strIconSize + ' onmousedown="toc.onLabelClick(' + this.tocid + ');" alt="Toggle Labels">';
				}
				s += '&nbsp;<img id="wmsLoading' + this.idxWMS + '" border="0" src="/lib/IDEAragon/themes/default/images/loadingWMS.gif" width="16" height="16" style="visibility:hidden;"/>';

				//-----------------
				// END OF LAYER ROW
				//-----------------
				s += '</TD></TR>';

				//--------------------
				// OPTIONAL LEGEND ROW
				//--------------------
				/*if ((this.legend != null) && (this.legendVisible)) {
					s += '<TR><TD NOWRAP>' + sibling_shim; // connecting lines
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // layer icon
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // visible checkbox
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // active radio
					s += '<IMG class="imgToc" SRC="' + this.legend.src + '" WIDTH="' + this.legend.width + '" HEIGHT="' + this.legend.height + '">'; // line up legend with swatch
					s += '</TD></TR>';
				}*/
				//} // fin de layervisible
			}
		}
		return s;
	};

	return this;

}

function LAYEROL(name,caption,swatch,legend,labelField,linkURL, layerOL, fun, noMostrar,  usrIdAttribute) {
	var lyr = new LAYER(name,caption,swatch,legend,labelField,linkURL,usrIdAttribute);
	inherit(this,lyr);
	this.layerOL = layerOL;
	this.noMostrar = noMostrar;


	this.fun = fun;

	this.getVisible = function() {
		return this.layerOL.getVisible();
	};

	this.setVisible = function(value) {
	
		this.layerOL.setVisible(value);
	};

	this.toggleVisible = function() {
		this.layerOL.setVisible(! this.layerOL.getVisible());
		
	};

	this.onVisibleClick = function() {
		
		this.toggleVisible();
		
	/*	if (this.fun) {
			eval(this.fun + "(" + idxWMS + ")");
		}*/
	};

	this.init = function() {
			// para que no informe que no existe
	};



	this.writeHTML = function(child_shim, sibling_shim) {
		var s = '';
		if (this.visible) {
			if (! this.noMostrar) {
					// si no estamos a la escala, no pintar
				//if ((mapScaleFactor>=LayerMinScale[this.index]) && (mapScaleFactor<=LayerMaxScale[this.index])) {
		
				//-------------------
				// START OF LAYER ROW
				//-------------------
				var highlight = (ActiveLayerIndex == this.index) ? ' CLASS="Highlight"' : '';
				s += '<TR' + highlight + '><TD NOWRAP>';
		
				//-----------------
				// CONNECTING LINES
				//-----------------
				s += child_shim;
				//s += '<IMG class="imgToc" SRC="' + this.icon.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="" >';
		
				//--------------------
				// VISIBILITY CHECKBOX
				//--------------------
		
				if (this.layerOL.getVisible()) {
					s += '<a href="#" title="Pulse para ocultar"><IMG class="imgToc" SRC="' + _cache.iconVisible.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para ocultar" border="0"></a>';
				} else {
					s += '<a href="#" title="Pulse para mostrar"><IMG class="imgToc" SRC="' + _cache.iconHidden.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para mostrar" border="0"></a>';
				}
		
				//-------------
				// SWATCH IMAGE
				//-------------
				if (this.swatch != null) {
					s += '<IMG class="imgToc" SRC="' + this.swatch.src + '"' + _cache.strIconSize + ' onmousedown="toc.onSwatchClick(' + this.tocid + ');" alt="">';
				}
		
				//---------
				// LINK
				//---------
		
				/*if (this.linkURL != '') {
					s += '<a href="' + this.linkURL + '" title="Ver Leyenda y Metadato" target="_blank"><img class="imgToc" border="0" src="imagesTOC/icon_layerMD.gif"/></a>';
				} else {
					s += '<img class="imgToc" onmousedown="alert(\'Este contenido no está catalogado\');" border="0" src="imagesTOC/icon_layerNO_MD.gif"/>';
				}*/
				//--------------
				// LAYER CAPTION
				//--------------
				//s += '<A HREF="toc.onCaptionClick(' + this.tocid + ');">' + this.caption + '</A>';
				s += '&nbsp;<LABEL class="textToc">' + this.caption + '</LABEL>';
		
		
				//-------------
				// LABEL TOGGLE
				//-------------
				if (this.labelField != "") {
					var labelicon = (this.labelled) ? _cache.iconLabelOn.src : _cache.iconLabelOff.src;
					s += '<IMG class="imgToc" SRC="' + labelicon + '"' + _cache.strIconSize + ' onmousedown="toc.onLabelClick(' + this.tocid + ');" alt="Toggle Labels">';
				}
				//s += '&nbsp;<img id="wmsLoading' + this.idxWMS + '" border="0" src="/lib/IDEAragon/themes/default/images/loadingWMS.gif" width="16" height="16" style="visibility:hidden;"/>';
		
				//-----------------
				// END OF LAYER ROW
				//-----------------
				s += '</TD></TR>';
		
				//--------------------
				// OPTIONAL LEGEND ROW
				//--------------------
				/*if ((this.legend != null) && (this.legendVisible)) {
					s += '<TR><TD NOWRAP>' + sibling_shim; // connecting lines
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // layer icon
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // visible checkbox
					s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // active radio
					s += '<IMG class="imgToc" SRC="' + this.legend.src + '" WIDTH="' + this.legend.width + '" HEIGHT="' + this.legend.height + '">'; // line up legend with swatch
					s += '</TD></TR>';
				}*/
			//} // fin de layervisible
			}
		}
		return s;
	};

	return this;

}
//---------------------------------------------------------------------------
//GROUPVL
//a group that acts like a virtual layer - it can't be opened
//and all layer visiblity is synchronized, either all
//visible or all invisible.	no active layer control provided.
//most useful with imagery since active layer control is absent
//(or with feature layers that aren't intended to ever be
//selected as the active layer).	for instance, individual tiles
//of aerial photographs to be treated as a single aggregate
//layer, a set of feature layers containing only label renderers
//grouped in a single "annotation" virtual layer, etc.	assumes
//that all items in the group are LAYERS, doesn't support nested
//groups.	does NOT enforce initial state of all-or-none visible,
//that's controled by your mapservice.
//sample usage:
//var grpPri = grpLoc.addGroup( new GROUPVL('Private',true,'swatch_theaters_agencies.gif') );
//---------------------------------------------------------------------------

function GROUPVL(caption,opened,swatch,legend,linkURL, editable) {
	var group = new GROUP(caption,opened,swatch,legend,linkURL);
	inherit(this,group);
	this.editable=editable;

	this.opened = false;

	// override GROUP toggleOpened
	//
	this.toggleOpened = function() {
		this.opened = false;
	};

	this.setVisible = function(value) {
		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].setVisible(value);
		}
		if (value){
			loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
		}
		else{
			removeLayerForLegend(this.tocid);
		}
	};
	this.toggleVisible = function() {

		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].toggleVisible();
		}
		if (LayerVisible[this.index]){
			loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
		}
		else{
			removeLayerForLegend(this.tocid);
		}
	};
	this.toggleEditable = function() {
		EditingLayer = this.tocid;
	};
	// use a custom icon (since the folder icon would be
	// confusing if you can't actually open it)
	//
	//this.iconOpened = _cache.loadIcon("icon_groupvl.gif");
	//this.iconClosed = this.iconOpened;
	this.iconOpened = null;
	this.iconClosed = null;

	return this;
}

//---------------------------------------------------------------------------
//GROUP1
//a group that only allows (and forces) exactly one layer to be visible.
//for instance, within a thematic group of layers that the user cannot
//turn off but are only logically viewed one at a time, either because
//they obscure each other or cause other map interpretation problems.
//such as historical aerial photography layers, fire districts vs school
//districts vs police districts all drawn as filled polygons and
//completely covering aoi, etc.	assumes that all items in the group
//are LAYERS, doesn't support nested groups.	does NOT enforce initial
//state of all-or-none visible, that's controled by your mapservice.
//sample usage:
//var grpBas = toc.addGroup( new GROUP1('Base Map',true) );
//---------------------------------------------------------------------------

function GROUP1(caption,opened,swatch) {
	var group = new GROUP(caption,opened,swatch);
	inherit(this,group);

	// override GROUP getVisible
	//
	this.getVisible = function() {
		return 1;	// always indicate visible
	};

	// override GROUP setVisible
	//
	this.setVisible = function(value) {
		// nop
	};

	// override GROUP init
	//
	this.init = function() {
		this.zuper.init();
		// override LAYER toggleVisible
		//
		var ltv = function() {
			this.parent.zuper.setVisible(0); // hide all layers in this group
			this.setVisible(1); // show just the one clicked, thus exclusive
		};
		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].toggleVisible = ltv;
		}
	};

	return this;
}

//---------------------------------------------------------------------------
//GROUP01
//a group that only allows zero or one layers to be visible.
//similar to GROUP1 as describe above, except that it supports the
//all-layers-are-hidden state.	a bit trickier than GROUP1 due to
//special handling necessary to overcome tristate conditions.
//sample usage:
//var grpBas = toc.addGroup( new GROUP01('Base Map',true) );
//---------------------------------------------------------------------------

function GROUP01(caption,opened,swatch) {
	var group = new GROUP(caption,opened,swatch);
	inherit(this,group);

	// override GROUP getVisible
	//
	this.getVisible = function() {
		var gvr = this.zuper.getVisible();
		return (gvr==0) ? 0 : 1;	// never return tri-state condition
	};

	// override GROUP setVisible
	//
	this.setVisible = function(value) {
		if (value==0) { // hide, simple case
			this.zuper.setVisible(0);
		} else { // show, tricky case
			var gvr = this.zuper.getVisible();
			if (gvr==0) { // currently all layers hidden, so show only first one
				this.getItem(0).toggleVisible();
			} // else some number of layers already visible (hopefully only one), so do nothing
		}
	};

	// override GROUP init
	//
	this.init = function() {
		this.zuper.init();
		// override LAYER toggleVisible
		//
		var ltv = function() {
			var wasVisible = (this.getVisible() == 1); // current visible state of layer
			this.parent.setVisible(0); // hide all layers in this group
			if (!wasVisible) {
				this.setVisible(1); // show this layer *IFF* it wasn't visible before
			}
		};

		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].toggleVisible = ltv;
		}

	};

	return this;
}

function GROUP01NV(caption,opened,swatch, legend,linkURL) {
	var group = new GROUPNV(caption,opened,swatch,legend,linkURL);
	inherit(this,group);

	// override GROUP getVisible
	//
	this.getVisible = function() {
		var gvr = this.zuper.getVisible();
		return (gvr==0) ? 0 : 1;	// never return tri-state condition
	};

	// override GROUP setVisible
	//
	this.setVisible = function(value) {
		if (value==0) { // hide, simple case
			this.zuper.setVisible(0);
		} else { // show, tricky case
			var gvr = this.zuper.getVisible();
			if (gvr==0) { // currently all layers hidden, so show only first one
				this.getItem(0).toggleVisible();
			} // else some number of layers already visible (hopefully only one), so do nothing
		}
	};

	// override GROUP init
	//
	this.init = function() {
		this.zuper.init();
		// override LAYER toggleVisible
		//
		var ltv = function() {
			var wasVisible = (this.getVisible() == 1); // current visible state of layer
			this.parent.setVisible(0); // hide all layers in this group
			if (!wasVisible) {
				this.setVisible(1); // show this layer *IFF* it wasn't visible before
			}
		};

		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].toggleVisible = ltv;
			//this.items[i].setVisible = funcAux;
		}
	};

	return this;
}

function GROUPVL01(caption,opened,swatch,legend,linkURL) {
	var group = new GROUPVL(caption,opened,swatch,legend,linkURL);
	inherit(this,group);


	this.onVisibleClick = function(value) {
		// esto no es setVisible, sino otro metodo que desactive el resto de grupos y active solo esa capa del Grupo padre
		//this.parent.setVisible(value);
		this.parent.hideRest();
		this.setVisible(value);
	};

	return this;
}

function GROUP01VLNV(caption,opened,swatch,legend,linkURL) {
	var group = new GROUPNV(caption,opened,swatch,legend,linkURL);
	inherit(this,group);

	// override GROUP getVisible
	//
	this.getVisible = function() {
		var gvr = this.zuper.getVisible();
		return (gvr==0) ? 0 : 1;	// never return tri-state condition
	};

	// override GROUP setVisible
	//
	this.setVisible = function(value) {
		if (value==0) { // hide, simple case
			this.zuper.setVisible(0);
		} else { // show, tricky case
			var gvr = this.zuper.getVisible();
			if (gvr==0) { // currently all layers hidden, so show only first one
				this.getItem(0).toggleVisible();
			} // else some number of layers already visible (hopefully only one), so do nothing
		}
	};

	// override GROUP init
	//
	this.init = function() {
		this.zuper.init();

		// override LAYER toggleVisible
		//
		var ltv = function() {
			var wasVisible = (this.getVisible() == 1); // current visible state of layer
			this.parent.setVisible(0); // hide all layers in this group
			if (!wasVisible) {
				this.setVisible(1); // show this layer *IFF* it wasn't visible before
			}
		};

		var funcAux =function() {
			var wasVisible = (this.getVisible() == 1);

			for (var i=0, n=this.parent.items.length; i<n; i++) {
				//alert(this.parent.items[i].caption + ", " + this.parent.items[i].getVisible());

				for (var j=0, nn=this.parent.items[i].items.length; j<nn; j++) {
					this.parent.items[i].items[j].setVisible(0);
				}
			}

			if (!wasVisible) {
				for (var i=0, n=this.items.length; i<n; i++) {
					this.items[i].setVisible(1);
				}
			}
		};


		// chapucilla para obtener el 01 entre layers virtuales
		var funcAux2 =function(value) {
			var wasVisible = (this.getVisible() == 1);

			for (var i=0, n=this.parent.items.length; i<n; i++) {
				//alert(this.parent.items[i].caption + ", " + this.parent.items[i].getVisible());

				//oculto todos los demas grupos, y el actual lo dejo como esta
				if (this.parent.items[i].tocid != this.tocid) {
					for (var j=0, nn=this.parent.items[i].items.length; j<nn; j++) {
						this.parent.items[i].items[j].setVisible(0);
					}
				}
			}
		};


		for (var i=0, n=this.items.length; i<n; i++) {
			this.items[i].toggleVisible = ltv;
			this.items[i].setVisible = funcAux;
			this.items[i].hideRest = funcAux2;
		}
	};

	return this;
}


//---------------------------------------------------------------------------
//GROUPNV
//a group with no visibility checkbox - all visibility toggling
//must be done at the LAYER level.
//sample usage:
//var grpPri = grpLoc.addGroup( new GROUPNV('Private',true,'swatch_theaters_agencies.gif') );
//---------------------------------------------------------------------------

function GROUPNV(caption,opened,swatch,legend,linkURL) {
	var group = new GROUP(caption,opened,swatch,legend,linkURL);
	inherit(this,group);

	this.writeHTML = function(child_shim, sibling_shim) {
		var s = "";

		if (this.visible) {
			//-------------------
			// START OF GROUP ROW
			//-------------------
			s += '<TR><TD NOWRAP VALIGN="TOP">';

			//------------
			// FOLDER ICON
			//------------
			s += child_shim;
			if (this.iconOpened != null) {
				if (this.opened) {
					s += '<a href="#" title="Pulse para plegar"><IMG class="imgToc" SRC="' + this.iconOpened.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="Pulse para plegar" border="0"></a>';
				} else {
					s += '<a href="#" title="Pulse para desplegar"><IMG class="imgToc" SRC="' + this.iconClosed.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="Pulse para desplegar" border="0"></a>';
				}
			}

			//-----------------
			// NO VISIBLE CHECKBOX
			//-----------------
			s += '<IMG class="imgToc" SRC="' + _cache.noicon_visib.src + '"' + _cache.strIconSize + ' border="0">';

			var vis = this.getVisible();


			if (vis==0){
				removeLayerForLegend(this.tocid);
			}
			else{
				loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
			}
			//-------------
			// SWATCH IMAGE
			//-------------
			if (this.swatch != null) {
				s += '<IMG class="imgToc" SRC="' + this.swatch.src + '"' + _cache.strIconSize + ' onmousedown="toc.onSwatchClick(' + this.tocid + ');" alt="">';
			}

			//---------
			// LINK
			//---------

			/*	if (this.linkURL != '') {
				s += '<a href="' + this.linkURL + '" title="Ver Leyenda y Metadato" target="_blank"><img class="imgToc" border="0" src="imagesTOC/icon_layerMD.gif"/></a>';
			} else {
				s += '<img class="imgToc" onmousedown="alert(\'Este contenido no está catalogado\');" border="0" src="imagesTOC/icon_layerNO_MD.gif"/>';
			}*/

			//--------------
			// GROUP CAPTION
			//--------------
			//s += '<A HREF="toc.onCaptionClick(' + this.tocid + ');">' + this.caption + '</A>';
			//s += '&nbsp;<LABEL>' + this.caption + '</LABEL>';
			s += '<label class="textToc" onmousedown="toc.onIconClick(' + this.tocid + ');"> ' + this.caption + '</label>';

			//-----------------
			// END OF GROUP ROW
			//-----------------
			s += '</TD></TR>';

			//--------
			// CASCADE
			//--------
			if (this.opened) {
				for (var i=0, n=this.items.length; i<n; i++) {
					var new_child_shim = sibling_shim + '<img class="imgToc" src="' +
					((i==n-1) ? _cache.iconChildLast.src : _cache.iconChild.src) + '"' + _cache.strIconSize2 + '>';
					var new_sibling_shim = sibling_shim + '<img class="imgToc" src="' +
					((i==n-1) ? _cache.iconBlank.src : _cache.iconSibling.src) + '"' + _cache.strIconSize2 + '>';
					s += this.items[i].writeHTML( new_child_shim, new_sibling_shim );
				}
			}
		}

		return s;
	};

	return this;
}

//-----------------
//ITEM
//-----------------
function ITEM(caption,swatch,linkURL) {
	var group = new GROUP(caption,false,swatch,null,linkURL);
	inherit(this,group);

	this.writeHTML = function(child_shim, sibling_shim) {
		var s = "";
		if (this.visible) {
			s += '<TR><TD NOWRAP VALIGN="TOP">';

			s += child_shim;

			if (this.linkURL != '') {
				s += '<a class="textToc" href="' + this.linkURL + '" title="Ver" target="_blank"><img border="0" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_layerNO_MD.gif"/>' + this.caption + '</a>';
			}

			s += '</TD></TR>';

			if (this.opened) {
				for (var i=0, n=this.items.length; i<n; i++) {
					var new_child_shim = sibling_shim + '<img src="' +
					((i==n-1) ? _cache.iconChildLast.src : _cache.iconChild.src) + '"' + _cache.strIconSize2 + '>';
					var new_sibling_shim = sibling_shim + '<img src="' +
					((i==n-1) ? _cache.iconBlank.src : _cache.iconSibling.src) + '"' + _cache.strIconSize2 + '>';
					s += this.items[i].writeHTML( new_child_shim, new_sibling_shim );
				}
			}
		}

		return s;
	};

	return this;
}


//---------------------------------------------------------------------------
//UTILITY
//---------------------------------------------------------------------------

function inherit(sub,zuper) {
	sub.zuper = zuper;
	for (var i in zuper) {
		sub[i] = zuper[i];
	}
	return sub;
}


