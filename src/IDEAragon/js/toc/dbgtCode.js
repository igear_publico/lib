//---------------------------------------------------------------
// dbgtCode.js v.1.5a
// Copyright (C) 2002,2003 David Bollinger (davebollinger@hotmail.com)
//
// Support code for the "dbGroupToc" modification - A grouped
// table of contents for ArcIMS 3.1+ HTML viewer sites.
//
// Notice: This code may be freely distributed, used and
//				 modified provided that this comment remains intact.
//---------------------------------------------------------------

okToSend = true;

function noMetadata() {
	alert("No existe metadato");
}

//------------------
// _TOC_ID_GENERATOR
//------------------

function _TOC_ID_GENERATOR() {
	this.nextID = 0;
	this.getID = function() {
		var id = this.nextID;
		this.nextID = this.nextID + 1;
		return id;
	};
}
var _idgen = new _TOC_ID_GENERATOR();

//-----------------
// _TOC_IMAGE_CACHE
//-----------------

function _TOC_IMAGE_CACHE(icon_size) {
	this.iconPath = '/lib/IDEAragon/themes/default/images/imagesTOC/';									// path to icon images
	this.swatchPath = '/IDEAragon/lib/themes/default/images/imagesTOC/';								// path to swatch images
	this.legendPath = '/lib/IDEAragon/legends/';								// path to legend images

	this.strIconSize = ' WIDTH="'+icon_size+'" HEIGHT="'+icon_size+'" ';
	this.strIconSize2 = ' WIDTH="36" HEIGHT="16" ';

	this.loadImage = function(path,file) {
		if ((file || '') != '') {
			var img = new Image();
			img.src = (file.indexOf("http:")>=0 ?'':path) + file;
			return img;
		} else {
			return null;
		}
	};
	this.loadIcon = function(file) { return this.loadImage(this.iconPath,file); };
	this.loadSwatch = function(file) { return this.loadImage(this.swatchPath,file); };
	this.loadLegend = function(file) { 
		if ((file || '') != '') {
		return (file.indexOf("http:")>=0 ?'':this.legendPath) + file;
		} else {
			return null;
		}
		//return this.loadImage(this.legendPath,file); 
		};

	this.iconPixel = this.loadIcon('icon_pixel.gif');					// single transparent pixel
	this.iconBlank = this.loadIcon('icon_blank.gif');					// all transparent placeholder
	this.iconClosed = this.loadIcon('icon_closed.png');				// closed group folder
	this.iconOpened = this.loadIcon('icon_opened.png');				// opened group folder
	this.iconChild = this.loadIcon('icon_child.gif');					// connecting line to child item (continued)
	this.iconChildLast = this.loadIcon('icon_childlast.gif');	// connecting line to child item (last one)
	this.iconSibling = this.loadIcon('icon_sibling.gif');			// vertical connector to sibling item
	//this.iconLabelOn = this.loadIcon('icon_labelon.gif');			// labelling enabled
	//this.iconLabelOff = this.loadIcon('icon_labeloff.gif');		// labelling disabled
	//this.iconLayer = this.loadIcon('icon_layer.gif');					// map layer icon
	this.iconHidden = this.loadIcon('icon_hidden.png');				// layer not visible, unselected checkbox
	this.iconVisible = this.loadIcon('icon_visible.png');			// layer visible, selected checkbox
	//this.iconVisscale = this.loadIcon('icon_visscale.gif');		// layer visible, but not at this scale
	this.iconTristate = this.loadIcon('icon_tristate.png');		// tri-state checkbox, some layers visible
	this.iconSelected = this.loadIcon('icon_selected.png');				// selected radio
	this.iconUnselected = this.loadIcon('icon_unselected.png');		// unselected radio
	//this.iconHelp = this.loadIcon('icon_help.gif');						// not currently used
	this.noicon_visib = this.loadIcon('noicon_visib.gif');		// layer not visible

}
var _cache = new _TOC_IMAGE_CACHE(16);


//-----------------
// TOC
//-----------------

function TOC(title,caption,autoRefreshMap,swatch, includeConfigButton, includeLoadButtons) {
	// PROPERTIES
	//this.title = title || 'LAYERS';
	//this.caption = caption || title || 'LAYERS';
	this.title = title || '';
	this.caption = caption || title || '';
	this.root = new GROUP(caption,true,swatch);
	this.isInited = false;
	this.divToc = null;
	this.divTocHelp = null;
	this.autoRefreshMap = autoRefreshMap || false;
	this.LayersGroups = new Array();
	this.includeConfigButton = includeConfigButton || false;
	this.includeLoadButtons = includeLoadButtons || false;
}

_TOC = TOC.prototype;

// EVENTS

_TOC.onIconClick = function(tocid) {
	//ontop('tocWin');

	var item = this.root.findItemByTocID(tocid);
	item.onIconClick();
	this.refresh();
	return false;
};

_TOC.onVisibleClick = function(tocid,value) {
//	ontop('tocWin');

	var item = this.root.findItemByTocID(tocid);
	item.onVisibleClick(value);
	this.refresh();
	this.refreshMap();
	return false;
};
_TOC.toggleEditable = function(tocid) {
//	ontop('tocWin');
	EditingLayer=tocid;
	this.refresh();
	
	return false;
};

_TOC.onActiveClick = function(tocid) {
	var item = this.root.findItemByTocID(tocid);
	item.onActiveClick();
	this.refresh();
	return false;
};

_TOC.onSwatchClick = function(tocid) {
	var item = this.root.findItemByTocID(tocid);
	item.onSwatchClick();
	this.refresh();
	return false;
};

_TOC.onCaptionClick = function(tocid) {
	var item = this.root.findItemByTocID(tocid);
	item.onCaptionClick();
	this.refresh();
};

_TOC.onLabelClick = function(tocid) {
	var item = this.root.findItemByTocID(tocid);
	item.onLabelClick();
	this.refresh();
	return false;
};


_TOC.findItemByAxlID = function(axlid) {
	if (this.axlid == axlid) {
		return this;
	}
	for (var i=0, n=this.root.items.length; i<n; i++) {
		var item = this.root.items[i].findItemByAxlID(axlid);
		if (item) return item;
	}
	return null;
};

// METHODS

_TOC.init = function() {
	if (LayerName.length == 0) { return; }
	if (this.root.items.length == 0) {
		var addTo;
		for (var i=0, n=LayerName.length; i<n; i++) {
			addTo = this;
			if (this.LayersGroups) {
				if ((this.LayersGroups.length > i) && (this.LayersGroups[i] != '')) {
					addTo = this.root.findItemByAxlID(this.LayersGroups[i]);
					if (addTo == null) {
						addTo = this.root.addGroup( new GROUP( this.LayersGroups[i], true, null ) );
					}
				}
			}
			addTo.addLayer( new LAYER(LayerID[i], LayerName[i], null, null, null) );
		}
	}
	this.root.init();
	this.isInited = true;
};

_TOC.setOutput = function(divToc, divTocHelp) {
	this.divToc = divToc;
	this.divTocHelp = divTocHelp;
};

_TOC.addItem = function(item) {
	return this.root.addItem(item);
};


_TOC.findItemByCaption = function(caption) {
	return this.root.findItemByCaption(caption);
};

_TOC.getEditingLayer = function(){
	if (typeof EditingLayer != 'undefined'){
	return this.root.findItemByTocID(EditingLayer);
	}
	else{
		return null;
	}
};
_TOC.addGroup = _TOC.addItem;
_TOC.addLayer = _TOC.addItem;

_TOC.refresh = function() {
	if (this.divToc != null) {
		$("#toc_control *").remove();
		$("#toc_control").append(toc.writeHTML());
		refreshTOCAccordion();
	}
	if (this.divTocHelp != null) {
		this.divTocHelp.innerHTML = this.writeHelpHTML();
	}
};

_TOC.refreshMap = function() {
	if ((this.autoRefreshMap) & (okToSend)) {
		//sendMapXML();
		getMapWithCurrentExtent();
	}
};

_TOC.autoRefreshClick = function() {
	this.autoRefreshMap = ! this.autoRefreshMap;
};

_TOC.writeHTML = function() {
	var s = "";
	if (!this.isInited)	{ this.init();}
	if (!this.isInited) { return ''; }

	//------
	// TITLE
	//------
	if (this.title != '') {
		s += '<CENTER><P CLASS="LayerListTitle">' + this.title + '<BR>';
	}

	s += '<center><img class="updateMap" src="/lib/IDEAragon/themes/default/images/imagesTOC/updateMap.png" border="0" onclick="getMapWithCurrentExtent()" alt="Actualizar mapa" style="padding-top:5px;padding-bottom:5px"></center>';
	if (this.includeLoadButtons) {
		s += '<div title="Carga de GPX" onclick="showGPXDialog()" style="cursor:pointer;float:right; margin-right: 10px;margin-top:1px;border:solid 1px #0079B1; color:#0079B1; padding:2px; font-weight: bold;">GPX</div>';
		s += '<div title="Carga de GML" onclick="showGMLDialog()" style="cursor:pointer;float:right; margin-right: 10px;margin-top:1px;border:solid 1px #0079B1; color:#0079B1; padding:2px; font-weight: bold;">GML</div>';

		s += '<div title="Carga de GeoJSON" onclick="showGeoJSONDialog()" style="cursor:pointer;float:right; margin-right: 10px;margin-top:1px;border:solid 1px #0079B1; color:#0079B1; padding:2px; font-weight: bold;">GeoJSON</div>';

		s += '<div title="Carga de KML" onclick="showKMLDialog()" style="cursor:pointer;float:right; margin-right: 10px;margin-top:1px;border:solid 1px #0079B1; color:#0079B1; padding:2px; font-weight: bold;">KML</div>';

	}
	if (this.includeConfigButton) {
		s += '<div style="float:right; margin-right: 10px"><i class="fa fa-cog fa-2x" onclick="configureTOC()"></i></div>';
	}

	//-------------------
	// START OF TOC TABLE
	//-------------------
	s += '<TABLE WIDTH="100%"	BORDER="0" CELLSPACING="0" CELLPADDING="0" NOWRAP>';

	//--------
	// CASCADE
	//--------
	s += this.root.writeHTML('','');

	//-----------------
	// END OF TOC TABLE
	//-----------------
	//s += '</TABLE><BR></CENTER>\n';

	s += '</TABLE>\n';

	s += '<br/><center><input type="checkbox" id="autoMapUpd" class="customCheckBox" onclick="toc.autoRefreshClick()" ';
	if (this.autoRefreshMap) {
		s += ' checked ';
	}
	s+= '	alt="Actualizaci&oacute;n autom&aacute;tica"><label for="autoMapUpd">Actualizaci&oacute;n autom&aacute;tica</label></center>';

	s += '<br/><center><img class="updateMap" src="/lib/IDEAragon/themes/default/images/imagesTOC/updateMap.png" border="0" onclick="getMapWithCurrentExtent()" alt="Actualizar mapa"></center>';
	if (this.title != '') {
		s += '</CENTER>\n';
	}

	return s;
};

_TOC.writeHelpHTML = function() {
	var s = '';
	return s;
};

//-----------------
// GROUP
//-----------------

function GROUP(caption,opened,swatch,legend,linkURL) {
	// PROPERTIES
	this.parent = null;
	this.tocid = _idgen.getID();
	this.caption = caption || '';
	this.axlid = this.caption;
	this.opened = opened || false;
	this.items = new Array();
	this.iconOpened = _cache.iconOpened;
	this.iconClosed = _cache.iconClosed;
	this.swatch = _cache.loadSwatch(swatch);
	this.linkURL = linkURL || '';
	this.legend = _cache.loadLegend(legend);
	this.visible = true;
};

_GROUP = GROUP.prototype;

// EVENTS

_GROUP.onIconClick = function() {
	this.toggleOpened();
};

_GROUP.onVisibleClick = function(value) {
	this.setVisible(value);
};

_GROUP.onActiveClick = function() {
	// nop
};

_GROUP.onSwatchClick = function() {
	// nop
};

_GROUP.onCaptionClick = function() {
	this.toggleOpened();
};

_GROUP.onLabelClick = function() {
	// nop
};

// METHODS

_GROUP.init = function() {
	for (var i=0, n=this.items.length; i<n; i++) {
		this.items[i].init();
	}
};

_GROUP.findItemByCaption = function(caption) {
	if (this.caption == caption) {
		return this;
	}
	for (var i=0, n=this.items.length; i<n; i++) {
		var item = this.items[i].findItemByCaption(caption);
		if (item) return item;
	}
	return null;
};

_GROUP.findItemByAxlID = function(axlid) {
	if (this.axlid == axlid) {
		return this;
	}
	for (var i=0, n=this.items.length; i<n; i++) {
		var item = this.items[i].findItemByAxlID(axlid);
		if (item) return item;
	}
	return null;
};

_GROUP.findItemByTocID = function(tocid) {
	if (this.tocid == tocid) {
		return this;
	}
	for (var i=0, n=this.items.length; i<n; i++) {
		var item = this.items[i].findItemByTocID(tocid);
		if (item) {
			return item;
		}
	}
	return null;
};

_GROUP.addItem = function(item) {
	item.parent = this;
	this.items[this.items.length] = item;
	return item;
};

_GROUP.addLayer = _GROUP.addItem;
_GROUP.addGroup = _GROUP.addItem;

_GROUP.getItem = function(index) {
	return this.items[index];
};

_GROUP.toggleOpened = function() {
	this.opened = !this.opened;
};

_GROUP.setActive = function() {
	// nop
	// though might be useful as part of a modified identify-all tool
};

_GROUP.toggleLabel = function() {
	for (var i=0, n=this.items.length; i<n; i++) {
		this.items[i].toggleLabel();
	}
};

_GROUP.getVisible = function() {
	for (var i=0, n=this.items.length, v=0; i<n; i++) {
		v += this.items[i].getVisible();
	}
	return (v==0) ? 0 : (v==n) ? 1 : 99999; // 99999=tristate
};

_GROUP.setVisible = function(value) {
	for (var i=0, n=this.items.length; i<n; i++) {
		this.items[i].setVisible(value);
	}
};

_GROUP.toggleVisible = function() {
	for (var i=0, n=this.items.length; i<n; i++) {
		this.items[i].toggleVisible();
	}
};

_GROUP.writeHTML = function(child_shim, sibling_shim) {
	var s = "";

	if (this.visible) {
		//-------------------
		// START OF GROUP ROW
		//-------------------
		s += '<TR><TD NOWRAP VALIGN="TOP">';
	
		//------------
		// FOLDER ICON
		//------------
		s += child_shim;
		if (this.iconOpened != null) {
			if (this.opened) {
				s += '<a href="#" title="Pulse para plegar"><IMG class="imgToc" SRC="' + this.iconOpened.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="Pulse para plegar" border="0"></a>';
			} else {
				s += '<a href="#" title="Pulse para desplegar"><IMG class="imgToc" SRC="' + this.iconClosed.src + '"' + _cache.strIconSize + ' onmousedown="toc.onIconClick(' + this.tocid + ');" alt="Pulse para desplegar" border="0"></a>';
			}
		}
	
		//-----------------
		// VISIBLE CHECKBOX
		//-----------------
		var vis = this.getVisible();
		if (vis == 0) {
			s += '<a href="#" title="Pulse para mostrar"><IMG class="imgToc" SRC="' + _cache.iconHidden.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ',1);" alt="Pulse para mostrar" border="0"></a>';
		} else if (vis == 1) {
			s += '<a href="#" title="Pulse para ocultar"><IMG class="imgToc" SRC="' + _cache.iconVisible.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ',0);" alt="Pulse para ocultar" border="0"></a>';
		
		} else {
			s += '<a href="#" title="Pulse para mostrar"><IMG class="imgToc" SRC="' + _cache.iconTristate.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ',1);" alt="Pulse para mostrar" border="0"></a>';
		}
	
		if (vis==0){
			removeLayerForLegend(this.tocid);
		}
		else{
			loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
		}
		
		if (this.editable) {
			if ((typeof EditingLayer != 'undefined')&&(EditingLayer ==this.tocid)){
				s += '<a href="#" ><IMG class="imgToc" SRC="' + _cache.iconSelected.src + '"' + _cache.strIconSize + ' border="0"></a>';
			} else {
				s += '<a href="#" title="Pulse para editar"><IMG class="imgToc" SRC="' + _cache.iconUnselected.src + '"' + _cache.strIconSize + ' onmousedown="toc.toggleEditable(' + this.tocid + ');" alt="Pulse para editar" border="0"></a>';
			}
		}
		//-------------
		// SWATCH IMAGE
		//-------------
		if (this.swatch != null) {
			s += '<IMG class="imgToc" SRC="' + this.swatch.src + '"' + _cache.strIconSize + ' onmousedown="toc.onSwatchClick(' + this.tocid + ');" alt="">';
		}
	
		//---------
		// LINK
		//---------
	
	
		/*if (this.linkURL != '') {
			s += '<a href="' + this.linkURL + '" title="Haga click para abrir el metadato " target="_blank"><img class="imgToc" border="0" src="imagesTOC/icon_layerMD.gif"/></a>';
		} else {
			s += '<img class="imgToc" onmousedown="alert(\'Este contenido no está catalogado\');" border="0" src="imagesTOC/icon_layerNO_MD.gif"/>';
		}*/
	
		//--------------
		// GROUP CAPTION
		//--------------
		//s += '<A HREF="toc.onCaptionClick(' + this.tocid + ');">' + this.caption + '</A>';
		//s += '&nbsp;<LABEL>' + this.caption + '</LABEL>';
		s += '<label class="textToc" onmousedown="toc.onIconClick(' + this.tocid + ');"> ' + this.caption + '</label>';
	
		//-----------------
		// END OF GROUP ROW
		//-----------------
		s += '</TD></TR>';
	
		//--------
		// CASCADE
		//--------
		if (this.opened) {
			for (var i=0, n=this.items.length; i<n; i++) {
				var new_child_shim = sibling_shim + '<img class="imgToc" src="' +
					((i==n-1) ? _cache.iconChildLast.src : _cache.iconChild.src) + '"' + _cache.strIconSize2 + '>';
				var new_sibling_shim = sibling_shim + '<img class="imgToc" src="' +
					((i==n-1) ? _cache.iconBlank.src : _cache.iconSibling.src) + '"' + _cache.strIconSize2 + '>';
				s += this.items[i].writeHTML( new_child_shim, new_sibling_shim );
			}
		}
	}
	return s;
};

//-----------------
// LAYER
//-----------------

function LAYER(name,caption,swatch,legend,labelField,linkURL, usrIdAttribute) {
	// PROPERTIES
	this.parent = null;
	this.tocid = _idgen.getID();
	this.name = name || '';
	this.axlid = this.name;
	this.caption = caption || name || '';
	this.index = -1;
	this.icon = _cache.iconLayer;
	this.swatch = _cache.loadSwatch(swatch);
	this.legend = _cache.loadLegend(legend);
	this.legendVisible = false;
	this.labelField = labelField || '';
	this.labelled = false;
	this.linkURL = linkURL || '';
	this.usrIdAttribute=usrIdAttribute;
	this.visible = true;
};

_LAYER = LAYER.prototype;

// EVENTS

_LAYER.onIconClick = function() {
	this.setActive();
};

_LAYER.onVisibleClick = function(value) {
	this.toggleVisible();
};

_LAYER.onActiveClick = function() {
	this.setActive();
};

_LAYER.onSwatchClick = function() {
	this.legendVisible = !this.legendVisible;
};

_LAYER.onCaptionClick = function() {
	this.setActive();
};

_LAYER.onLabelClick = function() {
	this.toggleLabel();
};

// METHODS

_LAYER.init = function() {
	for (var i=0, n=LayerID.length; i<n; i++) {
		if (LayerID[i] == this.name) {
			this.index = i;
			return;
		}
	}
	for (var i=0, n=LayerName.length; i<n; i++) {
		if (LayerName[i] == this.name) {
			this.index = i;
			return;
		}
	}
	if (debugOn > 0) {
		alert('Possible error in TOC definition.\nUnable to get layer index for "' + this.name + '".\nCheck TOC definition in dbgtData.js.');
	}
};

_LAYER.findItemByCaption = function(caption) {
	if (this.caption == caption) {
		return this;
	}
	return null;
};

_LAYER.findItemByAxlID = function(axlid) {
	if (this.axlid == axlid) {
		return this;
	}
	return null;
};

_LAYER.findItemByTocID = function(tocid) {
	if (this.tocid == tocid) {
		return this;
	}
	return null;
};

_LAYER.setActive = function() {
};

_LAYER.toggleLabel = function() {
	this.labelled = !this.labelled;
};

_LAYER.getVisible = function() {
	return LayerVisible[this.index];
};

_LAYER.setVisible = function(value) {
	LayerVisible[this.index] = value;
	if ((this.parent!=null)&&(this.parent.iconOpened!=null)){
		if (value){
			loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
		}
		else{
			removeLayerForLegend(this.tocid);
		}
	}
};

_LAYER.toggleEditable = function() {
	EditingLayer = this.index;
};

_LAYER.toggleVisible = function() {
	LayerVisible[this.index] = 1 - LayerVisible[this.index];
	if ((this.parent!=null)&&(this.parent.iconOpened!=null)){
		if (LayerVisible[this.index]){
			loadLayerForLegend(this.tocid,this.caption, (this.legend != null? this.legend:""));
		}
		else{
			removeLayerForLegend(this.tocid);
		}
		}
};

_LAYER.writeHTML = function(child_shim, sibling_shim) {
	var s = '';

	if (this.visible) {
		// si no estamos a la escala, no pintar
		//if ((mapScaleFactor>=LayerMinScale[this.index]) && (mapScaleFactor<=LayerMaxScale[this.index])) {
	
		//-------------------
		// START OF LAYER ROW
		//-------------------
		var highlight = (ActiveLayerIndex == this.index) ? ' CLASS="Highlight"' : '';
		s += '<TR' + highlight + '><TD NOWRAP>';
	
		//-----------------
		// CONNECTING LINES
		//-----------------
		s += child_shim;
	
		if (LayerVisible[this.index]) {
			s += '<a href="#" title="Pulse para ocultar"><IMG class="imgToc" SRC="' + _cache.iconVisible.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para ocultar" border="0"></a>';
		} else {
			s += '<a href="#" title="Pulse para mostrar"><IMG class="imgToc" SRC="' + _cache.iconHidden.src + '"' + _cache.strIconSize + ' onmousedown="toc.onVisibleClick(' + this.tocid + ');" alt="Pulse para mostrar" border="0"></a>';
		}
		
		//-------------
		// SWATCH IMAGE
		//-------------
		if (this.swatch != null) {
			s += '<IMG class="imgToc" SRC="' + this.swatch.src + '"' + _cache.strIconSize + ' onmousedown="toc.onSwatchClick(' + this.tocid + ');" alt="">';
		}
	
		//---------
		// LINK
		//---------
	
		/*if (this.linkURL != '') {
			s += '<a href="' + this.linkURL + '" title="Ver Leyenda y Metadato" target="_blank"><img class="imgToc" border="0" src="imagesTOC/icon_layerMD.gif"/></a>';
		} else {
			s += '<img class="imgToc" onmousedown="alert(\'Este contenido no está catalogado\');" border="0" src="imagesTOC/icon_layerNO_MD.gif"/>';
		}*/
		//--------------
		// LAYER CAPTION
		//--------------
		//s += '<A HREF="toc.onCaptionClick(' + this.tocid + ');">' + this.caption + '</A>';
		s += '&nbsp;<LABEL class="textToc">' + this.caption + '</LABEL>';
	
	
		//-------------
		// LABEL TOGGLE
		//-------------
		if (this.labelField != "") {
			var labelicon = (this.labelled) ? _cache.iconLabelOn.src : _cache.iconLabelOff.src;
			s += '<IMG class="imgToc" SRC="' + labelicon + '"' + _cache.strIconSize + ' onmousedown="toc.onLabelClick(' + this.tocid + ');" alt="Toggle Labels">';
		}
	
		//-----------------
		// END OF LAYER ROW
		//-----------------
		s += '</TD></TR>';
	
		//--------------------
		// OPTIONAL LEGEND ROW
		//--------------------
	/*	if ((this.legend != null) && (this.legendVisible)) {
			s += '<TR><TD NOWRAP>' + sibling_shim; // connecting lines
			s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // layer icon
			s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // visible checkbox
			s += '<IMG class="imgToc" SRC="' + _cache.iconBlank.src + '"' + _cache.strIconSize + '>'; // active radio
			s += '<IMG class="imgToc" SRC="' + this.legend.src + '" WIDTH="' + this.legend.width + '" HEIGHT="' + this.legend.height + '">'; // line up legend with swatch
			s += '</TD></TR>';
		}*/
		//} // fin de layervisible
	}
	return s;
};


//-----------------
// eof
//-----------------


function configureTOC() {
	var htmlCode = '<ul>';
	for (var i=0, n=toc.root.items.length; i<n; i++) {
		firstLevelGroup = toc.root.items[i];
		if (firstLevelGroup.caption != "") {
			htmlCode += '<li style="clear:both;float:left;">'
			htmlCode += '<input id="config_toc_' + i + '" type="checkbox" style="margin-right:3px;" ';
			if (firstLevelGroup.visible) {
				htmlCode += 'checked ';
			}
			htmlCode += '>';
			htmlCode += firstLevelGroup.caption;
			htmlCode += '</li>';
		}
	}
	htmlCode += '</ul>';

	var dialog= $('<div></div>')
	.html(htmlCode)
	.dialog({
		autoOpen: true,
		close: function(){
			$( this ).dialog( "destroy" );
		},
		buttons: {
			Aceptar: function() {
	      for (var i=0, n=toc.root.items.length; i<n; i++) {
					console.log(toc.root.items[i].caption + $("#config_toc_" + i).prop("checked"));
					toc.root.items[i].visible = $("#config_toc_" + i).prop("checked");
				}
				toc.refresh();
				$( this ).dialog( "close" );
			},
			Cancelar: function() {
				$( this ).dialog( "close" );
			}
		},
		width: 600,
		title: 'Actualizar tabla de contenidos' ,
		modal: true
	});
}
