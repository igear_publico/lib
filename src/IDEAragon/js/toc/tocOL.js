function initToc(reverse){
	 $( "#transpTocSlider" ).slider({
		 min: 0,
		 max: 100,
		 step: 1,
		 slide: function( event, ui ) {
			 toc.group.layerGroup.setOpacity((100-ui.value)/100);
		 },
		 change: function( event, ui ) {
			 toc.group.layerGroup.setOpacity((100-ui.value)/100);
			// $(".tocLayer").css("opacity",(100-ui.value)/100);
			 showTranspWarning(ui.value) ;
			 
			 
		 }
		 });
	toc.init();
	toc.writeTOC(reverse);
}

function TOC(divTableId,oneVisLayer) {

	this.tableToc = divTableId;
	this.group = new GROUP('toc',true,null,null,null,null,oneVisLayer);
	this.editingLayer=null;
}

_TOC = TOC.prototype;

_TOC.getEditingLayer = function(){
	if (this.editingLayer==null){
		return null;
	}
	else{
		return this.getOLLayer(this.editingLayer);
	}
	
};

_TOC.addGroup= function(group,index) {
	this.group.addGroup(group,index);
	/*$("#"+tableToc).append('<tr><td nowrap="" valign="TOP"><a href="#" title="Pulse para desplegar"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png" onmousedown="toc.onCollapseClick('+this.layers.length+');" alt="Pulse para desplegar" border="0" height="16" width="16"></a><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/noicon_visib.gif" border="0" height="16" width="16"><label class="textToc" onCollapseClick('+this.layers.length+');"> <b>'+OLGroup.get("caption")+'</b></label></td></tr>');
	this.layers.push(OLGroup);*/
}; 

_TOC.addWMSLayer= function(caption, url, layers,styles, version,format, visible, opacity, tiled,gfi, editable, getLegendGraphic,legendURL, minScale, hidden,cql_filter,clustered,wfs_color) {
	return this.group.addWMSLayer(caption, url, layers,styles, version,format, visible, opacity, tiled,gfi, editable, getLegendGraphic, legendURL, minScale, hidden,cql_filter, clustered,wfs_color);
	
}; 

_TOC.addWTMSLayer= function(caption, capabilities, layerName,gridset,format, visible, opacity, getLegendGraphic, legendURL, minScale,hidden) {
	this.group.addWTMSLayer(caption, capabilities, layerName,gridset,format, visible, opacity, getLegendGraphic, legendURL, minScale,hidden);
};

_TOC.init = function(){
	mapOL.addLayer(this.group.layerGroup);
};


function getIntervalDate( element ) {
	var dateFormat = "yyyy-mm-dd";
	var date=element.datepicker("getDate");
	if (date){

		 var mm = date.getMonth() + 1; // getMonth() is zero-based
		  var dd = date.getDate();

		  return [date.getFullYear(),
		          (mm>9 ? '' : '0') + mm,
		          (dd>9 ? '' : '0') + dd
		         ].join('-');
	
	}
	return date;
}
function updateWMSTime(layer,idx){
	 layer.getSource().updateParams({'TIME': getIntervalDate( $( "#"+idx+" .fromDateWMS" ) )+"/"+getIntervalDate( $( "#"+idx+" .toDateWMS" ) )});
}

function checkIntervalDates(idx, maxDays){
	$( "#"+idx+" .intervalWMSError" ).hide();
	var fromDate =$( "#"+idx+" .fromDateWMS" ).datepicker("getDate");
	var toDate =$( "#"+idx+" .toDateWMS" ).datepicker("getDate");
	if ((toDate.getTime()-fromDate.getTime()>maxDays*24*60*60*1000)||
			(toDate.getTime()-fromDate.getTime()<0)){
		$( "#"+idx+" .intervalWMSError" ).show();
		$("#"+idx+" .intervalWMSError").tooltip('open');
		setTimeout( function(){$("#"+idx+" .intervalWMSError").tooltip('close');},2000);
		return false;
	}
	else{
		return true;
	}
}

function onchangeWMSInterval(groupOL,idx){
	if (groupOL.get("maxDays")){
		 if(!checkIntervalDates(idx, groupOL.get("maxDays"))){
			 return;
		 }
	}
	groupOL.getLayers().forEach(function(layer, i) {
		updateWMSTime(layer,idx);
	});
}
function writeOLGroup(tableToc,groupOL,idx, siblings,tocClass,groupOneVisLayer){
	if (groupOL.get("hidden")==true){
		return;
	}
	var titulo = groupOL.get("caption");
	if (groupOL.get("timeInterval")==true){
		titulo+=' <span class="intervalWMS"><input autocomplete="off" class="fromDateWMS"  onclick="stopPropagation(event);"  type="text"></input> a <input autocomplete="off" class="toDateWMS"  onclick="stopPropagation(event);"  type="text"></input> '+
'<a class=\"intervalWMSError\" title=\"Intervalo erróneo'+(groupOL.get("maxDays")?" (debe abarcar entre 0 y "+groupOL.get("maxDays")+" días":"")+'\"><b>!</b></a></span>';
	}
	
	if (groupOL.get("hideLayers")==true){
		$("#"+tableToc).append('<tr id="'+idx+'" '+tocClass+'><td nowrap="" valign="TOP">'+siblings+
				(groupOL.get('visibOpt')?getCheckBoxHtml(idx,groupOL.get('isVisible')):'<img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/noicon_visib.gif" border="0" height="16" width="16">')
				+'<label class="textToc" onVisibleClick(\''+idx+'\');"> '+titulo+'</label></td></tr>');
	
	}
	else{
	if (groupOL.get('opened')){
		$("#"+tableToc).append('<tr id="'+idx+'" '+tocClass+'><td nowrap="" valign="TOP">'+siblings+'<a id="collapse'+idx+'" href="#" title="Pulse para plegar"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png" onmousedown="toc.onCollapseClick(\''+idx+'\');" alt="Pulse para plegar" border="0" height="16" width="16"></a>'+
				(groupOL.get('visibOpt')?getCheckBoxHtml(idx,groupOL.get('isVisible')):'<img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/noicon_visib.gif" border="0" height="16" width="16">')
				+'<label class="textToc" onCollapseClick(\''+idx+'\');"> '+titulo+'</label></td></tr>');
		
	}
	else{
		$("#"+tableToc).append('<tr id="'+idx+'" '+tocClass+'><td nowrap="" valign="TOP">'+siblings+
				'<a id="collapse'+idx+'" href="#" title="Pulse para desplegar"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png" onmousedown="toc.onCollapseClick(\''+idx+'\');" alt="Pulse para desplegar" border="0" height="16" width="16"></a>'+
				(groupOL.get('visibOpt')?getCheckBoxHtml(idx,groupOL.get('isVisible')):'<img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/noicon_visib.gif" border="0" height="16" width="16">')+
				'<label class="textToc" onCollapseClick(\''+idx+'\');">'+titulo+'</label></td></tr>');
		tocClass="class='tocHidden'";
	}
	if (groupOL.get("timeInterval")==true){
		$("#"+idx+" .intervalWMSError").tooltip( {
			position: { my: "right top", at: "rith bottom" }
		});
		
		$( "#"+idx+" .fromDateWMS" )
			.datepicker({
				defaultDate: new Date(),
				changeMonth: true,
				changeYear: true,
				numberOfMonths: 1
			})
			.on( "change", function() {
				onchangeWMSInterval(groupOL,idx);
			});
		$( "#"+idx+" .fromDateWMS" ).datepicker( "setDate", new Date() );
		$( "#"+idx+" .toDateWMS" ).datepicker({
			defaultDate: new Date(),
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1
		})
		.on( "change", function() {
			onchangeWMSInterval(groupOL,idx);
		});
		$( "#"+idx+" .toDateWMS" ).datepicker( "setDate", new Date() );
	}
	groupOL.getLayers().forEach(function(layer, i) {
		if (layer.get("hidden")!=true){
		var layerId= idx+"_"+i;
		var child_image;
		if (groupOL.getLayers().getLength()==i+1){
			child_image='icon_childlast';
		}
		else{
			child_image='icon_child';
		}
		if ((layer instanceof ol.layer.Group)&&(layer.get("hideLayers")!=true)){
			var grupoExcluyente = groupOneVisLayer;
			if ((grupoExcluyente==null)&&(layer.get("oneVisLayer")!=null)&&(layer.get("oneVisLayer")==true)){
				grupoExcluyente=layerId;
			}
			writeOLGroup(tableToc,layer,layerId,
					siblings+'<img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_sibling.gif" height="16" width="36">',
					tocClass,grupoExcluyente)	;		
		}
		else{
			if (groupOL.get("timeInterval")==true){
				updateWMSTime(layer,idx);	
			}
			$("#"+tableToc).append('<tr id="'+layerId+'" '+tocClass+'><td   nowrap="" valign="TOP">'+siblings+
					'<img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/'+child_image+'.gif" height="16" width="36">'+
					getCheckBoxHtml(layerId,(layer instanceof ol.layer.Group?layer.get('isVisible'):layer.getVisible()),groupOneVisLayer)+getRadioBtnHtml(layer, layerId)+'<label class="textToc" onmousedown="toc.onVisibleClick(\''+layerId+'\');"> '+layer.get("caption")+'</label></td></tr>');
			
			
		
		}
		}
      });
	}
}

_TOC.refresh=function(){
	var tableToc = this.tableToc;
	$("#"+tableToc).empty();
	this.writeTOC();
};
_TOC.writeTOC=function(reverse){
	var tableToc = this.tableToc;
	var layers =this.group.layerGroup.getLayers().getArray();
	var oneVisGroup=null;
	if (this.group.layerGroup.get('oneVisLayer')){
		oneVisGroup="toc";
	}
	this.group.layerGroup
	for (var j=0;j<layers.length;j++){
		var layer=layers[j];
		var i=j;
		if (reverse){
			i=layers.length-j-1;
			layer = layers[i];
		}
	
		if (layer instanceof ol.layer.Group){
			if (oneVisGroup==null){ // no es toc excluyente
				oneVisGroup = (layer.get("oneVisLayer")?i:null);
			}
			
			
			writeOLGroup(tableToc,layer,i,'','',oneVisGroup);			
		}
		else{
			if (layer.get("hidden")!=true){
			$("#"+tableToc).append('<tr><td  id="'+i+'" nowrap="" valign="TOP">'+getCheckBoxHtml(i,layer.getVisible())+getRadioBtnHtml(layer, i)+'<label class="textToc" onmousedown="toc.onVisibleClick(\''+i+'\');"> '+layer.get("caption")+'</label></td></tr>');
			}
		
		}
      };
};

_TOC.onVisibleClick=function(layerIdx){
	var layer = this.getOLLayer(layerIdx);
	setVisibleLayer(layer,layerIdx, ((layer instanceof ol.layer.Group)?!layer.get('isVisible'):!layer.getVisible()));
	
};

// clickado checkbox en una capa excluyente. El grupo de capas excluyentes se indica en groupOneVisible
_TOC.onVisibleOneClick=function(layerIdx, groupOneVisible){
	var layer = this.getOLLayer(layerIdx);
	var visible = ((layer instanceof ol.layer.Group)?layer.get('isVisible'):layer.getVisible());
	if (!visible){ // si se va a activar, primero desactivar todas las del grupo excluyente
		this.noVisibleLayer(groupOneVisible);
	}
	this.onVisibleClick(layerIdx);	
};

_TOC.noVisibleLayer=function(layerIdx){
	var layer = this.getOLLayer(layerIdx);
	setVisibleLayer(layer,layerIdx, false);
};

_TOC.onCollapseClick=function(layerIdx){
	var group = this.getOLLayer(layerIdx);
	collapseGroup(group,layerIdx);
	refreshTOCAccordion();
};
_TOC.onGFIClick=function(layerIdx){
	var capa = this.getOLLayer(layerIdx);
	if (checkMinScale(capa)){
	if (this.editingLayer!=null){
		var capaEdit = this.getOLLayer(this.editingLayer);
		var txt = "consultar";
		if(capaEdit.get("editable")){
			removeWFSLayer();
			txt = "editar";
		}
		$("#gfi_layer"+this.editingLayer)[0].title="Pulse para "+txt;
		$("#gfi_layer"+this.editingLayer+" img")[0].alt="Pulse para "+txt;

		$("#gfi_layer"+this.editingLayer+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_unselected.png";
		
	}
	this.editingLayer=layerIdx;
	$("#gfi_layer"+this.editingLayer)[0].title="";
	$("#gfi_layer"+this.editingLayer+" img")[0].alt="";

	$("#gfi_layer"+this.editingLayer+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_selected.png";
//	var capa = this.getOLLayer(this.editingLayer);
	if(capa.get("editable")){
		showWFSLayer(capa);
	}
	if (!capa.getVisible()){
		setVisibleLayer(capa,layerIdx, true);
	}
	info();
	}
	
};

_TOC.getLayerIdx=function(layerName){
	
	return getLayerIdx(this.group.layerGroup,layerName);
}
_TOC.getOLLayer=function(layerIdx){
	if (layerIdx=="toc"){
		return this.group.layerGroup;
	}
	var idx = layerIdx.toString().split("_");
	var layer = this.group.layerGroup.getLayers().item(idx[0]);
	for (var i=1;i<idx.length;i++){
		layer = layer.getLayers().item(idx[i]);
	}
	return layer;
};

function getLayerIdx(grupo,layerName){
	var idx ;
	var layers = grupo.getLayers()
	for (var i=0;i<layers.getLength();i++){
		if (layers.item(i) instanceof ol.layer.Group){
			idx = getLayerIdx(layers.item(i),layerName);
			if (idx){
				return i+"_"+idx;
			}
		}
		else{
			try{
				var ows_layer;
				try{
					ows_layer=layers.item(i).getSource().getParams().LAYERS;
				}
				catch(err){//sera wmts
					ows_layer=layers.item(i).getSource().getLayer();
				}
			if(ows_layer==layerName){
				return i;
			}
			}
			catch(err){ //no sera capa wms
				console.log(err);
			}
		}
	}
	return idx;
}
function GROUP(caption,opened,visible,hideLayers,legend,hidden,oneVisLayer,timeInterval,maxDays) {
	this.layerGroup = new ol.layer.Group({ });
	this.layerGroup.set('caption',caption);
	this.layerGroup.set('hidden',hidden);
	this.layerGroup.set('legend',legend);
	this.layerGroup.set('opened',opened);
	this.layerGroup.set('hideLayers',hideLayers);
	this.layerGroup.set('oneVisLayer',oneVisLayer);
	if (visible != null){
		this.layerGroup.set('isVisible',visible);
		this.layerGroup.set('visibOpt',true);
	}
	else{this.layerGroup.set('visibOpt',false);}
	this.layerGroup.set('timeInterval',timeInterval);
	this.layerGroup.set('maxDays',maxDays);
	
};

_GROUP = GROUP.prototype;

_GROUP.addGroup= function(group,index) {
	if (group.layerGroup.get("oneVisLayer")==null){
		group.layerGroup.set("oneVisLayer",this.layerGroup.get("oneVisLayer"));
	}
	var layers = this.layerGroup.getLayers();
	if ((typeof index !='undefined')&&(index!=null)){
		layers.insertAt(index,group.layerGroup);
	}
	else{
		layers.push(group.layerGroup);
	}
	this.layerGroup.setLayers(layers);
	
}; 

_GROUP.addLayer= function(caption, layer, visible, opacity,hidden) {
	layer.setVisible(visible);
	layer.set("caption",caption);
	layer.set('hidden',hidden);
	layer.set('oneVisLayer',this.layerGroup.get("oneVisLayer"));
	layer.setOpacity(opacity);
	var layers = this.layerGroup.getLayers();
	layers.push(layer);
	this.layerGroup.setLayers(layers);
	return layer;
};

_GROUP.addBottomLayer= function(caption, layer, visible, opacity) {
	layer.setVisible(visible);
	layer.set("caption",caption);
	layer.setOpacity(opacity);
	var layers = this.layerGroup.getLayers();
	layers.insertAt(0,layer);
	this.layerGroup.setLayers(layers);
	return layer;
};

_GROUP.addWMSLayerSRS= function(caption, url, layers,styles, version,format, visible, opacity, tiled,srs) {
	var layer;
	if (tiled){
		layer =new ol.layer.Tile({
		    source: new ol.source.TileWMS({
		        params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
		        url: url,
		        projection:srs
		      })
		    });
	}
	else{
		layer =new ol.layer.Image({
		    source: new ol.source.ImageWMS({
		        params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
		        url: url,
		        projection:srs
		      })
		    });
	}
	
	layer.setVisible(visible);
	layer.setOpacity(opacity);
	layer.set('caption',caption);
	layer.set('gfi',false);
	layer.set('editable',false);
	layer.set('glg',false);
	layer.set('hidden',false);
	layer.set('oneVisLayer',this.layerGroup.get("oneVisLayer"));
	var layers = this.layerGroup.getLayers();
	layers.push(layer);
	
	this.layerGroup.setLayers(layers);
	return layer;
	
}; 
_GROUP.addWMSLayer= function(caption, url, layers,styles, version,format, visible, opacity, tiled,gfi, editable, getLegendGraphic, legendURL, minScale,hidden,cql_filter,clustered,wfs_color,projection) {
	var layer;
	if (tiled){
		layer =new ol.layer.Tile({
		    source: new ol.source.TileWMS({
		        params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
		        url: url,
		        projection: projection
		      })
		    });
	}
	else{
		layer =new ol.layer.Image({
		    source: new ol.source.ImageWMS({
		        params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
		        url: url,
		        projection: projection
		      })
		    });
	}
	if (cql_filter){
		layer.getSource().getParams().CQL_FILTER= cql_filter;
	}
	layer.setVisible(visible);
	layer.setOpacity(opacity);
	layer.set('caption',caption);
	layer.set('gfi',gfi);
	layer.set('editable',editable);
	layer.set('glg',getLegendGraphic);
	layer.set('legend',legendURL);
	layer.set('minScale',minScale);
	layer.set('hidden',hidden);
	layer.set('clustered',clustered);
	layer.set('wfsColor',wfs_color);
	layer.set('oneVisLayer',this.layerGroup.get("oneVisLayer"));
	var layers = this.layerGroup.getLayers();
	layers.push(layer);
	this.layerGroup.setLayers(layers);
	return layer;
	
}; 
 
_GROUP.addWTMSLayer= function(caption, capabilities, layerName,gridset,format, visible, opacity, getLegendGraphic, legendURL, minScale,hidden) {
	var layer;
	var parser = new ol.format.WMTSCapabilities();
	var result = parser.read(capabilities);
	var options = ol.source.WMTS.optionsFromCapabilities(result,
			{layer: layerName, matrixSet: gridset, format:format});

	layer =new ol.layer.Tile({
		source: new ol.source.WMTS(options)
	});
	layer.setVisible(visible);
	layer.setOpacity(opacity);
	layer.set('caption',caption);
	layer.set('glg',getLegendGraphic);
	layer.set('legend',legendURL);
	layer.set('minScale',minScale);
	layer.set('hidden',hidden);
	layer.set('oneVisLayer',this.layerGroup.get("oneVisLayer"));
	
	var layers = this.layerGroup.getLayers();
	layers.push(layer);
	this.layerGroup.setLayers(layers);
	return layer;
	
}; 

_GROUP.addWMSLayers= function( url, url_capabilities,  version, visible, opacity, tiled) {
	 var parser = new ol.format.WMSCapabilities();

     fetch(url_capabilities).then(function(response) {
       return response.text();
     }).then(function(text) {
       var result = parser.read(text);
    
    
	var layer;
	if (tiled){
		layer =new ol.layer.Tile({
		    source: new ol.source.TileWMS({
		        params: {'LAYERS': layers,'VERSION':version},
		        url: url
		      })
		    });
	}
	else{
		layer =new ol.layer.Image({
		    source: new ol.source.ImageWMS({
		        params: {'LAYERS': layers,'VERSION':version},
		        url: url
		      })
		    });
	}
	layer.setVisible(visible);
	layer.setOpacity(opacity);
	layer.set('caption',caption);
	layer.set('oneVisLayer',this.layerGroup.get("oneVisLayer"));
	var layers = this.layerGroup.getLayers();
	layers.push(layer);
	this.layerGroup.setLayers(layers);
     });
}; 

function getRadioBtnHtml(layer, layerId){
	var gfiRadio ='';
	if (layer.get('gfi')){
		var txt = "consultar";
		if(layer.get("editable")){
			
			txt = "editar";
		}
		gfiRadio='<a id="gfi_layer'+layerId+'" href="#" title="Pulse para '+txt+'"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_unselected.png" onmousedown="toc.onGFIClick(\''+layerId+'\');" alt="Pulse para "'+txt+' border="0" height="16" width="16"></a>';
	}
	return gfiRadio;
}
function getCheckBoxHtml(layerId,visible,groupOneLayer){
	
	if (visible){
		return '<a id="chk_layer'+layerId+'" class="chk_layer" href="#" title="Pulse para ocultar"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_visible.png" onmousedown="toc.onVisibleClick(\''+layerId+'\');" alt="Pulse para mostrar" border="0" height="16" width="16"></a>';	
	}
	else{
		var clickFunction = 'toc.onVisibleClick(\''+layerId+'\')';
		if (groupOneLayer!=null){
			clickFunction = 'toc.onVisibleOneClick(\''+layerId+'\',\''+groupOneLayer+'\')';
		}
	return '<a id="chk_layer'+layerId+'"  class="chk_layer" href="#" title="Pulse para mostrar"><img class="imgToc" src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_hidden.png" onmousedown="'+clickFunction+';" alt="Pulse para ocultar" border="0" height="16" width="16"></a>';
	}

}

function collapseGroup(group, layerIdx){
	if (group.get('opened')){
		group.getLayers().forEach(function(layer, i) {
			$("#"+layerIdx+"_"+i).addClass('tocHidden');
			if (layer instanceof ol.layer.Group){
				if (layer.get('opened')){
					collapseGroup(layer, layerIdx+"_"+i);
				}
			}

		});
		$("#collapse"+layerIdx)[0].title="Pulse para desplegar";
		$("#collapse"+layerIdx+" img")[0].alt="Pulse para desplegar";

		$("#collapse"+layerIdx+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png";

		group.set('opened',false);
	}else{
		group.getLayers().forEach(function(layer, i) {
			$("#"+layerIdx+"_"+i).removeClass('tocHidden');
			

		});
		$("#collapse"+layerIdx)[0].title="Pulse para mostrar";
		$("#collapse"+layerIdx+" img")[0].alt="Pulse para mostrar";

		$("#collapse"+layerIdx+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png";

		group.set('opened',true);
	}
}
function showLegend(layerIdx,layer){
	var url="";
	var layer_legend = layer.get('legend');

	if (layer_legend){
		var legend_class = layer_legend.replace(/ /g,'_').replace(/\./g,'_').replace(/\//g,'_').replace(/&/g,'_').replace(/\?/g,'_').replace(/=/g,'_');
		url = "/lib/IDEAragon/legends/"+layer_legend;
		if ($("."+legend_class).length>0){
			legend_class += ' undisplayed';
			}
		
		if (layer_legend.indexOf("http")==0){
			legend_class+=" legendHttp";
			url=layer_legend;
		}
		
		
		$('#legend_panel').append("<div class=\""+legend_class+"\" id=\"leyenda_"+layerIdx+"\"><img src=\""+url+"\"></div>");

	}
	else if (layer.get('glg')){
		try{
		url = layer.getSource().getUrls()[0];
		}
		catch(err){
			url = layer.getSource().getUrl();
		}

		if (url.indexOf("?")<0){
			url = url+"?";
		}
		if (url.indexOf("?")<url.length-1){
			url = url+"&";
		}
/*		var ows_layer,ows_style;
		try{*/
			var ows_layer=layer.getSource().getParams().LAYERS;
			var ows_style=layer.getSource().getParams().STYLES;
		/*}
		catch(err){//será wmts
			ows_layer=layer.getSource().getLayer();
			ows_style=layer.getSource().getStyle();
		}*/
		url += "SERVICE=WMS&REQUEST=GetLegendGraphic&format=image/png&layer="+ows_layer+"&width=20&height=20&SCALE="+document.getElementById('scaleTextBox').value+(ows_style? "&STYLE="+ows_style:"");
		if ($("#leyenda_"+layerIdx).length>0){
			$("#leyenda_"+layerIdx).append("<img src=\""+url+"\">");
		}
		else{
			var titulo = layer.get('caption').split("<");
		$('#legend_panel').append("<div class='leyendaCapa' id=\"leyenda_"+layerIdx+"\"><p>"+titulo[0]+"</p> <img src=\""+url+"\"></div>");
		}
	}
}
function setSublayersVisible(group,visible,layerIdx){
	group.getLayers().forEach(function(sublayer, i) {
		sublayer.setVisible(visible);
		showLegend(layerIdx,sublayer);
		if (sublayer instanceof ol.layer.Group){
			setSublayersVisible(sublayer,visible,layerIdx);
		}
	});
}
function setVisibleLayer(layer,layerIdx, visible){
	try{
	if (!visible){
		$("#chk_layer"+layerIdx)[0].title="Pulse para mostrar";
		$("#chk_layer"+layerIdx+" img")[0].alt="Pulse para mostrar";

		$("#chk_layer"+layerIdx+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_hidden.png";

		//layer.setVisible(false);
	}else{
		$("#chk_layer"+layerIdx)[0].title="Pulse para ocultar";
		$("#chk_layer"+layerIdx+" img")[0].alt="Pulse para ocultar";
		$("#chk_layer"+layerIdx+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_visible.png";
		//layer.setVisible(true);
	}
	}
	catch(err){
		// Es un grupo y no tiene checkbox
	}
	if ((layer instanceof ol.layer.Group)&&(layer.get("hideLayers")!=true)){
		layer.set('isVisible',visible);
		layer.getLayers().forEach(function(sublayer, i) {
			
			if ((layer instanceof ol.layer.Group)||(sublayer.getVisible()!=visible)){
				setVisibleLayer(sublayer,(layerIdx=="toc"?i:layerIdx+"_"+i), visible);
			}

		});
		
	}
	else{
		if (layer instanceof ol.layer.Group){
			layer.set('isVisible',visible);
			
			setSublayersVisible(layer,visible,layerIdx);
		}
		layer.setVisible(visible);
		if (visible){
			try{
				markersOnTop();
			}
			catch(err){
				console.log(err);
			}
			showLegend(layerIdx,layer);
			
		}
		else{
			
			$('#leyenda_'+layerIdx).remove();
			var layer_legend = layer.get('legend');

			if (layer_legend){
				var legend_class = layer_legend.replace(/ /g,'_').replace(/\./g,'_').replace(/\//g,'_');
				if (($("."+legend_class).length>0)&&($("."+legend_class).length==$(".undisplayed."+legend_class).length)){
					$("."+legend_class+":eq(0)").removeClass( 'undisplayed');
				}
			}
			if (toc.editingLayer==layerIdx){
				
				var txt = "consultar";
				if(layer.get("editable")){
					removeWFSLayer();
					txt = "editar";
				}
				$("#gfi_layer"+layerIdx)[0].title="Pulse para "+txt;
				$("#gfi_layer"+layerIdx+" img")[0].alt="Pulse para "+txt;

				$("#gfi_layer"+layerIdx+" img")[0].src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_unselected.png";
				toc.editingLayer=null;
			}
		}
		refreshLegendAccordion();
	}
}

