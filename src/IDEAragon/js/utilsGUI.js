var codelists;
$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '< Ant',
		 nextText: 'Sig >',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
		 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
 
function act(idObj) {
	document.getElementById(idObj).style.border = "#CC0000 solid 2px";
}

function deac(idObj) {
	document.getElementById(idObj).style.border = "";
}

function inputKeyUp(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
    	buscar();
    }
}
function isEnterKey(evt) {
	if (!evt) {
		return (evt.keyCode == 13);
	} else if (!evt.keyCode) {
		return (evt.which == 13);
	}
	return (evt.keyCode == 13);
}
function showGenerico(theId) {
	document.getElementById(theId).style.display = "block";
	document.getElementById(theId).style.visibility = "visible";
}

function hideGenerico(theId) {
	document.getElementById(theId).style.display = "none";
	document.getElementById(theId).style.visibility = "hidden";
}

function creaVentanaAviso(msj, esModal) {
	var aux = $('<div></div>')
				.html('<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 1em 1em;"><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;margin-bottom: .3em;"></span><strong>Aviso:</strong></p></div><br/>' + msj + '</div>')
				.dialog({
					autoOpen: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					},
					title: 'Aviso',
					modal: esModal 
				});
	return aux;
}




function initCodelists(){

	$.ajax({
		url: cartoUpdateURL+"?REQUEST=GETCODELISTS",
		type: 'GET',
		success: function(data) {
			
				codelists=  data;
				
			 },
			 async:false,
		error: function(data, textStatus, errorThrown) {
			codelists=["error"];
			alert("No se pudieron cargar las listas de valores");
			
		}
	});
}

function getSelectOptions(codelist){
	if (codelists){
		var options="";
		try{
		var values = codelists[codelist];
		for (var i=0; i<values.length;i++){
			options+='<option value="'+values[i]["codigo"]+'">'+values[i]["descripcion"]+'</option>';
		}
		}
		catch(err){
			console.log("No se pudeo cargar la lista de valores "+codelist);
		}
		return options;
	}
	else {
		 initCodelists();
		 return getSelectOptions(codelist);
	}
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}