var tokenId;

function checkUsr(service) {
	showLoading(true);
	$("#users_manager").hide();
	var newUsuario = document.getElementById('login').value;
	var newPasswd = encryptPwd(document.getElementById('passwd').value);
	
	var url = '/gestorUsuarios/gestorUsuariosIGEAR?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(newPasswd);
	$("#btnEnviar").button( "disable");
	$("#btnCancelar").button( "disable");
	$.ajax({
		url: url,
		type: 'GET',
		
		success: function(data) {
					tokenId=data;	
					showBtns();
					
					setLogged(newUsuario);
					dialogLog.dialog("close");
					writeLoginCookieIGEAR(data);
		},
		error: function(data, textStatus, errorThrown) {
			showLoading(false);
			if (textStatus =='timeout'){
			
					alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
				
			}
			else if (data.status==403){
				$dialogRecibido = $('<div></div>')
				.html('<div>Usuario y contrase&ntilde;a no v&aacute;lidos</div>')
				.dialog({
					autoOpen: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					},
					title: 'Error',
					modal: true
			});
			}
			else{
			creaVentanaAviso("No se ha podido validar su usuario. Por favor, int&eacute;ntelo m&aacute;s adelante.\n", true);
			console.log("No se pudo validar el usuario. Respuesta servicio:"+data.responseText);
			}
			$("#btnEnviar").button( "enable");
			$("#btnCancelar").button( "enable");
		}
	});
}

function setLogged(newUsuario){
	document.getElementById("nomUsrConnected").innerHTML = "Conectado como usuario " + newUsuario;
	document.getElementById("connection").innerHTML = "Cerrar sesión";
	document.getElementById("connection").title = "Cerrar sesión";
}
