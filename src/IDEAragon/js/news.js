/**
 * Carga del carrusel noticias
 */

var new_actual=0; // indice de la siguiente noticia a mostrar
var aviso_actual=0;
var linesAviso=new Array();
var lines_news=new Array();
var recargaAvisos=false;
var recargaNoticias=false;
var dot;
/**
 * Obtiene las noticias del servicio jsp y programa la muestra periódica de una en una
 */
function initNoticias(){
	$.ajax({
		url: AVISOS_JSP_URL,
		type: 'GET',
		success: function(data) {
			var text = data.replace("<html>","").replace("</html>","").replace("<body>","").replace("</body>","");
			
							 linesAviso =  $.trim(text).split("\n");
								recargaAvisos=true;
								
			 },
		error: function(data, textStatus, errorThrown) {
			if ((typeof debugOn !='undefined')&&(debugOn) ){
				alert('Error obteniendo los avisos. State: ' + data.readyState + ", Status:" + data.status);
			}
		}
	});
	$.ajax({
		url: NOTICIAS_JSP_URL,
		type: 'GET',
		success:  function(data) { 
			var text = data.replace("<html>","").replace("</html>","").replace("<body>","").replace("</body>","");
			lines_news = $.trim(text).split("\n");
			if ((typeof debugOn !='undefined')&&(debugOn) ){
				alert('Recibidas las noticias: ' + data);
			}
			recargaNoticias=true;
		
			
			
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) {
			if ((typeof debugOn !='undefined')&&(debugOn) ){
					 	alert("Error al cargar las noticias "+textStatus +"\n "+errorThrown);
			}
		},
		processData: false,
		async: true
	});
	timer = setInterval("programarRecarga()", 5000);
}

function programarRecarga(){
	
	 if(recargaNoticias){
			
			// muestra la primera noticia
			recargarNoticias();
	 }
			else if (recargaAvisos){
				// muestra la primera noticia
				recargarAvisos();
				
			
	}
}
function resetDotDotDot(){
	$("#recargado").css("width",$("#foot_int").width()-$("#foot_int .d_d").width()-100);
	$("#recargado").attr("sytle","width:"+$("#recargado").width()+"px;");
	

	//Llamada al plugin de jQuery dotdotdot para insertar una ellipsis
	try{
		dot.data('dotdotdot',false);
	}
	catch(err){
		
	}
	dot=$("#recargado").dotdotdot({
		//Optionally set a max-height, can be a number or function.
		//If null, the height will be measured. 
		height		: 20,
 
		/*	Deviation for the height-option. */
		tolerance	: 5
	});

}
/**
 * Muestra la siguiente noticia
 * @param misNoticias lista de noticias
 */
function recargarNoticias(){
	var variable_post=new_actual;	
	if (lines_news[variable_post]){
	var noticia = lines_news[variable_post].replace("<br>","");
	
	$("#recargado").removeClass("textoInvRojo");
	$("#recargado").addClass("textoInvNegro");	

	$("#recargado").attr("title",noticia);
	$("#recargado").html(noticia);
	resetDotDotDot();

	if (new_actual<lines_news.length-1) {  // no es la última noticia
		new_actual=new_actual+1;
		// programamos la carga de una nueva noticia cada 5 segundos
	
	} else { // es la última noticia
		// se vuelve a empezar
		new_actual=0;
		// programamos la carga de una nueva noticia cada 5 segundos
		recargaNoticias=false;
		recargaAvisos=true;
	}
	}
	else{
		new_actual=0;
	}
	

}

/**
 * Muestra la siguiente noticia
 * @param misNoticias lista de noticias
 */
function recargarAvisos(){
	var variable_post=aviso_actual;	
	var noticia = linesAviso[variable_post].replace("<br>","");
	
	$("#recargado").removeClass("textoInvNegro");	
	$("#recargado").addClass("textoInvRojo");
	$("#recargado").attr("title",noticia);
	$("#recargado").html(noticia);
	resetDotDotDot();
	if (aviso_actual<linesAviso.length-1) {  // no es la última noticia
		aviso_actual=aviso_actual+1;
		
	} else { // es la última noticia
		// se vuelve a empezar
		aviso_actual=0;
		recargaNoticias=true;
		recargaAvisos=false;
	}



}