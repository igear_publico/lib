var encryptKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDMtnFB5Pn1m2RKiVMe0Im5AjfMQQ0LBelz3tz8U2BMrJCb1tYx2FbDsRAJ9SLRiTXQb8rLeGGqGymaRKE7qHZ1t5sPzSiAISTdX9piEHcwj+QGuKJlgYRqf5DRO6+UB+UhBuGUSxlbotR/RgBGtTFu2CUoOCAeicOwPAxkYiRkrwIDAQAB";
var TOKEN_COOKIE_NAME="tokenIdearagon";
var TOKEN_COOKIE_IGEAR="tokenIGEAR";
var protDatos=' <div class="centro_cuadro">'+
'<div class="centro_cuadro_int">'+
'<div style="text-align:justify;">'+
'<h3><b>Protección de datos personales</b></h3>'+
'<p><br/></p>	'+
'La recogida y el tratamiento de los datos de carácter personal está sujeto a lo establecido por la legislación vigente en materia de protección de datos: el <a href="https://eur-lex.europa.eu/eli/reg/2016/679/oj" target="_blank"> Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016</a>, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos (RGPD) y la <a href="https://www.boe.es/eli/es/lo/2018/12/05/3/con" target="_blank"> Ley Orgánica 3/2018, de 5 de diciembre</a>, de Protección de Datos Personales y garantía de los derechos digitales.'+ 
'<br/>'+
'<br/>'+
'<b>Responsable del tratamiento de datos</b>'+	
'<br/>'+
'<ul>'+
'<li style="margin-left: 20px;"><b>Órgano responsable:</b> Dirección General de Ordenación del Territorio. Departamento de Vertebración del Territorio, Movilidad y Vivienda. Gobierno de Aragón</li>'+
'<li style="margin-left: 20px;"><b>CIF:</b> S5011001D</li>'+
'<li style="margin-left: 20px;"><b>Dirección Postal:</b> Edificio Pignatelli. Paseo María Agustín, n 36 C.P 50004 Zaragoza</li>'+
'<li style="margin-left: 20px;"><b>Teléfono:</b> +34 976 714 000</li>'+
'</ul> '+
'<br/>'+
'<b>Finalidad de tratamiento de datos</b>'+	
'<br/>'+
'La gestión de alta de usuario se realiza para las siguientes aplicaciones geográficas de IDEARAGON: Registro Cartográfico de Aragón, Atlas de Aragón, IDEDidáctica (Geojuegos) y Nomenclátor Geográfico de Aragón (Toponimia).'+
'<br/>'+
'<br/>'+
'<b>Licitud del tratamiento de datos</b>'+	
'<br/>'+
'La licitud del tratamiento es el <b>consentimiento del interesado</b> de acuerdo con lo establecido en el <a href="https://eur-lex.europa.eu/eli/reg/2016/679/oj" target="_blank"> artículo 6.1 a) del Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016</a>, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos (RGPD) y el <a href="https://eur-lex.europa.eu/eli/reg/2016/679/oj" target="_blank"> artículo 6 de la Ley Orgánica 3/2018, de 5 de diciembre</a>, de Protección de Datos Personales y garantía de los derechos digitales.'+
'<br/>'+
'<br/>'+
'<b>Comunicación de los datos</b>'+	
'<br/>'+
'Los datos recogidos no se comunicarán a terceros salvo obligación legal.'+
'<br/>'+
'<br/>'+
'<b>Derechos en relación a los datos</b>'+	
'<br/>'+
'Podrá ejercer derechos de acceso, rectificación, supresión y portabilidad de sus datos, de limitación y oposición a su tratamiento, de conformidad con lo dispuesto en el Reglamento General de Protección de Datos (RGPD), ante la Dirección General de Ordenación del Territorio, Paseo María Agustín 36, 50004 Zaragoza o en la dirección de correo electrónico del Instituto Geográfico de Aragón <a href="mailto:igear@aragon.es" title="CONTACTO">igear@aragon.es</a>.'+
'<br/>'+
'<br/>'+
'<b>Registro de actividades de tratamiento de dato</b>'+	
'<br/>'+
'Podrá consultar información adicional y detallada en el <a href="https://aplicaciones.aragon.es/notif_lopd_pub/" target="_blank"> Registro de Actividades de Tratamiento del Gobierno de Aragón</a> indicando en la denominación: REGISTRO DE USUARIOS DE IDEARAGON.'+ 
'<br/>'+
'<br/>'+
'<b>Conservación de la información</b>'+	
'<br/>'+
'Los datos personales proporcionados se conservarán durante el tiempo necesario para cumplir con la finalidad para la que se recabaron y para determinar las posibles responsabilidades que se pudieran derivar de la finalidad, cumpliendo, al menos, el tiempo mínimo de conservación de la información.'+
'<br>'+
'<br>'+
'<div class="br_m"></div>'+
'</div>'+
'</div>'+
'<div class="clear"></div>'+
'</div>';

function showAvisoProtDatos(height){

		$('<div  class="innerWindow"></div>')
		.html(protDatos)
		.dialog({
			autoOpen: true,
			width:'500px',
			height:height,
			close: function(){
				$( this ).dialog( "destroy" );
			},
			buttons: [{
				text: "Aceptar",
				click: function() {
					$( this ).dialog( "close" );
					
				}}
			],
			title: 'AVISO LEGAL Y PROTECCIÓN DE DATOS',
			modal: true
		});
		
	}
/**
 * Encripta la contrase�a
 * @param pwd contrase�a
 * @returns contrase�a encriptada
 */
function encryptPwd(pwd){
	  var encrypt = new JSEncrypt();
      encrypt.setPublicKey(encryptKey);
      return  encrypt.encrypt(pwd);

}

/**
 * Muestra di�logo para restablecer contrase�a
 * @param token
 */
function showDialogPwd(token) {
	var htmlUsr = '<div><table width="100%" cellpadding="1" cellspacing="1" style="table-layout:fixed;">';
	htmlUsr += '<tr><td colspan="2" align="center"><hr/>Introduzca su nueva contrase&ntilde;a</td></tr>';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="login">Contrase&ntilde;a:</label></td><td>';
	htmlUsr += '<input type="password" onFocus="act(\'pwd\')" onBlur="deac(\'pwd\')" id="pwd" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="passwd">Confirme contrase&ntilde;a:</label></td><td>';
	htmlUsr += '<input type="password" onFocus="act(\'pwd2\')" onBlur="deac(\'pwd2\')" id="pwd2" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '</table></div>';

	dialogPwd = $('<div id="loginDialog"></div>')
		.html(htmlUsr)
		.dialog({
			autoOpen: false,
			title: 'Restablecer contrase�a',
			modal: true,
			width: "300"
		});
	dialogPwd.dialog( "option", "buttons", [ { id:"btnEnviar", text: "Enviar", click: function() { changePwd(token); } } ] );

	dialogPwd.dialog("open");
}



/**
 * Cambia la contrase�a del usuario correspondiente al token
 * @param token
 */
function changePwd(token) {
	
	var newPasswd = document.getElementById('pwd').value;
	var newPasswd2 = document.getElementById('pwd2').value;
	if (checkPwd(newPasswd,newPasswd2)) {
		$.ajax({
			url:"/gestorUsuarios/gestorUsuarios?REQUEST=CHANGEPWD&cl="+encodeURIComponent(encryptPwd(newPasswd))+"&TOKEN="+token,
			type: 'GET',
			
			success: function(data) {
				
						alert("Su contraseña se ha restablecido correctamente");
						dialogPwd.dialog("destroy");
						goToInicio();
			},
			error: function(data, textStatus, errorThrown) {
				
				if (textStatus =='timeout'){
					
						alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
					
				}
				else if (data.status==412){
					$dialogRecibido = $('<div></div>')
					.html('<div>El usuario no existe</div>')
					.dialog({
						autoOpen: true,
						buttons: {
							Aceptar: function() {
								$( this ).dialog( "close" );
							}
						},
						title: 'Error',
						modal: true
				});
				}
				else{
				creaVentanaAviso("No se ha podido restablecer la contrase&ntilde;a. Por favor, int&eacute;ntelo m&aacute;s adelante.\n", true);
				console.log("No se pudo restablecer la contrase&ntilde;a. Respuesta servicio:"+data.responseText);
				}
				$("#btnEnviar").prop('disabled', false);;
				$("#btnEnviar").button( "enable");
			},
			timeout:15000/*,
			async: false*/
		});
	} 
}

/**
 * Modifica la GUI para indicar que el usuario est� logueado
 * @param newUsuario nombre del usuario logueado
 */
function setLogged(newUsuario){
	document.getElementById("nomUsrConnected").innerHTML = "Conectado como usuario " + newUsuario;
	try{
	$("#imgLogged").attr("title","Conectado como usuario " + newUsuario);
	}
	catch(err){
		
	}
	document.getElementById('username').value=newUsuario;
	$("#login").css("display","none");
	$("#logout").css("display","block");
	$("#btnEnviar").prop('disabled', true);
}

function setNotLogged(){
	$("#login").css("display","block");
	$("#logout").css("display","none");
	$("#btnEnviar").prop('disabled', false);
	$("#btnEnviar").button( "enable");
}
/**
 * Solicita el inicio de sesi�n
 * @param callback codigo js a ejecutar en caso de que el inicio de sesi�n sea correcto (logueo propio de la aplicaci�n si procede)
 */
function logUser(callback,callback_403){
	var newUsuario = document.getElementById('username').value;
	var newPasswd = encryptPwd(document.getElementById('passwd').value);
	var url;
	if ((typeof esRiesgos!='undefined') && esRiesgos){
		url =riesgosWS_url+ '?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(newPasswd);
	}
	else{
		url = '/gestorUsuarios/gestorUsuarios?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(newPasswd);
	}
	var userLDAP=false;
	if (newUsuario && (newUsuario.indexOf("@")>0) && (newUsuario.endsWith("aragon.es"))){
		userLDAP=true;
		 url = '/gestorUsuarios/gestorUsuariosIGEAR?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(newPasswd);
	}
	$("#btnEnviar").prop('disabled', true);

	$.ajax({
		url:url,
		type: 'GET',
		
		success: function(data) {
			tokenId=data;
			if (callback){
				eval(callback);
			}
			
			if (userLDAP ||((typeof esRiesgos!='undefined') && esRiesgos)){

				writeLoginCookieIGEAR(data);
			}
			else{
				writeLoginCookie(data);
			}
			
					//alert("Usuario validado");
			setLogged(newUsuario);
		},
		error: function(data, textStatus, errorThrown) {
			
			if (textStatus =='timeout'){
				
					alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
				
			}
			else if (data.status==403){
				if (callback_403){
					eval(callback_403)
				}
				else{
				$dialogRecibido = $('<div></div>')
				.html('<div>Usuario y contrase&ntilde;a no v&aacute;lidos</div>')
				.dialog({
					autoOpen: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					},
					title: 'Error',
					modal: true
			});
				}
			}
			else{
			creaVentanaAviso("No se ha podido validar su usuario. Por favor, int&eacute;ntelo m&aacute;s adelante.\n", true);
			console.log("No se pudo validar el usuario. Respuesta servicio:"+data.responseText);
			}
			$("#btnEnviar").prop('disabled', false);;
			$("#btnEnviar").button( "enable");
		},
		timeout:15000/*,
		async: false*/
	});
}

/**
 * Cierra sesi�n
 */
function doLogout(){
	tokenId=null;
	deleteLoginCookie();
	deleteLoginCookieIGEAR();
	goToInicio();
}

/**
 * Cierra sesi�n
 */
function doLogoutIGEAR(){
	tokenId=null;
	deleteLoginCookieIGEAR();
	goToInicio();
}
/**
 * 
 * @param email_path
 * @param modal_win
 */
function initUserControl(modal_win){
	var htmlUsr = '<div><table width="100%" cellpadding="1" cellspacing="1" style="table-layout:fixed;">';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="login">Nombre de usuario:</label></td><td>';
	htmlUsr += '<input onFocus="act(\'login\')" onBlur="deac(\'login\')" id="login" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '<tr><td align="center" style="width:120px" valign="top"><label class="textoInv" for="passwd">Contrase&ntilde;a:</label></td><td>';
	htmlUsr += '<input type="password" onFocus="act(\'passwd\')" onBlur="deac(\'passwd\')" id="passwd" class="textoInv"></td><td style="width:10px;"></td></tr>';
	htmlUsr += '</table></div>';

	dialogLog = $('<div id="loginDialog"></div>')
		.html(htmlUsr)
		.dialog({
			autoOpen: false,
			closeOnEscape: false,
			open: function(event, ui) { $(".ui-dialog[aria-describedby='loginDialog'] .ui-dialog-titlebar-close").hide(); },
			title: 'Registro de usuarios',
			modal: modal_win,
			width: "300"
		});
}

function getRegistrationForm(){
	return '<form id="registerUserForm" autocomplete="off" name="registerUserForm" accept-charset="UTF-8" action="/gestorUsuarios/gestorUsuarios" method="post"><table class="text-aligned-left">'+
	'<input type="hidden" id="REQUEST" name="REQUEST" value="ADDUSER">'+
	'<input type="hidden" id="new_email" name="email" value="">'+
	'<tr>'+
                  ' <th class="padded">Correo electrónico (*)</th>'+
                  ' <td class="padded"><input class="content" type="text" id="new_username" name="us" value=""  ></td>'+
               ' </tr>'+
               ' <tr>'+
               '    <th class="padded">Contrase&ntilde;a (*)</th>'+
               '    <td class="padded"><input class="content" type="password" id="new_password" name="cl" value=""></td>'+
               ' </tr>'+
              '  <tr>'+
              '     <th class="padded">Confirmar la Contrase&ntilde;a (*)</th>'+
              '     <td class="padded"><input class="content" type="password" id="new_password2" name="cl2" value="" ></td>'+
              '  </tr>'+
			   '  <tr>'+
              '     <th class="padded">Nombre (*)</th>'+
              '     <td class="padded"><input required class="content" type="text" name="nombre" value="" ></td>'+
             '   </tr>'+
              '  <tr>'+
              '     <th class="padded">Apellidos (*)</th>'+
              '     <td class="padded"><input required size="45" class="content" type="text" name="apellidos" value="" ></td>'+
           /*   '  </tr>'+
             
             '   <tr>'+
             '      <th class="padded">Email (*)</th>'+
             '      <td class="padded"><input size="45"  class="content" type="text" id="new_email" name="email" value=""></td>'+*/
             '   </tr></table><br/>'+

             '<div><input id="protDataAuthorization" type="checkbox">Autorizo la inclusión de mis datos personales en el fichero de tratamiento de datos "<b>REGISTRO DE USUARIOS DE IDEARAGON</b>"</input></div>'+
             '<div>Le informamos que los datos personales recogidos a través de este formulario se incluirán en el <b>fichero de tratamiento de datos “REGISTRO DE USUARIOS DE IDEARAGON”</b> y serán tratados con la finalidad de darle de alta en la presente aplicación web geográfica. El órgano responsable del tratamiento es la Dirección General de Ordenación del Territorio del Gobierno de Aragón. Sus datos personales no serán comunicados a terceros salvo obligación legal. Podrá ejercer sus derechos de acceso, rectificación, supresión y portabilidad de sus datos, de limitación y oposición a su tratamiento ante el responsable del tratamiento. Antes de dar su autorización, le aconsejamos leer la siguiente <b><a href="javascript:showAvisoProtDatos(500)">información adicional sobre el aviso legal y la protección de datos personales</a></b></div>'+
			'</form>';
}
/**
 * Muestra di�logo para registro de nuevo usuario
 */
function registerUser(){


	 dialog= $('<div></div>')
		.html(getRegistrationForm())
		.dialog({
			autoOpen: true,
			close: function(){
				dialog.dialog( "destroy" );
			},
			buttons: {
				"Solicitar el alta de usuario": registrar
					
				
			},
			width: 600,
			title: 'Registro de usuario',
			modal: true
		});
	}

/**
 * Comprueba que la contrase�a es v�lida
 * @param pass contrase�a introducida
 * @param pass2 contrase�a introducida en casilla de confirmaci�n
 * @returns {Boolean} true si ambas contrase�as coinciden y tienen al menos 6 caracteres, false en caso contrario
 */
function checkPwd(pass, pass2){
	if ((!pass || pass.length<=0)||(!pass2 || pass2.length<=0)){	
		ignoreKeyPressEvent=true;
			alert("Debe rellenar la contraseña y confirmarla");
			ignoreKeyPressEvent=false;
			return false;
		}
		if  (pass != pass2){
			ignoreKeyPressEvent=true;
			alert("La contraseña y su confirmación no coinciden");
			ignoreKeyPressEvent=false;
			return false;
		}
		if (pass.length<6){
			ignoreKeyPressEvent=true;
		alert("La contraseña deben tener al menos 6 caracteres");
		ignoreKeyPressEvent=false;
			return false;
		}
		return true;
}

/**
 * Comprueba que los datos de registro introducidos en el formulario son correctos
 * @param newUser true si se trata de un nuevo usuario, false si se trata de una edici�n de un usuario registrado
 * @returns {Boolean} true si los datos son correctos, false en caso contrario
 */
function comprobarRegistro(newUser){
	if (newUser){
	var login = document.getElementById("new_username").value;
	if (!login || login.length<=0){
	ignoreKeyPressEvent=true;
		alert("Debe introducir el nombre de usuario");
		ignoreKeyPressEvent=false;
		return false;
	}
	var pass = document.getElementById("new_password").value;
	var pass2 = document.getElementById("new_password2").value;
	if (!checkPwd(pass,pass2)){
		return false;
	}
}
		var email = document.getElementById("new_email").value;
	if (!email || email.length<=0){
		ignoreKeyPressEvent=true;
		alert("Debe introducir el correo electrónico");
			ignoreKeyPressEvent=false;
			return false;
	}
	try{
	var org = document.getElementById("new_org").value;
	var cif = document.getElementById("cif_new").value;
	var phone = document.getElementById("phone_new").value;
//	var fax = document.getElementById("fax_new").value;
	var address = document.getElementById("address_new").value;
	var zip = document.getElementById("zip_new").value;
	var city = document.getElementById("city_new").value;
	var state = document.getElementById("state_new").value;
	if (!(((!org || org.length<=0)&&(!cif || cif.length<=0)&&(!phone || phone.length<=0)&&/*(!fax || fax.length<=0)&&*/(!address || address.length<=0)&&(!zip || zip.length<=0)&&(!city || city.length<=0)&&(!state || state.length<=0))||
	((org && org.length>0)&&(cif && cif.length>0)&&(phone && phone.length>0)&&/*(fax && fax.length>0)&&*/(address && address.length>0)&&(zip && zip.length>0)&&(city && city.length>0)&&(state && state.length>0)))){
	ignoreKeyPressEvent=true;
		alert("Si representa a una organización debe rellenar todos los datos relativos a la misma");
			ignoreKeyPressEvent=false;
			return false;
	
	}
	}
	catch(err){
		// no están en el formulario esos campos
	}
	if ($("#protDataAuthorization")&& (!$("#protDataAuthorization").attr("checked"))&& (!$("#protDataAuthorization").prop("checked"))){
		alert('Debe autorizar la inclusión de mis datos personales en el fichero de tratamiento de datos "REGISTRO DE USUARIOS DE IDEARAGON" marcando la casilla correspondiente');
		return false;
	}
	return true;
}

function closeRegisterDialog(){
	dialog.dialog( "close" );
	dialog.dialog( "destroy" );
}
/**
 * Registra el usuario con los datos introducidos en el formulario
 */
function registrar(){
$("#new_email").val($("#new_username").val());
				if (!comprobarRegistro(true)){
					return;
				}
				var pass = document.getElementById("new_password").value;
				document.getElementById("new_password").value = encryptPwd(pass);
				document.getElementById("new_password2").value = document.getElementById("new_password").value ;
				$('#registerUserForm').ajaxForm({
					beforeSubmit: function(a,f,o) {
				$('#registerUserForm input').attr('disabled', 'disabled');
					},
					success: function(data) { 
						
								alert("El registro se ha completado con éxito. En breve recibirá un correo electrónico con las instrucciones para su activación");
								 closeRegisterDialog();
						
						
						
					},
					 error:  function(  jqXHR,  textStatus,  errorThrown) { 
							if (jqXHR.status=412){
								alert("El usuario ya existe");
							}
							else{
						  alert("No se puedo completar el registro ("+errorThrown+")"+"\n "+jqXHR.responseText);
							}
						
						document.getElementById("new_password").value = "";
				document.getElementById("new_password2").value ="" ;
				$('#registerUserForm input').removeAttr('disabled');
					}
				});
				$('#registerUserForm').submit();
}

function getResetPwdForm(url_pwd){
	if (!url_pwd){
		url_pwd=document.location.href;
	}
	return '<form id="resetPwdForm" autocomplete="off" name="resetPwdForm" accept-charset="UTF-8" action="/gestorUsuarios/gestorUsuarios" method="get">'+
	'<input name="REQUEST" type="hidden" value="resetPwd">'+
	'<input name="url_pwd" type="hidden" value="'+encodeURIComponent(url_pwd)+'">'+
				'<div>Introduzca el correo electr&oacute;nico con el que se registr&oacute;:</div>'+
				'<table class="text-aligned-left">'+
                        '<tr>'+
                          ' <th class="padded">Correo electr&oacute;nico</th>'+
                          ' <td class="padded"><input class="content" type="text" id="new_email" name="email" value="" ></td>'+
                  
                     
                     '   </tr></table'+
					
                                             '</form>';
}
/**
 * Restablecer contrase�a. Se muestra un di�logo para introducir el e-mail en el que se recibir�n las instrucciones
 * para recuperar la contrase�a
 * @param url_pwd URL que debe mostrar el di�logo para introducir la nueva contrase�a. Se le a�adir� el par�metro TOKEN
 */
function resetPwd(url_pwd){
	
 dialog= $('<div></div>')
	.html(getResetPwdForm(url_pwd))
	.dialog({
		autoOpen: true,
		close: function(){
			dialog.dialog( "destroy" );
		},
		buttons: {
			Aceptar: recuperarPwd
				
			
		},
		width: 600,
		title: 'Recuperar contraseña',
		modal: true
	});
}

function closePwdDialog(){
	dialog.dialog( "close" );
	dialog.dialog( "destroy" );
}
/**
 * Solicita el restablecimiento de contrase�a para el usuario correspondiente al e-mail introducido en el formulario
 */
function recuperarPwd(){
$('#resetPwdForm').ajaxForm({
					beforeSubmit: function(a,f,o) {
				$('#resetPwdForm input').attr('disabled', 'disabled');
					},
					success: function(data) { 
						
								alert("Le hemos enviado un correo electrónico con las instrucciones para restablecer su contraseña");
								closePwdDialog()
						
						
						
					},
					error: function(jqXHR,  textStatus,  errorThrown) { 
						alert("No se puedo restablecer la contraseña ("+errorThrown+"):\n"+getErrorMsg(jqXHR.responseText));
						
						
						$('#resetPwdForm input').removeAttr('disabled');
					}
				});
				$('#resetPwdForm').submit();
}

/**
 * Rellena el formulario con los datos del usuario correspondiente al token
 * @param token
 */
function fillUserData(token){
	$.ajax({
		url: "/gestorUsuarios/gestorUsuarios?REQUEST=GETUSER&TOKEN="+tokenId,
		type: 'POST',
		success:  function(data){
			$("#nombre").val(data.nombre);
			$("#apellidos").val(data.apellidos);
			$("#new_email").val(data.email);
			$("#new_org").val(data.organizacion);
			$("#cif_new").val(data.cif);
			$("#phone_new").val(data.telefono);
			$("#fax_new").val(data.organizacion);
			$("#zip_new").val(data.codigopostal);
			$("#address_new").val(data.direccion);
			$("#city_new").val(data.municipio);
			$("#state_new").val(data.provincia);


		},
		error:  function(  jqXHR,  textStatus,  errorThrown) { 

			alert("No se han podido obtener los datos del usuario "+textStatus +"\n "+errorThrown);}



	});
}

/**
 * Comprueba si existe una cookie de sesi�n v�lida
 * @param callback codigo JS que se ejecutar� si la cookie es v�lida
 */
function testLoginCookie(callback,errorCallback){


		tokenId = getCookieValue(TOKEN_COOKIE_NAME);
		if (tokenId && (tokenId.length>0)){
			$.ajax({
				url:"/gestorUsuarios/gestorUsuarios?REQUEST=VALIDATETOKEN&TOKEN="+tokenId,
				type: 'GET',
				
				success: function(data) {
					
					setLogged(data);
					eval(callback);
				},
				error: function(data, textStatus, errorThrown) {
					tokenId=null;
					if (errorCallback){
						eval(errorCallback);
					}
					
					
				}/*,
				async: false*/
			});
		}
		else{
			if (errorCallback){
				eval(errorCallback);
			}
		}
}

/**
 * Comprueba si existe una cookie de sesi�n v�lida
 * @param callback codigo JS que se ejecutar� si la cookie es v�lida
 */
function testLoginCookieIGEAR(callback,errorCallback){


		tokenId = getCookieValue(TOKEN_COOKIE_IGEAR);
		
		var url="/gestorUsuarios/gestorUsuariosIGEAR"
		if ((typeof esRiesgos!='undefined') && esRiesgos){
			url =riesgosWS_url;
		}
		if (tokenId && (tokenId.length>0)){
			$.ajax({
				url:url+"?REQUEST=REFRESHTOKEN&TOKEN="+tokenId,
				type: 'GET',
				
				success: function(data) {
					var campos = data.split("#");
					setLogged(campos[0]);
					tokenId=campos[1];
					eval(callback);
				},
				error: function(data, textStatus, errorThrown) {
					tokenId=null;
					if (errorCallback){
						eval(errorCallback);
					}
					
					
				}/*,
				async: false*/
			});
		}
		else{
			if (errorCallback){
				eval(errorCallback);
			}
		}
}

/**
 * Guarda en una cookie el token de sesi�n
 * @param data token
 */
function writeLoginCookieIGEAR(data){

			writeSessionCookie (TOKEN_COOKIE_IGEAR, data) ;

}
/**
 * Elimina la cookie de sesi�n
 */
function deleteLoginCookieIGEAR(){

			writeSessionCookie (TOKEN_COOKIE_IGEAR, "") ;
}
/**
 * Elimina la cookie de sesi�n
 */
function deleteLoginCookie(){
	if(testPersistentCookie ()){
		deleteCookie(TOKEN_COOKIE_NAME);
		}
		else{
			writeSessionCookie (TOKEN_COOKIE_NAME, "") ;
		}
}

/**
 * Guarda en una cookie el token de sesi�n
 * @param data token
 */
function writeLoginCookie(data){
	if(testPersistentCookie ()){
		writePersistentCookie(TOKEN_COOKIE_NAME, data, "months", 1);
		}
		else{
			writeSessionCookie (TOKEN_COOKIE_NAME, data) ;
		}
}
