/**
 * 
 */

function getHTTPObject() {
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
			// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

var isNav = (window.navigator.appName.toLowerCase().indexOf("netscape")>=0);
var isIE = (window.navigator.appName.toLowerCase().indexOf("microsoft")>=0);

var isIE6 = false;
if (window.navigator.appVersion.indexOf("MSIE") != -1) {
	if (parseFloat(window.navigator.appVersion.split("MSIE")[1]) < 7) {
		isIE6 = true;
	}
}

//replace the five problem characters for the server's XML parser
function makeXMLsafe(oldString) {
	//alert(oldString);
	oldString = oldString.replace(/&/g, "&amp;");
	oldString = oldString.replace(/'/g, "&apos;");
	oldString = oldString.replace(/>/g, "&gt;");
	oldString = oldString.replace(/</g, "&lt;");
	oldString = oldString.replace(/"/g, "&quot;");
	/*
	oldString = swapStuff(oldString, "'", "&apos;");
	oldString = swapStuff(oldString, ">", "&gt;");
	oldString = swapStuff(oldString, "<", "&lt;");
	oldString = swapStuff(oldString, '"', "&quot;");
	*/
	//alert(oldString);
	oldString = normalizaCadenaURL (oldString);
	return oldString;
}

// replace the five problem characters for the server's XML parser
function makeXMLsafeAcentos(oldString) {
	var aux = oldString;
	aux = aux.replace(/&nbsp;/g, " ");

	aux = aux.replace(/&/g, "&amp;");

	aux = aux.replace(/<link href=".*page.css" rel="StyleSheet" type="text\/css">/g, "");

	aux = aux.replace(/&amp;ordm;/g, "&#186;");

	if (aux.indexOf("src='") != -1) {
		if (aux.indexOf("src='http://") == -1) {
			aux = aux.replace(/src='/g, "src='" + urlApp);
		}
	}

	if (aux.indexOf("src=\"") != -1) {
		if (aux.indexOf("src=\"http://") == -1) {
			aux = aux.replace(/src="/g, "src=\"" + urlApp);
		}
	}

	aux = aux.replace(/&amp;ordm;/g, "&#186;");
	aux = aux.replace(/&amp;ordf;/g, "&#170;");
	aux = aux.replace(/&amp;quot;/g, "&#34;");
	aux = aux.replace(/&amp;aacute;/g, "&#225;");
	aux = aux.replace(/&amp;eacute;/g, "&#233;");
	aux = aux.replace(/&amp;iacute;/g, "&#237;");
	aux = aux.replace(/&amp;oacute;/g, "&#243;");
	aux = aux.replace(/&amp;uacute;/g, "&#250;");
	aux = aux.replace(/&amp;ntilde;/g, "&#241;");
	aux = aux.replace(/&amp;uuml;/g, "&#252;");
	aux = aux.replace(/&amp;Aacute;/g, "&#193;");
	aux = aux.replace(/&amp;Eacute;/g, "&#201;");
	aux = aux.replace(/&amp;Iacute;/g, "&#205;");
	aux = aux.replace(/&amp;Oacute;/g, "&#211;");
	aux = aux.replace(/&amp;Uacute;/g, "&#218;");
	aux = aux.replace(/&amp;Ntilde;/g, "&#209;");
	aux = aux.replace(/&amp;Uuml;/g, "&#220;");

	aux = aux.replace(/á/g, "&#225;");
	aux = aux.replace(/é/g, "&#233;");
	aux = aux.replace(/í/g, "&#237;");
	aux = aux.replace(/ó/g, "&#243;");
	aux = aux.replace(/ú/g, "&#250;");
	aux = aux.replace(/ñ/g, "&#241;");
	aux = aux.replace(/ü/g, "&#252;");
	aux = aux.replace(/Á/g, "&#193;");
	aux = aux.replace(/É/g, "&#201;");
	aux = aux.replace(/Í/g, "&#205;");
	aux = aux.replace(/Ó/g, "&#211;");
	aux = aux.replace(/Ú/g, "&#218;");
	aux = aux.replace(/Ñ/g, "&#209;");
	aux = aux.replace(/Ü/g, "&#220;");
	return aux;
}



function replaceChar(entry, oldCh, newCh) {
	var result = entry;
	if (entry) {
		var posAnt = -1;
		var posActual = result.indexOf(oldCh);

		while ((posActual != -1) && (posAnt != posActual)) {
			result = result.replace(oldCh, newCh);
			posAnt = posActual;
			posActual = result.indexOf(oldCh);
		}
	}

	return result;
}

function corrigeCaracteresExtranos(entry) {
	entry = replaceChar(entry, 'á', '&aacute;');
	entry = replaceChar(entry, 'é', '&eacute;');
	entry = replaceChar(entry, 'í', '&iacute;');
	entry = replaceChar(entry, 'ó', '&oacute;');
	entry = replaceChar(entry, 'ú', '&uacute;');
	entry = replaceChar(entry, 'Á?', '&Aacute;');
	entry = replaceChar(entry, 'É', '&Eacute;');
	entry = replaceChar(entry, 'Í?', '&Iacute;');
	entry = replaceChar(entry, 'Ó', '&Oacute;');
	entry = replaceChar(entry, 'Ú', '&Uacute;');
	entry = replaceChar(entry, 'Ä?', '&Auml;');
	entry = replaceChar(entry, 'ä', '&auml;');
	entry = replaceChar(entry, 'ë', '&euml;');
	entry = replaceChar(entry, 'ï', '&iuml;');
	entry = replaceChar(entry, 'ü', '&uuml;');
	//entry = replaceChar(entry, 'ü', '&Uuml;');
	entry = replaceChar(entry, 'Ü', '&Uuml;');
	entry = replaceChar(entry, 'ñ', '&ntilde;');
	entry = replaceChar(entry, 'Ñ', '&Ntilde;');
	entry = replaceChar(entry, 'À', '&Agrave;');
	entry = replaceChar(entry, 'à ', '&agrave;');
	entry = replaceChar(entry, 'è', '&egrave;');
	entry = replaceChar(entry, 'ò', '&ograve;');
	entry = replaceChar(entry, 'ç', '&ccedil;');
	entry = replaceChar(entry, 'Ç', '&Ccedil;');
	entry = replaceChar(entry, '´', '&acute;');
	entry = replaceChar(entry, '’', '&#39;'); // apost
	entry = replaceChar(entry, '‘', '&#39;'); // apost
	entry = replaceChar(entry, 'º', '&ordm;');
	entry = replaceChar(entry, 'ª', '&ordf;');
	entry = replaceChar(entry, '¡', '&iexcl;');
	return entry;
}


function normalizaCadenaSimple(entry) {
	entry = replaceChar(entry, 'á', 'a');
	entry = replaceChar(entry, 'é', 'e');
	entry = replaceChar(entry, 'í', 'i');
	entry = replaceChar(entry, 'ó', 'o');
	entry = replaceChar(entry, 'ú', 'u');
	entry = replaceChar(entry, 'Á', 'A');
	entry = replaceChar(entry, 'É', 'E');
	entry = replaceChar(entry, 'Í', 'I');
	entry = replaceChar(entry, 'Ó', 'O');
	entry = replaceChar(entry, 'Ú', 'U');
	return entry;
}

function normalizaCadena(entry) {
	entry = normalizaCadenaSimple(entry);
	entry = replaceChar(entry, 'ñ', 'n');
	entry = replaceChar(entry, 'Ñ', 'N');
	entry = replaceChar(entry, 'ü', 'u');
	entry = replaceChar(entry, 'Ü', 'ü');
	return entry;
}

function normalizaCadenaURL(entry) {
	entry = normalizaCadena(entry);
	entry = replaceChar(entry, '%E1', 'a');
	entry = replaceChar(entry, '%E9', 'e');
	entry = replaceChar(entry, '%ED', 'i');
	entry = replaceChar(entry, '%F3', 'o');
	entry = replaceChar(entry, '%FA', 'u');
	entry = replaceChar(entry, '%FC', 'u');
	entry = replaceChar(entry, '%C1', 'a');
	entry = replaceChar(entry, '%C9', 'e');
	entry = replaceChar(entry, '%CD', 'i');
	entry = replaceChar(entry, '%D3', 'o');
	entry = replaceChar(entry, '%DA', 'u');
	entry = replaceChar(entry, '%DC', 'u');
	entry = replaceChar(entry, '%20', ' ');
	entry = replaceChar(entry, '%F1', '�');
	entry = replaceChar(entry, '%D1', '�');
	return entry;
}

function getSafeParam(inParam) {
	var aux = replacePlus(inParam);
	aux = unescape(aux);
	return makeXMLsafe(aux);
}

function getInitialParamCaseInsensitive(str, strToFind) {
	var strLower = str.toLowerCase();
	var strToFindLower = strToFind.toLowerCase();
	pos = strLower.indexOf(strToFindLower);
	if (pos != -1) {
		startpos = pos + strToFind.length;
		endpos = str.indexOf("&",startpos);
		if (endpos == -1) {
			endpos = str.length;
		}
		return str.substring(startpos,endpos);
	} else {
		return "";
	}
}
function getInitialParam(str, strToFind, caseInsensitive) {
	pos = str.indexOf(strToFind);
	if (pos != -1) {
		startpos = pos + strToFind.length;
		endpos = str.indexOf("&",startpos);
		if (endpos == -1) {
			endpos = str.length;
		}
		return str.substring(startpos,endpos);
	} else {
		return "";
	}
}
function replacePlus(inText) {
	var re = /\+/g;
	inText = inText.replace(re, " ");
	return inText;
}

function urlAbsoluta(theUrl){
	var url = theUrl
	if ((theUrl.indexOf("http:/")<0)&&(theUrl.indexOf("https:/")<0)&&(theUrl.indexOf("//")!=0)){
		url = server+theUrl;
		
	}
	if ((url.indexOf("http:/")<0)&&(url.indexOf("https:/")<0)){
		url = location.protocol +url;
	}
		return url;
}