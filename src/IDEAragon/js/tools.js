/**
 * Funciones asociadas al panel de herramientas (ventanas del lado izq) de la
 * aplicaci�n
 */

var tools_collapsed=false;
var toolsWin;


function showToolsWin(){

			 toolsWin.open();

}
function refreshTOCAccordion(){
	if ($("#toc_window.ui-accordion").length>0){
		// refrescar el acorde�n
		try{
			if($( "#toc_window" ).accordion( 'option','active' )=="true"){
				
			$( "#toc_window" ).accordion( "refresh" );
			}
		}
		catch(err){

		}
	}
}
function refreshLegendAccordion(){
	if ($("#legend_window.ui-accordion").length>0){
		// refrescar el acorde�n
		try{
		var active = $( "#legend_window" ).accordion( 'option','active' );
				if ((active != 'false')||(isIE &&  (navigator.appVersion.indexOf("MSIE 8")>=0))){
			$( "#legend_window" ).accordion( "refresh" );
			 $( "#legend_window" ).accordion( 'option','active',active );
				}
		
		}
		catch(err){

		}
	}
}

function collapseOrShowTools(){
	if (tools_collapsed){
		$("#tools").css("display","table-cell");
		$("#top-left-arrow").css("visibility","visible");
		$("#bottom-left-arrow").css("visibility","visible");
		$("#top-right-arrow").css("visibility","hidden");
		$("#bottom-right-arrow").css("visibility","hidden");
		tools_collapsed = false;
		mapOL.updateSize();
		/*if(toolsClosed && !$( "#legend_control" ).accordion( "option", "active")){
			$( "#legend_control" ).accordion( "option", "active",true);
			toolsClosed = false;
			$( "#legend_control" ).accordion( "option", "active",false);
		}*/
	}
	else{
		collapseTools();
	}

	
}

function collapseTools(noReload){
	$("#tools").css("display","none");
	$("#top-left-arrow").css("visibility","hidden");
	$("#bottom-left-arrow").css("visibility","hidden");
	$("#top-right-arrow").css("visibility","visible");
	$("#bottom-right-arrow").css("visibility","visible");
	
	mapOL.updateSize();

	tools_collapsed =true;
	
	
}
	

function buscar(){
	$("#tabs").tabs( "enable", 1);
	$("#tabs").tabs("option", "active", 1);
	if (document.getElementById("resultados_tabla")==null)
		initBuscador("searchTab",10);
	buscarSOAP(1);

}


/**
 * Borra la leyenda
 */
function cleanLegend(){
	// Borrar las clases y su leyenda
	$("#legend tr").remove();
	// Quitar el texto de unidades del mapa
	$("#legend_units").text("");
	// Quitar (por si estuviera) la clase que define que la leyenda tenga scroll
	$("#legend_panel").removeClass("legend_panel_with_scroll");
}


/**
 * inicializa los botones de JQuery
 */
function initButtons() {
	$("#searchButton").button({
		text:false,
		icons : {
			primary : "search-icon"
		}
	});

	
}

function loadLayerForLegend(id,title, legend){
	if (legend && (legend !="null")&&(legend !="")){
		removeLayerForLegend(id);
		var imgs = legend.split(",");
		for (var i=0; i<imgs.length; i++){
		$("#legend_panel").append('<img class="ly_'+id+'" SRC="' + imgs[i] + '">'); // line up legend with swatch");
		}
	}
}

function removeLayerForLegend(id){
	$("#legend_panel .ly_"+id).remove();
}


/**
 * inicializaci�n de componentes de las ventanas de herramientas
 */
function initTools(){
	if (isMobile()){
	var html = document.getElementById('tools').innerHTML;
	$("#tools").remove();
		 $("body").append('<div id="tools" data-remodal-id="modalTools">'+
		  '<button data-remodal-action="close" class="remodal-close"></button>'+
		html+
		'</div>');
		toolsWin= $('[data-remodal-id=modalTools]').remodal({closeOnOutsideClick:false,hashTracking:false});
		$(document).on('closing', '.remodal', function (e) {

			  // Reason: 'confirmation', 'cancellation'
			  console.log('Modal is closing' + (e.reason ? ', reason: ' + e.reason : ''));
			});
	 
	}
	//initTipos();
	initButtons();
	$(function() {
		$( "#toc_window" ).accordion({
			heightStyle: "content",
			collapsible:"true",
				disabled:"true",
				active:"false"
		});
	});
	$(function() {
		$( "#legend_window" ).accordion({
			heightStyle: "content",
			collapsible:"true",
			active:"false"
		});
	});
	$(function() {
		$( "#verEjemplos" ).accordion({
			heightStyle: "content",
			collapsible:"true",
			active:"false"
		});
	});
	$.get("/BuscadorCajaUnica/municipios_catastrales.html", function(data){
	    $("#muniCList").append(data);
	});
	var municlist_width =510;
	if((typeof TABLET_WIDTH !== 'undefined')&&(screen.width<=TABLET_WIDTH)){
		municlist_width=360;
	}
	 $( "#muniCList" ).dialog({
		 autoOpen: false,
		 width:municlist_width,
		 maxHeight: 600});
	 
	 
}


function getFechaActual() {
	var fecha=new Date();

	return fecha.getDate() + "/" +	(fecha.getMonth() +1) + "/" + fecha.getFullYear();
}

function downloadData(){
	var editingLayer = getEditingLayer();
	if (editingLayer == null){
		
		return;
	}
	
	var shpDialog=$('<div id="dialogDownloadForm" class="innerWindow"></div>')
	.html(	'<form id="downloadShpForm" name="downloadShpForm" action="'+cartoUpdateURL+'" method="post">'
			+'<input type="hidden" id="TOKEN" name="TOKEN" value="'+tokenId+'"></input>'
			+'<input type="hidden" id="REQUEST" name="REQUEST" value="EXPORT"></input>'
			+'<input type="hidden" id="layerId" name="layerId" value="'+editingLayer.getSource().getParams().LAYERS+'"></input>'
			+'<input type="hidden" id="filter" name="filter" value="0"></input>'
			+'<input type="hidden" id="value" name="value" value=""></input>'+
			
			
			'<table><tr><td>Formato:</td><td><select class="content" id="format" name="format" ><option value="SHP_ESRI" selected="">Shapefile (proyección ArcGIS)</option><option value="SHP">Shapefile</option><option value="KML">KML</option><option value="GPX">GPX-GDB</option></select></td></tr>'+
			'<td>Huso:</td><td><select class="content" id="huso" name="huso" ><option value="30" selected="">30</option><option value="31">31</option></select></td></tr>'+
			'<tr><td>	<input type="radio" checked id="todo" name="filtro" onclick="setDownloadFilter(0)"><label  for="todo">Capa completa</label></td><td></td></tr>'+
			'<tr><td><input type="radio"  id="provi" name="filtro" onclick="activateAutocomplete(\'Provi\')"><label  for="provi">Provincia:</label></td>'+
			'<td><input class="autoCompleteFilter" id="autoCompleteProvi" type="text"></td></tr>'+
			'<tr><td><input type="radio"  id="comarca" name="filtro" onclick="activateAutocomplete(\'Comarca\')"><label  for="comarca">Comarca:</label></td>'+
			'<td><input class="autoCompleteFilter" id="autoCompleteComarca" type="text"></td></tr>'+
			'<tr><td><input type="radio"  id="municipio" name="filtro" onclick="activateAutocomplete(\'Municipio\')"><label  for="municipio">Municipio:</label></td>'+
			'<td><input  class="autoCompleteFilter" id="autoCompleteMunicipio" type="text"></td></tr>'+
			'<tr><td><input  type="radio"  id="bbox" name="filtro" onclick="activateBboxFilter()"><label  for="bbox">Selecci&oacute;n en mapa</label></td><td></td></tr></table>'+
'<input id="descargar" type="submit" onclick="mostrarDescargando()" value="Descargar">'+
	'</form>')
	.dialog({
		autoOpen: true,
		title: 'Descargar datos',
		width:400,
		modal: false,
		close: function(){
			clearExportBbox();
			$( this ).dialog( "destroy" );}
	});
	
	
}

function mostrarDescargando(){
	alert("En breve se iniciará la descarga de los datos solicitados");
}

function activateBboxFilter(){
	setDownloadFilter(9);
	dragBoxControl = new ol.interaction.DragBox({
		
		  style: new ol.style.Style({
		    stroke: new ol.style.Stroke({
		      color: [0, 0, 255, 1]
		    })
		  })
		});

		        /* add the DragBox interaction to the map */
	mapOL.addInteraction(dragBoxControl);

		dragBoxControl.on('boxend', function (evt) {
			
		        var coords = dragBoxControl.getGeometry().getCoordinates();
		       
		        var feature = new ol.Feature({
					  geometry: new ol.geom.Polygon(coords),
					 
					  name: 'Bbox'
					});
		        markers.getSource().addFeature(feature);
		       
		        var coordArray = feature.getGeometry().getCoordinates()[0];
		        var jsonGeom = '{"type":"Polygon","coordinates":[[';
		        for (var i=0; i< coordArray.length; i++){
		        	if (i>0){
		        		jsonGeom +=",";
		        	}
		        	jsonGeom= jsonGeom+"["+coordArray[i][0]+","+coordArray[i][1]+"]";
		        	
		        }
		        jsonGeom +=']],"crs":{"type":"name","properties":{"name":"EPSG:25830"}}}';
		        $("#value").val(jsonGeom);
		});
}

function clearExportBbox(){
	try{
		mapOL.removeInteraction(dragBoxControl);
		}
		catch(err){
			
		}
		removeMarkers();
}
function setDownloadFilter(value){
	clearExportBbox();
	$("#filter").val(value);
}


/**
 * carga las unidades correspondientes en los filtros geográficos de la exportación a SHP
 */
function activateAutocomplete(codEsquema){
	
	setDownloadFilter(9);
	$(".autoCompleteFilter").each(function(index, element) {
	   $(this).val("");
	} );
	cUnitList = new Object();
	dUnitList = new Object();
	
	var unidades;
var capa;
var campo;
	
	if (codEsquema=="Municipio"){
	
		if (typeof municipios=="undefined"){
			// Obtener la lista de municipios del servicio BD_GIS correspondiente

			municipios =muniDefault.split("\n")	;
			muniDefault = null;
			municipios.sort();
		}
		unidades = municipios;
		capa = muniLayer;
		campo = muniId;
	}
	else if (codEsquema=="Comarca"){
		if (typeof comarcas=="undefined"){
			
			comarcas =comarcaDefault.split("\n")	;
			comarcaDefault=null;
			comarcas.sort();
			
		}
		unidades = comarcas;
		capa = comarcaLayer;
		campo = comarcaId;
	}
	else if (codEsquema=="Provi"){
		if (typeof provincias=="undefined"){
			
			provincias =provinciasDefault.split("\n")	;
			provinciasDefault = null;
			provincias.sort();
			
		}
		capa = provinciasLayer;
		campo = provinciasId;
		unidades = provincias;
	}
	// se ordenan por el nombre de forma ascendente
		customarrayUnitJQ=new Array();
		for (var i=0; i<unidades.length; i++){
			if (unidades[i].length>0){
				var atr = unidades[i].split(";");
				
					addU(atr[0],parseInt(atr[1],10));
					
					customarrayUnitJQ.push(atr[0]);

				
			}
		}
		
	initAutocomplete(customarrayUnitJQ, codEsquema, capa, campo);
}
	
	

function addU(dunidad, cunidad, c_comarca) {
	cUnitList[dunidad]=cunidad;
	dUnitList[cunidad]=dunidad;
	if (c_comarca){
		comarcaUnitList[dunidad]=c_comarca;
	}

}

function initAutocomplete(customarrayUnitJQ, esquema, capa, campo){

	$( "#autoComplete"+esquema ).autocomplete({
		select: function( event, ui ) {
			showLoading(true);
			$("#value").val("");
			$.ajax({
				url: queryUrlRef+'?service=WFS&' +
				'version=1.0.0&request=GetFeature&typename='+capa+'&' +
				'outputFormat=application/json&srsname=EPSG:25830&' +
				'CQL_FILTER='+campo+"="+cUnitList[ui.item.value],
				type: 'GET',
				 contentType: "application/text;charset=utf-8",
				success: function(data) {
					
					
					if (data.totalFeatures<=0){
						alert("No se ha podido obtener la extensión de "+ui.item.value);
					}
					else if (data.totalFeatures==1) {
						$("#value").val(JSON.stringify(data.features[0].geometry).replace("}",
								',"crs":{"type":"name","properties":{"name":"EPSG:25830"}}}'));
					}
					showLoading(false);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("No se ha podido obtener el el "+esquema+" indicado "+textStatus +"\n "+errorThrown);
					
					showLoading(false);
				}
			});
		},
		source: function(request, response) {

			var results = $.ui.autocomplete.filter(customarrayUnitJQ, request.term);
			response(results.slice(0, 10));
		}
	});

}

/**
 * muestra u oculta el mensaje de cargando
 * @param show si es true se mostrar� el mensaje si es false se ocultar�
 */
function showLoading(show){
	if (show)
		document.getElementById("visorLoadingOverlay").style.visibility="visible";
	else
		document.getElementById("visorLoadingOverlay").style.visibility="hidden";
}