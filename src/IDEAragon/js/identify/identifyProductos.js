function identifyProductos(xCoordUsr, yCoordUsr,func, esVisor) {
	showSearching(true);
	$.ajax({
		url: CONSULTA_PRODUCTOS_URL + "?x="+xCoordUsr+"&y="+yCoordUsr+"&filtrosBusqueda="+filtrosBusqueda,
		type: 'POST',
		success: function(result){
			var jsondata = result.replace(/\n/g,"").replace(/},]}/g, "}]}").trim();
			jsondata = jsondata.replace(/\\/g, "/");

		//	console.log(jsondata);
			var dataJson=JSON.parse(jsondata);
			productos_result=dataJson.productos;
			var idContent=parseResponseGetProductosXY(productos_result, esVisor);
			showSearching(false);
			eval(func);
		},
		error: function(jqXHR, textStatus, errorThrown) { showSearching(false);alert("Error al realizar la búsqueda "+textStatus +"\n "+errorThrown);},
		processData: true,
		//contentType: "text/xml; charset=\"utf-8\"",
		async: true
	});	
}
function situaHtml(idContent,resultadosIdZone){
	document.getElementById(resultadosIdZone).style.top = 100 + "px";
	document.getElementById(resultadosIdZone).style.left = 100 + "px";
	//document.getElementById('resultadosIdZone').style.border = "#000000 solid 2px";
	document.getElementById(resultadosIdZone).innerHTML = idContent;
	for(var i=0;i<idsDelete.length;i++)
	{
		$("#"+idsDelete[i]).hide();
	}
}

function isFechaValida(fecha)
{
	var fechaItemInicio,fechaItemFin
	var fechaValida = false;
	if (fecha == "s XVIII") {
		fechaItemInicio = "1700";
		fechaItemFin = "1799";
		fechaValida = true;
	} else if (fecha == "s.XX") {
		fechaItemInicio = "1900";
		fechaItemFin = "1999";
		fechaValida = true;
	} else if (fecha == "1956-57") {
		fechaItemInicio = "1956";
		fechaItemFin = "1957";
		fechaValida = true;
	} else if (parseInt(fecha.substring(0, 4))) {
		fechaItemInicio = fecha.substring(0, 4);
		fechaItemFin = fecha.substring(0, 4);
		fechaValida = true;
	}
	if (fechaValida) {
		if ((fechaItemInicio > fechaFin) || (fechaItemFin < fechaInicio)) {
			return false;
		}
		else{
			return true;
		}
	}
}
var idsDelete=[];
function parseResponseGetProductosXY(productos, esVisor)
{
	var idContent = '';
	idContent += "<table class='search_map' width='100%' cellpadding='0' cellspacing='2'>";
	if (productos.length==0) {
		//if (listaColeccCount == 0) {
			idContent += '<tr><td class="normal item" align="left">No hay datos</td></tr>';
		/*}
		else {
			idContent += '<tr><td class="item header" align="center"><br/>No hay datos visibles con los criterios de filtrado seleccionados.</td></tr>';
		}*/
	} else {
		idContent += '<tr class="item header">';
		idContent += '<td align="center" colspan="'+gfiColumnsCount+'">';
		if(!esVisor)
		{
			idContent += '<input id="mantenerExtension" type="checkbox" checked alt="Mantener extensi&oacute;n actual del mapa">Mantener extensi&oacute;n actual del mapa</input>'; 
		}
		idContent += '</td>';
		idContent += '</tr>';
		idContent += '<tr class="item header">';
		idContent += '<td align="center" colspan="6">';
		idContent += '</td>';
		idContent += '</tr>';
	}

	var colecciones= [];
	var coleccProduct=[];
	var tipos=[];
	var coleccTipo=[];
	idsDelete=[];
	for (var i = 0; i < productos.length; i++) {

		var tipoI=productos[i].tipo;
		if(tipos.indexOf(tipoI)==-1)
		{
			tipos.push(tipoI);
			coleccTipo[tipoI]=[];
		}
		if(coleccTipo[tipoI].indexOf(productos[i].label_coleccion)==-1)
		{
			coleccTipo[tipoI].push(productos[i].label_coleccion);
		}
		var colI=productos[i].label_coleccion;
		if(colecciones.indexOf(colI)==-1)
		{
			colecciones.push(colI);
			coleccProduct[colI]=[];
			coleccProduct[colI].push(productos[i]);
		}
		else
		{
			coleccProduct[colI].push(productos[i]);
		}   
	}

	for(var i=0;i<tipos.length;i++)
	{
		idContent+='<tr class="itemBig header blanco" id="tipo'+i+'">'+
			'<td align="left" colspan="5">'+
				'<table width="100%" cellpadding="0" cellspacing="0">'+
					'<tbody>'+
						'<tr class="itemBig header blanco">'+
							'<td align="left">'+
								'<a class="treeIcon" href=\'javascript:toggleZoneCarpeta("coleccTipo'+i+'", "informaci&oacute;n de '+tipos[i]+'")\' title="Mostrar informaci&oacute;n de '+tipos[i]+'" id="coleccTipo'+i+'Link">'+
									'<img src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed_blanco.png" border="0"></img>'+
								'</a>'+
								'<span class="alignMiddle">'+tipos[i]+'</span>'+
							'</td>'+
						'</tr>'+
					'</tbody>'+
				'</table>'+
			'</td>'+
		'</tr>'+
		'<tr class="itemBig blanco colecc">'+
			'<td align="left" colspan="5">'+
				'<table id="coleccTipo'+i+'Table" style="background-color: rgb(255, 255, 255); display: none; visibility: hidden; table-layout: fixed; width: 100%;" cellpadding="0" cellspacing="0">'+
					'<tbody>';
		var coleccionesTipo=coleccTipo[tipos[i]];
		var colTieneProds=false;
		for(var colI=0;colI<coleccionesTipo.length;colI++)
		{
			var productosCol=coleccProduct[coleccionesTipo[colI]];
			idContent+='<tr class="itemNormal headerNormal blanco" id="col'+colI+'Tipo'+i+'">'+
							'<td align="left" style="width:100%;">'+
								'<table class="itemNormal headerNormal blanco" cellpadding="0" cellspacing="0" width="100%">'+
									'<tbody>'+
										'<tr>'+
											'<td nowrap="">'+
												'<img src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png" border="0" style="visibility:hidden;"></img>'+
												'<a class="treeIcon" href=\'javascript:toggleZoneCarpeta("colecc'+colI+'Tipo'+i+'", "informaci&oacute;n")\' title="Mostrar informaci&oacute;n" id="colecc'+colI+'Tipo'+i+'Link">'+
													'<img src="/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png" border="0"></img>'+
												'</a>'+
												'<a class="blanco alignMiddle" href=\'javascript:toggleZoneCarpeta("colecc'+colI+'Tipo'+i+'", "informaci&oacute;n")\' title="Mostrar informaci&oacute;n">'+coleccionesTipo[colI]+' ('+productosCol.length+')'+
												'</a>&nbsp;';
												var altText = "Ver "  + productosCol[0].capa;
												if (productosCol[0].capa.length > tamMaxCapa) {
													altText = "Ver "  + productosCol[0].capa.substring(0, tamMaxCapa) + "...";
												}
												var _xMin = replaceChar(productosCol[0].x1, ",", ".");
												var _yMin = replaceChar(productosCol[0].y1, ",", ".");
												var _xMax = replaceChar(productosCol[0].x2, ",", ".");
												var _yMax = replaceChar(productosCol[0].y2, ",", ".");
												if ((productosCol[0].location.length>0 && productosCol[0].location!="null")&& (productosCol[0].servicio.length>0 && productosCol[0].servicio!="null" ) ) {
													if (esVisor) {
														idContent += '<a class="normal itemEnlace" title="' + altText + '" href="javascript:selectedImageVisor(\'' + productosCol[0].location + '\',\'' + productosCol[0].servicio + '\',\'' + productosCol[0].capa + '\',\'' + productosCol[0].coleccion + '\')">';
													} else {
														idContent += '<a class="normal itemEnlace" title="' + altText + '" href="javascript:selectedImage(\'' + productosCol[0].location + '\',\'' + productosCol[0].servicio + '\',\'' + productosCol[0].capa + '\',' + _xMin + ',' + _yMin + ',' + _xMax + ',' + _yMax + ',\'' + productosCol[0].coleccion + '\', false, ' + (productosCol[0].georref=='S') + ',' + 0 + "," + 0 + ',' + 0 + ',\'' + productosCol[0].escala + '\',\'' + productosCol[0].fecha + '\',\'' +productosCol[0].legend + '\')">';
													}
													idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altText + '"></a>&nbsp;';
												} else {
													idContent += '-';
												}
												
										idContent+='<a class="itemBlancoEnlace blanco" href=\'javascript:activaEsquema("'+i+'","'+colI+'", "'+productosCol[0].coleccion+'")\' title="Activar esquema" id="aActEsquemaTipo'+i+'col'+colI+'">'+
													'<img src="/lib/IDEAragon/themes/default/images/actEsquema.png" border="0" id="imgActEsquemaTipo'+i+'col'+colI+'">'+
												'</a>'+
											'</td>'+
										'</tr>'+
									'</tbody>'+
								'</table>'+
							'</td>'+
						'</tr><tr>'+
								'<td>'+
									'<table id="colecc'+colI+'Tipo'+i+'Table" style="display:none;visibility:hidden;table-layout:fixed;" class="search_map" width="100%" cellpadding="0" cellspacing="2">'+
										'<tbody>';
			if (esVisor) {
				idContent+=	'<tr class="itemBig negro fondoGrisClaro">'+
												'<td align="left" class="paddingIzquierda" style="width:36%;">'+
													'<b>Nombre</b>'+
												'</td>'+
												'<td align="center" style="width:12%;">'+
													'<b>Ver en visor</b>'+
												'</td>'+
												'<td align="center" style="width:12%;">'+
													'<b>Ver en cartoteca</b>'+
												'</td>'+
												'<td align="center" style="width:12%;">'+
													'<b>Descargar</b>'+
												'</td>'+
												'<td align="center" style="width:14%;">'+
													'<b>Fecha</b>'+
												'</td>'+
												'<td align="center" style="width:14%;">'+
													'<b>Escala</b>'+
												'</td>'+
											'</tr><tr></tr>';
			} else {
				idContent+=	'<tr class="itemBig negro fondoGrisClaro">'+
												'<td align="left" class="paddingIzquierda" style="width:42%;">'+
													'<b>Nombre</b>'+
												'</td>'+
												'<td align="center" style="width:12%;">'+
													'<b>Ver</b>'+
												'</td>'+
												'<td align="center" style="width:12%;">'+
													'<b>Descargar</b>'+
												'</td>'+
												'<td align="center" style="width:17%;">'+
													'<b>Fecha</b>'+
												'</td>'+
												'<td align="center" style="width:17%;">'+
													'<b>Escala</b>'+
												'</td>'+
											'</tr><tr></tr>';
			}

			var tieneProds=false;
			for(var prodI=0;prodI<productosCol.length;prodI++)
			{
				var productoFila=productosCol[prodI];
				var valida=isFechaValida(productoFila.fecha);
				if(!valida && filtrarPorFecha)
				{
					continue;
				}
				else
				{
					tieneProds=true;
					colTieneProds=true;
				}
				var itemStyle=(prodI%2!=0) ? " oddRow" : "evenRow";
				idContent+='<tr class="normal item '+itemStyle+'">'+
												'<td class="normal item paddingIzquierda" align="left">'+productoFila.descrip+
												'</td><td class="normal item" align="center">';
				var altText = "Ver "  + productoFila.capa;
				if (productoFila.capa.length > tamMaxCapa) {
					altText = "Ver "  + productoFila.capa.substring(0, tamMaxCapa) + "...";
				}
				var _xMin = replaceChar(productoFila.x1, ",", ".");
				var _yMin = replaceChar(productoFila.y1, ",", ".");
				var _xMax = replaceChar(productoFila.x2, ",", ".");
				var _yMax = replaceChar(productoFila.y2, ",", ".");

				if ((productoFila.location.length>0 && productoFila.location!="null")&& (productoFila.servicio.length>0 && productoFila.servicio!="null" ) ) {
					if (esVisor) {
						idContent += '<a class="normal itemEnlace" title="' + altText + '" href="javascript:selectedImageVisor(\'' + productoFila.location + '\',\'' + productoFila.servicio + '\',\'' + productoFila.capa + '\',\'' + productoFila.coleccion + '\')">';
						idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altText + '"></a>&nbsp;';
						idContent+='</td><td class="normal item" align="center">';
						idContent += '<a class="normal itemEnlace" title="' + altText + '" href="javascript:selectedImageCartoteca(\'' + productoFila.location + '\',\'' + productoFila.servicio + '\',\'' + productoFila.capa + '\',\'' + _xMin + '\',\'' + _yMin + '\',\'' + _xMax + '\',\'' + _yMax + '\',\'' + productoFila.coleccion + '\', false, ' + (productoFila.georref=='S') + ',' + 0 + "," + 0 + ',' + 0 + ',\'' + productoFila.escala + '\',\'' + productoFila.fecha + '\',\'' +productoFila.legend + '\')">';
						idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altText + '"></a>&nbsp;';
					} else {
						idContent += '<a class="normal itemEnlace" title="' + altText + '" href="javascript:selectedImage(\'' + productoFila.location + '\',\'' + productoFila.servicio + '\',\'' + productoFila.capa + '\',' + _xMin + ',' + _yMin + ',' + _xMax + ',' + _yMax + ',\'' + productoFila.coleccion + '\', false, ' + (productoFila.georref=='S') + ',' + 0 + "," + 0 + ',' + 0 + ',\'' + productoFila.escala + '\',\'' + productoFila.fecha + '\',\'' +productoFila.legend + '\')">';
						idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altText + '"></a>&nbsp;';
					}
				} else {
					if (esVisor) {
						idContent += '-';
					  idContent+='</td><td class="normal item" align="center">';
						idContent += '-';
					} else {
						idContent += '-';
					}
				}


									  idContent+='</td><td class="normal item" align="center">'+
													getDescargasProducto(productoFila.descargas.split(","),productoFila.path,productoFila.name,"",productoFila.coleccion)+
												'</td>'+
												'<td class="normal item" align="center">'+productoFila.fecha+'</td>'+
												'<td class="normal item" align="center">'+productoFila.escala+'</td>'+
											'</tr>';
			}
			idContent+='</tbody></table></td></tr>';
			if(!tieneProds)
			{
				//ELIMINAR COLECCION
				idsDelete.push('col'+colI+'Tipo'+i)
			}
		}
		idContent+='</tbody></table></td></tr>';
		if(!tieneProds)
		{
			//Eliminar tipo
			idsDelete.push('tipo'+i);
		}
	}

	idContent+='</table></td></tr></tbody></table>';
	
	return idContent;

	//hideFotograma();

	//hideTextoAyuda();

	//showResults();
}

var closeHeaderCol='</tr></tbody></table></td></tr></tbody></table></td></tr>';

var closeSubHeader='</tbody></table></td></tr>';

function getDescargasProducto(formatosLista,ruta,theName,tam,theCollection){
	var rutaImg="";
	var formatoTxt="";
	var formatoHtml="";
	if(formatosLista!="null")
	{
		for (var i = 0; i < formatosLista.length; i++) {
			var laRuta = ruta + theName + formatosLista[i];
			var rutaImg = "/lib/IDEAragon/themes/default/images/i/ico_default.png";
			var formatoTxt = "";
			var formato = formatosLista[i];
			if (formato.indexOf(".") != 0) {
				formato = "." + formato;
			}
			var res=getFormato(formato,rutaImg,formatoTxt);
			rutaImg=res.rutaImg;
			formatoTxt=res.formatoTxt;
			formatoHtml += '<a class="normal itemEnlace" align="right" title="Descargar en formato ' + formatoTxt + tam + '" href="#" >';
			formatoHtml += '<img border="0" src="' + rutaImg + '" alt="Descargar ' + theName + " (" + theCollection + ')" onclick="window.open(\'' + laRuta + '\')"></a>&nbsp;&nbsp;';
		}
	}
	else
	{
		formatoHtml="<a>-</a>";
	}
	return formatoHtml;
}