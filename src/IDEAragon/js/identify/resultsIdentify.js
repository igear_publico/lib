restrictions = new Array();
restrictionGrupoCount = 0;

restrictionList = new Array();
restrictionListCount = 0;

queryableLayerCount = 0;
queryableLayer = new Array();

// TIPO == 0 => Solo para busquedas
// TIPO == 1 => Permite activar capa tematica (mediante enlace)
// TIPO == 2 => NO Permite activar capa tematica
function addQueryableLayer(id, name, tipo, idQuery) {
	idx = queryableLayerCount;

	queryableLayer[idx] = new Array();
	queryableLayer[idx]["ID"] = id;
	queryableLayer[idx]["NAME"] = name;
	queryableLayer[idx]["TIPO"] = tipo;
	if (idQuery) {
		queryableLayer[idx]["ID_QUERY"] = idQuery;
	} else {
		queryableLayer[idx]["ID_QUERY"] = id;
	}
	queryableLayerCount++;

	return idx;
}

function getIndexQueryableLayer(idToFind) {
	for (kk = 0; kk < queryableLayerCount; kk++) {
		if (queryableLayer[kk]["ID"] == idToFind) {
			return kk;
		}
	}

	return -1;
}

function addGrupo(nombre) {
	idx = restrictionGrupoCount;

	restrictions[idx] = new Array();
	restrictions[idx]["nombre"] = nombre;
	restrictions[idx]["count"] = 0;

	restrictionGrupoCount++;
	if (typeof masInfo!= 'undefined'){
	masInfo[idx] = new Array();
	}
}

function addSubgrupo(grupo, nombre) {
	idx = restrictions[grupo]["count"];

	restrictions[grupo][idx] = new Array();
	restrictions[grupo][idx]["nombre"] = nombre;
	restrictions[grupo][idx]["count"] = 0;

	restrictions[grupo]["count"]++;
	if (typeof masInfo!= 'undefined'){
	masInfo[grupo][idx] = masInfoDefault;
	}
}

function addRestriction(grupo, subgrupo, id, nombre, showResultCount, tooltip) {
	idx = restrictions[grupo][subgrupo]["count"];

	restrictions[grupo][subgrupo][idx] = new Array();
	restrictions[grupo][subgrupo][idx]["NOMBRE"] = nombre;
	if (id) {
		restrictions[grupo][subgrupo][idx]["ID"] = id;
	} else {
		restrictions[grupo][subgrupo][idx]["ID"] = nombre;
	}

	if (tooltip) {
		restrictions[grupo][subgrupo][idx]["TOOLTIP"] = tooltip;
	} else {
		restrictions[grupo][subgrupo][idx]["TOOLTIP"] = nombre;
	}
	restrictions[grupo][subgrupo][idx]["showResultCount"] = showResultCount;
		// numero de capas asociadas a esta restriccion
	restrictions[grupo][subgrupo][idx]["count"] = 0;

	restrictions[grupo][subgrupo]["count"]++;

	// actualizar tambien la lista unidimensional paralela
	restrictionList[restrictionListCount] = new Array();
	restrictionList[restrictionListCount]["grupo"] = grupo;
	restrictionList[restrictionListCount]["subgrupo"] = subgrupo;
	restrictionList[restrictionListCount]["subsubgrupo"] = idx;
	restrictionList[restrictionListCount]["showResultCount"] = showResultCount;
	restrictionList[restrictionListCount]["ID"] = restrictions[grupo][subgrupo][idx]["ID"];
	restrictionListCount++;
}

function addLayerToRestriction(idRestriccion, idLayer) {
				console.log("DEPRECATED: Quitar consulta a capa vectorial por una GFI para (" + idRestriccion + "-" + idLayer + ")");
				/*
	var posLayer = getIndexQueryableLayer(idLayer);

	if (posLayer == -1) {
		alert("Capa con id " + idLayer + " no encontrada");
	}
	addItemToRestriction(idRestriccion, idLayer, posLayer, false);*/
}

gfiCount = 0;
gfiList = new Array();

function addWFSToRestriction(idRestriccion, urlGFI, id) {
	gfiList[gfiCount] = new Array();
	var urlGFIAux = urlGFI;

	if (typeof selFieldList[id] !='undefined') {
		gfiList[gfiCount]["propertyName"] =selFieldList[id];
	}

	gfiList[gfiCount]["url"] = urlGFIAux;
	gfiList[gfiCount]["ID"] = id;
	gfiList[gfiCount]["esWFS"] = true;

	addItemToRestriction(idRestriccion, urlGFI, gfiCount, false);

	gfiCount++;
}

function addGFIToRestriction(idRestriccion, urlGFI, id) {
	gfiList[gfiCount] = new Array();
	gfiList[gfiCount]["url"] = urlGFI;
	gfiList[gfiCount]["ID"] = id;
	gfiList[gfiCount]["esWFS"] = false;

	addItemToRestriction(idRestriccion, urlGFI, gfiCount, true);

	gfiCount++;
}

function addItemToRestriction(idRestriccion, valor, posic, esGFI) {
	var idx = -1;
	for (kk = 0; kk < restrictionListCount; kk++) {
		if (restrictionList[kk]["ID"] == idRestriccion) {
			idx = kk;
		}
	}

	if (idx == -1) {
		alert("Restricción con id " + idRestriccion + " no encontrada");
		return -1;
	} else {
		var grupo = restrictionList[idx]["grupo"];
		var subgrupo = restrictionList[idx]["subgrupo"];
		var subsubgrupo = restrictionList[idx]["subsubgrupo"];

		idx2 = restrictions[grupo][subgrupo][subsubgrupo]["count"];
		restrictions[grupo][subgrupo][subsubgrupo][idx2] = new Array();
		restrictions[grupo][subgrupo][subsubgrupo][idx2]["value"] = valor;
		restrictions[grupo][subgrupo][subsubgrupo][idx2]["posicion"] = posic;
		restrictions[grupo][subgrupo][subsubgrupo][idx2]["esGFI"] = esGFI;
		restrictions[grupo][subgrupo][subsubgrupo]["count"]++;
	}
}
function generaInicioCabecera(_nombre, _id) {
	var idContent = '';
	idContent += "<tr class='itemBig header blanco'>";
	idContent += "<td>";
	idContent += "<a class='treeIcon' href='toggleZoneCarpeta(\"" + _id + "\", \"informaci&oacute;n de " + _nombre + "\")' title='Ocultar informaci&oacute;n de " + _nombre + "' id='" + _id + "Link'>";
	idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened_blanco.png' border='0'></img></a>";
	idContent += _nombre;
	idContent += "</td>";
	idContent += "</tr>";

	idContent += "<tr><td><table width='100%' cellpadding='0' cellspacing='0' id='" + _id + "Table' style='display:block;visibility:visible;table-layout:fixed;'>";

	return idContent;
}

function generaFinCabecera() {
	return "</table></td></tr>";
}

function isIncluded(listaNames, nombre, numTotal) {
	if (numTotal > 0) {
		for (var i = 0; i < numTotal; i++) {
			if (listaNames[i] == nombre) {
				return true;
			}
		}
	}
	return false;
}

var opcionSelecc = '';
function highlightOpcion(newI, newJ, newK) {
	unselectOpcion();
	opcionSelecc = 'opcion_' + newI + "_" + newJ + "_" + newK;
	document.getElementById(opcionSelecc).style.fontWeight = 'bold';
	document.getElementById(opcionSelecc).style.color = '#bb0000';
}

function unselectOpcion() {
	if (opcionSelecc != '') {
		document.getElementById(opcionSelecc).style.fontWeight = 'normal';
		document.getElementById(opcionSelecc).style.color = '#000000';
	}
	opcionSelecc = '';
}

function updateResults() {
	var idContent = '';
	idContent += "<table width='100%' cellpadding='0' cellspacing='0' style='table-layout:fixed;'>";
	idContent += "<tr>";
	idContent += "<td class='toc_info' valign='top' width='340px'>";

	idContent += "<table width='100%' cellpadding='0' cellspacing='2'>";

	for (var i=0; i < restrictionGrupoCount; i++) {
		idContent += generaInicioCabecera(restrictions[i]["nombre"], "coleccTipo_" + i);

		for (var j=0; j < restrictions[i]["count"]; j++) {
			if (restrictions[i][j]["nombre"] != "") {

				idContent += "<tr>";
				idContent += "<td class='itemNormal headerNormal'>";
				//idContent += "<fieldset class='item normal'>";
				//idContent += "<legend class='item normal cursiva'>";

				idContent += "<a id='colecc" + i + "_" + j + "Link' class='treeIconLevel2' title='Mostrar informaci&oacute;n' href='toggleZoneCarpeta(\"colecc" + i + "_" + j +"\" , \"informaci&oacute;n\")'>";
				idContent += "<img border='0' src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png'>";
				idContent += "</a>";

				idContent += restrictions[i][j]["nombre"];
				//idContent += "</legend>";
				idContent += "<div id='colecc" + i + "_" + j + "Table'>";
				for (var k=0; k < restrictions[i][j]["count"]; k++) {
					idContent += "<table class='toc_info' style='width:340px;' cellpadding='0' cellspacing='0'>";
					idContent += drawResult(i,j,k);
					idContent += "</table>";
				}
				idContent += "</div>";
				idContent += "</td>";
				idContent += "</tr>";
				//idContent += "</fieldset>";
			} else {
				for (var k=0; k < restrictions[i][j]["count"]; k++) {
					idContent += drawResult(i,j,k);
				}
			}
		}
		idContent += generaFinCabecera();
	}
	idContent += "</table>";

	idContent += "</td>";
	idContent += "<td valign='top' id='detailedResultsTable'>";
	idContent += getDetailedResultHTML(0,0,0);
	idContent += "</td>";
	idContent += "</tr>";

	idContent += "</table>";

	document.getElementById("infoTab").innerHTML=idContent;
}

function getNumResultados(i, j, k) {
	var numResultados = 0;
	for (var l=0; l < restrictions[i][j][k]["count"]; l++) {
		idx = restrictions[i][j][k][l]["posicion"];
		numResultados = numResultados + gfiList[idx]["camposRespuesta"]["count"];
	}
	return numResultados;
}

function drawResult(i,j,k) {
	idContent = '';
		// Buscar en la lista de capas asociadas a ver si alguna tiene datos
	nombre = restrictions[i][j][k]["NOMBRE"];
	tooltip = restrictions[i][j][k]["TOOLTIP"];
	theId = restrictions[i][j][k]["ID"];

		// hago que siempre sea clikable ya que como puede que no haya consultado todavia, no se si hay o no resultados
	idContent += "<tr><td class='item normal paddingIzquierda'>";

	idContent += "<a class='itemEnlace' id='opcion_" + i + "_" + j + "_" + k + "' href='#' onclick='getDetailedResult(" + i + "," + j + "," + k +");' title='" + tooltip + "'>"+ nombre;
	idContent += "</a>";

	idContent += "</td>";
	idContent += "</tr>";

	return idContent;
}

function getNombreCampo(str) {
	var tempString = str;
	var nombreCampo = "";
	if ( (shortenSDENames == true) && (tempString.split(".").length > 2) ) {
		nombreCampo = tempString.split(".")[tempString.split(".").length - 1];
	} else {
		nombreCampo = tempString;
	}
	return nombreCampo;
}


function getData(i, j, k) {
	var idContent = "";

	for (var l=0; l < restrictions[i][j][k]["count"]; l++) {
		if (restrictions[i][j][k][l]["esGFI"]) {
			idContent += getResultsGFIHTML(i, j, k, l);
		} else {
			idContent += getResultsWFSHTML(i, j, k, l);
		}
	}
	if (idContent == "") {
		idContent += "<tr><td colspan='3' class='item normal'>";
		idContent += "No hay datos disponibles en esta categor&iacute;a";
		idContent += "</td></tr>";
	}
	return idContent;
}

function isValidField(nombreCampo) {
	if (nombreCampo == "#SHAPE#") {
	} else if (nombreCampo == "#ID#") {
	} else if (nombreCampo == "OBJECTID") {
	} else if (nombreCampo == "OBJECTID_1") {
	} else {
		return true;
	}
	return false;
}

function drawDetailedResult(i, j, k) {
	if (capasEstanListas(i,j,k)) {
		updateAcetateMarca(findXCoordUsr, findYCoordUsr, xCoordUsr, yCoordUsr);

		var objDcha = document.getElementById('detailedResultsTable');
		if (! objDcha) {
			if (! isDownloading) {
				updateResults();
			}
		}
		$("#tabs").tabs( "enable", 2);
		$("#tabs").tabs("option", "active", 2);
		 $("#infoTab").animate({ scrollTop: 0 }, "slow");
		idContent = getDetailedResultHTML(i, j, k);
		if( (i==0)&&(j==0)&&(k==1)&&(typeof TABLET_WIDTH != 'undefined')&&(screen.width<=TABLET_WIDTH)){
		isDownloading=true;
		}
		if (isDownloading) {
			document.getElementById("infoTab").innerHTML=idContent;
			isDownloading = false;
		} else {
			document.getElementById('detailedResultsTable').innerHTML = idContent;
			highlightOpcion(i, j, k);
		}
		ocultaLayerLoading();
		showSearching(false);
		if ((typeof needCalculateBOA!='undefined')&&(needCalculateBOA)){
			 $( "#boaDateI" ).datepicker();
		}
	}
}

function getDetailedResultHTML(i, j, k) {
	idContent = '';

	if (restrictions[i][j][k]["count"] > 0) {
		idContent += "<table valign='top' width='100%' cellpadding='0' cellspacing='10'>";
		idContent += "<tr><td align='center' class='itemTitulo' colspan='3'>";
		idContent += restrictions[i][j][k]["NOMBRE"];
		idContent += "</td></tr>";

		idContent += "<tr><td colspan='3'>";

		idContent += "<table valign='top' width='100%' cellpadding='0' cellspacing='10'>";
		idContent += "<tr><td align='center'>";

		if ((i==0) && (j==0) && (k==0)) {
				// es Informacion general, añadir x,y,z y refcat
			idContent += "<fieldset class='item normal'>";
			idContent += "<legend>";
			idContent += "Coordenadas";
			idContent += "</legend>";

			idContent += "<table width='100%' cellpadding='2' cellspacing='0'>";
			idContent += "<tr><td class='item normal' align='left'><b>Sistema de referencia espacial</b>: UTM ETRS89 Huso 30</td></tr>";
			idContent += "<tr><td class='item normal' align='left'><b>Coordenada X</b>: " + RoundDecimal(xCoordUsr, 2) + "</td></tr>";
			idContent += "<tr><td class='item normal' align='left'><b>Coordenada Y</b>: " + RoundDecimal(yCoordUsr, 2) + "</td></tr>";
			if (needCalculateAltitud) {
				idContent += "<tr><td class='item normal' align='left'><b>Relieve (según MDT 25 / MDT 5)</b>: " + laCota + " </td></tr>";
			}
			idContent += "</table>";

			idContent += "</fieldset>";

			if (needCalculateOVC) {
				idContent += "<tr><td class='item normal' align='left'><b>Referencia catastral de la parcela seg&uacute;n la Sede Electr&oacute;nica de Catastro</b>: " + resultOVC + "</td></tr>";
			}
			if ((typeof needCalculateBOA!='undefined')&&(needCalculateBOA)){
				idContent += "<tr><td class='item normal' align='left'>"+boas+"</td></tr>";
			}
			
			idContent += "<tr><td class='item normal' align='left' valign='middle'><b><a href='javascript:showRJT(" + RoundDecimal(xCoordUsr, 2)+","+RoundDecimal(yCoordUsr, 2) +")' title='' class='itemEnlace' ><img src='/lib/IDEAragon/themes/default/images/rjtRO.png' alt='Logo RJT' border='0' style='vertical-align: bottom;padding-right:10px;' />Consulta del r&eacute;gimen jur&iacute;dico asociado al punto <img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a></b></td></tr>";
		}

		idContent += getData(i,j,k);

		if (masInfo[i][j]) {
			idContent += "<tr><td class='item normal' align='center'><br>" + masInfo[i][j] + "</td></tr>";
		} else {
			idContent += "<tr><td class='item normal' align='center'><br>" + masInfoDefault + "</td></tr>";
		}

		idContent += "</td></tr>";
		idContent += "</table>";

		idContent += "</td></tr>";
		idContent += "</table>";
	}
	return idContent;
}

function getDetailedResult(i, j, k) {
	if($("#tabs").tabs("option", "active")> 0){
		showSearching(true);
	}
	askedI = i;
	askedJ = j;
	askedK = k;
	drawDetailedResult(askedI, askedJ, askedK);
}

function hayQueConsultarGFI(idx) {
	if (typeof queryableOnlyIfVisible != 'undefined') {
		var idCapa = gfiList[idx]["ID"];
		if (typeof queryableOnlyIfVisible[idCapa] != 'undefined') {
			if (queryableOnlyIfVisible[idCapa]) {
				console.log("La capa "+ idCapa  + "esta configurada como consultable solo si es visible");
				var lay = toc.findItemByAxlID(idCapa);
				console.log(lay);

				if (lay) {
					console.log("La capa "+ idCapa  + "esta configurada como consultable solo si es visible y está "+ lay.getVisible());
					return lay.getVisible();
				}
			}
		}
	}
		// no esta configurado (comportamiento previo) o está como mostrable siempre
	return true;
}

function capasEstanListas(i, j, k) {
	if ((i==0) && (j==0) && (k==1)) {
		identifyProductos(xCoordUsr, yCoordUsr, "muestraResultadosDescarga(idContent,'detailedResultsTable');", true);
		return true;
	} else {
		for (var l=0; l < restrictions[i][j][k]["count"]; l++) {
			var idx = restrictions[i][j][k][l]["posicion"];
			if (hayQueConsultarGFI(idx)) {
				if (gfiList[idx]["camposRespuesta"]["count"] == -1) {
					currentQueriedGFI = idx;
					console.log('Esta la consulto porque se consulta siempre')
					if (gfiList[idx]["esWFS"]) {
						var urlWFS = gfiList[idx]["url"];
	
	//					urlWFS += '&BBOX=' + idMinX + ',' + idMinY + ',' + idMaxX + ',' + idMaxY;
	
						if (debugOn) {
							alert("Falta el WFS" + currentQueriedGFI);
						}
						var propNames ="";
						if (typeof gfiList[idx]["propertyName"]!='undefined'){
							var names = gfiList[idx]["propertyName"].split(",");
							for (var i=0; i<names.length; i++){
								propNames+='<wfs:PropertyName>'+names[i]+'</wfs:PropertyName>';
							}
						}
						var data = '<wfs:GetFeature '+
	' xmlns:wfs="http://www.opengis.net/wfs"'+
	' service="WFS"'+
	' version="1.0.0"'+
	' outputFormat="json"'+
	' xsi:schemaLocation="http://www.opengis.net/wfs'+
	' http://schemas.opengis.net/wfs/1.1.0/wfs.xsd"'+
	' xmlns:gml="http://www.opengis.net/gml"'+
	' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+
	  '<wfs:Query typeName="'+gfiList[idx]["ID"]+'" srsName="EPSG:25830">'+
	 propNames+
	   ' <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">'+
	      '<ogc:Intersects>'+
	        '<ogc:PropertyName>shape</ogc:PropertyName>'+
	        '<gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="EPSG:25830">'+
	        '  <gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>'+idMinX+','+ idMinY+' '+idMinX+','+ idMaxY+' '+idMaxX+','+ idMaxY+' '+idMaxX+','+ idMinY+' '+idMinX+','+ idMinY+
	        '</gml:coordinates>'+
	      '      </gml:LinearRing>'+
	     '     </gml:outerBoundaryIs>'+
	    '    </gml:Polygon>'+
	   '   </ogc:Intersects>'+
	 /*   ' <Contains>'+
	   '<PropertyName>shape</PropertyName>'+
	   ' <gml:Point srsName="http://www.opengis.net/gml/srs/epsg.xml#25830">'+
	   '   <gml:coordinates>'+idMinX+','+ idMinY+'</gml:coordinates>'+
	   ' </gml:Point>'+
	   ' </Contains>'+*/
	  '  </ogc:Filter>'+
	 ' </wfs:Query>'+
	'</wfs:GetFeature>';
						sendWFSQuery(urlWFS, data);
					} else {
						var urlGFI = gfiList[idx]["url"];
	
						urlGFI += '&BBOX=' + minx + ',' + miny + ',' + maxx + ',' + maxy;
						urlGFI += '&WIDTH=' + mwidth +'&HEIGHT=' + mheight + '&X=' + xUsr + '&Y=' + yUsr;
	
						if (debugOn) {
							alert("Falta el GFI " + currentQueriedGFI);
						}
	
						sendGFIQuery(urlGFI);
					}
					return false;
				}
			} else {
					console.log('Esta me la salto porque solo se consulta si esta visible')
			}
		}
	}
	return true;
}


function getResultsWFSHTML(i, j, k, l) {
	var idContent = "";
	var idx = restrictions[i][j][k][l]["posicion"];

	if (gfiList[idx]["camposRespuesta"]["count"]>0) {
		id = gfiList[idx]["ID"];

		for (var kk = 0; kk < gfiList[idx]["camposRespuesta"]["count"]; kk++) {
			hayAlgunDato = true;

			fName1 = gfiList[idx]["camposRespuesta"][kk];
			fValue1 = gfiList[idx]["valoresRespuesta"][kk];
			var idFeature = "";

			var listaIndexMainField = new Array();
			var listaFieldNames = mainFieldName[id].split(' ');

			var indexIdField = -1;
			var indexIAESTField = -1;
			var indexAOD = -1;
			for (var ii=0;ii<fName1.length;ii++) {
				nombreCampo = getNombreCampo(fName1[ii]);

				if (isIncluded(listaFieldNames, nombreCampo, listaFieldNames.length)) {
					listaIndexMainField[nombreCampo] = ii;
				}
				if (nombreCampo == nombreCampoId) {
					indexIdField = ii;
				}
				if (nombreCampo == nombreCampoIAEST) {
					indexIAESTField = ii;
				}
				if (nombreCampo == nombreCampoAOD) {
					indexAOD = ii;
				}
			}

			if (fName1.length == 1) {
					// poner una sola linea
				mainFieldText = "";
				for (var jj = 0; jj < listaFieldNames.length; jj++) {
					if ((fValue1[listaIndexMainField[listaFieldNames[jj]]])) {
						if ((fValue1[listaIndexMainField[listaFieldNames[jj]]]).indexOf("http://") == 0) {
							mainFieldText += "<a class='normal' target='_blank' href='" + (fValue1[listaIndexMainField[listaFieldNames[jj]]]) + "'>Enlace<img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a> ";
						} else {
							//mainFieldText += corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
							mainFieldText += (fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
						}
					} else {
						//mainFieldText += corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
						mainFieldText += (fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
					}
				}

				idContent += "<tr><td colspan='3' class='item normal'>";
				if (titleFieldName[id] != null) {
					idContent += "<b>" + titleFieldName[id] + "</b> " + mainFieldText;
				} else {
					idContent += mainFieldText;
				}
				if (indexIdField != -1) {
					if (queryableLayer[idx]["TIPO"] == 1) {
						idContent += "&nbsp;&nbsp;(<a class='textoInvEnlace' href='activarCapaConcreta(\"" + idx + "\", \"" + fValue1[indexIdField] + "\");'>Activar capa cartogr&aacute;fica en el mapa.</a>)";
					}
				}

				idContent += "</td></tr>";

			} else {
					// generar fieldset
				mainFieldText = "";
				for (var jj = 0; jj < listaFieldNames.length; jj++) {
					if (fValue1[listaIndexMainField[listaFieldNames[jj]]]) {
						//mainFieldText += corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
						mainFieldText += (fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
					}
				}

				idContent += "<tr><td colspan='3' class='item normal'>";
				idContent += "<fieldset>";
				idContent += "<legend>";
				if (titleFieldName[id] != null) {
					idContent += titleFieldName[id] + " " + mainFieldText;
				} else {
					idContent += mainFieldText;
				}
				idContent += "</legend>";

				idContent += "<table id='detailData" + l + "_" + kk + "Table' width='100%' cellpadding='2' cellspacing='0' style='display:block;'>";

				jj = 0;
				for (var ii=0;ii<fName1.length;ii++) {
					idContent += "<tr>";

					nombreCampo = getNombreCampo(fName1[ii]);

					if (isValidField(nombreCampo)) {
						//var valor = corrigeCaracteresExtranos(fValue1[ii]);
						var valor = (fValue1[ii]);

						if (fValue1[ii].indexOf("http://") == 0) {
							valor = "<a class='textoInvEnlace' target='_blank' href='" + (fValue1[ii]) + "'>Enlace</a> ";
						}

						if (fieldAliasList2[id]) {
							if (fieldAliasList2[id][nombreCampo]) {
								if (ii == indexIAESTField) {
									//idContent += "<td width='100%' align='center' class='item normal'><center><b>Ficha con datos estadísticos del municipio (Fuente IAEST): </b><a class='normal' target='_blank' title='Enlace a ficha con datos estadísticos del municipio (Fuente IAEST)' href='" + corrigeCaracteresExtranos(fValue1[ii]) + "'>Enlace<img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a>" + "</center></td>";
									idContent += "<td width='100%' align='center' class='item normal'><center><b>Ficha con datos estad&iacute;sticos del municipio (Fuente IAEST): </b><a class='normal' target='_blank' title='Enlace a ficha con datos estad&iacute;sticos del municipio (Fuente IAEST)' href='" + (fValue1[ii]) + "'>Enlace<img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a>" + "</center></td>";
								} else if (ii == indexAOD) {
									//idContent += "<td width='100%' align='left' class='item normal'><b>Ver información en Aragopedia: </b><a class='normal' target='_blank' title='Ver información en Aragopedia' href='" + corrigeCaracteresExtranos(fValue1[ii]) + "'>Enlace<img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a>" + "</td>";
									idContent += "<td width='100%' align='left' class='item normal'><b>Ver informaci&oacute;n en Aragopedia: </b><a class='normal' target='_blank' title='Ver informaci&oacute;n en Aragopedia' href='" + (fValue1[ii]) + "'>Enlace<img border='0' src='/lib/IDEAragon/themes/default/images/i/ico_link.png'/></a>" + "</td>";
								} else {
									idContent += "<td class='item normal'><b>" + (fieldAliasList2[id][nombreCampo]) + "</b>: " + valor + "</td>";
								}
								// el WFS devuelve todos los campos, aunque no le pidas todos. Mejor no poner los que no se piden
							//} else {
							//	idContent += "<td class='item normal'><b>" + nombreCampo + "</b>: " + valor + "</td>";
							}
						} else {
							idContent += "<td class='item normal header'><b>" + nombreCampo + "</b>: " + valor + "</td>";
						}
						jj++;
					}
					idContent += "</tr>";
				}
				if (indexIdField != -1) {
					if (queryableLayer[idx]["TIPO"] == 1) {
						idContent += "<tr>";
						idContent += "<td class='item normal' align='left'><a class='textoInvEnlace' href='activarCapaConcreta(\"" + idx + "\", \"" + fValue1[indexIdField] + "\");'>Activar capa cartogr&aacute;fica en el mapa.</a>";
						idContent += "</td>";
						idContent += "</tr>";
					}
				}

				idContent += "</table>";
				idContent += "</fieldset>";
				idContent += "</td></tr>";
			}
		}
	}
	return idContent;
}
