var tamMaxCapaDescr=120;
var titleNovedad = "Contenidos recientemente incorporados";

var aliasTipoColeccion = new Array();
aliasTipoColeccion["Cartografia"] = "Cartograf&iacute;a";
aliasTipoColeccion["Satelite"] = "Sat&eacute;lite";
aliasTipoColeccion["Tematica"] = "Tem&aacute;tica";

var coleccEnOrden = new Array();
coleccEnOrdenIdx = 1;

var nomColeccEnOrden = new Array();
nomColeccEnOrdenIdx = 1;

var tipoColeccEnOrden = new Array();
tipoColeccEnOrdenIdx = 1;

var infoColecc = new Array();

var tamMaxCapa = 60;

/** extraido de ajaxMensajes.js **/
var linesColecc;


var fechaMinimo = 1700;
var fechaInicio = fechaMinimo;
var fechaMaximo = new Date().getFullYear();;
var fechaFin = fechaMaximo;
var fechaStep = 1;
var filtrarPorFecha = false;

var escalaMinima = 1;
var escalaMaxima = 10000000;
var escalaInicio = escalaMinima;
var escalaFin = escalaMaxima;
var filtrarPorEscala = false;
var filtrarPorTipo = false;
var filtrarPorColecc = false;

var fechaItemInicio = "";
var fechaItemFin = "";
var escalaItemInicio = "";
var escalaItemFin = "";

var tipoAFiltrar = "";
var coleccAFiltrar = "";

var filtrarSoloVisualizables = false;
var filtrarSoloDescargables = false;

var listaIdSelectionLayerEsquema = new Array;
listaIdSelectionLayerEsquema[0] = "ED";
listaIdSelectionLayerEsquema[1] = "EV";
var selectionTypeEsquema = "poly";
var selectionWhereEsquema = "";
var nomColeccEsquema = "";
//var colIdxEsquemaSelected = -1;
var nomColeccEsquemaSelected = "";
var esquemaLayer= null;

//Esto es para que funcione el hover en explorer 6
function loadColecciones(){


$.ajax({
	url: '/BD_GIS/consultaColecciones.jsp',
	type: 'GET',
	success:	function(data) {
							linesColecc = data.split("\n");
							cargaColecciones(linesColecc);

							if (debugOn) {
								alert('Recibidas las colecc: ' + data);
							}
						},
	error: 		function(data, textStatus, errorThrown) {
							linesColecc = coleccDefault.split("\n");
							cargaColecciones(linesColecc);

							if (debugOn) {
								alert('Error obteniendo las colecciones. Utilizando colecciones por defecto. State: ' + data.readyState + ", Status:" + data.status);
							}
						}
});

}

var coleccionesCargadas = false;

function cargaColecciones(coleccList) {
		// quito las que son lineas <html> etc. que no son datos 
	for (var jj = 0; jj < coleccList.length; jj++) {
		if ((coleccList[jj] != "")) {
			var datosColecc = coleccList[jj].split(";");
			if (datosColecc.length > 5) {
				//addColeccEnOrden(datosColecc[2], datosColecc[1], datosColecc[3], datosColecc[4], datosColecc[5]);
				addColeccEnOrden(datosColecc[3], datosColecc[2], datosColecc[4], datosColecc[5], datosColecc[6], datosColecc[7]);
			}
		}
	}
	coleccionesCargadas = true;
}

function addColeccEnOrden(nombreColecc, tipo, cuantos, esNovedad, descrip,leyendas) {
	var colecc = nombreColecc;
	if (esNovedad){
		addColeccEnOrden(nombreColecc, tipo, cuantos, false, descrip, leyendas) ;
		colecc="new_"+nombreColecc;
	}
	
	coleccEnOrden[colecc] = coleccEnOrdenIdx;
	coleccEnOrdenIdx++;

	addTipoIfNotExist(tipo);
	addColeccIfNotExist(colecc);

	infoColecc[colecc] = new Array();
	infoColecc[colecc]["TIPO"] = tipo;
	infoColecc[colecc]["CUANTOS"] = cuantos;
	infoColecc[colecc]["ES_NOVEDAD"] = esNovedad;
	infoColecc[colecc]["DESCRIP"] = descrip;
	infoColecc[colecc]["LEYENDA"] = leyendas;
	
	

}

function addTipoIfNotExist(tipoColecc) {
	var estaYa = false;
	for (var jj = 1; jj < tipoColeccEnOrdenIdx; jj++) {
		if (tipoColeccEnOrden[jj] == tipoColecc) {
			estaYa = true;	
		}
	}

	if (! estaYa) {
		tipoColeccEnOrden[tipoColeccEnOrdenIdx] = tipoColecc;
		tipoColeccEnOrdenIdx++;
	}
}


function addColeccIfNotExist(nomColecc) {
	var estaYa = false;
	for (var jj = 1; jj < nomColeccEnOrdenIdx; jj++) {
		if (nomColeccEnOrden[jj] == nomColecc) {
			estaYa = true;	
		}
	}

	if (! estaYa) {
		nomColeccEnOrden[nomColeccEnOrdenIdx] = nomColecc;
		nomColeccEnOrdenIdx++;
	}
}

resultadoGFI_sitar = new Array();
resultadoGFI_sitar["count"] = 0;

listaColecc = new Array();
listaColeccCount = 0;

function resetResultadosAnterioresGFI() {
	resultadoGFI_sitar = new Array();
	resultadoGFI_sitar["count"] = 0;

	listaColecc = new Array();
	listaColeccCount = 0;

	if (typeof gfiCount!='undefined'){
	for (var jj = 0; jj < gfiCount; jj++) {
		gfiList[jj]["camposRespuesta"] = new Array();
		gfiList[jj]["camposRespuesta"]["count"] = -1;
		gfiList[jj]["valoresRespuesta"] = new Array();
		gfiList[jj]["valoresRespuesta"]["count"] = -1;
	}
	}
}

function sinRespuestaGFI() {
	if (typeof gfiList != 'undefined'){
		// marco como que no tiene resultados 
	gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
	}
		//processResultQueryGFI_sitarDescarga(strResGFI);
	drawDetailedResult(askedI,askedJ,askedK);
}

function sinRespuestaWFS() {
	if (typeof gfiList != 'undefined'){
		// marco como que no tiene resultados 
	gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
	}
		//processResultQueryGFI_sitarDescarga(strResGFI);
	drawDetailedResult(askedI,askedJ,askedK);
}

var t_timeout;

function sendGFIQuery(urlGFI) {
	if (debugOn) {
		window.open(urlGFI);
	}
	http = getHTTPObject();
	if ((http != null)) {
		http.open("GET", urlGFI);
/*
		if (urlGFI.indexOf(urlGFI_SITAR) != -1) {
			if (urlGFI.indexOf("QUERY_LAYERS=EN") != -1){
				http.onreadystatechange = parseResultGFIQuery_sitar_novedades;
			}
			else{
				http.onreadystatechange = parseResultGFIQuery_sitar;
			}
			t_timeout = setTimeout("sinRespuestaGFI()", 15000);
		} else if ((typeof urlGFI_SITAR_sita3 !='undefined')&&(urlGFI.indexOf(urlGFI_SITAR_sita3) != -1)) {
			http.onreadystatechange = parseResultGFIQuery_sitar;
		} else */
		if (urlGFI.indexOf(urlGFI_OVC) != -1) {
			http.onreadystatechange = parseResultOVC;
			t_timeout = setTimeout("sinRespuestaGFI_OVC()", 8000);
		} else {
			http.onreadystatechange = parseResultGFIQuery;
			t_timeout = setTimeout("sinRespuestaGFI()", 15000);
		}
		isWorking = true;
		//muestraLayerLoading();

		http.send(null);
	}
}

function parseResultGFIQuery() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			processResultQueryGFI(result);
			drawDetailedResult(askedI,askedJ,askedK);
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI();
		}
	}
}

function sendWFSQuery(urlWFS,data) {
	$.ajax({
		url: urlWFS,
		type: 'POST',
		contentType: "text/xml",
		  dataType:"text",
		data: data,
		 timeout:15000,
		success:	function(data) {
								

								if (debugOn) {
									alert('Respuesta WFS: ' + data);
								}
								parseResultWFSQuery(data);
							},
		error: 		function(data, textStatus, errorThrown) {
			sinRespuestaWFS();
								if (debugOn) {
									alert('Error obteniendo features. State: ' + data.readyState + ", Status:" + data.status);
								}
							}
	});

	
}

function parseResultWFSQuery(result) {
	
			processResultQueryWFS(result);
			drawDetailedResult(askedI,askedJ,askedK);
		
}

/**
function parseResultGFIQuery_sitar2() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			processResultQueryGFI_sitarDescarga(result, true);

			var idContent = getResultsGFIHTML_sitar();
		
			document.getElementById("infoTab").innerHTML=idContent;
			$("#tabs").tabs( "enable", 2);
			$("#tabs").tabs("option", "active", 2);

			updateAcetateMarca(findXCoordUsr, findYCoordUsr, xCoordUsr, yCoordUsr);

			ocultaLayerLoading();
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI();
		}
	}
}
function parseResultGFIQuery_sitar() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			processResultQueryGFI_sitarDescarga(result, false,false);
			drawDetailedResult(askedI,askedJ,askedK);
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI();
		}
	}
}

function parseResultGFIQuery_sitar_novedades() {
	if (http.readyState == 4) {
		if (http.status == 200) {
			clearTimeout(t_timeout);

			var result = http.responseText;
			processResultQueryGFI_sitarDescarga(result, false,true);
			drawDetailedResult(askedI,askedJ,askedK);
		} else {
			clearTimeout(t_timeout);
			sinRespuestaGFI();
		}
	}
}
*/
function processResultQueryGFI(theReply) {
	//TODO : Use XML DOM parser instead to parse the response

	var selectedData = "";
	var endpos = 1;
	reply = theReply;

	gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
	gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = 0;

	resultNum = 0;
	var pos = reply.indexOf("<FIELDS ",endpos);
	while (pos != -1) {
		startpos = pos + 8;
		endpos = reply.indexOf('"/>',startpos);
		selectedData = reply.substring(startpos, endpos);

		if(selectedData != "") {
			var fValue1 = getFieldValues(selectedData);
			var fName1 = getFieldNames(selectedData);

			gfiList[currentQueriedGFI]["camposRespuesta"][resultNum] = fName1;
			gfiList[currentQueriedGFI]["valoresRespuesta"][resultNum] = fValue1;

			gfiList[currentQueriedGFI]["camposRespuesta"]["count"]++;
			gfiList[currentQueriedGFI]["valoresRespuesta"]["count"]++;

			resultNum++;
		}

		reply = reply.substring(endpos);
		pos = reply.indexOf("<FIELDS ",1);
	}
}

function processResultQueryWFS(theReply) {
	try{
	var response = $.parseJSON(theReply);

	if (typeof response.totalFeatures  !='undefined') {
		var totalFeatures = response.totalFeatures;

		// TODO: AVISAR QUE hay resultados pendientes!  paginar????
		//gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = totalFeatures;
		//gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = totalFeatures;
		gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = response.features.length;
		gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = response.features.length;


		//for (var i = 0; i < totalFeatures; i++) {
		for (var i = 0; i < response.features.length; i++) {
			var fNames = new Array();
			var fValues = new Array();

			for (var j in response.features[i].properties) {
				fNames.push(j);
				fValues.push("" + eval("response.features[i].properties." + j));
			}

			gfiList[currentQueriedGFI]["camposRespuesta"][i] = fNames;
			gfiList[currentQueriedGFI]["valoresRespuesta"][i] = fValues;
		}
	} else {
		gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
		gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = 0;
	}
	}
	catch(err){ // si falla la consulta a una capa seguir con el resto
		console.log(err);
		gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = 0;
		gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = 0;
	}
}

function calculaDistanciaCentroide(laX, laY, centroideX,centroideY) {
	var xD = Math.abs(laX - centroideX);
	var yD = Math.abs(laY - centroideY);
	var theDistance = Math.sqrt(Math.pow(xD,2) + Math.pow(yD,2));

	return theDistance;
}

/*
function processResultQueryGFI_sitarDescarga(theReply, avoidUpdateGFIList, esNovedad) {
	//TODO : Use XML DOM parser instead to parse the response

	var selectedData = "";
	var endpos = 1;
	var reply = theReply;
	resultNumber = 0;

	var pos = reply.indexOf("<FIELDS ",endpos);
	while (pos!=-1) {
		resultadoGFI_sitar["count"] = resultadoGFI_sitar["count"] + 1;
		startpos = pos + 8;
		endpos = reply.indexOf('"/>',startpos);
		selectedData = reply.substring(startpos,endpos);

		if(selectedData != "") {
			var fValue1 = getFieldValuesGFI(selectedData);
			var fName1 = getFieldNamesGFI(selectedData);

			var colecc =  (esNovedad ? "new_":"")+getResultGFI(fName1, fValue1, ".COLECCION");
			var estaGeorref = getResultGFI(fName1, fValue1, ".GEORREF");
			var _xMin = replaceChar(getResultGFI(fName1, fValue1, ".X1"), ",", ".");
			var _yMin = replaceChar(getResultGFI(fName1, fValue1, ".Y1"), ",", ".");
			var _xMax = replaceChar(getResultGFI(fName1, fValue1, ".X2"), ",", ".");
			var _yMax = replaceChar(getResultGFI(fName1, fValue1, ".Y2"), ",", ".");

			var puntoMedioFotogramaX = (_xMin*1) + ((_xMax - _xMin) / 2);
			var puntoMedioFotogramaY = (_yMin*1) + ((_yMax - _yMin) / 2);

			if (colecc != "") {
				if (! resultadoGFI_sitar[colecc]) {
					resultadoGFI_sitar[colecc] = new Array();
					resultadoGFI_sitar[colecc]["count"] = 0;

					listaColecc[listaColeccCount] = colecc;
					listaColeccCount++;
				}

				idxResult = resultadoGFI_sitar[colecc]["count"];

				resultadoGFI_sitar[colecc][idxResult] = new Array();
				resultadoGFI_sitar[colecc][idxResult]["camposRespuesta"] = fName1;
				resultadoGFI_sitar[colecc][idxResult]["valoresRespuesta"] = fValue1;

				var distanc = calculaDistanciaCentroide(puntoMedioFotogramaX, puntoMedioFotogramaY, xCoordUsr, yCoordUsr);
				resultadoGFI_sitar[colecc][idxResult]["distanciaCentroide"] = distanc;

				resultadoGFI_sitar[colecc][idxResult]["deltaX"] = 0;
				resultadoGFI_sitar[colecc][idxResult]["deltaY"] = 0;

				resultadoGFI_sitar[colecc]["count"]++;
			}
			resultNumber++;
		}

		reply = reply.substring(endpos);
		pos = reply.indexOf("<FIELDS ",1);

	}

	gfiList[currentQueriedGFI]["camposRespuesta"]["count"] = resultadoGFI_sitar["count"];
	gfiList[currentQueriedGFI]["valoresRespuesta"]["count"] = resultadoGFI_sitar["count"];

	//updateResultsGFI();
	//updateResultsGFI_SITAR();

}
*/

function getResultsGFIHTML(i, j, k, l) {
 if ( (i==1) && (j==2) && (k==0)) {
		return getResultsGFIHTML_educacion(i, j, k, l);
	} else if ( (i==0) && (j==0) && (k==0)) {
		return ""; 
	} else {
		return getResultsGFIHTML_generico(i, j, k, l);
	}
	return ""; 
}

function ordenaColeccion(theCollection, startIdx) {
	var firstIdx = startIdx;
	var idxMinimum = getMinimunDistance(resultadoGFI_sitar[theCollection], firstIdx);
	var auxCR = resultadoGFI_sitar[theCollection][firstIdx]["camposRespuesta"];
	var auxVR = resultadoGFI_sitar[theCollection][firstIdx]["valoresRespuesta"];
	var auxDC = resultadoGFI_sitar[theCollection][firstIdx]["distanciaCentroide"];
	resultadoGFI_sitar[theCollection][firstIdx]["camposRespuesta"] = resultadoGFI_sitar[theCollection][idxMinimum]["camposRespuesta"];
	resultadoGFI_sitar[theCollection][firstIdx]["valoresRespuesta"] = resultadoGFI_sitar[theCollection][idxMinimum]["valoresRespuesta"];
	resultadoGFI_sitar[theCollection][firstIdx]["distanciaCentroide"] = resultadoGFI_sitar[theCollection][idxMinimum]["distanciaCentroide"];
	resultadoGFI_sitar[theCollection][idxMinimum]["camposRespuesta"] = auxCR;
	resultadoGFI_sitar[theCollection][idxMinimum]["valoresRespuesta"] = auxVR;
	resultadoGFI_sitar[theCollection][idxMinimum]["distanciaCentroide"] = auxDC;
	firstIdx++;
	if (firstIdx < resultadoGFI_sitar[theCollection]["count"]) {
		ordenaColeccion(theCollection, firstIdx);
	}
}

function getMinimunDistance(colecc, firstIdx) {
	var minimumDistanceValue = 999999;
	var minimumDistanceIndex = firstIdx;
	for (var resIdx = firstIdx; resIdx < colecc["count"]; resIdx++) {
		if (! isNaN(colecc[resIdx]["distanciaCentroide"])) {
			if (colecc[resIdx]["distanciaCentroide"] < minimumDistanceValue) {
				minimumDistanceIndex = resIdx;
				minimumDistanceValue = colecc[resIdx]["distanciaCentroide"];
			}
		}
	}
	return minimumDistanceIndex;
}

function ordenaCandidatos() {
	for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
		theCollection = listaColecc[colIdx];
		ordenaColeccion(theCollection, 0);
	}
}

function getNextMinimum(oldMinimum) {
	var candidatoIdx = -1;
	var candidatoValue = 9999999;
	for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
		if (listaAuxColecc[colIdx] <= candidatoValue) {
			if (listaAuxColecc[colIdx] > oldMinimum) {
				candidatoIdx = colIdx;
				candidatoValue = listaAuxColecc[colIdx];
			}
		}
	}
	return candidatoIdx;
}

var listaAuxColecc = new Array();
function ordenaColecciones() {
	var max = 9999;
	var posic = max;
	var min = -1;

	for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
		theCollection = listaColecc[colIdx];
		posic = posic++;
			// si no tiene posicion, le genero un numero grande mayor a todos
		if (coleccEnOrden[theCollection]) {
			posic = coleccEnOrden[theCollection];
			if ((typeof infoColecc[theCollection]["ES_NOVEDAD"] !='undefined')&&(infoColecc[theCollection]["ES_NOVEDAD"] != 0) ){
				infoColecc[theCollection]["TIPO"] = titleNovedad;

				posic = min;
				min--;
			}
		} else {
			posic = max++;
		}
		listaAuxColecc[colIdx] = posic;
	}

	var currentMinimum = min;
	var listaColeccAux = new Array();
	for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
		var currentMinimumIdx = getNextMinimum(currentMinimum);
		currentMinimum = listaAuxColecc[currentMinimumIdx];
		listaColeccAux[colIdx] = listaColecc[currentMinimumIdx];
	}
	listaColecc = listaColeccAux;
}

function hayCandidatosVisibles() {
	if (listaColeccCount == 0) {
		return false;
	} else {
		for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
			theCollection = listaColecc[colIdx];
			var countVis = getItemsVisibles(theCollection);
			if (countVis > 0) {
				return true;
			}	
		}
	}
	return false;
}
function getItemsVisibles(theCollection) {
	var count = 0;
	for (var resIdx = 0; resIdx < resultadoGFI_sitar[theCollection]["count"]; resIdx++) {
		if (isVisibleItem(theCollection, resIdx)) {
			count++;
		}
	}
	return count;
}

function isVisibleItem(theCollection, resIdx) { 
	if ((typeof hayFiltros !='undefined')&&(!hayFiltros)){
	return true;
	}


		var fNam = resultadoGFI_sitar[theCollection][resIdx]["camposRespuesta"];
		var fVal = resultadoGFI_sitar[theCollection][resIdx]["valoresRespuesta"];

		var theName = getResultGFI(fNam, fVal, ".NAME");
		
		var theDescription = getResultGFI(fNam, fVal, ".DESCRIPCION");
		if (theDescription == "") {
			theDescription = getResultGFI(fNam, fVal, ".DESCRIP");
			if (theDescription == "") {
				theDescription = theName;
			}
		}
		
		var estaGeorref = replaceChar(getResultGFI(fNam, fVal, ".GEORREF"), ",", ".");

		var capa = getResultGFI(fNam, fVal, ".CAPA");
		var servic = getResultGFI(fNam, fVal, ".SERVICIO");
		var locat = getResultGFI(fNam, fVal, ".LOCATION");
		var ruta = getResultGFI(fNam, fVal, ".PATH");
		var fecha = getResultGFI(fNam, fVal, ".FECHA");
		var escala = getResultGFI(fNam, fVal, ".ESCALA");
		var tamPx = getResultGFI(fNam, fVal, ".TAM_PIXEL");
		var formatos = getResultGFI(fNam, fVal, ".DESCARGAS");
		var formatosLista = formatos.split(",");
		var tamannos = getResultGFI(fNam, fVal, ".TAMANO");
		var tamannosLista = tamannos.split(",");

		var colecc = getResultGFI(fNam, fVal, ".COLECCION");

		if (filtrarSoloVisualizables) {
			if (locat == "") {
				return false;
			} else if (locat == "&nbsp;") {
				return false;
			}
		}

		if (filtrarSoloDescargables) {
			if (ruta == "&nbsp;") {
				return false;
			}

			if (formatosLista.length <= 0) {
				return false;
			}
		}

		if (filtrarPorFecha) {
			var fechaValida = false;
			if (fecha == "s XVIII") {
				fechaItemInicio = "1700";
				fechaItemFin = "1799";
				fechaValida = true;
			} else if (fecha == "s.XX") {
				fechaItemInicio = "1900";
				fechaItemFin = "1999";
				fechaValida = true;
			} else if (fecha == "1956-57") {
				fechaItemInicio = "1956";
				fechaItemFin = "1957";
				fechaValida = true;
			} else if (parseInt(fecha.substring(0, 4))) {
				fechaItemInicio = fecha.substring(0, 4);
				fechaItemFin = fecha.substring(0, 4);
				fechaValida = true;
			}
			if (fechaValida) {
				if ((fechaItemInicio > fechaFin) || (fechaItemFin < fechaInicio)) {
					return false;
				}
			}
		}
		if (filtrarPorEscala) {
			var escalaValida = false;
			if (parseInt(escala)) {
				escalaItemInicio = parseInt(escala);
				escalaItemFin = parseInt(escala);
				escalaValida = true;
			}
			if (escalaValida) {
				if (((escalaFin!="")&&(escalaItemInicio > escalaFin)) ||
						((escalaInicio!="")&&(escalaItemFin < escalaInicio))) {
					return false;
				}
			}
		}
		if (filtrarPorColecc) {
			if (colecc != coleccAFiltrar) {
					return false;
			}
		}
		if (filtrarPorTipo) {
			if (infoColecc[colecc]) {
				if (infoColecc[colecc]["TIPO"]) {
					var tipoCollection = infoColecc[theCollection]["TIPO"];
					if (tipoCollection != tipoAFiltrar) {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;

	
}
function getGFITableHeader(){
	var idContent = '<td class="paddingIzquierda" align="left" width="24%"><b>Nombre</b></td>';
	idContent += '<td align="center" width="12%"><b>Ver en visor</b></td>';
	idContent += '<td align="center" width="12%"><b>Ver en cartoteca</b></td>';
	idContent += '<td align="center" width="12%"><b>Descargar</b></td>';
	idContent += '<td align="center" width="14%"><b>Fecha</b></td>';
	idContent += '<td align="center" width="12%"><b>Escala</b></td>';
	idContent += '<td align="center" width="14%"><b>Tama&ntilde;o de pixel</b></td>';
	return idContent;
}

function getResultsGFIHTML_sitar() {
	ordenaCandidatos();
	ordenaColecciones();

	var idContent = '';

	idContent += "<table class='search_map' width='100%' cellpadding='0' cellspacing='2'>";
	if (! hayCandidatosVisibles()) {
		if (listaColeccCount == 0) {
			idContent += '<tr><td class="normal item" align="left">No hay datos</td></tr>';
		}
		else {
			idContent += '<tr><td class="item header" align="center"><br/>No hay datos visibles con los criterios de filtrado seleccionados.</td></tr>';
		}
	} else {
		idContent += '<tr class="item header">';
		idContent += '<td align="center" colspan="'+gfiColumnsCount+'">';
		idContent += '<input id="mantenerExtension" type="checkbox" checked alt="Mantener extensi&oacute;n actual del mapa">Mantener extensi&oacute;n actual del mapa</input>'; 
		idContent += '</td>';
		idContent += '</tr>';
		idContent += '<tr class="item header">';
		idContent += '<td align="center" colspan="6">';
		idContent += '</td>';
		idContent += '</tr>';
		var tipoAnterior = "IMPOSIBLE";
		var tipoIdx = 0;
		var generadoNuevoTipo = false;	
		var esLaPrimera = true;

		var abierto = (listaColeccCount<=1);
		for (var colIdx = 0; colIdx < listaColeccCount; colIdx++) {
			if (getItemsVisibles(listaColecc[colIdx]) > 0) {
				generadoNuevoTipo = false;

			theCollection = listaColecc[colIdx];

				if (infoColecc[theCollection]) {
					if (infoColecc[theCollection]["TIPO"] != tipoAnterior) {
						if (! esLaPrimera) {
							idContent += getFinCollectionHTML();
							idContent += '</td>';
							idContent += '</tr>';
						}
						esLaPrimera = false;

						var visibColecc = (infoColecc[theCollection]["TIPO"] == titleNovedad ? true : false);

			idContent += '<tr class="itemBig header blanco">';
			idContent += '<td align="left" colspan="'+gfiColumnsCount+'">';
						idContent += getCabeceraTipoCollectionHTML(theCollection, tipoIdx, visibColecc||abierto);
						idContent += '</td>';
						idContent += '</tr>';

						idContent += '<tr class="itemBig blanco">';
						idContent += '<td align="left" colspan="'+gfiColumnsCount+'">';
						idContent += getInicioCollectionHTML(theCollection, tipoIdx, visibColecc||abierto);

						tipoAnterior = infoColecc[theCollection]["TIPO"];
						tipoIdx++;
					}
				} else {
					if (! esLaPrimera) {
						idContent += getFinCollectionHTML();
						idContent += '</td>';
						idContent += '</tr>';
					}
					esLaPrimera = false;

					idContent += '<tr class="itemBig header blanco">';
					idContent += '<td align="left" colspan="'+gfiColumnsCount+'">';
					idContent += getInicioCollectionHTML(theCollection, tipoIdx, true);
					tipoIdx++;
					generadoNuevoTipo = true;
				}
			idContent += getCabeceraCollectionHTML(theCollection, colIdx, generadoNuevoTipo);
			//idContent += '</td>';//???
			//idContent += '</tr>';//???

			idContent += '<tr>';
			idContent += '<td>';

			idContent += "<table id='colecc" + colIdx + "Table' style='display:none;visibility:hidden;table-layout:fixed;' class='search_map' width='100%' cellpadding='0' cellspacing='2'>";
			idContent += '<tr class="itemBig negro fondoGrisClaro">';
			idContent += getGFITableHeader();
			/*idContent += '<td class="paddingIzquierda" align="left" width="24%"><b>Nombre</b></td>';
			idContent += '<td align="center" width="12%"><b>Ver en visor</b></td>';
			idContent += '<td align="center" width="12%"><b>Ver en cartoteca</b></td>';
			idContent += '<td align="center" width="12%"><b>Descargar</b></td>';
			idContent += '<td align="center" width="14%"><b>Fecha</b></td>';
			idContent += '<td align="center" width="12%"><b>Escala</b></td>';
			idContent += '<td align="center" width="14%"><b>Tama&ntilde;o de pixel</b></td>';
		*/	//idContent += '<td align="center" width="20%"><b>Distancia a centroide</b></td>';
			idContent += '</tr>';

			var evenOdd;
				var cuantos = 0;
			for (var resIdx = 0; resIdx < resultadoGFI_sitar[theCollection]["count"]; resIdx++) {
					if ((cuantos % 2) == 0) {
					evenOdd = "oddRow";
				}	else {
					evenOdd = "evenRow";
				}
					if (isVisibleItem(theCollection, resIdx)) {
				idContent += getItemGFI_descarga(theCollection, resIdx, false, evenOdd);
						cuantos++;
					}
			}
			idContent += "</table>";
			idContent += '</td>';
			idContent += '</tr>';
		}
	}

		if (! esLaPrimera) {
			idContent += getFinCollectionHTML();
			idContent += '</td>';
			idContent += '</tr>';
		}
	}
	idContent += "</table>";

	return idContent;
}

function getAliasTipoColeccion(tipoActual) {
		if (aliasTipoColeccion[tipoActual] != null) {
			return aliasTipoColeccion[tipoActual];
		}
	return tipoActual;
}


function getCabeceraTipoCollectionHTML(theCollection, colIdx, isVisib) {
	var idContent = "";
	var tipoCollectionAux = infoColecc[theCollection]["TIPO"];
	var tipoCollection = getAliasTipoColeccion(tipoCollectionAux);

	idContent += "<table width='100%' cellpadding='0' cellspacing='0'>";
	idContent += '<tr class="itemBig header blanco">';
	idContent += '<td align="left">';

	if (isVisib) {
		idContent += "<a class='treeIcon' href='javascript:toggleZoneCarpeta(\"coleccTipo" + colIdx + "\", \"informaci&oacute;n de " + tipoCollection + "\")' title='Ocultar informaci&oacute;n de " + tipoCollection + "' id='coleccTipo" + colIdx + "Link'>";
		idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened_blanco.png' border='0'></img></a>";
	} else {
		idContent += "<a class='treeIcon' href='javascript:toggleZoneCarpeta(\"coleccTipo" + colIdx + "\", \"informaci&oacute;n de " + tipoCollection + "\")' title='Mostrar informaci&oacute;n de " + tipoCollection + "' id='coleccTipo" + colIdx + "Link'>";
		idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed_blanco.png' border='0'></img></a>";
	}

	idContent +='<span  class="alignMiddle" >'+ tipoCollection+'</span>';

	idContent += '</td>';
	idContent += '</tr>';
	idContent += '</table>';

	return idContent;
}

function getInicioCollectionHTML(theCollection, colIdx, visib) {
	var idContent = "";
	if (visib) {
		idContent += "<table id='coleccTipo" + colIdx + "Table' style='background-color:#FFFFFF;display:table;visibility:visible;table-layout:fixed;width:100%;' cellpadding='0' cellspacing='0'>";
	} else {
		idContent += "<table id='coleccTipo" + colIdx + "Table' style='background-color:#FFFFFF;display:none;visibility:hidden;table-layout:fixed;width:100%;' cellpadding='0' cellspacing='0'>";
	}

	return idContent;
}

function getFinCollectionHTML() {
	var idContent = "";
	idContent += '</table>';

	return idContent;
}


function getTamMaxCapaDescr(){
	return tamMaxCapaDescr;
}


function getCabeceraCollectionHTML(theCollection, colIdx, generadoNuevoTipo,visible) {
	if (typeof visible =='undefined'){
		visible = false;
	}
	var idContent = "";
	if (generadoNuevoTipo) { 
		idContent += '<tr class="itemBig headerBig blanco">';
		idContent += '<td align="left" style="width:100%;">';
		idContent += "<table class='itemBig header blanco' cellpadding='0' cellspacing='0' width='100%'>";
	} else {
		idContent += '<tr class="itemNormal headerNormal blanco">';
		idContent += '<td align="left"  style="width:100%;">';
		idContent += "<table class='itemNormal headerNormal blanco' cellpadding='0' cellspacing='0' width='100%'>";
	}

	idContent += '<tr>';
	idContent += '<td nowrap>';
	if (! generadoNuevoTipo) {
		// dejar un hueco
		idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png' border='0' style='visibility:hidden;'></img>";
	}

	var txtDescrip = theCollection;
	if (infoColecc[theCollection]) {
		if (infoColecc[theCollection]["DESCRIP"]) {
			txtDescrip = infoColecc[theCollection]["DESCRIP"];
		}
	}
	if (visible){
		idContent += "<a class=\"treeIcon\" href='javascript:toggleZoneCarpeta(\"colecc" + colIdx + "\", \"informaci&oacute;n\")' title='Ocultar informaci&oacute;n' id='colecc" + colIdx + "Link'>";
		idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png' border='0'></img></a>";
		}
		else{
	idContent += "<a class='treeIcon' href='javascript:toggleZoneCarpeta(\"colecc" + colIdx + "\", \"informaci&oacute;n\")' title='Mostrar informaci&oacute;n' id='colecc" + colIdx + "Link'>";
	idContent += "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png' border='0'></img></a>";
		}

	tamMaxCapaDescr = getTamMaxCapaDescr();
	if ((tamMaxCapaDescr>=0)&&(txtDescrip.length > tamMaxCapaDescr)) {
		txtDescrip =txtDescrip.substring(0, tamMaxCapaDescr) + "...";
	
	}


	idContent += "<a class='blanco alignMiddle' href='javascript:toggleZoneCarpeta(\"colecc" + colIdx + "\", \"informaci&oacute;n\")' title='"+(visible ? "Ocultar":"Mostrar")+" informaci&oacute;n'>";
	idContent += txtDescrip + ' (' + getItemsVisibles(theCollection) + ')';
	idContent += "</a>";
	
	var distancMejorCandidato = resultadoGFI_sitar[theCollection][0]["distanciaCentroide"];
		// como estan ordenados, sera el primero de la lista
	if (! isNaN(distancMejorCandidato)) {
		idContent += "&nbsp;" + getItemGFI_descarga(theCollection, 0, true);
	}
	
	if( (typeof verEsquema !='undefined')&& verEsquema){
	idContent += "&nbsp;&nbsp;<a class='itemBlancoEnlace blanco' href='javascript:activaEsquema(\"" + colIdx + "\", \"" + theCollection + "\")' title='Activar esquema' id='aActEsquema" + colIdx + "'>";
	idContent += "<img src='/lib/IDEAragon/themes/default/images/actEsquema.png' border='0' id='imgActEsquema" + colIdx + "'></img>";
	idContent += "</a>";
	}
	idContent += '</td>';

	idContent += '</tr>';
	idContent += '</table>';

	idContent += '</td>';

				idContent += '</tr>';
	return idContent;
}

function addFormatoDescarga(formatosLista,ruta, theName,tamannosLista){
	var idContent="";
	for (var i = 0; i < formatosLista.length; i++) {
		var laRuta = ruta + theName + formatosLista[i];
		var rutaImg = "/lib/IDEAragon/themes/default/images/i/ico_default.png";
		var formatoTxt = "";
		var formato = formatosLista[i];
		if (formato.indexOf(".") != 0) {
			formato = "." + formato;
		}

		var tam = "";
		if (tamannosLista[i]) {
			if (tamannosLista[i] != "&nbsp;") {
				tam = " (" + tamannosLista[i] + ")";
			}
		}
		var res=getFormato(formato,rutaImg,formatoTxt)
		rutaImg=res.rutaImg
		formatoTxt=res.formatoTxt
		idContent += '<a class="normal itemEnlace" align="right" title="Descargar en formato ' + formatoTxt + tam + '" href="#" >';
		idContent += '<img border="0" src="' + rutaImg + '" alt="Descargar ' + theName + " (" + theCollection + ')" onclick="window.open(\'' + laRuta + '\')"></a>&nbsp;&nbsp;';
	}
	return idContent;
}
function getFormato(formato,rutaImg,formatoTxt)
{	
	switch (formato) {
		case ".dgn.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DGN.png"; formatoTxt = "DGN"; break; }
		case ".dgn": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DGN.png"; formatoTxt = "DGN"; break; }
		case ".dxf.zip": {	rutaImg = "/lib/IDEAragon/themes/default/images/i/DXF.jpg"; formatoTxt = "DXF"; break; }
		case ".dxf": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DXF.jpg"; formatoTxt = "DXF"; break; }
		case ".dwg.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DWG.png"; formatoTxt = "DWG"; break; }
		case ".dwg": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DWG.png"; formatoTxt = "DWG"; break; }
		case ".shp.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/SHP.png"; formatoTxt = "Shapefile"; break; }
		case ".shp": { rutaImg = "/lib/IDEAragon/themes/default/images/i/SHP.png"; formatoTxt = "Shapefile"; break; }
		case ".gml.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/GML.png"; formatoTxt = "GML"; break; }
		case ".gml": { rutaImg = "/lib/IDEAragon/themes/default/images/i/GML.png"; formatoTxt = "GML"; break; }
		case ".kml.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/KML.png"; formatoTxt = "KML"; break; }
		case ".kml": { rutaImg = "/lib/IDEAragon/themes/default/images/i/KML.png"; formatoTxt = "KML"; break; }
		case ".kmz": { rutaImg = "/lib/IDEAragon/themes/default/images/i/KMZ.png"; formatoTxt = "KMZ"; break; }
		case ".kmz.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/KMZ.png"; formatoTxt = "KMZ"; break; }
		case ".jpg": { rutaImg = "/lib/IDEAragon/themes/default/images/i/JPG.png"; formatoTxt = "JPG"; break; }
		case ".jpg.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/JPG.png"; formatoTxt = "JPG"; break; }
		case ".png": { rutaImg = "/lib/IDEAragon/themes/default/images/i/PNG.png"; formatoTxt = "PNG"; break; }
		case ".png.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/PNG.png"; formatoTxt = "DGN"; break; }
		case ".gif": { rutaImg = "/lib/IDEAragon/themes/default/images/i/GIF.png"; formatoTxt = "GIF"; break; }
		case ".gif.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/GIF.png"; formatoTxt = "GIF"; break; }
		case ".tif": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TIF.png"; formatoTxt = "TIF"; break; }
		case ".tif.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TIF.png"; formatoTxt = "TIF"; break; }
		case ".ecw": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ECW.png"; formatoTxt = "ECW"; break; }
		case ".ecw.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ECW.png"; formatoTxt = "ECW"; break; }
		case ".mdt": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MDT.png"; formatoTxt = "MDT"; break; }
		case ".mdt.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MDT.png"; formatoTxt = "MDT"; break; }
		case ".asc": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ASC.png"; formatoTxt = "ASC"; break; }
		case ".asc.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ASC.png"; formatoTxt = "ASC"; break; }
		case ".avi": { rutaImg = "/lib/IDEAragon/themes/default/images/i/AVI.png"; formatoTxt = "AVI"; break; }
		case ".avi.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/AVI.png"; formatoTxt = "AVI"; break; }
		case ".mpg": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MPG.png"; formatoTxt = "MPG"; break; }
		case ".mpg.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MPG.png"; formatoTxt = "MPG"; break; }
		case ".dbf": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DBF.png"; formatoTxt = "DBF"; break; }
		case ".dbf.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DBF.png"; formatoTxt = "DBF"; break; }
		case ".xls": { rutaImg = "/lib/IDEAragon/themes/default/images/i/XLS.png"; formatoTxt = "XLS"; break; }
		case ".xls.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/XLS.png"; formatoTxt = "XLS"; break; }
		case ".mdb": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MDB.png"; formatoTxt = "MDB"; break; }
		case ".mdb.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/MDB.png"; formatoTxt = "MDB"; break; }
		case ".csv": { rutaImg = "/lib/IDEAragon/themes/default/images/i/CSV.png"; formatoTxt = "CSV"; break; }
		case ".csv.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/CSV.png"; formatoTxt = "CSV"; break; }
		case ".tab": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TAB.png"; formatoTxt = "TAB"; break; }
		case ".tab.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TAB.png"; formatoTxt = "TAB"; break; }
		case ".pdf": { rutaImg = "/lib/IDEAragon/themes/default/images/i/PDF.png"; formatoTxt = "PDF"; break; }
		case ".pdf.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/PDF.png"; formatoTxt = "PDF"; break; }
		case ".doc": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DOC.png"; formatoTxt = "DOC"; break; }
		case ".doc.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/DOC.png"; formatoTxt = "DOC"; break; }
		case ".odf": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ODF.png"; formatoTxt = "ODF"; break; }
		case ".odf.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ODF.png"; formatoTxt = "ODF"; break; }
		case ".las": { rutaImg = "/lib/IDEAragon/themes/default/images/i/las.png"; formatoTxt = "LAS"; break; }
		case ".las.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/las.png"; formatoTxt = "LAS"; break; }
		case ".laz": { rutaImg = "/lib/IDEAragon/themes/default/images/i/laz.png"; formatoTxt = "LAZ"; break; }
		case ".laz.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/laz.png"; formatoTxt = "LAZ"; break; }
		case ".xml": { rutaImg = "/lib/IDEAragon/themes/default/images/i/XML.png"; formatoTxt = "XML"; break; }
		case ".xml.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/XML.png"; formatoTxt = "XML"; break; }
		case ".txt": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TXT.png"; formatoTxt = "Texto"; break; }
		case ".txt.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/TXT.png"; formatoTxt = "Texto"; break; }
		case ".json": { rutaImg = "/lib/IDEAragon/themes/default/images/i/JSON.png"; formatoTxt = "JSON"; break; }
		case ".json.zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/JSON.png"; formatoTxt = "JSON"; break; }
		case ".zip": { rutaImg = "/lib/IDEAragon/themes/default/images/i/ZIP.png"; formatoTxt = "ZIP"; break; }
	}
	return {"rutaImg":rutaImg,"formatoTxt":formatoTxt}
}

function getItemGFI_descargaHTML(theCollection, resIdx, esCompacto,evenOdd, capa,locat, servic,_xMin, _yMin, _xMax, _yMax, _deltaX, _deltaY, escala, fecha, theDescription, estaGeorref, formatosLista, ruta, theName, tamannosLista ,tamPx){
	var idContent="";
	var altTextCartoteca = "Ver en cartoteca "  + theDescription;
	var altTextVisor = "Ver en visor "  + theDescription;

	if (theDescription.length > tamMaxCapa) {
		altTextCartoteca = "Ver en cartoteca "  + theDescription.substring(0, tamMaxCapa) + "...";
		altTextVisor = "Ver en visor "  + theDescription.substring(0, tamMaxCapa) + "...";
	}

	if (esCompacto) {
		if (locat != "") {
			if (locat != "&nbsp;") {
				idContent += "</td><td style='float:right'>";
				idContent += '<a align="right" title="' + altTextCartoteca + '" href="javascript:selectedImageVisor(\'' + locat + '\',\'' + servic + '\',\'' + capa + '\',\'' + theCollection + '\')">Ver</a>&nbsp;';
			}
		}
		//idContent += '&nbsp;&nbsp;&nbsp;<a class="itemBlancoEnlace" align="right" title="Descargar" href="' + ruta + '">Descargar</a>';
	} else {
		idContent += '<tr class="normal item ' + evenOdd + '">';
		idContent += '<td class="paddingIzquierda normal item" align="left">' + theDescription + '</td>';

		idContent += '<td class="normal item" align="center">';
		if (locat != "") {
			if (locat != "&nbsp;") {
				idContent += '<a class="normal itemEnlace" title="' + altTextVisor + '" href="javascript:selectedImageVisor(\'' + locat + '\',\'' + servic + '\',\'' + capa + '\',\'' + theCollection + '\')">';
				idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altTextVisor + '"></a>&nbsp;';
			} else {
				idContent += '-';
			}
		} else {
			idContent += '-';
		}
		idContent += '</td>';

		idContent += '<td class="normal item" align="center">';
		if (locat != "") {
			if (locat != "&nbsp;") {
				idContent += '<a class="normal itemEnlace" title="' + altTextCartoteca + '" href="javascript:selectedImageCartoteca(\'' + locat + '\',\'' + servic + '\',\'' + capa + '\',\'' + _xMin + '\',\'' + _yMin + '\',\'' + _xMax + '\',\'' + _yMax + '\',\'' + theCollection + '\', false, ' + (estaGeorref=='S') + ',' + _deltaX + "," + _deltaY + ',' + resIdx + ',\'' + escala + '\',\'' + fecha + '\')">';
				idContent += '<img border="0" src="/lib/IDEAragon/themes/default/images/verMini.png" title="' + altTextCartoteca + '"></a>&nbsp;';
			} else {
				idContent += '-';
			}
		} else {
			idContent += '-';
		}
		idContent += '</td>';

		idContent += '<td class="normal item" align="center">';
		if (ruta != "&nbsp;") {
			idContent += addFormatoDescarga(formatosLista,ruta, theName,tamannosLista);
		} else {
			idContent += '-';
		}
		idContent += '</td>';

		idContent += '<td class="normal item" align="center">' + fecha + '</td>';
		idContent += '<td class="normal item" align="center">' + escala + '</td>';
		idContent += '<td class="normal item" align="center">' + tamPx + '</td>';


		idContent += '</tr>';
	}
	return idContent;
}
function getItemGFI_descarga(theCollection, resIdx, esCompacto, evenOdd) {
	var idContent = "";
	var fNam = resultadoGFI_sitar[theCollection][resIdx]["camposRespuesta"];
	var fVal = resultadoGFI_sitar[theCollection][resIdx]["valoresRespuesta"];

	var theName = getResultGFI(fNam, fVal, ".NAME");
	var estaGeorref = replaceChar(getResultGFI(fNam, fVal, ".GEORREF"), ",", ".");

	var capa = getResultGFI(fNam, fVal, ".CAPA");
	var servic = getResultGFI(fNam, fVal, ".SERVICIO");
	var locat = getResultGFI(fNam, fVal, ".LOCATION");
	var ruta = getResultGFI(fNam, fVal, ".PATH");
	var fecha = getResultGFI(fNam, fVal, ".FECHA");
	var escala = getResultGFI(fNam, fVal, ".ESCALA");
	var tamPx = getResultGFI(fNam, fVal, ".TAM_PIXEL");
	var formatos = getResultGFI(fNam, fVal, ".DESCARGAS");
	var formatosLista = formatos.split(",");
	var tamannos = getResultGFI(fNam, fVal, ".TAMANO");
	var tamannosLista = tamannos.split(",");

	var theDescription = getResultGFI(fNam, fVal, ".DESCRIPCION");
	if (theDescription == "") {
		theDescription = getResultGFI(fNam, fVal, ".DESCRIP");
		if (theDescription == "") {
			theDescription = theName;
		}
	}
	
	var _xMin = replaceChar(getResultGFI(fNam, fVal, ".X1"), ",", ".");
	var _yMin = replaceChar(getResultGFI(fNam, fVal, ".Y1"), ",", ".");
	var _xMax = replaceChar(getResultGFI(fNam, fVal, ".X2"), ",", ".");
	var _yMax = replaceChar(getResultGFI(fNam, fVal, ".Y2"), ",", ".");

	var _deltaX = resultadoGFI_sitar[theCollection][resIdx]["deltaX"];
	var _deltaY = resultadoGFI_sitar[theCollection][resIdx]["deltaY"];

	//var distanc = resultadoGFI_sitar[theCollection][resIdx]["distanciaCentroide"];

//	var altText = "Ver en cartoteca "  + theName + " (" + theCollection + ")";

	return getItemGFI_descargaHTML(theCollection, resIdx, esCompacto,evenOdd,capa,locat, servic,_xMin, _yMin, _xMax, _yMax, _deltaX, _deltaY, escala, fecha, theDescription, estaGeorref, formatosLista, ruta, theName, tamannosLista ,tamPx);
}

function getResultsGFIHTML_generico(i, j, k, l) {
	var idContent = '';

	var idx = restrictions[i][j][k][l]["posicion"];

	if (gfiList[idx]["camposRespuesta"]["count"]>0) {
		var id = gfiList[idx]["ID"];

		for (var kk = 0; kk < gfiList[idx]["camposRespuesta"]["count"]; kk++) {
			fName1 = gfiList[idx]["camposRespuesta"][kk];
			fValue1 = gfiList[idx]["valoresRespuesta"][kk];
			var idFeature = "";

			var listaIndexMainField = new Array();
			var listaFieldNames = mainFieldName[id].split(' ');

			indexIdField = -1;
			for (var ii=0;ii<fName1.length;ii++){
				nombreCampo = getNombreCampo(fName1[ii]);

				if (isIncluded(listaFieldNames, nombreCampo, listaFieldNames.length)) {
					listaIndexMainField[nombreCampo] = ii;
				}
				if (nombreCampo == nombreCampoId) {
					indexIdField = ii;
				}
			}

			if (fName1.length == 1) {
					// poner una sola linea
				mainFieldText = "";
				for (var jj = 0; jj < listaFieldNames.length; jj++) {
					mainFieldText += corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
				}

				idContent += "<tr><td colspan='3' class='item normal'>";
				if (titleFieldName[id] != null) {
					idContent += "<b>" + titleFieldName[id] + "</b> " + mainFieldText;
				} else {
					idContent += mainFieldText;
				}
			
				idContent += "</td></tr>";

			} else {
					// generar fieldset
				mainFieldText = "";
				for (var jj = 0; jj < listaFieldNames.length; jj++) {
					if (fValue1[listaIndexMainField[listaFieldNames[jj]]]) {
						mainFieldText += corrigeCaracteresExtranos(fValue1[listaIndexMainField[listaFieldNames[jj]]]) + " ";
					}
				}

				idContent += "<tr><td colspan='3' class='item normal'>";
				idContent += "<fieldset>";
				idContent += "<legend>";
				if (titleFieldName[id] != null) {
					idContent += titleFieldName[id] + " " + mainFieldText;
				} else {
					idContent += mainFieldText;
				}
				idContent += "</legend>";

				idContent += "<table id='detailData" + l + "_" + kk + "Table' width='100%' cellpadding='5' cellspacing='0' style='display:table;'>";

				jj = 0;
				for (var ii=0;ii<fName1.length;ii++) {
					idContent += "<tr>";

					nombreCampo = getNombreCampo(fName1[ii]);

					if (isValidField(nombreCampo)) {
						if (fieldAliasList2[id]) {
							if (fieldAliasList2[id][nombreCampo]) {
								idContent += "<td class='item normal'><b>" + (fieldAliasList2[id][nombreCampo]) + "</b>: " + corrigeCaracteresExtranos(fValue1[ii]) + "</td>";
							}
						}
						jj++;
					}
					idContent += "</tr>";
				}

				idContent += "</table>";
				idContent += "</fieldset>";
				idContent += "</td></tr>";
			}
		}
	}
	return idContent;
}

function getResultsGFIHTML_educacion(i, j, k, l) {
	var idContent = '';

	idContent += "<table class='search_map' width='100%' cellpadding='0' cellspacing='2'>";


	var idx = restrictions[i][j][k][l]["posicion"];

			if (gfiList[idx]["camposRespuesta"]["count"]>0) {
				idContent += '<tr class="itemBig negro fondoGrisClaro">';
				idContent += '<td align="center" width="42%"><b>Nombre de la v&iacute;a</b></td>';
				//idContent += '<td align="center" width="7%"><b>N&uacute;mero</b></td>';
				idContent += '<td align="center" width="20%"><b>Referencia catastral</b></td>';
				idContent += '<td align="center" width="20%"><b>Coord X</b></td>';
				idContent += '<td align="center" width="20%"><b>Coord Y</b></td>';
				idContent += '</tr>';

				var evenOdd;


				for (var kk = 0; kk < gfiList[idx]["camposRespuesta"]["count"]; kk++) {

					fNam = gfiList[idx]["camposRespuesta"][kk];
					fVal = gfiList[idx]["valoresRespuesta"][kk];

					if((kk%2) == 0) {
						evenOdd = "oddRow";
					}	else {
						evenOdd = "evenRow";
					}

					var elNomVia = getResultGFI(fNam, fVal, "CARTO.T04_TROIDESVISOR.DENOMINA");
					var laRefCat = getResultGFI(fNam, fVal, "CARTO.T04_TROIDESVISOR.REFCAT");
					var laX = getResultGFI(fNam, fVal, "CARTO.T04_TROIDESVISOR.X");
					var laY = getResultGFI(fNam, fVal, "CARTO.T04_TROIDESVISOR.Y");

					idContent += '<tr class="normal item ' + evenOdd + '">';

					idContent += '<td class="normal item" align="center">' + elNomVia + '</td>';
					//idContent += '<td class="normal item" align="center">' + elNum + '</td>';
					idContent += '<td class="normal item" align="center">' + laRefCat + '</td>';
					idContent += '<td class="normal item" align="center">' + laX + '</td>';
					idContent += '<td class="normal item" align="center">' + laY + '</td>';

					idContent += '</tr>';
				}
			}



	idContent += "</table>";

	return idContent;
}

function selectedImage(_locat, _servicio, _capa, _bboxXMin, _bboxYMin, _bboxXMax, _bboxYMax, _theCollection, hideResultsBack, _isGeorref, _deltaX, _deltaY, _resIdx, _escala, _fecha) {
	if (document.getElementById('mantenerExtension')) {
		if (!document.getElementById('mantenerExtension').checked) {
			
			minx = _bboxXMin;
			miny = _bboxYMin;
			maxx = _bboxXMax;
			maxy = _bboxYMax;
			
		}
	} else {
		minx = _bboxXMin;
		miny = _bboxYMin;
		maxx = _bboxXMax;
		maxy = _bboxYMax;
		
	}
	selectedImageVisor(_locat, _servicio, _capa, _theCollection);
	
}

function selectedImageCartoteca(_locat, _servicio, _capa, _bboxXMin, _bboxYMin, _bboxXMax, _bboxYMax, _theCollection, hideResultsBack, _isGeorref, _deltaX, _deltaY, _resIdx, _escala, _fecha) {
	var str = urlCartoteca + "?LOCAT=" + _locat;
	str += "&SERVICIO=" + _servicio;
	str += "&CAPA=" + _capa;

/*	if (document.getElementById('mantenerExtension').checked) {
		if (getMapScale(false) > _escala) {
			str += "&BBOXFOTOGRAMA=" + xCoordUsr + ":" + yCoordUsr + ":" + xCoordUsr + ":" + yCoordUsr;
			str += "&ESCALA=" + _escala;
		} else {
			str += "&BBOXFOTOGRAMA=" + minx + ":" + miny + ":" + maxx + ":" + maxy;
		}
	} else {*/
		str += "&BBOXFOTOGRAMA=" + _bboxXMin + ":" + _bboxYMin + ":" + _bboxXMax + ":" + _bboxYMax;
	//}
	str += "&COLECCION=" + _theCollection;
	str += "&ISGEORREF=" + _isGeorref;
	str += "&FECHA=" + _fecha;

	window.open(str);
}

//@deprecated. Nueva versión en visor/js/tools.js
function selectedImageVisor(_locat, _servicio, _capa, _theCollection) {
	
	var locats = _locat.split("@");
	var servicios = _servicio.split("@");
	var capas = _capa.split("@");
	var urlItem="";
	for (var i=0; i<servicios.length; i++){
		if (i>0){
			urlItem+="@";
		}
		urlItem+= locats[i];
		//urlWMSFotograma = "http://preiws.aragon.es/ecwp/ecw_wms.dll?";
		urlItem += "ServiceName=" + servicios[i];
		urlItem += "&Layers=" + capas[i];
		
			urlItem+=  formatoWMS_PNG;
		

		
			var styles = "";
			if (servicios[i] == "DIT") {
				var styles ="Default";
				var layer_count = capas[i].split(",").length;
				for (var i=1; i<layer_count;i++){
					styles +=",Default";
				}
				styles += "&Styles=" + styles;
			}
			urlItem += styles;
			urlItem += restoWMS;
	}
	
	

	//var urlItem = _locat + "ServiceName=" + _servicio + "&Layers=" + _capa + formatoWMS_PNG;
	
	var tituloWM = _capa + " (" + _theCollection + ")"; 
								/*
	var idxWM = addWMS(urlItem, 99, true, '', false);
	toc.addLayer( new LAYERWMS('Item',tituloWM,'',null,null,null, idxWM) );
	*/
	
	wmsList[idxWMS_invisible]["url"] = urlItem;
	wmsList[idxWMS_invisible]["visib"] = true;
	layerInvisibleWMS.noMostrar = false; 
	layerInvisibleWMS.noMostrar = false; 
	layerInvisibleWMS.caption = tituloWM; 
	layerInvisibleWMS.name = tituloWM; 

	//tocWin.setcontent(toc.writeHTML());
	$("#toc_control").html(toc.writeHTML());
	$("#tabs").tabs("option", "active", 0);

	getMapWithCurrentExtent();
}

var restoWMS = "&SRS=EPSG:25830&service=WMS&request=GetMap&version=1.1.1&EXCEPTIONS=application/vnd.ogc.se_xml";
var	formatoWMS_PNG = '&Format=image/png&TRANSPARENT=true';

function getResultGFI(fName1, fValue1, nomCampo) {
	idx = -1;
			// buscar la posicion del campo
	for (var ii=0;ii<fName1.length;ii++) {
		nombreCampo = fName1[ii];

		var auxIdx = nombreCampo.toLowerCase().indexOf(nomCampo.toLowerCase());

		if (auxIdx != -1) {
				// compruebo que la cadena acaba justo con eso para evitar nombres de campos que esten contenidos (ej. FECHA y FECHA_I)
			if (nombreCampo.substr(auxIdx + nomCampo.length).length == 0) {
				idx = ii;
			}
		}
	}

	if (idx != -1) {
		return corrigeCaracteresExtranos(fValue1[idx]);
	} else {
		return "";
	}
}

// get a list of field names from the returned record
function getFieldNamesGFI(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fName1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		fName1[f] = v[0];
	}
	return fName1;

}
// get a list field values from the returned record
function getFieldValuesGFI(recordString) {
	var theStuff = new String(recordString);
	var theList = theStuff.split('" ');
	var fValue1 = new Array();
	for (var f=0;f<theList.length;f++) {
		var v = theList[f].split('="');
		if ((v[1]=="") || (v[1]==null)) v[1] = "&nbsp;";
		fValue1[f] = v[1];
	}
	return fValue1;
}

function toggleZoneCarpeta(theId, theText) {
	theTableId = theId + "Table";
	theLinkId = theId + "Link";
	theTable = document.getElementById(theTableId);
	theLink = document.getElementById(theLinkId);
	if (theTable.style.display == 'none') {
		theTable.style.display = 'table';
		theTable.style.visibility = 'visible';
		if (theId.indexOf("coleccTipo")==0){
			theLink.innerHTML = "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened_blanco.png' border='0'></img>";
		}
		else{
		theLink.innerHTML = "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_opened.png' border='0'></img>";
		}
		theLink.title = 'Ocultar ' + theText;
	} else {
		theTable.style.display = 'none';
		theTable.style.visibility = 'hidden';
		if (theId.indexOf("coleccTipo")==0){
			theLink.innerHTML = "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed_blanco.png' border='0'></img>";
		}
		else{
		theLink.innerHTML = "<img src='/lib/IDEAragon/themes/default/images/imagesTOC/icon_closed.png' border='0'></img>";
		}
		theLink.title = 'Mostrar ' + theText;
	}
}


function mostrarOpciones() {
	
	var idContent = getOpcionesHTML();
	
		document.getElementById('filtrosZone').innerHTML = idContent;


	if ($('#sliderFecha')) {
		$('#sliderFecha').slider({
			range: true,
			min: fechaMinimo,
			max: fechaMaximo,
			step: fechaStep,
			values: [ fechaInicio, fechaFin],
			slide: function( event, ui ) {
				$( "#rangoFecha" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
				fechaInicio = ui.values[0];
				fechaFin = ui.values[1];
			}	,
			change: function (event,ui){
				if (filtrarPorFecha){
				finOpciones();
				}
			}
		});
	}
	
	checkErrorEscalas();
	$(".customCBox").chosen({ disable_search: true});
}

function changedMinScaleValue() {
	var valor = parseInt(document.getElementById("scaleMinValueCbo").value);
	if (! isNaN(valor)) {
		escalaInicio = valor;
	} else {
		escalaInicio = escalaMinima;
	}
	checkErrorEscalas();
	if (filtrarPorEscala){
		finOpciones();
		}
}

function changedMaxScaleValue() {
	var valor = parseInt(document.getElementById("scaleMaxValueCbo").value);
	if (! isNaN(valor)) {
		escalaFin = valor;
	} else {
		escalaFin = escalaMaxima;
	}
	checkErrorEscalas();
	if (filtrarPorEscala){
	finOpciones();
	}
}

function checkErrorEscalas() {
	if (escalaInicio > escalaFin) {
		document.getElementById("avisoErrorEscala").innerHTML = "El valor m&iacute;nimo no puede ser superior al valor de escala m&aacute;ximo";

	} else {
		document.getElementById("avisoErrorEscala").innerHTML = "";
	}
}

function changedColeccFiltro() {
	var valor = document.getElementById("criterioColecc").value;
	coleccAFiltrar = valor;
	if (filtrarPorColecc){
	finOpciones();
	}
}

function changedTipoFiltro() {
	var valor = document.getElementById("criterioTipo").value;
	tipoAFiltrar = valor;
	if (filtrarPorTipo){
	finOpciones();
	}
}

function getOpcionesHTML() {
	var idContent = '';
//	idContent +="<legend>Opciones de filtrado de resultados</legend>";
	idContent += "<table class='search_map' width='90%' cellpadding='0' cellspacing='5'>";
	idContent += '	<tr class="item ">';
	idContent += '		<td style="text-align:left">';
	idContent += '			<input type="checkbox" alt= "Mostrar s&oacute;lo datos que se pueden visualizar" ' + (filtrarSoloVisualizables ? "checked" : "") + ' class="item header customCheckBox" id="datosVisualizablesCbo" onclick="javascript:clickFiltrarSoloVisualizables();" style="border:0;margin-right:5px"><label for="datosVisualizablesCbo">Mostrar s&oacute;lo datos que se pueden visualizar</label>';
	idContent += '		</td>';
	idContent += '	</tr>';
	idContent += '	<tr class="item ">';
	idContent += '		<td style="text-align:left">';
	idContent += '			<input type="checkbox" alt= "Mostrar s&oacute;lo datos que se pueden descargar" ' + (filtrarSoloDescargables ? "checked" : "") + ' class="item header customCheckBox" id="datosDescargablesCbo" onclick="javascript:clickFiltrarSoloDescargables();" style="border:0;margin-right:5px"><label for="datosDescargablesCbo">Mostrar s&oacute;lo datos que se pueden descargar</label>';
	idContent += '		</td>';
	idContent += '	</tr>';
	idContent += '	<tr class="item ">';
	idContent += '		<td style="text-align:left">';
	idContent += '			<input type="checkbox" alt= "Filtrar por intervalo de fecha" ' + (filtrarPorFecha ? "checked" : "") + ' class="item header customCheckBox" id="rangoFechasCbo" onclick="javascript:clickFiltrarFechas();" style="border:0;margin-right:5px"><label for="rangoFechasCbo">Filtrar por intervalo de fecha:</label>';
	idContent += '				<input type="text" class="item " id="rangoFecha" style="border:0;" value="' + fechaInicio + " - " + fechaFin + '"></input>';
	idContent += '		</td>';
	idContent += '	</tr>';
	idContent += '	<tr class="item ">';
	idContent += '		<td style="text-align:center;">';
	idContent += '			<div style="text-align:center;width:80%;left:50px;" id="sliderFecha"></div>';
	idContent += '		</td>';
	idContent += '	</tr>';
	idContent += '	<tr class="item ">';
	idContent += '		<td style="text-align:left">';
	idContent += '			<input type="checkbox" alt= "Filtrar por intervalo de escala" ' + (filtrarPorEscala ? "checked" : "") + ' class="item header customCheckBox" id="rangoEscalasCbo" onclick="javascript:clickFiltrarEscalas();" style="border:0;margin-right:5px"><label for="rangoEscalasCbo">Filtrar por escala </label>';
	idContent += '				<label for="scaleMinValueCbo">m&iacute;nima 1:</label>';
	idContent += '				<select class="item customCBox" id="scaleMinValueCbo" onchange="changedMinScaleValue()">';
	idContent += '					<option ' + (escalaInicio==escalaMinima ? "selected" : "") + ' value="">-</option>';
	idContent += '					<option ' + (escalaInicio==500 ? "selected" : "") + ' value="500">500</option>';
	idContent += '					<option ' + (escalaInicio==1000 ? "selected" : "") + ' value="1000">1.000</option>';
	idContent += '					<option ' + (escalaInicio==5000 ? "selected" : "") + ' value="5000">5.000</option>';
	idContent += '					<option ' + (escalaInicio==10000 ? "selected" : "") + ' value="10000">10.000</option>';
	idContent += '					<option ' + (escalaInicio==25000 ? "selected" : "") + ' value="25000">25.000</option>';
	idContent += '					<option ' + (escalaInicio==50000 ? "selected" : "") + ' value="50000">50.000</option>';
	idContent += '					<option ' + (escalaInicio==100000 ? "selected" : "") + ' value="100000">100.000</option>';
	idContent += '				</select>';
	idContent += '				<label for="scaleMaxValueCbo">m&aacute;xima 1:</label>';
	idContent += '				<select class="item customCBox" id="scaleMaxValueCbo" onchange="changedMaxScaleValue()">';
	idContent += '					<option ' + (escalaFin==escalaMaxima ? "selected" : "") + ' value="">-</option>';
	idContent += '					<option ' + (escalaFin==500 ? "selected" : "") + ' value="500">500</option>';
	idContent += '					<option ' + (escalaFin==1000 ? "selected" : "") + ' value="1000">1.000</option>';
	idContent += '					<option ' + (escalaFin==5000 ? "selected" : "") + ' value="5000">5.000</option>';
	idContent += '					<option ' + (escalaFin==10000 ? "selected" : "") + ' value="10000">10.000</option>';
	idContent += '					<option ' + (escalaFin==25000 ? "selected" : "") + ' value="25000">25.000</option>';
	idContent += '					<option ' + (escalaFin==50000 ? "selected" : "") + ' value="50000">50.000</option>';
	idContent += '					<option ' + (escalaFin==100000 ? "selected" : "") + ' value="100000">100.000</option>';
	idContent += '				</select>';
	idContent += '			<div id="avisoErrorEscala" class="textoInvRojo"></div>';
	idContent += '			</div>';
	idContent += '		</td>';
	idContent += '	</tr>';
	if (tipoColeccEnOrdenIdx > 1) {
		idContent += '	<tr class="item ">';
		idContent += '		<td style="text-align:left">';
		idContent += '			<input type="checkbox" alt= "Filtrar por tipo de contenido" ' + (filtrarPorTipo ? "checked" : "") + ' class="item header customCheckBox" id="tipoCbo" onclick="javascript:clickFiltrarTipo();" style="border:0;margin-right:5px"><label for="tipoCbo">Filtrar por tipo de contenido:</label>';
		idContent += '				<select class="item customCBox" id="criterioTipo" onchange="changedTipoFiltro()">';

		if (tipoAFiltrar == "") {
			tipoAFiltrar = tipoColeccEnOrden[1];
		}

		for (var jj = 1; jj < tipoColeccEnOrdenIdx; jj++) {
			var theCollection = tipoColeccEnOrden[jj] ;
			idContent += '					<option ' + (theCollection == tipoAFiltrar ? "selected" : "") + ' value="' + theCollection + '">' + theCollection + '</option>';
		}

		idContent += '				</select>';
		idContent += '			</div>';
		idContent += '		</td>';
		idContent += '	</tr>';
	}

	if (nomColeccEnOrdenIdx > 1) {
		if (coleccAFiltrar == "") {
			coleccAFiltrar = nomColeccEnOrden[1];
		}
		if (jQuery.inArray(coleccAFiltrar,nomColeccEnOrden)<0){
			filtrarPorColecc = false;
		}
		idContent += '	<tr class="item ">';
		idContent += '		<td style="text-align:left">';
		idContent += '			<input type="checkbox" alt= "Filtrar por colecci&oacute;n" ' + (filtrarPorColecc ? "checked" : "") + ' class="item header customCheckBox" id="coleccCbo" onclick="javascript:clickFiltrarColecc();" style="border:0;margin-right:5px"><label for="coleccCbo">Filtrar por colecci&oacute;n:</label>';
		idContent += '				<select class="item customCBox" id="criterioColecc" onchange="changedColeccFiltro()">';

		

		for (var colIdx = 1; colIdx < nomColeccEnOrdenIdx; colIdx++) {
			var theCollection = nomColeccEnOrden[colIdx];
			if (theCollection.indexOf("new_")!=0){
			idContent += '					<option ' + (theCollection == coleccAFiltrar ? "selected" : "") + ' value="' + theCollection + '">' + theCollection.replace(/_/g," ") + '</option>';
		
			}
		}
		idContent += '				</select>';
		idContent += '			</div>';
		idContent += '		</td>';
		idContent += '	</tr>';
	}

	
	
	idContent += "</table>";

	return idContent;
}

var filtrosBusqueda="";
function clickFiltrarSoloVisualizables() {
	if (document.getElementById('datosVisualizablesCbo')) {
		filtrarSoloVisualizables = document.getElementById('datosVisualizablesCbo').checked;
		
	}
	finOpciones();
}

function clickFiltrarSoloDescargables() {
	if (document.getElementById('datosDescargablesCbo')) {
		filtrarSoloDescargables = document.getElementById('datosDescargablesCbo').checked;
	}
	finOpciones();
}

function clickFiltrarFechas() {
	if (document.getElementById('rangoFechasCbo')) {
		filtrarPorFecha = document.getElementById('rangoFechasCbo').checked;
	}
	finOpciones();
	
}

function clickFiltrarEscalas() {
	if (document.getElementById('rangoEscalasCbo')) {
		filtrarPorEscala = document.getElementById('rangoEscalasCbo').checked;
		checkErrorEscalas();
	}
	finOpciones();

}

function clickFiltrarTipo() {
	if (document.getElementById('tipoCbo')) {
		filtrarPorTipo = document.getElementById('tipoCbo').checked;
	}
	finOpciones();
	
}

function clickFiltrarColecc() {
	if (document.getElementById('coleccCbo')) {
		filtrarPorColecc = document.getElementById('coleccCbo').checked;
	}
	finOpciones();
	
}

function finOpciones() {
	filtrosBusqueda=""
	if(filtrarPorTipo)
	{
		filtrosBusqueda+=" col.tipo='"+$("#criterioTipo").val()+"' and ";
	}
	if(filtrarPorColecc)
	{
		filtrosBusqueda+=" prod.coleccion='"+$("#criterioColecc").val()+"' and ";	
	}
	if(filtrarPorEscala)
	{
		if($("#scaleMinValueCbo").val()!="")
		{
			filtrosBusqueda+=" prod.escala>="+$("#scaleMinValueCbo").val()+" and ";	
		}
		if($("#scaleMaxValueCbo").val()!="")
		{
			filtrosBusqueda+=" prod.escala<="+$("#scaleMaxValueCbo").val()+" and ";	
		}
	}
	if(filtrarSoloDescargables)
	{
		filtrosBusqueda+=" desca.descargas!='' and ";	
	}
	if(filtrarSoloVisualizables)
	{
		filtrosBusqueda+=" prod.location!='' and prod.servicio!='' and ";	
	}
	if ($("#resultadosIdZone .search_map").length>0){
	//updateResultsGFI_SITAR();
		identifyProductos(xCoordUsr, yCoordUsr,"situaHtml(idContent,'resultadosIdZone');", true);
	}
}



var lastIdx=null;
function activaEsquema(tipoIdx, colIdx, nomColecc) {
	/*if (colIdxEsquemaSelected != -1) {
		desactivaEsquema(colIdxEsquemaSelected, nomColeccEsquemaSelected, false);
	}

	selectionWhereEsquema = "COLECCION LIKE ('" + nomColecc + "')";
	nomColeccEsquema = nomColecc;
	getMapWithCurrentExtent();

	document.getElementById('aActEsquema' + colIdx).href = "javascript:desactivaEsquema(\"" + colIdx + "\", \"" + nomColecc + "\", true)";
	document.getElementById('aActEsquema' + colIdx).title = "Desactivar Esquema";
	document.getElementById('imgActEsquema' + colIdx).src = "/lib/IDEAragon/themes/default/images/desactEsquema.png";
	
	colIdxEsquemaSelected = colIdx;
	nomColeccEsquemaSelected = nomColecc;*/
	
	var myIdx={"tipoIdx":tipoIdx,"colIdx":colIdx,"nomColecc":nomColecc};
	if(esquemaLayer!=null && myIdx!=lastIdx)
	{
		desactivaEsquema(lastIdx.tipoIdx,lastIdx.colIdx,lastIdx.nomColecc,true);
	}
	document.getElementById('aActEsquemaTipo'+tipoIdx+'col' + colIdx).href = "javascript:desactivaEsquema(\"" + tipoIdx + "\",\""+colIdx+"\", \"" + nomColecc +"\", true)";
	document.getElementById('aActEsquemaTipo'+tipoIdx+'col' + colIdx).title = "Desactivar Esquema";
	document.getElementById('imgActEsquemaTipo'+tipoIdx+'col' + colIdx).src = "/lib/IDEAragon/themes/default/images/desactEsquema.png";

	esquemaLayer =new ol.layer.Image({
	    source: new ol.source.ImageWMS({
	        params: {'LAYERS': "AragonReferencia:Aragon,AragonReferencia",
	        	'VERSION':'1.1',
	        	'FORMAT':"image/png",
	        	'STYLES':"",
	        	'CQL_FILTER':"sum_sup_of=47188.700000000;coleccion='"+nomColecc+"'"},
	        url: '/AragonReferencia'
	      })
	    });
	lastIdx=myIdx;
	mapOL.addLayer(esquemaLayer);
	esquemaLayer.setVisible(true);
}

function desactivaEsquema(tipoIdx, colIdx, nomColecc, actualizar) {
	mapOL.removeLayer(esquemaLayer);
	
	document.getElementById('aActEsquemaTipo'+tipoIdx+'col' + colIdx).href = "javascript:activaEsquema(\"" + tipoIdx + "\",\""+colIdx+"\", \"" + nomColecc + "\")";
	document.getElementById('aActEsquemaTipo'+tipoIdx+'col' + colIdx).title =  "Activar Esquema";
	document.getElementById('imgActEsquemaTipo'+tipoIdx+'col' + colIdx).src = "/lib/IDEAragon/themes/default/images/actEsquema.png";

	//colIdxEsquemaSelected = -1;
	
	//selectionWhereEsquema = "";
	if (actualizar) {
		//getMapWithCurrentExtent();
	}
}
