var daramapOverlay;
var popupOverlay,popupContent;
//var daramapOverlayBackup;

var atributosDara = new Array();
atributosDara.push('label');
atributosDara.push('type');
atributosDara.push('index');
atributosDara.push('imageURL');
atributosDara.push('imageURL2');
atributosDara.push('texto');
atributosDara.push('url');
atributosDara.push('url2');
atributosDara.push('tipo');
atributosDara.push('provincia');
atributosDara.push('comarca');
atributosDara.push('localidad');
atributosDara.push('epoca');
atributosDara.push('recursos');
atributosDara.push('id');
atributosDara.push('latLong');

proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs");
proj4.defs("EPSG:4326","+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");

function generateOverlay(title, urlData, atributos, layerBackup) {


    var overlay = new ol.layer.Vector({
		source: new ol.source.Vector(), 
		style: new ol.style.Style({
					image: new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'})
				})
	});

    var atribFeature = {};

    $.getJSON(urlData, function(json) {

        var atribFeature = {};
        $.each(json.items, function(i, item) {

            if (i % 2 === 0) {
                //Parte del elemento Json donde aparecen todos los atributos menos la lat y long

                //Reset de atribFeature
                atribFeature = {};

                if (item.label) {
                    // Nos aseguramos que el elemento contiene un campo 'label' (y por tanto el resto tb)
                    for (var n = 0; n < atributos.length - 2; n++) {
                        //Los dos últimos elementos se rellenan a partir del siguiente elemento de Items
                        var campo = atributos[n];
                        atribFeature[campo] = eval("item." + campo);
                    }
                }

            } else {
                //Parte del elemento Json donde aparece la lat y long

                if (item.birthLatLng && item.birthLatLng !== null) {
                    var latLong = item.birthLatLng;

                    //Extracción de la latitud y longitud
                    //En el fich. Json aparece como "birthLatLng":"41.654486, -0.876418"
                    var _lng = latLong.substring(latLong.indexOf(',') + 1).trim();
                    var _lat = latLong.substring(0, latLong.indexOf(',')).trim();

                    atribFeature['id'] = item.id;
                    atribFeature['birthLatLng'] = latLong;

                    var punto =new ol.Feature({
						geometry:  new ol.geom.Point(ol.proj.fromLonLat([_lng, _lat], 'EPSG:25830'))
					});
										punto.setStyle(new ol.style.Style({image:new ol.style.Icon({src:"./img/"+ atribFeature['imageURL'],scale:0.5})}));
					punto.setProperties(atribFeature);
					//new OpenLayers.Geometry.Point(_lng, _lat).transform('EPSG:4326', 'EPSG:25830');
                   // var feature = new OpenLayers.Feature.Vector(punto, atribFeature);
                    overlay.getSource().addFeature(punto);
                }
            }

        });

    });
    return overlay;
}




function onFeatureSelectFuncion(evt) {
		var feature = evt.element;


		// el globo a crear mostrar� este texto y en este orden
	var textoHtmlAMostrar = '<div style="max-width:400px;text-align:center;">'+
'<div class="map-lens" itemid="Archivo Histórico Provincial de Huesca" style="display: block;">'+
                       ' <div><img width="100" height="128" alt="DARA: Documentos y Archivos de Aragón" src="'+feature.getProperties().imageURL2.replace("./","./img/")+'"></div>'+
                        '<strong><div data-ex-content=".label"><span>'+feature.getProperties().label+'</span></div></strong>'+
						'<div data-ex-content=".localidad"><span>'+feature.getProperties().localidad+'</span></div>'+
						'<div data-ex-content=".texto"><span>'+feature.getProperties().texto+' </span></div>	'+				   
						'<a target="_blank" href="'+feature.getProperties().url+'">Búsqueda avanzada</a>';

	textoHtmlAMostrar += '</div>';

	showPopup(feature,textoHtmlAMostrar);
	

	}
	
function showPopup(feature,textoHtmlAMostrar){
	
	 popupContent.innerHTML=textoHtmlAMostrar;
	  popupOverlay.setPosition(feature.getGeometry().getCoordinates());

}
