var daramapOverlay;
//var daramapOverlayBackup;

var atributosDara = new Array();
atributosDara.push('label');
atributosDara.push('type');
atributosDara.push('index');
atributosDara.push('imageURL');
atributosDara.push('imageURL2');
atributosDara.push('texto');
atributosDara.push('url');
atributosDara.push('url2');
atributosDara.push('tipo');
atributosDara.push('provincia');
atributosDara.push('comarca');
atributosDara.push('localidad');
atributosDara.push('epoca');
atributosDara.push('recursos');
atributosDara.push('id');
atributosDara.push('latLong');

Proj4js.defs["EPSG:25830"] = "+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs";
Proj4js.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

function generateOverlay(title, urlData, atributos, layerBackup) {


    var overlay = new OpenLayers.Layer.Vector(title, {
        styleMap: new OpenLayers.StyleMap({
            default: {
                externalGraphic: 'marker.png',
            },
			cursor :"pointer",
            externalGraphic: "./img/${imageURL}",
            graphicWidth: 40,
            graphicHeight: 48,
            graphicYOffset: -24,
            title: '${label}'
        })
    });

    var atribFeature = {};

    $.getJSON(urlData, function(json) {

        var atribFeature = {};
        $.each(json.items, function(i, item) {

            if (i % 2 === 0) {
                //Parte del elemento Json donde aparecen todos los atributos menos la lat y long

                //Reset de atribFeature
                atribFeature = {};

                if (item.label) {
                    // Nos aseguramos que el elemento contiene un campo 'label' (y por tanto el resto tb)
                    for (var n = 0; n < atributos.length - 2; n++) {
                        //Los dos últimos elementos se rellenan a partir del siguiente elemento de Items
                        var campo = atributos[n];
                        atribFeature[campo] = eval("item." + campo);
                    }
                }

            } else {
                //Parte del elemento Json donde aparece la lat y long

                if (item.birthLatLng && item.birthLatLng !== null) {
                    var latLong = item.birthLatLng;

                    //Extracción de la latitud y longitud
                    //En el fich. Json aparece como "birthLatLng":"41.654486, -0.876418"
                    var _lng = latLong.substring(latLong.indexOf(',') + 1).trim();
                    var _lat = latLong.substring(0, latLong.indexOf(',')).trim();

                    atribFeature['id'] = item.id;
                    atribFeature['birthLatLng'] = latLong;

                    var punto = new OpenLayers.Geometry.Point(_lng, _lat).transform('EPSG:4326', 'EPSG:25830');
                    var feature = new OpenLayers.Feature.Vector(punto, atribFeature);
                    overlay.addFeatures(feature);
                }
            }

        });

    });
    return overlay;
}


function onFeatureUnselectFuncion(evt) {
	var feature = evt.feature;
	if (feature.popup) {
		popup.feature = null;
		mapOL.removePopup(feature.popup);
		feature.popup.destroy();
		feature.popup = null;
	}
}


function onFeatureSelectFuncion(evt) {
		var feature = evt.feature;


		// el globo a crear mostrar� este texto y en este orden
	var textoHtmlAMostrar = '<div style="max-width:400px;text-align:center;">'+
'<div class="map-lens" itemid="Archivo Histórico Provincial de Huesca" style="display: block;">'+
                       ' <div><img width="100" height="128" alt="DARA: Documentos y Archivos de Aragón" src="'+feature.attributes.imageURL2.replace("./","./img/")+'"></div>'+
                        '<strong><div data-ex-content=".label"><span>'+feature.attributes.label+'</span></div></strong>'+
						'<div data-ex-content=".localidad"><span>'+feature.attributes.localidad+'</span></div>'+
						'<div data-ex-content=".texto"><span>'+feature.attributes.texto+' </span></div>	'+				   
						'<a target="_blank" href="'+feature.attributes.url+'">Búsqueda avanzada</a>';

	textoHtmlAMostrar += '</div>';

	showPopup(feature,textoHtmlAMostrar);
	

	}
function showPopup(feature,textoHtmlAMostrar){
	// esta �ltima funci�n se define a continuaci�n
	popup = new OpenLayers.Popup.FramedCloud("featurePopup",
					feature.geometry.getBounds().getCenterLonLat(),
					new OpenLayers.Size(100,100),
					textoHtmlAMostrar,
					null, true, onPopupClose);
	feature.popup = popup;
	popup.feature = feature;
	mapOL.addPopup(popup, true);
}
function onPopupClose(evt) {
	var feature = this.feature;
	if (feature.layer) { // The feature is not destroyed
		selectControl.unselect(feature);
		if (feature.popup) {
			popup.feature = null;
			mapOL.removePopup(feature.popup);
			feature.popup.destroy();
			feature.popup = null;
		}
	} else {
		this.destroy();
	}
}