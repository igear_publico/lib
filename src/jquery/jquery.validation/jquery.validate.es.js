﻿$.validator.addMethod("pattern", function(value, element, param) {
	if (this.optional(element)) {
		return true;
	}
	if (typeof param === "string") {
		param = new RegExp("^(?:" + param + ")$");
	}
	return param.test(value);
}, "Formato inválido.");


jQuery.extend(jQuery.validator.messages, {
    required: "Debe rellenar este campo.",
    remote: "Corrija este campo por favor.",
    email: "Introduzca una dirección de correo electrónico válida.",
    url: "Introduzca una URL válida.",
    date: "Introduzca una fecha válida.",
    dateISO: "Introduzca una fecha válida (ISO).",
    number: "Introduzca un número válido.",
    digits: "Introduzca sólo digitos.",
    creditcard: "Introduzca un número de tarjeta de crédito válido.",
    equalTo: "Introduzca de nuevo el mismo valor.",
    accept: "Introduzca un valor con una extensión válida.",
	pattern: "Formato inválido.",
    maxlength: jQuery.validator.format("No introduzca más de {0} caracteres."),
    minlength: jQuery.validator.format("Introduzca al menos {0} caracteres."),
    rangelength: jQuery.validator.format("Introduzca entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Introduzca un valor entre {0} y {1}."),
    max: jQuery.validator.format("Introduzca un valor menor o igual que {0}."),
    min: jQuery.validator.format("Introduzca un valor mayor o igual que {0}.")
});